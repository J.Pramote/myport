#!Python3
################################################################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
################################################################################
##
##  Standard Library
##
import os
import re
from glob import glob

################################################################################
##
##  Nuke Library
##
import nuke


################################################################################
##
##  Relative Modules
##
from snow.common.lib import path_elem
from snow.nuke.lib import writenode


################################################################################
##
##  Global Varialbles
##
from . import Logger


################################################################################
##
##	Functions
##
def filter_source_layer(layer_dir):
    """Find folder which contain source files
    Parameters:
        layer_dir : <str> layer directory
    Return <dict> a pair of prefix and versions
    """
    source_data = dict()
    for src in os.listdir(layer_dir):
        src_path = '{}/{}'.format(layer_dir, src)
        if not os.path.isdir(src_path):
            continue
        if not os.listdir(src_path):
            continue
        found = re.findall('(\w+)_(v\d{3})$', src)
        if found:
            prefix = found[0][0]
            version = found[0][1]
            src_vers = source_data.get(prefix) or []
            src_vers.append(version)
            source_data[prefix] = src_vers
    return source_data


def filter_source_files(src_layer_dir):
    """List file sets in directory
    Parameters:
        src_layer_dir : <str> directory
    Return <list> filenames
    """
    if not os.path.exists(src_layer_dir):
        Logger.info('Not found:', src_layer_dir)
        return []

    source_files = []
    all_files = os.listdir(src_layer_dir)
    for file in all_files:
        if re.search('thumbs.db', file, re.IGNORECASE):
            continue
        if not os.path.isfile('{}/{}'.format(src_layer_dir, file)):
            continue

        found_img_seq = re.findall(r'(.*.)\.(\d+)(.*.)$', file)
        ## Image seqeunce form
        if found_img_seq:
            prefix = found_img_seq[0][0]
            frame = found_img_seq[0][1]
            postfix = found_img_seq[0][2]
            
            ## Find other frames
            brance_file = glob('{}/{}.{}{}'.format(
                src_layer_dir, prefix, '[0-9]'*len(frame), postfix))

            ## Check if image seqeunce
            if len(brance_file) == 1:
                source_files.append(file)
            else:
                img_seq = '{}.{}{}'.format(prefix, '#'*len(frame), postfix)
                if img_seq not in source_files:
                    source_files.append(img_seq)
        ## Single image
        else:
            source_files.append(file)
    return source_files


def percent_to_sharp_numpad(filename):
    found_numpad = re.findall(r'%0(\d+)d', filename)
    
    if found_numpad:
        numpad = found_numpad[0]
        filename = re.sub('%0'+numpad+'d', '#'*int(numpad), filename)

    return filename


def sharp_to_percent_numpad(filename):
    found_numpad = re.findall(r'(#+)', filename)
    if found_numpad:
        numpad = found_numpad[0]
        percent = '%0{}d'.format(len(numpad))
        filename = re.sub(r'#+', percent, filename)
    return filename


def digit_to_sharp_numpad(filename):
    dirname, filename = os.path.split(filename)
    found_numpad = re.findall(r'([a-zA-Z-_.]+)(\d+)(\.\w+)$', filename)
    if found_numpad:
        found_numpad = found_numpad[0]
        prefix = found_numpad[0]
        number = found_numpad[1]
        ext = found_numpad[2]
        filename = ''.join([prefix, '#'*len(number), ext])
    if dirname:
        return '/'.join([dirname, filename])
    return filename


def digit_to_percent_numpad(filename):
    dirname, filename = os.path.split(filename)
    found_numpad = re.findall(r'([a-zA-Z-_.]+)(\d+)(\.\w+)$', filename)
    if found_numpad:
        found_numpad = found_numpad[0]
        prefix = found_numpad[0]
        number = found_numpad[1]
        ext = found_numpad[2]
        percent = '%0{}d'.format(len(number))
        filename = ''.join([prefix, percent, ext])
    if dirname:
        return '/'.join([dirname, filename])
    return filename


def get_match_read_node(filepath):
    """Get read node that has the filepath
    Return <list> read nodes
    """
    filepath = percent_to_sharp_numpad(filepath)
    result_nodes = []
    for node in nuke.allNodes('Read'):
        found_file = node['file'].value()
        found_file = percent_to_sharp_numpad(found_file)

        if '#' not in found_file:
            found_file = digit_to_sharp_numpad(found_file)

        if re.search(found_file, filepath, re.IGNORECASE):
            result_nodes.append(node.name())
        elif '#' in filepath:
            elem_found = re.findall(
                        r'[a-zA-Z-_.](\d+)\.\w+$',
                        os.path.basename(found_file))
            if elem_found:
                if re.sub('#+', elem_found[0], filepath) == found_file:
                    result_nodes.append(node.name())


    return result_nodes


def select_node(names):
    """Select node by names
    Parameters:
        name : <str> or <list> node names
    """
    if isinstance(names, str):
        names = [names]

    for node in nuke.selectedNodes():
        node.knob("selected").setValue(False)

    for name in names:
        node = nuke.toNode(name)
        node['selected'].setValue(True)


def get_size_format(width, height):
    """Find format name by resolution
    Return <str> format name
    """
    for f in nuke.formats():
        if int(width) == f.width() and int(height) == f.height():
            return f.name()
    return None


def repath_template_node():
    
    for node in nuke.allNodes('Read'):
        file = node['file'].value()
        new_path = path_elem.re_asset_path(
                        nuke.scriptName(), file)
        ## Find last file version
        glob_dirname = re.sub('_v\d+$', '_v*', os.path.dirname(new_path))
        glob_path = '{}/{}'.format(glob_dirname, os.path.basename(new_path))
        found_files = glob(glob_path)
        if found_files:
            new_path = found_files[-1].replace('\\', '/')
        
        Logger.info('%s repath to %s', node.name(), new_path)
        node['file'].setValue(new_path)
    
    writenode.set_filename_as_scriptname()


def make_write_node_dir():
    for node in nuke.allNodes('Write'):
        file = node['file'].value()
        if not os.path.exists(os.path.dirname(file)):
            Logger.info('Create folder %s', os.path.dirname(file))
            os.makedirs(os.path.dirname(file))


def is_layer_loaded(prefix_dir):
    for node in nuke.allNodes('Read'):
        if node['file'].value().startswith(prefix_dir):
            return True


def set_image_range(read_node):
    image = read_node['file'].value()
    dirname, filename = os.path.split(image)
    filename = percent_to_sharp_numpad(filename)
    if not '#' in filename:
        return
    
    found_numpad = re.findall(r'(#+)', filename)
    filename = re.sub(r'#+', '[0-9]'*len(found_numpad[0]), filename)

    all_file = glob('/'.join([dirname, filename]))
    all_file.sort()
    if len(all_file) > 1:
        found_start = re.findall(
                        r'[a-zA-Z-_.](\d+)\.\w+$',
                        os.path.basename(all_file[0]))
        found_end = re.findall(
                        r'[a-zA-Z-_.](\d+)\.\w+$',
                        os.path.basename(all_file[-1]))
        if found_start and found_end:
            read_node['first'].setValue(int(found_start[0]))
            read_node['last'].setValue(int(found_end[0]))
