#!Python3
################################################################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
################################################################################
##
##  Standard Library
##
import os
import re
try:
    from PySide2 import QtWidgets, QtCore, QtGui
except ImportError:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets


################################################################################
##
##  Nuke Library
##
import nuke


################################################################################
##
##  Snow Modules
##
from snow.common.lib.widget import message_box
from snow.common.lib import shot_loader
from snow.nuke.lib import info


################################################################################
##
##  Relative Modules
##
import comp_builder_gui as gui
import comp_builder_helper as helper


################################################################################
##
##  Global Varialbles
##
from . import Logger
__Object_ID__ = 'CompBuilder'
NUKE_TEMPLATE_DIR = 'U:/{project}/all/library/Composite/nuke/template/hero'

################################################################################
##
##		Window Class
##
class CompBuilder(gui.CompBuilderWindow):

    def __init__(self, parent=None, scenefile=''):
        super(CompBuilder, self).__init__(parent)

        self.setObjectName(__Object_ID__)
        self.cur_scene = scenefile or nuke.scriptName()

        Logger.debug('cur_scene=%s', self.cur_scene)

        try:
            self.shot_ld = shot_loader.ShotLoader(self.cur_scene)
        except:
            msg = "It isn't  format path"
            message_box.MessageBox(self).error(msg)
            Logger.exception(msg)
            return

        Logger.debug('project_code: %s',self.shot_ld.project_code)
        Logger.debug('shot_name: %s', self.shot_ld.shot_name)
        Logger.debug('start_frame: %s', self.shot_ld.start_frame)
        Logger.debug('end_frame: %s', self.shot_ld.end_frame)

        ## Intialize default nodes
        nuke.knobDefault("Write.colorspace", "ACES - ACES2065-1")
        nuke.knobDefault("Write.file_type", "exr")
        nuke.knobDefault("Write.in_colorspace", "ACES - ACEScg")
        nuke.knobDefault("Write.out_colorspace", "ACES - ACEScg")

        ## Intialize project setting
        nuke.Root()['project_directory'].setValue(os.path.dirname(self.cur_scene))
        nuke.Root()["first_frame"].setValue(self.shot_ld.start_frame)
        nuke.Root()["last_frame"].setValue(self.shot_ld.end_frame)
        nuke.Root()["lock_range"].setValue("True")
        nuke.Root()["fps"].setValue(float(self.shot_ld.fps))
        width, height = self.shot_ld.get_resolution(preview=0)
        nuke.Root()['format'].setValue(helper.get_size_format(width, height))

        ## Initialize color management
        nuke.Root()['colorManagement'].setValue('OCIO')
        nuke.Root()['customOCIOConfigPath'].setValue(self.shot_ld.get_ocio_path())
        nuke.Root()['OCIO_config'].setValue('custom')
        nuke.Root()['defaultViewerLUT'].setValue("OCIO LUTs")
        nuke.Root()['workingSpaceLUT'].setValue("ACES - ACEScg")
        nuke.Root()['monitorLut'].setValue('ACES/sRGB')
        nuke.Root()['int8Lut'].setValue("Utility - Raw")
        nuke.Root()['logLut'].setValue("Input - ADX - ADX10")
        nuke.Root()['floatLut'].setValue("ACES - ACEScg")

        self.shot_ld.dept = 'lighting'
        self.lighting_version_dir = self.shot_ld.get_render_out_dir()
        self.template_dir = NUKE_TEMPLATE_DIR.format(project=self.shot_ld.project)

        ## Connect widget signals
        self.widget.refresh_pb.clicked.connect(self.refresh_source_item)
        self.widget.build_all_pb.clicked.connect(self.build_all)
        self.widget.update_all_pb.clicked.connect(lambda :self.build_all(update_only=True))
        self.widget.soruce_tw.create_clicked.connect(self.build_read_node)
        self.widget.soruce_tw.version_changed.connect(self.version_changed)
        self.widget.soruce_tw.itemClicked.connect(self.select_source)
        self.widget.soruce_tw.customContextMenuRequested.connect(self.source_menu)
        self.widget.load_temp_pb.clicked.connect(self.load_template)

        ## Setup widget
        self.widget.project_lb.setText(self.shot_ld.project)
        self.widget.episode_lb.setText(self.shot_ld.asset_type)
        self.widget.shot_lb.setText(self.shot_ld.shot_name)
        self.widget.fps_lb.setText(str(nuke.Root().fps()))
        self.widget.template_cb.addItems([n for n in
                    os.listdir(self.template_dir) if n.endswith('.nk')])

        self.source_item_data = dict()
        self.source_item_list = []
        self.initialize_source_item()

    def initialize_source_item(self):
        if not os.path.exists(self.lighting_version_dir):
            return

        for layer in os.listdir(self.lighting_version_dir):
            layer_item = gui.LayerSourceItem(self.widget.soruce_tw)
            layer_item.setText(gui.LAYER_COLUMN, layer)
            layer_dir = '{}/{}'.format(self.lighting_version_dir, layer)
            layer_item.setData(gui.LAYER_COLUMN, QtCore.Qt.UserRole, layer_dir)

            source_data = helper.filter_source_layer(layer_dir)
            if not source_data:
                continue
            for src, vers in source_data.items():
                self.source_item_data[layer] = src
                source_item = gui.SourceTreeWidgetItem(layer_item)
                self.source_item_list.append(source_item)
                source_item.setText(gui.LAYER_COLUMN, src)
                source_item.setData(
                                    gui.LAYER_COLUMN,
                                    QtCore.Qt.UserRole,
                                    '{}/{}'.format(layer_dir, src))

                source_item.setData(
                    gui.VERSION_COLUMN, QtCore.Qt.UserRole, vers)

                current_ver = self.check_cur_version(source_item)
                self.set_status_version(source_item, current_ver)

                source_item.setText(
                    gui.VERSION_COLUMN, current_ver or vers[-1])

                source_dir = '{}/{}_{}'.format(
                    layer_dir, src, source_item.text(gui.VERSION_COLUMN))
                self.add_source_files(source_item, source_dir, expanded=True)
            layer_item.setExpanded(True)
            
        self.widget.soruce_tw.expandAll()
        self.widget.soruce_tw.clearSelection()

    def source_menu(self, pos):
        item = self.widget.soruce_tw.itemAt(pos)
        if not item:
            return
        
        menu = QtWidgets.QMenu()
        action = menu.addAction('Reveal in File Explorer', self.open_explorer)
        data = item.data(
                        gui.LAYER_COLUMN, QtCore.Qt.UserRole)
        if data and isinstance(item, gui.SourceTreeWidgetItem):
            data += '_'+item.text(gui.VERSION_COLUMN)
        action.setData(data)

        menu.exec_(self.widget.soruce_tw.mapToGlobal(pos))
    
    def open_explorer(self):
        dirpath = self.sender().data()
        if dirpath and os.path.isdir(dirpath):
            os.system('explorer "{}"'.format(os.path.realpath(dirpath)))

    def add_source_files(self, source_item, source_dir, expanded=False):

        if source_item.childCount():
            source_item.takeChildren()

        source_file = helper.filter_source_files(source_dir)
        for file in source_file:
            file_item = QtWidgets.QTreeWidgetItem()
            file_item.setText(gui.SOURCE_COLUMN, file)
            file_item.setCheckState(gui.SOURCE_COLUMN, QtCore.Qt.Checked)
            filepath = '{}/{}'.format(source_dir, file)
            read_nodes = helper.get_match_read_node(filepath)
            if read_nodes:
                file_item.setText(gui.NODES_COLUMN, ','.join(read_nodes))

            file_item.setData(gui.LAYER_COLUMN, QtCore.Qt.UserRole, source_dir)
            source_item.addChild(file_item)
        source_item.setExpanded(expanded)
        self.widget.soruce_tw.setCurrentItem(source_item)
    
    def version_changed(self, item):
        layer_item = item.parent()
        layer_dir = layer_item.data(gui.LAYER_COLUMN, QtCore.Qt.UserRole)
        source_name = item.text(gui.LAYER_COLUMN)
        current_version = item.text(gui.VERSION_COLUMN)
        source_dir = '{}/{}_{}'.format(layer_dir, source_name, current_version)
        self.add_source_files(item, source_dir, expanded=True)
        self.set_status_version(item, self.check_cur_version(item))


    def build_read_node(self, item, check_conflict=True):
        layer_dir = item.parent().data(0, QtCore.Qt.UserRole)
        layer_dir = layer_dir.replace('\\', '/').replace('//', '/')
        prefix = item.text(0)
        version = item.text(2)
        target_dir = '{}/{}_{}'.format(layer_dir, prefix, version)
        updated_nodes = []
        for index in range(item.childCount()):
            file_item = item.child(index)
            if file_item.checkState(gui.SOURCE_COLUMN) == QtCore.Qt.Checked:
                filename = file_item.text(gui.SOURCE_COLUMN)
                # filename = helper.digit_to_sharp_numpad(filename)
                filepath = '{}/{}'.format(target_dir, filename)
                file_item.setData(
                    gui.SOURCE_COLUMN, QtCore.Qt.UserRole, filepath)
                file_item.setToolTip(gui.SOURCE_COLUMN, filepath)

                ## Get read node which contain the image layer
                nodes = self.get_image_layer_node(
                                        layer_dir, prefix, filename)
                if nodes:
                    ## Update read nodes
                    for node in nodes:
                        # node = nuke.toNode(name)
                        node['file'].setValue(filepath)
                        helper.set_image_range(node)
                        updated_nodes.append(node.name())

                else:
                    ## Create new node
                    read_nd = nuke.nodes.Read(file=filepath)
                    helper.set_image_range(read_nd)

                read_nodes = helper.get_match_read_node(filepath)
                if read_nodes:
                    file_item.setText(gui.NODES_COLUMN, ','.join(read_nodes))

        self.set_status_version(item, self.check_cur_version(item))
        if check_conflict:
            self.report_conflict(target_dir, only_nodes=updated_nodes)
        return target_dir

    def expand_selected(self, item, column):
        if item.isExpanded():
            item.setExpanded(False)
        else:
            item.setExpanded(True)
    
    def select_source(self, item, column):
        sel_read_nodes = []
        for item in self.widget.soruce_tw.selectedItems():
            if item.parent() and isinstance(
                            item.parent(), gui.SourceTreeWidgetItem):
                read_nodes = item.text(4)
                if read_nodes:
                    sel_read_nodes += read_nodes.split(',')
        
        if sel_read_nodes:
            helper.select_node(sel_read_nodes)
            nuke.zoomToFitSelected()

    def set_status_version(self, item, current_ver):
        versions = item.data(gui.VERSION_COLUMN, QtCore.Qt.UserRole)
        sel_ver = item.text(gui.VERSION_COLUMN)
        if not current_ver:
            item.set_status_item('build')
        elif versions and current_ver == versions[-1]:
            item.set_status_item('exist')
        elif versions and current_ver < versions[-1]:
            if sel_ver < versions[-1]:
                item.set_status_item('update')
            else:
                item.set_status_item('exist')
        elif versions and sel_ver < versions[-1]:
            item.set_status_item('update')
        else:
            item.set_status_item('exist')

    def check_cur_version(self, item):
        layer = item.parent().text(gui.LAYER_COLUMN)
        source = item.text(gui.LAYER_COLUMN)
        result = []
        for node in nuke.allNodes('Read'):
            prefix = '{}/{}/{}'.format(
                self.lighting_version_dir, layer, source)
            found_file = node['file'].value().replace('\\','/')

            found_version = re.findall(prefix+r'_(v\d+)',
                                os.path.dirname(found_file),
                                re.IGNORECASE)
            if found_version:
                result.append(found_version[0])
        if result:
            return sorted(result)[0]
        return None
    
    def get_image_layer_node(self, dirpath, layer_prefix, filename):
        match_nodes = []
        layer_dir_prefix = '{}/{}'.format(dirpath, layer_prefix)
        filename = helper.sharp_to_percent_numpad(filename)
        regx_layer_dir = layer_dir_prefix+r'_(v\d+)/'+filename

        for node in nuke.allNodes('Read'):
            node_file = node['file'].value().replace('\\', '/')
            node_file = helper.digit_to_percent_numpad(node_file)
            found = re.findall(regx_layer_dir, node_file)

            if found:
                match_nodes.append(node)

        return match_nodes

    def build_all(self, update_only=False):
        build_list = []
        for layer_index in range(self.widget.soruce_tw.topLevelItemCount()):
            layer_item = self.widget.soruce_tw.topLevelItem(layer_index)
            build_ver_dir = ''
            for index in range(layer_item.childCount()):
                item = layer_item.child(index)
                versions = item.data(gui.VERSION_COLUMN, QtCore.Qt.UserRole)
                item.setText(gui.VERSION_COLUMN, sorted(versions)[-1])
                if not update_only:
                    build_ver_dir = self.build_read_node(item, False)
                else:
                    prefix_src_dir = item.data(gui.LAYER_COLUMN, QtCore.Qt.UserRole)
                    if helper.is_layer_loaded(prefix_src_dir):
                        build_ver_dir = self.build_read_node(item, False)
            if build_ver_dir:
                build_list.append(build_ver_dir)

        self.report_conflict(build_list)

    def refresh_source_item(self):
        self.widget.soruce_tw.clear()
        self.initialize_source_item()

    def load_template(self):
        temp_name = self.widget.template_cb.currentText()
        temp_file = '{}/{}'.format(self.template_dir, temp_name)
        Logger.info('Load template: %s', temp_file)
        nuke.scriptSource(temp_file)
        helper.repath_template_node()
        helper.make_write_node_dir()
        self.refresh_source_item()

        message_box.MessageBox(self).success('Build already!')

    def report_conflict(self, target_dirs, only_nodes=[]):
        if isinstance(target_dirs, str):
            target_dirs = [target_dirs]

        result = []
        for target in target_dirs:
            found_prefix = re.findall(r'(.*.)_v\d+', target)
            if not found_prefix:
                continue
            prefix_dir = found_prefix[0]

            nodes = [nuke.toNode(n) for n in only_nodes]
            if not nodes:
                nodes = nuke.allNodes('Read')
            
            for node in nodes:
                node_file = helper.percent_to_sharp_numpad(node['file'].value())
                if re.search(prefix_dir+r'_v\d+/', node_file):
                    if os.path.dirname(node_file) != target:
                        result.append([node.name(), node_file])
        
        if not result:
            return

        msg = message_box.TextPlain(self)
        msg.setReadOnly(True)
        msg.setWindowTitle('Conflict Report')
        text = '--- Not found these files in current version ---\n'
        for r in result:
            text += '{} : {}\n'.format(*r)
        Logger.info(text)
        msg.insertPlainText(text)
        msg.exec_()



################################################################################
##
##		Main Function
##
def main(scenefile=''):
    nuke_main_window = QtGui.QApplication.activeWindow()
    widget = nuke_main_window.findChild(QtWidgets.QWidget, __Object_ID__)
    if widget:
        widget.close()
        widget.deleteLater()

    widget = CompBuilder(parent=nuke_main_window)
    widget.setWindowFlags(QtCore.Qt.Window)
    # widget.setWindowModality(QtCore.Qt.WindowModal)
    widget.show()
