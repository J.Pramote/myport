#!Python3
################################################################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
################################################################################
##
##  Standard Library
##
import os
try:
    from PySide2 import QtWidgets, QtCore, QtGui
except ImportError:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets	


################################################################################
##
##  Global Variables
##
Stylesheet = os.path.dirname(__file__)+"/stylesheet.css"
LAYER_COLUMN = 0
SOURCE_COLUMN = 1
VERSION_COLUMN = 2
BUTTON_COLUMN = 3
NODES_COLUMN = 4


################################################################################
##
##  Custom Widget Classes
##
class Widget(QtCore.QObject):
    def __init__(self):
        super(Widget, self).__init__()
        self.project_lb = QtWidgets.QLabel()
        self.episode_lb = QtWidgets.QLabel()
        self.shot_lb = QtWidgets.QLabel()
        self.fps_lb = QtWidgets.QLabel()
        self.template_cb = QtWidgets.QComboBox()
        self.load_temp_pb = QtWidgets.QPushButton('Load')
        self.soruce_tw = SourceTreeWidget()
        self.build_all_pb = QtWidgets.QPushButton('Build All')
        self.update_all_pb = QtWidgets.QPushButton('Update All')
        self.refresh_pb = QtWidgets.QPushButton('Refresh')


class LayerSourceItem(QtWidgets.QTreeWidgetItem):
    def __init__(self, *argv):
        super(LayerSourceItem, self).__init__(*argv)
        if self.treeWidget():
            for col in range(
                            self.treeWidget().columnCount()):
               self.setBackground(col, QtGui.QColor('#1d2024'))


class AbstractItemDelegate(QtWidgets.QItemDelegate):
    ''' Delegate QTreeWidgetItem'''
    commitData_version = QtCore.Signal(QtWidgets.QWidgetItem, QtCore.QModelIndex, str)
    commitData_button = QtCore.Signal(QtWidgets.QWidgetItem, QtCore.QModelIndex)

    def __init__(self, parent=None):
        super(AbstractItemDelegate, self).__init__(parent)

    def createEditor(self, parent, option, index):

        if index.column() == VERSION_COLUMN:
            version_cb = QtWidgets.QComboBox(parent)
            version_cb.currentIndexChanged[str].connect(
                    lambda x: self.commitData_version.emit(index.model(), index, x))

            # version_cb.currentIndexChanged.connect(
            #                     lambda: self.setModelData(
            #                                 version_cb, index.model(), index))
            return version_cb

        elif index.column() == BUTTON_COLUMN:
            update_pb = QtWidgets.QPushButton('Build', parent)
            update_pb.setFixedWidth(80)
            update_pb.setProperty('item', True)
            update_pb.clicked.connect(
                    lambda: self.commitData_button.emit(index.model(), index))
            return update_pb
        

    def updateEditorGeometry(self, editor, option, index):

        option.rect.setSize(editor.minimumSizeHint().expandedTo(
                                                        option.rect.size()))
        if isinstance(editor, QtWidgets.QComboBox):
            editor.setGeometry(option.rect)
        else:
            return QtWidgets.QItemDelegate.updateEditorGeometry(
                self, editor, option, index)

    def setModelData(self, editor, model, index):
        if isinstance(editor, QtWidgets.QComboBox):
            model.setData(index,
                        editor.itemData(
                            editor.currentIndex(), QtCore.Qt.UserRole),
                        QtCore.Qt.UserRole)


class SourceTreeWidgetItem(QtWidgets.QTreeWidgetItem):
    def __init__(self, *argv):
        super(SourceTreeWidgetItem, self).__init__(*argv)
        self.treeWidget().openPersistentEditor(self, VERSION_COLUMN)
        self.treeWidget().openPersistentEditor(self, BUTTON_COLUMN)

        # self.setTextAlignment(0, QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
        self.default_flags = self.flags()
        self.setFont(0, QtGui.QFont().setItalic(True))

    def text(self, column):
        widget = self.treeWidget().itemWidget(self, column)
        if isinstance(widget, QtWidgets.QComboBox):
            return widget.currentText()
        else:
            return super(SourceTreeWidgetItem, self).text(column)
    
    def setText(self, column, text):
        widget = self.treeWidget().itemWidget(self, column)
        if isinstance(widget, QtWidgets.QComboBox):
            widget.setCurrentIndex(
                    widget.findText(text, QtCore.Qt.MatchExactly))
        else:
            super(SourceTreeWidgetItem, self).setText(column, text)
    
    def data(self, column, role):
        widget = self.treeWidget().itemWidget(self, column)
        if isinstance(widget, QtWidgets.QComboBox):
            return [widget.itemText(i) for i in range(widget.count())]

        return super(
                SourceTreeWidgetItem, self).data(
                                    column, role)

    def setData(self, column, role, data, **kwargs):
        widget = self.treeWidget().itemWidget(self, column)
        if isinstance(widget, QtWidgets.QComboBox):
            if isinstance(data, list):
                widget.addItems(data)
            elif isinstance(data, str):
                widget.addItem(data, kwargs)

            super(
                SourceTreeWidgetItem, self).setData(
                                column, role, data)
            # if widget.count():
            #     widget.setCurrentIndex(widget.count()-1)

        elif isinstance(widget, QtWidgets.QLineEdit):
            widget.setText(data)

        elif isinstance(widget, QtWidgets.QCheckBox):
            widget.setCheckState(data)
        else:
            super(
                SourceTreeWidgetItem, self).setData(
                                column, role, data)

    def set_status_item(self, status):
        widget = self.treeWidget().itemWidget(self, VERSION_COLUMN)
        if status == 'update':
            widget.setStyleSheet('background: rgb(216, 50, 50);')
        elif status == 'build':
            widget.setStyleSheet('background: none;')
        elif status == 'exist':
            widget.setStyleSheet('background: rgb(50, 200, 64);')


class SourceTreeWidget(QtWidgets.QTreeWidget):
    version_changed = QtCore.Signal(QtWidgets.QTreeWidgetItem)
    create_clicked = QtCore.Signal(QtWidgets.QTreeWidgetItem)

    def __init__(self, parent=None):
        super(SourceTreeWidget, self).__init__(parent)
        self.setHeaderLabels(
                    ["Layer", "Source", "Version", "", "Node List"])
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        self.setColumnWidth(LAYER_COLUMN, 300)
        self.setColumnWidth(SOURCE_COLUMN, 300)

        delegate = AbstractItemDelegate(self)
        self.setItemDelegate(delegate)
        delegate.commitData_version.connect(self.column_version_changed)
        delegate.commitData_button.connect(self.column_button_changed)

    def column_version_changed(self, model, index):
        # data = model.data(index, QtCore.Qt.UserRole)
        item = self.itemFromIndex(index)
        self.version_changed.emit(item)

    def column_button_changed(self, model, index):
        item = self.itemFromIndex(index)
        self.create_clicked.emit(item)


################################################################################
##
##  Main Window
##
class CompBuilderWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(CompBuilderWindow, self).__init__(parent)
        self.setWindowTitle('Comp Build')
        self.setStyleSheet(open(Stylesheet, 'r').read())
        QtWidgets.QVBoxLayout(self)
        self.widget = Widget()
        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignLeft)
        self.layout().addLayout(layout)
        label = QtWidgets.QLabel('Project:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.project_lb)
        self.widget.project_lb.setStyleSheet('font:italic;')
        layout.addSpacing(10)
        label = QtWidgets.QLabel('Episode:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.episode_lb)
        self.widget.episode_lb.setStyleSheet('font:italic;')
        layout.addSpacing(10)
        label = QtWidgets.QLabel('Shot:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.shot_lb)
        self.widget.shot_lb.setStyleSheet('font:italic;')
        layout.addSpacing(10)
        label = QtWidgets.QLabel('FPS:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.fps_lb)
        self.widget.fps_lb.setStyleSheet('font:italic;')
        layout.addStretch()
        label = QtWidgets.QLabel('Template:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.template_cb)
        layout.addWidget(self.widget.load_temp_pb)

        self.layout().addSpacing(20)

        layout = QtWidgets.QHBoxLayout()
        self.layout().addLayout(layout)
        layout.addWidget(self.widget.refresh_pb)
        layout.addStretch()
        layout.addWidget(self.widget.build_all_pb)
        layout.addWidget(self.widget.update_all_pb)

        layout = QtWidgets.QVBoxLayout()
        self.layout().addLayout(layout)
        layout.addWidget(self.widget.soruce_tw)

        self.resize(1000, 800)

