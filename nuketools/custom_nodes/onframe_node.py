#######################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
################################################################################
##
##  Standard Library
##
import json

################################################################################
##
##  Nuke Library
##
import nuke

################################################################################
##
##  Snow Library
##
from snow.common.lib.widget import message_box
from snow.common.lib import shot_loader
from snow.nuke.lib import selection


def increment_node(node_name):
    increase = 1
    while nuke.exists(node_name):
        increase += 1
        prefix = node_name.split('_')[0]
        node_name = '{}_{}'.format(prefix, increase)
    return node_name


def create():
    nuke.endGroup()
    selection.deselect_all()
    igpath = shot_loader.ShotLoader(nuke.scriptName())
    data_publish = '{}/frames.json'.format(igpath.meta_dir)

    layer_data = []
    with open(data_publish, 'r') as readfile:
        try:
            read_frames = json.load(readfile)
        except ValueError:
            read_frames = dict()
        
        if isinstance( read_frames, dict):
            layer_data = read_frames.get('layers', [])

    if not layer_data:
        message_box.MessageBox().warning(
                'Data frame is older or incollect.\n'
                'Please check\n{}'.format(data_publish))
        return

    onframe_nodes = []
    layer_count = 1
    for data in layer_data:
        frame_sets = data.get('frame_sets')
        object_layout = data.get('objects')
        last_timeclip = None
        merge = None

        grp_name = 'TimeOnFrameLayer{}'.format(layer_count)
        grp_node = nuke.makeGroup(False)
        onframe_nodes.append(grp_node)

        grp_node['name'].setValue(increment_node(grp_name))
        tab_knob = nuke.Tab_Knob('Layer')
        grp_node.addKnob(tab_knob)
        grp_node.begin()

        ## Start create node inside the group
        framerange_node = nuke.createNode('FrameRange', inpanel=False)
        framerange_node.setInput(0, nuke.allNodes('Input')[0])
        framerange_node['first_frame'].setValue(nuke.Root().firstFrame())
        framerange_node['last_frame'].setValue(nuke.Root().lastFrame())

        ## Add group's knobs
        link = nuke.Link_Knob('framerange', 'Frame range')
        link.setLink('{}.first_frame'.format(framerange_node.name()))
        grp_node.addKnob(link)
        link = nuke.Link_Knob('last_frame', '')
        link.clearFlag(nuke.STARTLINE)
        link.setLink('{}.last_frame'.format(framerange_node.name()))
        grp_node.addKnob(link)
        
        knob = nuke.Text_Knob(
                'layer_name', 'Name', 'Layer{}'.format(layer_count))
        grp_node.addKnob(knob)
        knob = nuke.Text_Knob('divider', 'Object List')
        grp_node.addKnob(knob)
        knob = nuke.Text_Knob('object_list', '', '\n'.join(object_layout))
        grp_node.addKnob(knob)

        knob = nuke.Text_Knob('divider', 'Frame List')
        grp_node.addKnob(knob)
        knob = nuke.Text_Knob('frame_list', '',
                    '\n'.join(
                        ['{}x{}'.format(f, frame_sets[f]) for f in frame_sets]))
        grp_node.addKnob(knob)

        if len(frame_sets)>1:
            selection.deselect_all()
            merge = nuke.createNode('Merge2', inpanel=False)
            merge.setSelected(False)
            nuke.allNodes('Output')[0].setInput(0, merge)
        
        ## Create nodes for each frame set
        all_timeclip = []
        for frames, skip in sorted(frame_sets.items(),
                                key=lambda x: x[0].split('-')[0]):

            start, end = frames.split('-')
            framehold = nuke.createNode(
                    'FrameHold',
                    'first_frame {}.knob.first_frame'.format(
                            framerange_node.name()),
                    inpanel=False)
            framehold['increment'].setValue(skip)
            framehold.setInput(0, framerange_node)

            if not last_timeclip:
                timeclip = nuke.createNode(
                    'TimeClip',
                    'first {}.knob.first_frame'.format(
                            framerange_node.name()),
                    inpanel=False)
            else:
                timeclip = nuke.createNode(
                    'TimeClip',
                    'first {}'.format(start),
                    inpanel=False)
            all_timeclip.append(timeclip)
            timeclip.setInput(0, framehold)
            timeclip['last'].setValue(int(end))
            timeclip['origfirst'].setExpression(
                '{}.knob.first_frame'.format(framerange_node.name()))
            timeclip['origlast'].setExpression(
                '{}.knob.last_frame'.format(framerange_node.name()))

            last_timeclip = timeclip

        ## There is no more than a frame set don't need Merge node
        if not merge:
            timeclip['last'].setExpression(
                '{}.knob.last_frame'.format(framerange_node.name()))
            nuke.allNodes('Output')[0].setInput(0, last_timeclip)
        else:
            merge.setInput(0, None)
            for index in range(len(all_timeclip)):
                if index < 2:
                    merge.setInput(index, all_timeclip[index])
                else:
                    merge.setInput(index+1, all_timeclip[index])

        grp_node.end()
        grp_node.showControlPanel()
        layer_count += 1

        ## Deselect All
        selection.deselect_all()
        nuke.endGroup()
    
    selection.select_node(onframe_nodes)
    nuke.zoomToFitSelected()