#! nuke10
#######################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
#######################################
##
##	Standard Library
##
## GUI
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except:
    from PySide2 import QtWidgets, QtCore, QtGui

#######################################
##
##	Nuke Library
##
import nuke
#######################################
##
##	Module
##
import readhelper
import extractchannelgui


#######################################
##
## Main Class
##
class ExtractChannel:

    def __init__( self, readNode ):
        self.readNode = readNode
        self.allChannel = readhelper.getChannels( readNode )
        self.colorNode = {
                            "Exposure" : "EXPTool",
                            "Grade" : "Grade",
                            "HueCorrect" : "HueCorrect",
                            "HueShift" : "HueShift",
                            "Invert" : "Invert",
                            "Saturation": "Saturation",
                            }
    
    def setUi( self, parent, objName ):

        self.__gui = extractchannelgui.ExtractChannelGui( parent )
        self.__gui.setObjectName( objName )
        self.__gui.extractPB.clicked.connect( self.extract )
        self.__gui.readNodeLB.setText( self.readNode.name() )

        for ch in self.allChannel:
            item = QtWidgets.QListWidgetItem( ch )
            item.setFlags( item.flags() | QtCore.Qt.ItemIsUserCheckable )
            item.setCheckState( QtCore.Qt.Unchecked )
            item.setData( QtCore.Qt.UserRole, "Exposure" )
            self.__gui.channelLW.addItem( item )
        
        for nodeName, nodeType in self.colorNode.items():
            item = QtWidgets.QListWidgetItem( nodeName )
            item.setData( QtCore.Qt.UserRole, nodeType )
            self.__gui.colorNodeLW.addItem( item )
    
        self.__gui.exec_()
    
    
    def tuneNode( self, nodeList ):
        mergeNode = ''

        for node in nodeList:
            if node.Class() == "EXPTool":
                node['mode'].setValue( "Stops" )
            elif node.Class() == 'Merge2':
                mergeNode = node
        
        copyNode = nuke.nodes.Copy()
        copyNode['from0'].setValue("rgba.alpha")
        copyNode['to0'].setValue("rgba.alpha")

        mergeNode['operation'].setValue("plus")

        copyNode.setInput( 0, mergeNode )
        copyNode.setInput( 1, self.readNode )
    
    def extract( self ):

        channel = []
        for index in range( self.__gui.channelLW.count() ):
            item = self.__gui.channelLW.item( index )
            item
            if item.checkState() == QtCore.Qt.Checked:
                channel.append( item.text() )

        item = self.__gui.colorNodeLW.currentItem()
        output = []
        if item:
            color = item.data( QtCore.Qt.UserRole )
            output = [color]

        newNodeList = readhelper.createChannelShuffle( self.readNode, 
                                                        channel,
                                                        output=output,
                                                        mergeEnd=True )
        self.tuneNode( newNodeList )

#######################################
##
## Main Function
##
def main():
    readNode = ''
    selectedRead = nuke.selectedNodes( "Read" )
    if selectedRead:
        readNode = selectedRead[0]
    else:
        msg = QtWidgets.QMessageBox()
        msg.setIcon( QtWidgets.QMessageBox.Information )
        msg.setWindowTitle( "Inform" )
        msg.setText( "Please select Read node." )
        msg.exec_()
        return
    
    guiObjName = "ExtractChannelDialog"

    app = QtWidgets.QApplication.activeWindow()
    widget = app.findChild( QtWidgets.QDialog, guiObjName )
    if widget:
        widget.close()
        widget.deleteLater()

    extractChannel = ExtractChannel( readNode )
    extractChannel.setUi( app, guiObjName )
