#! nuke10
#######################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
#######################################
## Standard Library
## GUI
try:
	from PySide import QtGui, QtCore
	import PySide.QtGui as QtWidgets
except:
	from PySide2 import QtWidgets, QtCore, QtGui

#####################################################
##
##	Main Window
##
class ExtractChannelGui( QtWidgets.QDialog ):
	def __init__( self, parent=None ):		
		super( ExtractChannelGui, self ).__init__()
		self.setWindowTitle( "Extract Channel" )
		self.setStyleSheet( self.styleSheet )
		self.setLayout( QtWidgets.QVBoxLayout() )

		self.previousItems = []

		## Widget Intialize
		self.readNodeLB = QtWidgets.QLabel()
		self.readNodeLB.setStyleSheet("font: bold 15px;\
						color: rgb(220,120,50);\
						padding: 5px;")
		self.channelLW = QtWidgets.QListWidget()
		self.channelLW.setSelectionMode( QtGui.QAbstractItemView.ExtendedSelection )
		self.channelLW.itemChanged.connect( self.setCheckItem )
		self.channelLW.itemDoubleClicked.connect( self.toggleCheckItem )

		self.colorNodeLW = QtWidgets.QListWidget()
		self.extractPB = QtWidgets.QPushButton( "Extract Channel" )
	
		self.initialLayout()
	
	def initialLayout( self ):

		self.layout().addWidget( self.readNodeLB )

		layout = QtWidgets.QHBoxLayout()
		self.layout().addLayout( layout )

		channel_layout = QtWidgets.QVBoxLayout()
		label = QtWidgets.QLabel( "Channels" )
		label.setStyleSheet("border-bottom-style: solid;\
							border-bottom-width: 1px;\
							padding: 5px;")
		channel_layout.addWidget( label )
		channel_layout.addWidget( self.channelLW )
		layout.addLayout( channel_layout )

		color_layout = QtWidgets.QVBoxLayout()
		label = QtWidgets.QLabel( "Color" )
		label.setStyleSheet("border-bottom-style: solid;\
							border-bottom-width: 1px;\
							padding: 5px;")
		color_layout.addWidget( label )
		color_layout.addWidget( self.colorNodeLW )
		layout.addLayout( color_layout )
		
		extractPB_layout = QtWidgets.QHBoxLayout()
		extractPB_layout.setAlignment( QtCore.Qt.AlignRight )
		extractPB_layout.addWidget( self.extractPB )
		self.layout().addLayout( extractPB_layout )

	def setCheckItem( self, item ):

		state = item.checkState()

		selItem = self.channelLW.selectedItems()

		for item in selItem:
			item.setCheckState( state )


	def toggleCheckItem( self, item ):

		if item.checkState() == QtCore.Qt.Checked:
			item.setCheckState( QtCore.Qt.Unchecked )
		else:
			item.setCheckState( QtCore.Qt.Checked )

	@property
	def styleSheet( self ):
		return """
			* {
				background: rgb(50,50,50);
				font:13px;
				color: #DDDDDD;
				border: 1px groove rgb(38,38,38);
				border-radius: 3px;
			}
			QLabel, QCheckBox{
				border: none;
			}

			QPushButton {

				background: rgb(74,74,74);
				min-width: 5em;
				padding: 4px;
			}
			QPushButton:pressed {

				background: rgb(45,45,45);
				min-width: 5em;
				padding: 4px;
			}

			QPushButton:disabled{
				background: #353535;
				color: #5A5A5A;
			}
			QListWidget{
				border:none;
			}

		"""