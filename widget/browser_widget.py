#!Python3
################################################################################
##
##  Standard Library
##
import os
import re
from glob import glob
import tempfile
import json

# UI
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtWidgets, QtCore, QtGui

################################################################################
##
##  SNOW Modules
##
import snow.common.lib.config as snow_config
from snow.common.lib import os_file, path_elem

###############################
#
#   Widget Template
#
class ComboWidget(QtWidgets.QWidget):
    """ 
    Contain a pair of QLabel and QComboBox
        - QLabel is a label of the QComboBox
    
        Examples:
            widget = ComboWidget()
            widget.label.setText('Topic')
            widget.combo_box.addItems(a_list)
    """
    def __init__(self, parent=None):
        super(ComboWidget, self).__init__(parent)
        self.setContentsMargins(0, 0, 0, 0)
        self.label = QtWidgets.QLabel('')
        self.combo_box = QtWidgets.QComboBox()

        QtWidgets.QHBoxLayout(self)
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setAlignment(QtCore.Qt.AlignLeft)

        self.layout().addWidget(self.label)
        self.combo_box.setMinimumWidth(100)
        self.layout().addWidget(self.combo_box)


class FolderListWidget(QtWidgets.QListWidget):
    """
    QListWidget for input directory items

    Features:
        * Drag to drop a directory path
        * Ctrl+c copy directory to clipboard

    """

    def __init__(self, *args):
        super(FolderListWidget, self).__init__(*args)

    def startDrag(self, action):
        """This is a virtual protected methed in QListWidget for dragging an item,
        result is a directory from item data

        Args:
            action (QtCore.Qt.DropActions): supportedActions
        """
        drag = QtGui.QDrag(self)
        path = self.currentItem().data(QtCore.Qt.UserRole)
        mimeData = self.model().mimeData(self.selectedIndexes())
        mimeData.setText(path)
        drag.setMimeData(mimeData)
        drag.exec_(action, QtCore.Qt.MoveAction)
    
    def dragMoveEvent(self, event):
        """This is a override virtual protected methed in QListWidget

        Args:
            event (QDragMoveEvent): event
        """
        if event.mimeData().hasUrls():
            event.ignore()
        else:
            event.accept()
    
    def keyPressEvent(self, event):
        """This is a override virtual protected methed in QListWidget

        Args:
            event (QKeyEvent): event
        """
        if event.matches(QtGui.QKeySequence.Copy):
            # -- Copy directory to clipboard
            path = self.currentItem().data(QtCore.Qt.UserRole)
            QtWidgets.QApplication.clipboard().setText(path)


class ListWidget(QtWidgets.QWidget):
    """Contain a pair of QLabel and FolderListWidget

    Features:
        * Set layout to horizontal by __init__(h_layout=True)
        * QLabel is a label of the FolderListWidget
        * Double-clicke item to open folder
        * ContextMenuPolicy = QtCore.Qt.CustomContextMenu by default
    
    Examples:
        widget = ListWidget()
        widget.label.setText('Topic')
        widget.listwidget.add_item(folder_name, folder_path)
    """
    def __init__(self, parent=None, h_layout=False):
        super(ListWidget, self).__init__(parent)
        self.setContentsMargins(0, 0, 0, 0)

        if not h_layout:
            QtWidgets.QVBoxLayout(self)
        else:
            QtWidgets.QHBoxLayout(self)

        self.layout().setContentsMargins(0, 0, 0, 0)
        self.label = QtWidgets.QLabel('')
        font = self.label.font()
        font.setBold(True)
        self.label.setFont(font)
        self.listwidget = FolderListWidget()
        self.listwidget.itemDoubleClicked.connect(self.open_explorer)
        self.listwidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        self.layout().addWidget(self.label)
        self.layout().addWidget(self.listwidget)
    
    def open_explorer(self):
        """Open folder"""
        os_file.open_explorer(self.current_item_data(),select=False)
    
    def add_item(self, text, data):
        """Add item to listwidget

        Args:
            text (str): folder name
            data (str): folder path
        """
        item = QtWidgets.QListWidgetItem(self.listwidget)
        item.setText(text)
        item.setData(QtCore.Qt.UserRole, data)

    def current_item(self):
        """Current seleted item

        Returns:
            QListWidgetItem: an item is selected
        """
        return self.listwidget.currentItem()
    
    def current_item_data(self):
        """Current seleted item data

        Returns:
            str: directory
        """
        item = self.listwidget.currentItem()
        if item:
            return item.data(QtCore.Qt.UserRole)
        return None

    def clear(self):
        """Clear all item in listwidget
        """
        self.listwidget.clear()


class FileSystemWidget(QtWidgets.QTreeView):
    """
    QTreeView for QFileSystemModel

    Features:
        * Filter filename
            - filter_name()
        * Get file path from seleted item
            - current_file()
    
    Examples:
        widget = FileSystemWidget()
        widget.set_root_path(work_dir)

    """
    def __init__(self, *args):
        super().__init__(*args)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.model = QtWidgets.QFileSystemModel(self)
        self.model.setReadOnly(True)
        self.model.setNameFilterDisables(False)
        self.proxyModel = QtCore.QSortFilterProxyModel()
        self.setDragEnabled(True)
        self.setSortingEnabled(True)

        self.root_dir = ''
        
    def clear(self):
        """Clear all item
        """
        self.setModel(None)
    
    def filter_name(self, name):
        """Filter file include "name"

        Args:
            name (str): some word to search
        """
        if name == '-all-':
            self.model.setNameFilters([])
        else:
            self.model.setNameFilters(['*{}*'.format(name)])

    def set_root_path(self, path):
        """Set root directory

        Args:
            path (str): directory
        """
        self.root_dir = path
        if not self.proxyModel.sourceModel():
            self.proxyModel.setSourceModel(self.model)
        # -- exclude -- 
        #        > filename.ext~
        self.proxyModel.setFilterRegExp(r"^([^\~]+)$")
        # -------------
        self.setModel(self.proxyModel)
        index = self.proxyModel.sourceModel().setRootPath(path)       
        map_index = self.proxyModel.mapFromSource(index)
        self.setRootIndex(map_index)
  
        self.setColumnWidth(0, 400)
    
    def set_only_folder(self, state):
        """Show only folder or including file

        Args:
            state (bool): True is only folder
        """
        if state:
            self.model.setFilter(QtCore.QDir.NoDotAndDotDot | QtCore.QDir.AllDirs | QtCore.QDir.NoSymLinks)
        else:
            self.model.setFilter(QtCore.QDir.NoDotAndDotDot | QtCore.QDir.Files | QtCore.QDir.NoSymLinks)

    def current_file(self):
        """Current file path from seleted item

        Returns:
            str: file path
        """
        item_model = self.selectedIndexes()
        if not item_model:
            return
        model_index = item_model[0]
        file_path = '{}/{}'.format(self.root_dir, model_index.data())
        return file_path

###############################
#
#   Browser Element
#
class EntityType(ComboWidget):
    """Entity type box with label
    
    Features:
        * Item data contain {'main_folders': main_fodlers, 
                                'entity_type': entity_type}

    """
    def __init__(self, parent=None):
        super(EntityType, self).__init__(parent)
        self.label.setText('Entity Type:')
        all_entity = snow_config.get_entity_main_folders()
        for entity, dept_folders in sorted(all_entity.items()):
            # -- keep data each entity in item data --
            self.combo_box.addItem(entity, dept_folders)


class Department(ComboWidget):
    """Department box with label
    
    Features:
        * Item data contain dependent task list
    
    Examples:
        widget = Department()
        widget.add_items(dept_list)

    """
    def __init__(self, parent=None):
        super(Department, self).__init__(parent)
        self.label.setText('Department:')

    def add_items(self, depts=[]):
        """Add item list

        Args:
            depts (list, optional): department list. Defaults to [].

        """
        self.combo_box.clear()
        all_dept = snow_config.get_all_departments()

        for dep, tasks in sorted(all_dept.items()):
            if depts and dep not in depts:
                continue
            # -- keep tasks each department in item data --
            self.combo_box.addItem(dep, tasks)


class Task(ListWidget):
    """Task List

    Features:
        * Insert task item from filenmae

    Examples:
        widget = Task()
        widget.add_items(task_list)
        widget.insert_exists_file_task(work_dir)

    """
    
    def __init__(self, parent=None, h_layout=False):
        super().__init__(parent, h_layout)
        self.label.setText('Task:')
    
    def add_items(self, task_list=[]):
        """Add item into listwidget

        Args:
            task_list (list, optional): a list of task. Defaults to [].
        """
        self.listwidget.clear()
        self.listwidget.addItem('-all-')
        self.listwidget.addItems(task_list)
        self.listwidget.sortItems()

    def add_items_by_step(self, step):
        """It will use step/deparment to get tasks and add items to listwidget.
        
        Args:
            step (str): step name

        """
        self.listwidget.clear()
        self.listwidget.addItem('-all-')
        task_name_list = snow_config.tasks_in_department(step.lower())

        if task_name_list:
            self.listwidget.addItems(sorted(task_name_list))

        self.listwidget.sortItems()
    
    def insert_exists_file_task(self, folder):
        """Get task from filename in folder

        Args:
            folder (str): directory
        """
        task_regx = re.compile(snow_config.file_regx('task'))

        match_files = [task_regx.findall(f)[0] for f in os_file.list_files(folder) \
                        if task_regx.findall(f)]

        match_files = list(set(match_files))
        match_files = [t for t in match_files \
                        if not self.listwidget.findItems(t, QtCore.Qt.MatchExactly)]
        self.listwidget.addItems(match_files)
        self.listwidget.sortItems()

###############################
#
#   Asset Element
#
class AssetType(ListWidget):
    """Asset Type Widget

    Features:
        * Item data contain directory path

    Examples:
        widget = AssetType()
        widget.add_dir_items(
                    project=project,
                    ent_folder_name=folder_name)
        item = widget.listwidget.currentItem()
    """

    def __init__(self, parent=None, h_layout=False):
        super().__init__(parent, h_layout)
        self.label.setText('Asset Type:')
        self.listwidget.setDragEnabled(True)
    
    def add_dir_items(self, project, ent_folder_name):
        """It will search entity in diretory then add them to the listwidget

        Args:
            project (str): prject name
            ent_folder_name (str): foler name that contain the entity
        """
        self.listwidget.clear()
        asset_dir = snow_config.path_to_entity()[ent_folder_name]['dir']
        asset_dir = '{}{}/{}'.format(snow_config.work_repo(), project, asset_dir)
        
        if not os.path.exists(asset_dir):
            return

        for a_type in sorted(os_file.list_folders(asset_dir)):
            self.add_item(a_type, '{}/{}'.format(asset_dir, a_type))


class SubType(ListWidget):
    """Asset SubType Widget

    Features:
        * Item data contain directory path

    Examples:
        widget = SubType()
        widget.add_dir_items(root_dir=root_dir)
        item = widget.listwidget.currentItem()
    """
    def __init__(self, parent=None, h_layout=False):
        super().__init__(parent, h_layout)
        self.label.setText('Sub Type:')
        self.listwidget.setDragEnabled(True)

    def add_dir_items(self, root_dir):
        """It will search entity in diretory then add them to the listwidget

        Args:
            root_dir (str): direcotry
        """
        self.listwidget.clear()
        for a_sub in sorted(os_file.list_folders(root_dir)):
            self.add_item(a_sub, '{}/{}'.format(root_dir, a_sub))


class Asset(ListWidget):
    """Asset Widget

    Features:
        * Item data contain directory path
        * Get their ShotGrid entity
            - current_entity_item()

    Examples:
        widget = Asset()
        widget.add_dir_items(root_dir=root_dir)
        item = widget.listwidget.currentItem()
        asset_ent = widget.current_entity_item()
        
    """
    def __init__(self, parent=None, h_layout=False):
        super().__init__(parent, h_layout)
        self.label.setText('Asset Name:')
        self.listwidget.setDragEnabled(True)

    def current_entity_item(self):
        """Get ShotGrid entity

        Returns:
            dict: entity data
        """
        item = self.listwidget.currentItem()
        if not item:
            return
        path = item.data(QtCore.Qt.UserRole)
        project = path_elem.get_project_code(
                    re.findall(snow_config.folder_regx('project'),
                                path)[0])
        entity_type = re.findall(snow_config.folder_regx('asset_entity'), path)[0]
        entity_cache = path_elem.SG_ENTITY.format(sg_project=project,
                        entity=entity_type, code=item.text())
        
        if os.path.exists(entity_cache):
            return path_elem.yaml.read(entity_cache)

    def add_dir_items(self, root_dir):
        """It will search entity in diretory then add them to the listwidget

        Args:
            root_dir (str): direcotry
        """
        self.listwidget.clear()
        for name in sorted(os_file.list_folders(root_dir)):
            self.add_item(name, '{}/{}'.format(root_dir, name))

###############################
#
#   Shot Element
#
class Episode(ListWidget):
    """Episode Widget

    Features:
        * Item data contain directory path

    Examples:
        widget = Episode()
        widget.add_dir_items(
                    project=project,
                    ent_folder_name=folder_name)
        item = widget.listwidget.currentItem()
    """
    def __init__(self, parent=None, h_layout=False):
        super().__init__(parent, h_layout)
        self.label.setText('Episode:')
        self.listwidget.setDragEnabled(True)
    
    def add_dir_items(self, project, ent_folder_name, start_with=''):
        """It will search entity in diretory then add them to the listwidget

        Args:
            project (str): prject name
            ent_folder_name (str): foler name that contain the entity
            start_with (str, optional): filter name only start with this word. Defaults to ''.
        """
        self.listwidget.clear()

        ep_dir = snow_config.path_to_entity()[ent_folder_name]['dir']
        ep_dir = '{}{}/{}'.format(snow_config.work_repo(), project, ep_dir)

        if not os.path.exists(ep_dir):
            return

        for ep in sorted(os_file.list_folders(ep_dir,start_with=start_with)):
            self.add_item(ep, '{}/{}'.format(ep_dir, ep))


class Sequence(ListWidget):
    """Sequence Widget

    Features:
        * Item data contain directory path
        * Get their ShotGrid entity
            - current_entity_item()
    Examples:
        widget = Sequence()
        widget.add_dir_items(root_dir=root_dir)
        item = widget.listwidget.currentItem()
        seq_ent = widget.current_entity_item()
    """
    def __init__(self, parent=None, h_layout=False):
        super().__init__(parent, h_layout)
        self.label.setText('Sequence:')
        self.listwidget.setDragEnabled(True)

    def current_entity_item(self):
        """Get ShotGrid entity

        Returns:
            dict: entity data
        """
        item = self.listwidget.currentItem()
        if not item:
            return
        path = item.data(QtCore.Qt.UserRole)
        project = path_elem.get_project_code(
                    re.findall(snow_config.folder_regx('project'),
                                path)[0])
        episode = re.findall(snow_config.folder_regx('episode'), path)[0]
        code = '{}_{}_{}'.format(project, episode, item.text())
        entity_cache = path_elem.SG_ENTITY.format(sg_project=project, entity='Sequence', code=code)

        if os.path.exists(entity_cache):
            return path_elem.yaml.read(entity_cache)

    def add_dir_items(self, root_dir='', start_with=''):
        """It will search entity in diretory then add them to the listwidget

        Args:
            root_dir (str, optional): direcotry. Defaults to ''.
            start_with (str, optional): filter name only start with this word. Defaults to ''.
        """
        self.listwidget.clear()
        if not os.path.exists(root_dir):
            return

        for seq in sorted(os_file.list_folders(
                                        root_dir, start_with=start_with)):
            path = '{}/{}'.format(root_dir, seq)
            self.add_item(seq, path)


class Shot(ListWidget):
    """Shot Widget

    Features:
        * Item data contain directory path
        * Get their ShotGrid entity
            - current_entity_item()
    Examples:
        widget = Shot()
        widget.add_dir_items(root_dir=root_dir)
        item = widget.listwidget.currentItem()
        shot_ent = widget.current_entity_item()
    """
    def __init__(self, parent=None, h_layout=False):
        super().__init__(parent, h_layout)
        self.label.setText('Shot:')
        self.listwidget.setDragEnabled(True)

    def current_entity_item(self):
        """Get ShotGrid entity

        Returns:
            dict: entity data
        """
        item = self.listwidget.currentItem()
        if not item:
            return
        path = item.data(QtCore.Qt.UserRole)
        project = path_elem.get_project_code(
                    re.findall(snow_config.folder_regx('project'),
                                path)[0])
        episode = re.findall(snow_config.folder_regx('episode'), path)[0]
        code = '{}_{}_{}'.format(project, episode, item.text())
        entity_cache = path_elem.SG_ENTITY.format(sg_project=project,
                                                    entity='Shot', code=code)
        
        if os.path.exists(entity_cache):
            return path_elem.yaml.read(entity_cache)

    def add_dir_items(self, root_dir, start_with=''):
        """It will search entity in diretory then add them to the listwidget

        Args:
            root_dir (str, optional): direcotry. Defaults to ''.
            start_with (str, optional): filter name only start with this word. Defaults to ''.
        """
        self.listwidget.clear()
        for name in sorted(os_file.list_folders(
                                        root_dir, start_with=start_with)):
            path = '{}/{}'.format(root_dir, name)
            self.add_item(name, path)


###############################
#   Browser
class BrowserWidget(QtWidgets.QWidget):
    """Browser Widget

    Features:
        * Child object can add their own widget in main layouts include:
            - self.head_layout is top layout
            - self.central_widget.layout() is centre layout
        * Search Asset or Shot name by typing in search line like 0050, 0080.
        It will search all in project
        * Support to extend feature in child object
    
    Signals:
        work_dir_found : emit current entity work directory
        root_dir_changed: emit current root entity directory

    """

    work_dir_found = QtCore.Signal(str)
    root_dir_changed = QtCore.Signal(str)

    def __init__(self, parent=None, project=''):
        """Browser Widget

        Args:
            parent (QWidget, optional): Parent widget. Defaults to None.
            project (str, optional): project name. Defaults to ''.
        """
        super().__init__(parent)
        self.project = project
        self.search_found = []
        self.pre_word_search = ''
        self.file_dir_fmt = snow_config.project('file_dir')

        # -- init user setup --
        self.pref_file = '{}/.snow/browserwidget.yaml'.format(tempfile.gettempdir().replace('\\', '/'))
        tool_yaml =snow_config.get_tool_config(
                                            __file__,
                                            project=self.project,
                                            filename='browser.yaml')
        self.scene_browser_conf = snow_config.yaml.read(tool_yaml)
        # -- init widget --
        self.ent_type_wd = EntityType()
        self.dept_wd = Department()
        self.search_le = QtWidgets.QLineEdit()
        self.search_le.setPlaceholderText('Search Asset/Shot')
        self.asset_browser = self.asset_browser_widget()
        self.shot_browser = self.shot_browser_widget()
        self.status_bar = QtWidgets.QLabel()
        self.status_bar.setScaledContents(True)
        # -----------------
        self.connect_signal()
        self.__set_layout()
        self.initialize_content()
        self.init_user_setup()

    def __set_layout(self):
        """Initial layout
        """
        # -- initialize layout --
        QtWidgets.QVBoxLayout(self)
        self.head_layout = QtWidgets.QHBoxLayout()
        self.layout().addLayout(self.head_layout)
        self.head_layout.addWidget(self.ent_type_wd)
        self.head_layout.addWidget(self.dept_wd)
        self.head_layout.addStretch()
        self.head_layout.addWidget(self.search_le)
        self.layout().addSpacing(20)
        
        self.central_widget = self.central_splitter()
        self.central_widget.layout().addWidget(self.asset_browser)
        self.central_widget.layout().addWidget(self.shot_browser)

        self.central_widget.setStretchFactor(0, 2)
        self.central_widget.setStretchFactor(1, 2)

        self.layout().addWidget(self.central_widget)
        self.layout().addSpacing(20)
        self.layout().addWidget(self.status_bar)

    def initialize_content(self):
        """Initialize widget content
        """
        self.dept_wd.add_items(self.ent_main_folders)
        self.search_le.clear()
        self.status_bar.clear()
        self.set_mode()
        self.set_tasks()

    @property
    def ent_type(self):
        """Entity type

        Returns:
            dict: entity
        """
        return self.ent_type_wd.combo_box.currentData(QtCore.Qt.UserRole).get('entity_type')

    @property
    def ent_main_folders(self):
        """A list of folders is depend on an entity

        Returns:
            list: a list of folders
        """
        return self.ent_type_wd.combo_box.currentData(QtCore.Qt.UserRole).get('main_folders')    

    @property
    def ent_folder_name(self):
        """Current entity folder name

        Returns:
            str: entity folder name
        """
        return self.ent_type_wd.combo_box.currentText()
    
    @property
    def ent_root_dir(self):
        """A Directory is root of entity

        Returns:
            str: Directory path
        """
        root_dir = snow_config.path_to_entity()[self.ent_folder_name]['dir']
        return '{}{}/{}'.format(snow_config.work_repo(),
                                    self.project, root_dir)

    @property
    def department(self):
        """Current department

        Returns:
            str: department/step name
        """
        return self.dept_wd.combo_box.currentText()
    
    @property
    def task(self):
        """Current task name

        Returns:
            str: task name
        """
        taskwidget = self.get_task_widget()
        if taskwidget.listwidget.currentRow() > 0:
            return taskwidget.listwidget.currentItem().text()
    
    @property
    def task_list(self):
        """All task name in listwidget

        Returns:
            list: task name list
        """
        taskwidget = self.get_task_widget()
        task_list = []
        for row in range(1, taskwidget.listwidget.count()):
            task_list.append(
                        taskwidget.listwidget.item(row).text())
        return task_list
    
    def get_task_widget(self):
        """Task widget in Shot and Asset is different a widget.
        This will get what Task Widget is using

        Returns:
            QWidget: Task Widget
        """
        taskwidget = None
        if not self.shot_browser.isHidden():
            taskwidget = self.shot_browser.task
        elif not self.asset_browser.isHidden():
            taskwidget = self.asset_browser.task
        return taskwidget

    def init_user_setup(self):
        """Read user init file and set data to widget
        """
        data = dict()
        try:
            with open(self.pref_file) as config:
                data = json.load(config)
        except IOError:
            print('Cannot read user setup:', self.pref_file)
        except json.decoder.JSONDecodeError:
            print('Reset user setup:', self.pref_file)
            os.remove(self.pref_file)

        if not data:
            return

        ent_type = self.ent_type_wd.combo_box.findText(
                    data['ent_type'], QtCore.Qt.MatchExactly)
        if ent_type:
            self.ent_type_wd.combo_box.setCurrentIndex(ent_type)
        
        dept = self.dept_wd.combo_box.findText(
                    data['dept'], QtCore.Qt.MatchExactly)
        if dept:
            self.dept_wd.combo_box.setCurrentIndex(dept)
        # --------------------
        # -- Asset Browser --
        if not self.asset_browser.isHidden():
            items = self.asset_browser.type.listwidget.findItems(
                        data.get('asset_browser.type.listwidget'), QtCore.Qt.MatchExactly)
            if items:
                self.asset_browser.type.listwidget.setCurrentItem(items[0])

            items = self.asset_browser.subtype.listwidget.findItems(
                        data.get('asset_browser.subtype.listwidget'), QtCore.Qt.MatchExactly)
            if items:
                self.asset_browser.subtype.listwidget.setCurrentItem(items[0])

            items = self.asset_browser.asset.listwidget.findItems(
                        data.get('asset_browser.asset.listwidget'), QtCore.Qt.MatchExactly)
            if items:
                self.asset_browser.asset.listwidget.setCurrentItem(items[0])
        # --------------------
        # -- Shot Browser --
        if not self.shot_browser.isHidden():
            items = self.shot_browser.episode.listwidget.findItems(
                        data.get('shot_browser.episode.listwidget'), QtCore.Qt.MatchExactly)
            if items:
                self.shot_browser.episode.listwidget.setCurrentItem(items[0])

            items = self.shot_browser.sequence.listwidget.findItems(
                        data.get('shot_browser.sequence.listwidget'), QtCore.Qt.MatchExactly)
            if items:
                self.shot_browser.sequence.listwidget.setCurrentItem(items[0])

            items = self.shot_browser.shot.listwidget.findItems(
                        data.get('shot_browser.shot.listwidget'), QtCore.Qt.MatchExactly)
            if items:
                self.shot_browser.shot.listwidget.setCurrentItem(items[0])

    def hideEvent(self, event):
        """Get data from widget to write user init file
        """
        data = {'ent_type': self.ent_type_wd.combo_box.currentText(),
                'dept': self.dept_wd.combo_box.currentText(),
                }
        # -- Asset Browser --
        item = self.asset_browser.type.listwidget.currentItem()
        if item:
            data['asset_browser.type.listwidget'] = item.text()
        item = self.asset_browser.subtype.listwidget.currentItem()
        if item:
            data['asset_browser.subtype.listwidget'] = item.text()
        item = self.asset_browser.asset.listwidget.currentItem()
        if item:
            data['asset_browser.asset.listwidget'] = item.text()
        # --------------------
        # -- Shot Browser --
        item = self.shot_browser.episode.listwidget.currentItem()
        if item:
            data['shot_browser.episode.listwidget'] = item.text()
        item = self.shot_browser.sequence.listwidget.currentItem()
        if item:
            data['shot_browser.sequence.listwidget'] = item.text()
        item = self.shot_browser.shot.listwidget.currentItem()
        if item:
            data['shot_browser.shot.listwidget'] = item.text()

        try:
            with open(self.pref_file, 'w+') as configfile:
                json.dump(data, configfile, indent=4)
        except IOError:
            print('Cannot write user setup:', self.pref_file)

        event.accept()

    def connect_signal(self):
        """Connect widget signals
        """
        self.ent_type_wd.combo_box.currentIndexChanged.connect(self.initialize_content)
        self.dept_wd.combo_box.currentIndexChanged.connect(self.set_tasks)

        # -- shot widget signal --
        self.shot_browser.episode.listwidget.currentItemChanged.connect(self.episode_clicked)
        self.shot_browser.episode.listwidget.itemClicked.connect(self.episode_clicked)
        self.shot_browser.sequence.listwidget.currentItemChanged.connect(self.sequence_clicked)
        self.shot_browser.shot.listwidget.currentItemChanged.connect(self.shot_clicked)
# 
        # -- asset widget signal --
        self.asset_browser.type.listwidget.currentItemChanged.connect(self.asset_type_clicked)
        self.asset_browser.subtype.listwidget.currentItemChanged.connect(self.asset_subtype_clicked)
        self.asset_browser.asset.listwidget.currentItemChanged.connect(self.asset_clicked)
        self.work_dir_found.connect(self.append_exists_task)
        self.search_le.editingFinished.connect(self.filter_entity)

    def set_mode(self):
        """Set mode if Asset or Shot. It will display only widget for their mode
        """
        # -- Asset Mode --
        if self.ent_folder_name in ['asset', 'design']:
            self.asset_browser.show()
            self.shot_browser.hide()
            self.asset_browser.type.add_dir_items(
                                            project=self.project,
                                            ent_folder_name=self.ent_folder_name)
            self.asset_browser.subtype.clear()
            self.asset_browser.asset.clear()
        # -- Shot Mode --
        else:
            self.asset_browser.hide()
            self.shot_browser.show()
            self.shot_browser.episode.add_dir_items(
                                            project=self.project,
                                            ent_folder_name=self.ent_folder_name,
                                            start_with=self.scene_browser_conf['folder_filter']['episode']['prefix'])
            self.shot_browser.sequence.clear()
            self.shot_browser.shot.clear()

    def set_tasks(self):
        """Add task items for each mode
        """
        task_list = self.dept_wd.combo_box.currentData(QtCore.Qt.UserRole) or []
        
        # -- Shot Mode --
        if not self.shot_browser.isHidden():
            self.shot_browser.task.clear()
            self.shot_browser.task.add_items(task_list)
            if self.ent_folder_name == 'scene':
                item = self.shot_browser.shot.current_item()
                if item:
                    self.shot_clicked(item)
            elif self.ent_folder_name == 'sequence':
                item = self.shot_browser.sequence.current_item()
                if item:
                    self.sequence_clicked(item)
        # -- Asset Mode --
        elif not self.asset_browser.isHidden():
            self.asset_browser.task.clear()
            self.asset_browser.task.add_items(task_list)
            item = self.asset_browser.asset.current_item()
            if item:
                self.asset_clicked(item)
    
    def reset_listwidget(self):
        """Restore and clear selection listwidget
        """
        def _reset(listwidgets):
            for widget in listwidgets:
                # -- Show all items in a listwidget of episode or asset type --
                if widget is self.shot_browser.episode.listwidget or \
                    widget is self.asset_browser.type.listwidget:
                    for row in range(widget.count()):
                        item = widget.item(row)
                        item.setHidden(False)
                # -- Clear selection for every listwidget
                widget.clearSelection()

        _reset(self.asset_browser.entity_listwidgets)
        _reset(self.shot_browser.entity_listwidgets)

    def filter_listwidget(self, listwidget, path_list):
        """Hide item hasn't data include root path in "path_list"

        Args:
            listwidget (QListWidget): listwidget
            path_list (list): a list of path
        """
        for row in range(listwidget.count()):
            item = listwidget.item(row)
            item_path = item.data(QtCore.Qt.UserRole)
            item.setHidden(False)

            if not [f for f in path_list if f.startswith(item_path)]:
                item.setHidden(True)

    def search_entity_folder(self, words=[], path_found=[]):
        """Search any match folder by words

        Args:
            words (list, optional): keyword to search. Defaults to [].
            path_found (list, optional): paths for poking around to search. Defaults to [].
        Returns:
            list: file list
        """
        self.reset_listwidget()
        # -- when "path_found" is nothing and no "words", 
        # the listwidget will be cleared --
        self.search_found = path_found

        if words:
            for word in words:
                self.search_found = []
                glob_fmt = self.scene_browser_conf['search_entity'][self.ent_folder_name]
                glob_fmt = glob_fmt.format(root_dir=self.ent_root_dir, word=word)
                found = [f.replace('\\','/') for f in glob(glob_fmt)]
                if found:
                    self.search_found += found

        # -- filter entity --
        if self.ent_folder_name in ['asset', 'design']:
            self.filter_listwidget(self.asset_browser.type.listwidget, self.search_found)
            # -- clear dependent listwidgets --
            self.filter_listwidget(self.asset_browser.subtype.listwidget, [])
            self.filter_listwidget(self.asset_browser.asset.listwidget, [])
        else:
            self.filter_listwidget(self.shot_browser.episode.listwidget, self.search_found)
            # -- clear dependent listwidgets --
            self.filter_listwidget(self.shot_browser.sequence.listwidget, [])
            self.filter_listwidget(self.shot_browser.shot.listwidget, [])
        
        self.search_le.clearFocus()
    
    def get_filter_search(self):
        """This will prevent searching process when received a signal
        with no different or there isn't any word.

        Returns:
            str or None: current word to search
        """

        current_word = self.search_le.text()
        if (self.pre_word_search and\
            current_word == self.pre_word_search) or\
            (not self.pre_word_search and not current_word):
            return None
        self.pre_word_search = current_word
        return current_word

    def filter_entity(self, word=''):
        """Filter entity only include word

        Args:
            word (str, optional): word to search. It can be many word
                                with using comma(,) . Defaults to ''.
        """
        current_word = word or self.get_filter_search()
        if current_word:
            words = [w.strip() for w in current_word.split(',')]
            self.search_entity_folder(words)

    def append_exists_task(self, work_dir):
        """Append task from filename in the directory

        Args:
            work_dir (str): Directory
        """
        if not self.shot_browser.isHidden():
            self.shot_browser.task.insert_exists_file_task(work_dir)
        elif not self.asset_browser.isHidden():
            self.asset_browser.task.insert_exists_file_task(work_dir)

    def asset_type_clicked(self, item):
        """Add item in AssetSubType widget

        Args:
            item (QListWidgetItem): Asset type item
        """
        # -- clear ---
        self.asset_browser.subtype.clear()
        # ------------
        if not item or not self.asset_browser.type.listwidget.currentItem():
            return

        root_dir = item.data(QtCore.Qt.UserRole)
        self.status_bar.setText(root_dir)
        self.asset_browser.subtype.add_dir_items(root_dir)

        # -- Filter item for searching --
        if self.search_found:
            self.filter_listwidget(self.asset_browser.subtype.listwidget,
                                    self.search_found)

    def asset_subtype_clicked(self, item):
        """Add item in Asset widget

        Args:
            item (QListWidgetItem): Asset subtype item
        """
        # -- clear ---
        self.asset_browser.asset.clear()
        # ------------
        if not item:
            return
        root_dir = item.data(QtCore.Qt.UserRole)
        self.status_bar.setText(root_dir)
        self.asset_browser.asset.clear()
        self.asset_browser.asset.add_dir_items(root_dir)

        # -- Filter item for searching --
        if self.search_found:
            self.filter_listwidget(self.asset_browser.asset.listwidget,
                                    self.search_found)

    def asset_clicked(self, item):
        """Set root directorr to filesystem browser

        Args:
            item (QListWidgetItem): Asset item
        """
        if item:
            root_dir = item.data(QtCore.Qt.UserRole)
            self.status_bar.setText(root_dir)
            self.root_dir_changed.emit(root_dir)
        else:
            self.root_dir_changed.emit('')
    
    def episode_clicked(self, item):
        """Add item in Sequence widget

        Args:
            item (QListWidgetItem): Episode item
        """
        # -- clear ---
        self.shot_browser.sequence.clear()
        # ------------
        if not item or not self.shot_browser.episode.listwidget.currentItem():
            return

        folder_filter = self.scene_browser_conf['folder_filter']
        root_dir = item.data(QtCore.Qt.UserRole)

        if self.ent_folder_name == 'scene':
            self.shot_browser.shot.setEnabled(True)
        elif self.ent_folder_name == 'sequence':
            root_dir += '/Sequences'
            self.shot_browser.shot.setEnabled(False)
        
        self.status_bar.setText(root_dir)
        self.shot_browser.sequence.add_dir_items(root_dir, start_with=folder_filter['sequence']['prefix'])

        # -- Filter item for searching --
        if self.search_found:
            self.filter_listwidget(self.shot_browser.sequence.listwidget,
                                    self.search_found)

    def sequence_clicked(self, item):
        """Add item in Shot widget

        Args:
            item (QListWidgetItem): Sequence item
        """
        # -- clear ---
        self.shot_browser.shot.clear()
        # ------------
        if not item or not self.shot_browser.episode.listwidget.currentItem():
            return

        folder_filter = self.scene_browser_conf['folder_filter']
        root_dir = item.data(QtCore.Qt.UserRole)
        self.status_bar.setText(root_dir)

        if self.ent_folder_name == 'scene':
            self.shot_browser.shot.add_dir_items(root_dir, start_with=folder_filter['shot']['prefix'])
        
        elif self.ent_folder_name == 'sequence':
            work_dir = self.file_dir_fmt['work_dir'].format(asset_dir=root_dir,
                                        department=self.dept_wd.combo_box.currentText())
            if os.path.exists(work_dir):
                self.work_dir_found.emit(work_dir)

        # -- Filter item for searching --
        if self.search_found:
            self.filter_listwidget(self.shot_browser.shot.listwidget,
                                    self.search_found)

    def shot_clicked(self, item):
        """Set root directorr to filesystem browser

        Args:
            item (QListWidgetItem): Shot item
        """
        if item:
            root_dir = item.data(QtCore.Qt.UserRole)
            self.status_bar.setText(root_dir)
            self.root_dir_changed.emit(root_dir)
        else:
            self.root_dir_changed.emit('')

    def central_splitter(self):
        """QSpliter Widget has already set layout

        Returns:
            QSpliter: QSpliter
        """
        widget = QtWidgets.QSplitter()
        widget.setContentsMargins(0, 0, 0, 0)
        widget.setChildrenCollapsible(False)
        widget.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
                                QtWidgets.QSizePolicy.Expanding)
        QtWidgets.QHBoxLayout(widget)
        widget.layout().setContentsMargins(0, 0, 0, 0)
        return widget
           
    def asset_browser_widget(self):
        """A Widget contain all widget and set layout for asset

        Returns:
            QWidget: Asset browser widget
        """
        widget = QtWidgets.QWidget()

        widget.entity_listwidgets = []
        widget.type = AssetType()
        widget.entity_listwidgets.append(widget.type.listwidget)
        widget.subtype = SubType()
        widget.entity_listwidgets.append(widget.subtype.listwidget)
        widget.asset = Asset()
        widget.entity_listwidgets.append(widget.asset.listwidget)

        widget.task = Task()

        widget.setContentsMargins(0, 0, 0, 0)
        QtWidgets.QVBoxLayout(widget)
        
        widget.layout().setContentsMargins(0, 0, 0, 0)
        widget.layout().setSpacing(0)

        h_layout = QtWidgets.QHBoxLayout()
        h_layout.setSpacing(0)
        h_layout.setContentsMargins(0, 0, 0, 0)
        widget.layout().addLayout(h_layout)
        h_layout.addWidget(widget.type)
        h_layout.setSpacing(5)
        h_layout.addWidget(widget.asset)

        h_layout = QtWidgets.QHBoxLayout()
        h_layout.setSpacing(0)
        h_layout.setContentsMargins(0, 10, 0, 0)
        widget.layout().addLayout(h_layout)
        h_layout.addWidget(widget.subtype)
        h_layout.setSpacing(5)
        h_layout.addWidget(widget.task)

        return widget

    def shot_browser_widget(self):
        """A Widget contain all widget and set layout for shot

        Returns:
            QWidget: Shot browser widget
        """
        widget = QtWidgets.QWidget()

        widget.entity_listwidgets = []
        widget.episode = Episode()
        widget.entity_listwidgets.append(widget.episode.listwidget)
        widget.shot = Shot(self)
        widget.entity_listwidgets.append(widget.shot.listwidget)
        widget.sequence = Sequence()
        widget.entity_listwidgets.append(widget.sequence.listwidget)

        widget.task = Task()

        widget.setContentsMargins(0, 0, 0, 0)
        QtWidgets.QVBoxLayout(widget)
        widget.layout().setAlignment(QtCore.Qt.AlignRight)
        widget.layout().setContentsMargins(0, 0, 0, 0)
        widget.layout().setSpacing(0)

        h_layout = QtWidgets.QHBoxLayout()
        h_layout.setSpacing(0)
        h_layout.setContentsMargins(0, 0, 0, 0)
        widget.layout().addLayout(h_layout)
        
        h_layout.addWidget(widget.episode)
        h_layout.setSpacing(5)
        h_layout.addWidget(widget.shot)

        h_layout = QtWidgets.QHBoxLayout()
        h_layout.setAlignment(QtCore.Qt.AlignRight)
        h_layout.setSpacing(0)
        h_layout.setContentsMargins(0, 10, 0, 0)
        widget.layout().addLayout(h_layout)
      
        h_layout.addWidget(widget.sequence)
        h_layout.setSpacing(5)
        h_layout.addWidget(widget.task)

        return widget


if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    widget = EntityType()
    widget.show()
    sys.exit(app.exec_())
