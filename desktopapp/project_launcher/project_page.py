#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, April 2021
##
################################################################################
##
##  Standard Library
##
import os
import importlib
import sys

# GUI
try:
    from PySide import QtGui, QtCore
except ImportError:
    from PySide2 import QtWidgets, QtCore, QtGui

################################################################################
##
##  Snow Library
##
from snow.common.lib import config as snow_config

################################################################################
##
##  Relative Modules
##
import setup_helper
import browser_mode
import gui.project_page_widget as ppw 

################################################################################
##
##  Class Definitions
##	
class ProjectPage(ppw.ProjectPageWidget):
    """Navigate browsers and tools each project
    """
    def __init__(self, parent=None):
        """Intialize ProjectPage

        Args:
            parent (QWidget, optional): parent widget. Defaults to None.
        """
        super().__init__(parent)
        self.setObjectName('ProjectPage')
        self.page = self.parent.stackedlayout
        self.project = ''
        self.__config = ''
        self.init_user_setup()

    def setProject(self, project):
        """Setup ProjectPage

        Args:
            project (str): project name
        """
        self.project = project
        os.environ['PROJ_CONFIG_DIR'] = '{}/{}'.format(
                                            snow_config.CONFIG_DIR, project)
        header_page = self.projectpage_header
        header_page.project_label.setText(project)
        
        ## Connet Signal
        header_page.goto_home_button.clicked.connect(self.exit_page)
        self.mode_cbb.currentIndexChanged.connect(self.set_mode)
        self.tool_picker.itemClicked.connect(self.open_tool)
    
        # -- Load browser config -- 
        for name, module in browser_mode.mode.items():
            # -- Contain module in item data --
            self.mode_cbb.addItem(name, module)

        self.__config = setup_helper.get_project_integation_tools(self.project)['tools']
        self.create_tool_icon(self.__config)
        # -- Arrage tool index from user step --
        init_tool_picker = self.user_init_data.get('tool_picker')
        if init_tool_picker:
            for index, tool_path in enumerate(init_tool_picker):
                for row in range(self.tool_picker.count()):
                    item = self.tool_picker.item(row)
                    if item.data(QtCore.Qt.UserRole) == tool_path:
                        self.tool_picker.takeItem(row)
                        self.tool_picker.insertItem(index, item)
            
    def init_user_setup(self):
        """Read user init file and set data to widget
        """
        self.user_init_data = setup_helper.read_user_init(self.objectName())

        if not self.user_init_data:
            self.user_init_data = dict()
            return
    
    def hideEvent(self, event):

        data = setup_helper.read_user_init()

        self.user_init_data['tool_picker'] = []
        for row in range(self.tool_picker.count()):
            item = self.tool_picker.item(row)
            self.user_init_data['tool_picker'].append(item.data(QtCore.Qt.UserRole))

        data[self.objectName()] = self.user_init_data or []
        setup_helper.save_user_init(data)
        event.accept()
        
    def exit_page(self):
        """Remove ProjectPage and go back to HomePage
        """
        self.page.takeAt(self.page.currentIndex())
        self.page.setCurrentIndex(0)
       
    def set_mode(self, index):
        """Delete current browser mode and set new browser instead

        Args:
            index (int): mode index
        """
        app_widget = self.content.findChild(QtWidgets.QWidget)
        if app_widget:
            app_widget.deleteLater()

        utility_mod = self.mode_cbb.currentData()
        app_widget = utility_mod.main(self, self.project)
        self.content.layout().addWidget(app_widget)
    
    def open_tool(self, item):
        """Open tool from ToolPickerList

        Args:
            item (QListWidgetItem): ToolPickListItem
        """
        tool_path = item.data(QtCore.Qt.UserRole)
        if tool_path['type'] == 'module':
            package = importlib.import_module(tool_path['package'])
            if package.__spec__.submodule_search_locations[0] not in sys.path:
                sys.path.append(package.__spec__.submodule_search_locations[0])
            tool_module = importlib.import_module(tool_path['module'])
            QtWidgets.QApplication.processEvents()
            tool_module.main(self)
        
        elif tool_path['type'] == 'script':
            print(tool_path['command'])
            exec(tool_path['command'])
        
        elif tool_path['type'] == 'standalone':
            # TODO: execute standalone tool type
            pass
        