#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, Feb 2022
##
################################################################################
##
##  Standard Library
##
import sys
import os
# GUI
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtWidgets, QtCore, QtGui


from snow.shotgrid.widget import sg_user_widget

################################################################################
##
##  Relative Modules
##
import setup_helper

################################################################################
##
##  Global Variables
##
from gui.icon_path import Studio_Icon

################################################################################
##
##  Class Definitions
##
class LoginPage(QtWidgets.QWidget):
    """For login
    """
    access = QtCore.Signal(str)
    def __init__(self, parent=None):
        super(LoginPage, self).__init__(parent)
        # -- Create widget
        self.assign_cbb = sg_user_widget.Combo()
        self.assign_cbb.insertItem(0, '--select--')
        self.login_pb = QtWidgets.QPushButton('Login')

        self.login_pb.clicked.connect(self.login)
        self.set_layout()

    def set_layout(self):
        """Initial Layout
        """
        QtWidgets.QVBoxLayout(self)
        self.layout().setAlignment(QtCore.Qt.AlignCenter)
        self.layout().addStretch()

        label = QtWidgets.QLabel()
        pixmap = QtGui.QPixmap(Studio_Icon)
        label.setPixmap(pixmap)
        self.layout().addWidget(label,0, QtCore.Qt.AlignCenter)
        self.layout().addSpacing(10)

        label = QtWidgets.QLabel('Welcome!')
        label.setStyleSheet('font: 30px bold;')
        self.layout().addWidget(label,0, QtCore.Qt.AlignCenter)
        self.layout().addSpacing(10)

        self.assign_cbb.setFixedWidth(200)
        self.layout().addWidget(self.assign_cbb,0, QtCore.Qt.AlignCenter)
        self.layout().addWidget(self.login_pb,0, QtCore.Qt.AlignCenter)
        self.login_pb.setFixedWidth(200)
        self.layout().addWidget(self.login_pb,0, QtCore.Qt.AlignCenter)

        self.layout().addStretch()

    def login(self):
        """Write username into user setup file
        """
        if self.assign_cbb.currentIndex():
            username = self.assign_cbb.currentText()
            setup_helper.save_user_login(username)
            # os.environ['USERNAME'] = username
            print('USER LOGIN:', username)
            self.access.emit(username)


################################################################################
##
##  Test Function
##	
def main():
    app = QtWidgets.QApplication( sys.argv )
    window = LoginPage()
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()