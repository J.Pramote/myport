from . import scene_browser
from . import media_browser


# -- Register Browser Here --
# mode = {Display Name: module}
mode = {
    'Scene Browser': scene_browser,
    'Media Browser': media_browser,
}