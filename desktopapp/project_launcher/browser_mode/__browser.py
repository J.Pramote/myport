#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, Oct 2022
##
################################################################################
##
##  Standard Library
##
import os
import re
from glob import glob

# GUI
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtWidgets, QtCore, QtGui

################################################################################
##
##  SNOW Modules
##
from snow.common.lib import code_logging, os_file
from snow.common.lib.widget import browser_widget, message_box
from snow.shotgrid.lib import wrap as sg_wrap

################################################################################ 
##
##  Relative Modules
##
from snow.common.tools.project_launcher import setup_helper
from snow.common.tools.project_launcher.gui import gui_utils

################################################################################
##
##  Global Varialbles
##
LOG = code_logging.create_log('project_launcher.browser')
Stylesheet = os.path.dirname(__file__)+"/stylesheet.css"


################################################################################
##
##  Class Definitions
##
class MainBrowser(browser_widget.BrowserWidget):

    def __init__(self, parent=None, project='', repo_type=''):
        """Initialize SceneBrowser

        Args:
            parent (QWidget, optional): parent widget. Defaults to None.
            project (str, optional): project name. Defaults to ''.
        """
        # -- Create widget before call parent object --
        self.only_me_ckb = QtWidgets.QCheckBox('Only my task')
        self.open_pb = QtWidgets.QPushButton('Open/Check Out')
        self.file_system_widgets = []
        
        # -- Initialize parent object --
        super().__init__(parent, project=project, repo_type=repo_type)
        self.setStyleSheet(open(Stylesheet ,'r').read())

        # Connect signal
        self.__connect_signal()

        # Resize window and move to center of screen
        window = self.nativeParentWidget() or self
        window.resize(1250, 800)

        # 20230421,ob,modify,transfer window.move() to here - fix show window on middle of 2 monitors
        QtCore.QTimer.singleShot(0, lambda: gui_utils.center_window(window))

    def __connect_signal(self):
        """Connect widget signal
        """
        self.only_me_ckb.stateChanged.connect(self.only_my_task)
  
    @QtCore.Slot()
    def filter_entity(self):
        """Filter entity name in a project
        """
        if self.only_me_ckb.isChecked():
            current_word = self.get_filter_search()
            if current_word:
                words_in_mytask = []
                my_task_search = [os.path.basename(s.rstrip('/')) for s in self.search_found_mytask]
                words = [w.strip() for w in current_word.split(',')]
                words_in_mytask = [w for w in words if [m for m in my_task_search if w in m]]
                if words_in_mytask:
                    result = self.search_entity_folder(words=words_in_mytask,
                                            path_found=self.search_found_mytask)
                else:
                    result = self.search_entity_folder()
            else:
                self.reset_listwidget()
                result = self.search_entity_folder(path_found=self.search_found_mytask)
            
            if current_word and not result:
                message_box.MessageBox(self).warning('Not found')
        else:
            result = super().filter_entity()
            if self.search_le.text() and not result:
                message_box.MessageBox(self).warning('Not found')

        self.clear_all_filesystem()

    def clear_all_filesystem(self):
        """Clear all filesytem items
        """
        for tree in self.file_system_widgets:
            tree.clear()
        
    def only_my_task(self):
        """Show entities which user has assinged to a task
        """
        self.search_le.clear()
        self.clear_all_filesystem()
        if not self.only_me_ckb.isChecked():
            self.reset_listwidget()
            return
        self.set_status_bar('Searching..')
        sg_project = sg_wrap.project.Project(self.project)
        result = sg_project.get_tasks_in_project_by_assignee(
                    assignee=setup_helper.get_user_login(),
                    entity_type=self.ent_type,
                    step=self.department)

        entity_name_list = sorted(
                        list(
                            set([task['entity']['name'] for task in result])))

        self.search_found_mytask = []
        if self.ent_folder_name in ['asset', 'design']:
            for ent in entity_name_list:
                found_glob = glob('{}/*/*/{}/'.format(self.ent_root_dir, ent))
                if found_glob:
                    self.search_found_mytask += [f.replace('\\','/') for f in found_glob]

        else:
            for ent in entity_name_list:
                found = re.findall(r'^\w+_(\w+)_(\w+$)', ent)
                if found and len(found[0]) == 2:
                    ep, shot = found[0]
                    found_glob = glob('{}/{}/*/{}/'.format(self.ent_root_dir, ep, shot))
                    if found_glob:
                        self.search_found_mytask += [f.replace('\\','/') for f in found_glob]

        if not self.search_found_mytask:
            message_box.MessageBox(self).warning(
                            'Not found your task in department: "{}"\n'
                            'Please go to ShotGrid and check in assignee field'.format(self.department))

        self.search_entity_folder(path_found=self.search_found_mytask)
        self.status_bar.clear()

    @QtCore.Slot()
    def episode_clicked(self, item):
        # -- clear ---
        self.clear_all_filesystem()
        # ------------
        return super().episode_clicked(item)
    
    @QtCore.Slot()
    def sequence_clicked(self, item):
        # -- clear ---
        self.clear_all_filesystem()
        # ------------
        return super().sequence_clicked(item)
    
    @QtCore.Slot()
    def shot_clicked(self, item):
        # -- clear ---
        self.clear_all_filesystem()
        # ------------
        return super().shot_clicked(item)

    @QtCore.Slot()
    def asset_type_clicked(self, item):
        # -- clear ---
        self.clear_all_filesystem()
        # ------------
        return super().asset_type_clicked(item)

    @QtCore.Slot()
    def asset_subtype_clicked(self, item):
        # -- clear ---
        self.clear_all_filesystem()
        # ------------
        return super().asset_subtype_clicked(item)
    
    @QtCore.Slot()
    def asset_clicked(self, item):
        # -- clear ---
        self.clear_all_filesystem()
        # ------------
        return super().asset_clicked(item)

    def open_tab_folder(self, index):
        """Double click on Tab name to open the folder

        Args:
            index (int): an index of tab
        """
        file_widget = self.sender().widget(index)
        if not isinstance(file_widget, browser_widget.FileSystemWidget):
            file_widget = file_widget.findChildren(browser_widget.FileSystemWidget)
            file_widget = file_widget[0]
        if not file_widget or not file_widget.model():
            return

        if (file_widget.model().sourceModel().rootPath() != '.' and
            os.path.exists(file_widget.model().sourceModel().rootPath())):
            os_file.open_explorer(file_widget.model().sourceModel().rootPath(), select=False)
        else:
            print('Not found {}:'.format(file_widget.model().sourceModel().rootPath()))

    def action_clipboard(self, text, name='Copy Path', 
                        short_key=QtGui.QKeySequence.Copy):
        """Action to copy text to clipboard

        Args:
            text (str): text to copy
            name (str, optional): menu label. Defaults to 'Copy Path'.
            short_key (QtGui.QKeySequence, optional): short key.. Defaults to QtGui.QKeySequence.Copy.

        Returns:
            QAction: It use to parent in QMenu
        """
        action = QtWidgets.QAction(name)
        action.triggered.connect(lambda :
                    QtWidgets.QApplication.clipboard().setText(text))
        action.setShortcutVisibleInContextMenu(True)
        action.setShortcuts(short_key)
        return action

    def action_open_explorer(self, path, name='Reveal in File Explorer', 
                        short_key=None):
        """Action menu to open File Explorer

        Args:
            path (str): path to open
            name (str, optional): menu label. Defaults to 'Reveal in File Explorer'.
            short_key (QtGui.QKeySequence, optional): short key. Defaults to None.

        Returns:
            QAction: It use to parent in QMenu
        """
        select = False
        if os.path.isfile(path):
            select = True

        action = QtWidgets.QAction(name)
        action.triggered.connect(lambda :
                    os_file.open_explorer(path, select=select))
        if short_key:
            action.setShortcutVisibleInContextMenu(True)
            action.setShortcuts(short_key)

        return action
