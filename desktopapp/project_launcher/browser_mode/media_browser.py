#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, Oct 2022
##
################################################################################
##
##  Standard Library
##
import os
import subprocess

# GUI
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtWidgets, QtCore, QtGui

################################################################################
##
##  SNOW Modules
##
from snow.common.lib import code_logging
from snow.common.lib.widget import browser_widget, website, message_box

################################################################################ 
##
##  Relative Modules
##
from .__browser import MainBrowser

################################################################################
##
##  Global Varialbles
##
LOG = code_logging.create_log('project_launcher.media_browser')
Stylesheet = os.path.dirname(__file__)+"/stylesheet.css"


################################################################################
##
##  Class Definitions
##
class MediaBrowser(MainBrowser):

    def __init__(self, parent=None, project=''):
        """Initialize SceneBrowser

        Args:
            parent (QWidget, optional): parent widget. Defaults to None.
            project (str, optional): project name. Defaults to ''.
        """
        # -- Initialize parent object --
        super().__init__(parent, project=project, repo_type='render')
        self.setStyleSheet(open(Stylesheet ,'r').read())
        self.file_browser = self.file_browser_widget()

        # Connect signal
        self.connect_signal()

        # initialize
        self.set_layout()
    
    def connect_signal(self):
        """Connect widget signal
        """
        self.root_dir_changed.connect(self.emit_directory)
        self.file_browser.tab_widget.tabBarDoubleClicked.connect(self.open_tab_folder)
        self.dept_wd.combo_box.currentIndexChanged.connect(self.select_recent_entity())
        self.open_pb.clicked.connect(self.open_file)

        for file_tree in self.file_system_widgets:
            file_tree.doubleClicked.connect(self.open_file)
            file_tree.customContextMenuRequested.connect(self.menu_on_item)

    def init_header_layout(self):
        """Override self method for adding other widgets

        Returns:
            QHBoxLayout: layout
        """
        layout = QtWidgets.QHBoxLayout()
        layout.addWidget(self.ent_type_wd)
        layout.addWidget(self.dept_wd)
        layout.addStretch()
        layout.addWidget(self.search_le)
        layout.addWidget(self.only_me_ckb)
        layout.addWidget(
            website.help_button(
                'https://igloostudioth.atlassian.net/wiki/spaces/SCD/pages/387940473/Browser'))

        return layout

    def set_layout(self):
        """Initialize Layout
        """
        all_browser_wd = QtWidgets.QWidget()
        QtWidgets.QVBoxLayout(all_browser_wd)
        all_browser_wd.layout().setContentsMargins(0, 0, 0, 0)
        all_browser_wd.layout().addWidget(self.file_browser)

        h_layout = QtWidgets.QHBoxLayout()
        h_layout.addWidget(self.open_pb)
        all_browser_wd.layout().addLayout(h_layout)
        self.central_widget.addWidget(all_browser_wd)
        self.central_widget.setStretchFactor(2, 4)

    def file_browser_widget(self):
        """Create widget for contain filesystem widget

        Returns:
            QWidget: file browser widget
        """
        widget = QtWidgets.QWidget()
        widget.root_dir = ''
        widget.version_tree = browser_widget.FileSystemWidget(widget)
        self.file_system_widgets.append(widget.version_tree)
        widget.version_tree.clear()

        widget.hero_tree = browser_widget.FileSystemWidget(widget)
        self.file_system_widgets.append(widget.hero_tree)
        widget.hero_tree.clear()

        widget.output_tree = browser_widget.FileSystemWidget(widget)
        self.file_system_widgets.append(widget.output_tree)
        widget.output_tree.clear()

        QtWidgets.QVBoxLayout(widget)
        widget.layout().setContentsMargins(0, 0, 0, 0)
        widget.tab_widget = QtWidgets.QTabWidget()
        widget.tab_widget.addTab(widget.version_tree, "Version")
        widget.tab_widget.addTab(widget.hero_tree, "Hero")
        widget.tab_widget.addTab(widget.output_tree, "Output")
        widget.layout().addWidget(widget.tab_widget)
        return widget

    def emit_directory(self, root_dir):
        """Set child directory to browser widget

        Args:
            root_dir (str): entity directory
        """
        if not root_dir:
            return

        self.file_browser.root_dir = root_dir
        asset_dir = os.path.dirname(root_dir)
        dept = self.dept_wd.combo_box.currentText()
        dept_dir = asset_dir + '/' + dept
        if not dept and not asset_dir:
            return

        work_dir = self.file_dir_fmt['work_dir'].format(asset_dir=asset_dir,
                                    department=dept)
        hero_dir = self.file_dir_fmt['hero_dir'].format(asset_dir=asset_dir,
                                    department=dept)
        output_dir = self.file_dir_fmt['media_dir'].format(asset_dir=asset_dir,
                                    department=dept)

        if not os.path.exists(dept_dir):
            self.file_browser.version_tree.set_root_path(None)
            self.file_browser.hero_tree.set_root_path(None)
            self.file_browser.output_tree.set_root_path(None)
        else:
            # -- For work directory --
            self.file_browser.version_tree.set_root_path(work_dir)
            # -- For hero directory --
            self.file_browser.hero_tree.set_root_path(hero_dir)
            # -- For output directory --
            self.file_browser.output_tree.set_root_path(output_dir)

    def open_file(self):
        '''Open selected file
        '''
        file_path = browser_widget.FileSystemWidget.current_file
        if not os.path.exists(file_path):
            message_box.MessageBox(self).warning(text="Must select a file")
            return
        if os.path.isfile(file_path):
            # -- open by default without project environment --
            cmd = 'start "" "{}"'.format(file_path)
            LOG.info(cmd)
            subprocess.call(cmd, shell=True,
                    creationflags=subprocess.CREATE_NEW_CONSOLE)

    def menu_on_item(self, pos):
        """Popup menu when right-click on a file item

        Args:
            pos (QPos): Position has activated
        """
        item = self.sender().indexAt(pos)
        if not item:
            return
        file_path = self.sender().current_file
        menu = QtWidgets.QMenu(self.sender())

        explorer = self.action_open_explorer(file_path)
        menu.addAction(explorer)

        copy_path = self.action_clipboard(file_path)
        menu.addAction(copy_path)

        menu.exec_(self.sender().mapToGlobal(pos))

def main(*argv):
    """Open this browser

    Returns:
        QWidget: widget
    """
    widget = MediaBrowser(*argv)
    return widget
