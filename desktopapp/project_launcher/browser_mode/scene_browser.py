#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, June 2022
##
################################################################################
##
##  Standard Library
##
import os
import shutil
import re
import subprocess
from glob import glob

# GUI
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtWidgets, QtCore, QtGui

################################################################################
##
##  SNOW Modules
##
from snow.common.lib import os_file, path_elem
from snow.common.lib.widget import message_box, file_dialog_widget
from snow.common.lib.widget import browser_widget, website
from snow.common.lib import code_logging, app_env
from snow.shotgrid.lib import wrap as sg_wrap
from snow.common.lib import config as snow_config

################################################################################
##
##  Relative Modules
##
import setup_helper

################################################################################
##
##  Global Varialbles
##
LOG = code_logging.create_log('project_launcher.browser_mode.scene_browser')
Stylesheet = os.path.dirname(__file__)+"/stylesheet.css"

################################################################################
##
##  Class Definitions
##
class SceneBrowser(browser_widget.BrowserWidget):

    def __init__(self, parent=None, project=''):
        """Initialize SceneBrowser

        Args:
            parent (QWidget, optional): parent widget. Defaults to None.
            project (str, optional): project name. Defaults to ''.
        """
        # -- Create widget before call parent object --
        self.only_me_ckb = QtWidgets.QCheckBox('Only my task')
        self.new_file_pb = QtWidgets.QPushButton('Add New')
        self.open_pb = QtWidgets.QPushButton('Open/Check Out')
        self.task_wd = browser_widget.TaskBox()

        self.file_browser = self.file_browser_widget()
        self.hero_browser = self.hero_browser_widget()

        self.current_file_tree = None
        # -- Initialize parent object --
        super().__init__(parent, project=project)
        self.setStyleSheet(open(Stylesheet ,'r').read())

        # -- Read tool configuration file --
        self.__config_file = snow_config.yaml.read(
            snow_config.get_tool_config(__file__,
                                    project=project,
                                    filename='browser_mode/scene_browser.yaml'))

        # Connect signal
        self.connect_signal()

        # initialize
        self.set_layout()
        self.set_tab_control(self.file_browser.tab_widget.currentIndex())
        self.emit_directory(self.current_root_dir)
        self.department_changed()

    def connect_signal(self):
        """Connect widget signal
        """
        self.root_dir_changed.connect(self.emit_directory)
        self.dept_wd.combo_box.currentIndexChanged.connect(self.department_changed)
        self.task_wd.combo_box.currentIndexChanged.connect(self.set_filter_filename)

        self.asset_browser.asset.listwidget.customContextMenuRequested.connect(self.entity_menu)
        self.shot_browser.shot.listwidget.customContextMenuRequested.connect(self.entity_menu)
        self.shot_browser.sequence.listwidget.customContextMenuRequested.connect(self.entity_menu)

        self.ent_type_wd.combo_box.currentIndexChanged.connect(self.entity_changed)

        self.new_file_pb.clicked.connect(self.new_work_file)

        self.open_pb.clicked.connect(self.open_with_default_env)
        self.file_browser.tab_widget.currentChanged.connect(self.set_tab_control)
        self.file_browser.work_tree.doubleClicked.connect(self.open_with_default_env)
        self.file_browser.work_tree.customContextMenuRequested.connect(self.work_file_menu)
        self.file_browser.hero_tree.doubleClicked.connect(self.open_with_default_env)
        self.file_browser.output_tree.doubleClicked.connect(self.open_with_default_env)
        self.hero_browser.hero_tree.doubleClicked.connect(self.open_with_default_env)
        self.only_me_ckb.stateChanged.connect(self.only_my_task)

        self.asset_step_btg.buttonClicked.connect(self.step_button_changed)

    @property
    def department(self):
        """Override self property for hero folder in same department level

        Returns:
            str: department name or other folder name
        """
        if self.asset_step_btg.checkedButton().text() == 'hero':
            return 'hero'
        else:
            return super().department
    
    def step_button_changed(self, button):
        """Setting widget for seeing with mode

        Args:
            button (QAbstractButton): radion button in self.asset_step_btg
        """
        if button.text() == 'hero':
            self.dept_wd.setEnabled(False)
            self.hero_browser.setHidden(False)
            self.file_browser.setHidden(True)
        else:
            self.dept_wd.setEnabled(True)
            self.hero_browser.setHidden(True)
            self.file_browser.setHidden(False)
        
        self.initialize_content()
    
    def init_header_layout(self):
        """Override self method for adding other widgets

        Returns:
            QHBoxLayout: layout
        """
        asset_step_wd = QtWidgets.QWidget(self)
        asset_step_wd.setLayout(QtWidgets.QHBoxLayout())
        self.asset_step_btg = QtWidgets.QButtonGroup(asset_step_wd)
        radio1 = QtWidgets.QRadioButton('hero')
        radio2 = QtWidgets.QRadioButton('department')
        radio2.setChecked(True)
        self.dept_wd.label.clear()
        asset_step_wd.layout().addWidget(radio1)
        asset_step_wd.layout().addWidget(radio2)
        asset_step_wd.layout().addWidget(self.dept_wd)
        self.asset_step_btg.addButton(radio1)
        self.asset_step_btg.addButton(radio2)

        layout = QtWidgets.QHBoxLayout()
        layout.addWidget(self.ent_type_wd)
        layout.addWidget(asset_step_wd)
        layout.addStretch()
        layout.addWidget(self.search_le)
        layout.addWidget(self.only_me_ckb)
        layout.addWidget(
            website.help_button(
                'https://'))

        return layout

    def set_layout(self):
        """Initialize Layout
        """
        all_browser_wd = QtWidgets.QWidget()
        QtWidgets.QVBoxLayout(all_browser_wd)
        all_browser_wd.layout().addWidget(self.file_browser)
        all_browser_wd.layout().addWidget(self.hero_browser)
        self.hero_browser.setHidden(True)

        h_layout = QtWidgets.QHBoxLayout()
        h_layout.addWidget(self.new_file_pb)
        h_layout.addWidget(self.open_pb)
        all_browser_wd.layout().addLayout(h_layout)
        self.central_widget.layout().addWidget(all_browser_wd)
        self.central_widget.setStretchFactor(2, 3)

    def entity_menu(self, pos):
        """Popup menu when right-click on a entity item

        Args:
            pos (QPos): position has activated
        """
        listwidget = self.sender()
        item = listwidget.itemAt(pos)
        if not item:
            return

        widget = listwidget.parent()
        menu = QtWidgets.QMenu()
        if hasattr(widget, 'current_entity_item'):
            action = menu.addAction('View in ShotGrid', self.open_notes)
            action.setData(widget.current_entity_item())
            menu.exec_(widget.listwidget.mapToGlobal(pos))

    def open_notes(self):
        '''Open page on web browser
        '''
        data = self.sender().data()
        page_url = sg_wrap.project.Project(self.project).get_entity_page(data['type'], data['id'])
        QtWidgets.QApplication.processEvents()
        result = subprocess.Popen(['start', page_url], shell=True,
                    creationflags=subprocess.CREATE_NEW_CONSOLE,
                    stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8' )
        stdout, stderr = result.communicate()
        if result.returncode:
            LOG.error(stderr)
    
    def entity_changed(self):
        """Uncheck Only Me if entity type has changed
        """
        self.only_me_ckb.setCheckState(QtCore.Qt.Unchecked)

    def set_tab_control(self, index):
        """Setting when file browser tab changed

        Args:
            index (int): Tab index
        """
        self.current_file_tree = self.file_browser.tab_widget.currentWidget()
        tab_name = self.file_browser.tab_widget.tabText(index)
        if tab_name == 'Version':
            self.task_wd.setHidden(False)
            self.new_file_pb.setHidden(False)
            self.open_pb.setHidden(False)
        else:
            self.task_wd.setHidden(True)
            self.new_file_pb.setHidden(True)

    def new_work_file(self):
        '''Create new file
        '''
        task_list = self.task_wd.get_task_lists()
        # -- Open dialog widget --
        dialog = file_dialog_widget.NewFileWidget(
                            parent=self,
                            task_list=task_list,
                            department=self.department)
        # -- set current task to selected item --
        current_task = self.task_wd.combo_box.currentText()
        if current_task:
            seleted_task = dialog.task_cbb.findText(
                                current_task,
                                QtCore.Qt.MatchExactly)
            if seleted_task:
                dialog.task_cbb.setCurrentIndex(seleted_task)
        else:
            dialog.task_cbb.setCurrentIndex(-1)

        dialog.exec()
        if not dialog.result:
            return
        work_dir = self.file_browser.work_tree.root_dir
        path = path_elem.PathElem(work_dir)

        # -- Get a file name from the result --
        task, ext, template = dialog.result
        prefix = path.get_prefix_filename(task)
        if template:
            ext = os.path.splitext(template)[-1]
        if ext:
            ext = ext[1:]

        new_file = os_file.increase_version(
                        dir_path=work_dir,
                        prefix=prefix,
                        ext=ext)
        # -- Create new file ---
        if not template:
            os_file.NewWorkFile(new_file)
            LOG.info('create -- {}'.format(new_file))
        else:
            temp_file = '{}/{}'.format(self.temp_dir, template)
            LOG.info('create -- {}'.format(new_file))
            shutil.copyfile(temp_file, new_file)

    def open_with_default_env(self):
        '''Open with a default application and set environments
        '''
        file_path = browser_widget.FileSystemWidget.current_file
        LOG.info('open file -- %s', file_path)
        if os.path.exists(file_path):
            selected_ext = os.path.splitext(file_path)[-1]
            if selected_ext:
                app_name = self.find_open_which_app(selected_ext)

                if app_name:
                    primary_app = app_env.get_primary_app_list(
                                                self.project)
                    primary_app = [app for app in primary_app 
                                    if re.search('^' + app_name, app, re.IGNORECASE)]
                    if primary_app:
                        self.open_with_env(primary_app[0])
                        return

                # -- open by default without project environment --
                cmd = 'start "" "{}"'.format(file_path)
                LOG.info(cmd)
                subprocess.call(cmd, shell=True,
                        creationflags=subprocess.CREATE_NEW_CONSOLE)
            else:
                message_box.MessageBox(self).warning(text="This is not a file")
        else:
            message_box.MessageBox(self).warning(text="Must select a file")
            
    def emit_directory(self, root_dir):
        """Set child directory to browser widget

        Args:
            root_dir (str): entity directory
        """
        if os.path.basename(root_dir) == 'hero':
            if os.path.exists(root_dir):
                self.hero_browser.hero_tree.set_root_path(root_dir)
        else:
            self.file_browser.root_dir = root_dir
            asset_dir = os.path.dirname(root_dir)

            work_dir = self.file_dir_fmt['work_dir'].format(asset_dir=asset_dir,
                                        department=self.dept_wd.combo_box.currentText())
            hero_dir = self.file_dir_fmt['hero_dir'].format(asset_dir=asset_dir,
                                        department=self.dept_wd.combo_box.currentText())
            output_dir = self.file_dir_fmt['media_dir'].format(asset_dir=asset_dir,
                                        department=self.dept_wd.combo_box.currentText())

            if not os.path.exists(work_dir):
                os_file.create_folder(work_dir)
            if not os.path.exists(hero_dir):
                os_file.create_folder(hero_dir)
            if not os.path.exists(output_dir):
                os_file.create_folder(output_dir)

            # -- For work directory -- 
            self.file_browser.work_tree.set_root_path(work_dir)
            self.set_tasks(look_up_folder=work_dir)
            # -- For hero directory -- 
            self.file_browser.hero_tree.set_root_path(hero_dir)
            # -- For output directory -- 
            self.file_browser.output_tree.set_root_path(output_dir)

    def only_my_task(self):
        """Show entities which user has assinged to a task
        """
        self.search_le.clear()
        self.clear_all_filesystem()
        if not self.only_me_ckb.isChecked():
            self.reset_listwidget()
            return
        self.set_status_bar('Searching..')
        sg_project = sg_wrap.project.Project(self.project)
        result = sg_project.get_tasks_in_project_by_assignee(
                    assignee=setup_helper.get_user_login(),
                    entity_type=self.ent_type,
                    step=self.department)

        entity_name_list = sorted(
                        list(
                            set([task['entity']['name'] for task in result])))

        self.search_found_mytask = []
        if self.ent_folder_name in ['asset', 'design']:
            for ent in entity_name_list:
                found_glob = glob('{}/*/*/{}/'.format(self.ent_root_dir, ent))
                if found_glob:
                    self.search_found_mytask += [f.replace('\\','/') for f in found_glob]

        else:
            for ent in entity_name_list:
                found = re.findall(r'^\w+_(\w+)_(\w+$)', ent)
                if found and len(found[0]) == 2:
                    ep, shot = found[0]
                    found_glob = glob('{}/{}/*/{}/'.format(self.ent_root_dir, ep, shot))
                    if found_glob:
                        self.search_found_mytask += [f.replace('\\','/') for f in found_glob]

        if not self.search_found_mytask:
            message_box.MessageBox(self).warning(
                            'Not found your task in department: "{}"\n'
                            'Please go to ShotGrid and check in assignee field'.format(self.department))

        self.search_entity_folder(path_found=self.search_found_mytask)
        self.status_bar.clear()

    @QtCore.Slot()
    def filter_entity(self):
        """Filter entity name in a project
        """
        if self.only_me_ckb.isChecked():
            current_word = self.get_filter_search()
            if current_word:
                words_in_mytask = []
                my_task_search = [os.path.basename(s.rstrip('/')) for s in self.search_found_mytask]
                words = [w.strip() for w in current_word.split(',')]
                words_in_mytask = [w for w in words if [m for m in my_task_search if w in m]]
                if words_in_mytask:
                    result = self.search_entity_folder(words=words_in_mytask,
                                            path_found=self.search_found_mytask)
                else:
                    result = self.search_entity_folder()
            else:
                self.reset_listwidget()
                result = self.search_entity_folder(path_found=self.search_found_mytask)
            
            if current_word and not result:
                message_box.MessageBox(self).warning('Not found')
        else:
            result = super().filter_entity()
            if self.search_le.text() and not result:
                message_box.MessageBox(self).warning('Not found')

        self.clear_all_filesystem()

    def set_filter_filename(self):
        """Show files only include item name

        Args:
            item (QListWidgetItem): task item
        """
        taskname = self.task_wd.combo_box.currentText()
        self.file_browser.work_tree.filter_name(taskname)

    def clear_all_filesystem(self):
        """Clear all filesytem items
        """
        for tree in self.file_browser.findChildren(browser_widget.FileSystemWidget):
            tree.clear()
        
        for tree in self.hero_browser.findChildren(browser_widget.FileSystemWidget):
            tree.clear()

    def find_open_which_app(self, ext):
        '''Get an application name whice can open the file extention
        Parameters:-
            ext <str> : file extention such as ".ma", ".psd"
        Return <str> an application name such as "maya", "nuke"
        '''
        for dcc, info in self.__config_file['dcc_info'].items():
            if ext in info['extension']:
                return info['name']
        return ''

    def find_open_with(self, ext):
        '''Find applications which can open the file extention
        Parameters:-
            ext <str> : file extention such as ".ma", ".psd"
        Return [[application directory, .exe in relative path], ..]
        '''
        for dcc, info in self.__config_file['dcc_info'].items():
            if ext in info['extension']:
                return [[d.replace('\\', '/'), info['execute']] for d in glob(dcc)]
        return []
        
    def open_with_env(self, app_name):
        '''Execute an application with arguements
        '''
        # user = os.environ['USERNAME']
        file_path = self.current_file_tree.current_file
        config_file = app_env.get_env_file(
                            project=self.project,
                            app=app_name,
                            username=setup_helper.get_user_login(),
                            filepath=file_path)

        QtWidgets.QApplication.processEvents()
        app_laucnher = app_env.AppLauncher(env_file=config_file)
        app_laucnher.start_app(file=file_path, creationflags=subprocess.CREATE_NEW_CONSOLE)

    def open_with_selected(self):
        '''Command for open with an application that version was selected
        '''
        action = self.sender()

        execute_file, selected_file = action.data()
        LOG.info('%s %s', execute_file, selected_file)
        QtWidgets.QApplication.processEvents()
        result = subprocess.Popen('"{}" "{}"'.format(execute_file, selected_file),
                        shell=True, creationflags=subprocess.CREATE_NEW_CONSOLE,
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8' )
        stdout, stderr = result.communicate()
        if result.returncode:
            LOG.error(stderr)

    def open_with_app_version_env(self):
        '''Command for open with an application and set environments
        that version was selected
        '''
        widget = self.sender()
        app_name = widget.text()
        self.open_with_env(app_name)

    def work_file_menu(self, pos):
        """Popup menu when right-click on a file item

        Args:
            pos (QPos): Position has activated
        """
        item = self.sender().indexAt(pos)
        if not item:
            return

        file_path = self.file_browser.work_tree.current_file
        selected_ext = os.path.splitext(file_path)[-1]

        menu = QtWidgets.QMenu(self.sender())

        # TODO: Rename
        # menu.addAction('Rename')

        # menu.addAction('Open (default)', self.open_default)

        open_with_proj = menu.addMenu('Open With Env')
        openwith = menu.addMenu('Open With ..')

        # -- Use extension file search DCC which can open --
        dcc_version = self.find_open_with(selected_ext)
        if dcc_version:
            proj_env_dcc = app_env.read_app_config(self.project)
            for dcc, execute in dcc_version:
                dcc_name = os.path.basename(dcc).lower()
                # -- Action for open DCC which it's configuration file --
                if dcc_name in proj_env_dcc.keys():
                    act = open_with_proj.addAction(
                                dcc_name, self.open_with_app_version_env)
                # -- Action for DCC without configuration file --
                act = openwith.addAction(dcc_name, self.open_with_selected)

                # -- Add executer path to Action data --
                found_exe = glob('{}/{}'.format(dcc, execute))
                if found_exe:
                    act.setData([found_exe[0], file_path])

        menu.exec_(self.sender().mapToGlobal(pos))

    def set_tasks(self, look_up_folder=''):
        """Add task items

        Args:
            look_up_folder (str, optional): directory. Defaults to ''.
        """
        task_list = self.dept_wd.combo_box.currentData(QtCore.Qt.UserRole) or []
        self.task_wd.clear()
        self.task_wd.add_items(task_list, look_up_folder=look_up_folder)

    def department_changed(self):
        """Add task items for each mode
        """
        self.set_tasks()

        # -- Shot Mode --
        if not self.shot_browser.isHidden():

            if self.ent_folder_name == 'scene':
                # -- keep previous item -- 
                cur_shot = ''
                shot_item = self.shot_browser.shot.current_item()
                if shot_item:
                    cur_shot = shot_item.text()
                # ------------------------
                seq_item = self.shot_browser.sequence.current_item()
                self.sequence_clicked(seq_item)
                # -- set selected item --
                if cur_shot:
                    shot_item = self.shot_browser.shot.listwidget.findItems(
                                        cur_shot, QtCore.Qt.MatchExactly)
                    if shot_item:
                        self.shot_browser.shot.listwidget.setCurrentItem(shot_item[0])

            elif self.ent_folder_name == 'sequence':
                # -- keep previous item -- 
                cur_sequence = ''
                sequence_item = self.shot_browser.sequence.current_item()
                if sequence_item:
                    cur_sequence = sequence_item.text()
                # ------------------------
                ep_item = self.shot_browser.episode.current_item()
                self.episode_clicked(ep_item)
                # -- set selected item to previous --
                if cur_sequence:
                    sequence_item = self.shot_browser.sequence.listwidget.findItems(
                                        cur_sequence, QtCore.Qt.MatchExactly)
                    if sequence_item:
                        self.shot_browser.sequence.listwidget.setCurrentItem(sequence_item[0])

        # -- Asset Mode --
        elif not self.asset_browser.isHidden():
            # -- keep previous item -- 
            cur_asset = ''
            asset_item = self.asset_browser.asset.current_item()
            if asset_item:
                cur_asset = asset_item.text()
            # ------------------------
            subtype_item = self.asset_browser.subtype.current_item()
            if subtype_item:
                self.asset_subtype_clicked(subtype_item)
            # -- set selected item to previous --
            if cur_asset:
                asset_item = self.asset_browser.asset.listwidget.findItems(
                                    cur_asset, QtCore.Qt.MatchExactly)
                if asset_item:
                    self.asset_browser.asset.listwidget.setCurrentItem(asset_item[0])

    @QtCore.Slot()
    def episode_clicked(self, item):
        # -- clear ---
        self.clear_all_filesystem()
        # ------------
        return super().episode_clicked(item)
    
    @QtCore.Slot()
    def sequence_clicked(self, item):
        # -- clear ---
        self.clear_all_filesystem()
        # ------------
        return super().sequence_clicked(item)
    
    @QtCore.Slot()
    def shot_clicked(self, item):
        # -- clear ---
        self.clear_all_filesystem()
        # ------------
        return super().shot_clicked(item)

    @QtCore.Slot()
    def asset_type_clicked(self, item):
        # -- clear ---
        self.clear_all_filesystem()
        # ------------
        return super().asset_type_clicked(item)

    @QtCore.Slot()
    def asset_subtype_clicked(self, item):
        # -- clear ---
        self.clear_all_filesystem()
        # ------------
        return super().asset_subtype_clicked(item)
    
    @QtCore.Slot()
    def asset_clicked(self, item):
        # -- clear ---
        self.clear_all_filesystem()
        # ------------
        return super().asset_clicked(item)


    def file_browser_widget(self):
        """Create widget for contain filesystem widget

        Returns:
            QWidget: file browser widget
        """
        widget = QtWidgets.QWidget()
        widget.root_dir = ''
        widget.work_tree = browser_widget.FileSystemWidget(widget)
        widget.work_tree.clear()
        widget.work_tree.set_only_folder(False)

        widget.hero_tree = browser_widget.FileSystemWidget(widget)
        widget.hero_tree.clear()

        widget.output_tree = browser_widget.FileSystemWidget(widget)
        widget.output_tree.clear()

        QtWidgets.QVBoxLayout(widget)
        layout = QtWidgets.QHBoxLayout()
        layout.addStretch()
        self.task_wd.label.setText('Filter task:')
        layout.addWidget(self.task_wd)
        widget.layout().addLayout(layout)
        widget.tab_widget = QtWidgets.QTabWidget()
        widget.tab_widget.addTab(widget.work_tree, "Version")
        widget.tab_widget.addTab(widget.hero_tree, "Hero")
        widget.tab_widget.addTab(widget.output_tree, "Output")
        widget.layout().addWidget(widget.tab_widget)

        return widget

    def hero_browser_widget(self):
        """Create widget for contain filesystem widget

        Returns:
            QWidget: file browser widget
        """
        widget = QtWidgets.QWidget()
        widget.root_dir = ''
        widget.hero_tree = browser_widget.FileSystemWidget(widget)
        widget.hero_tree.clear()

        QtWidgets.QVBoxLayout(widget)
        widget.layout().addWidget(widget.hero_tree)

        return widget


def main(*argv):
    """Open this browser

    Returns:
        QWidget: widget
    """
    widget = SceneBrowser(*argv)
    return widget