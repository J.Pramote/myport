#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, April 2021
##
################################################################################
##
##  Standard Library
##
import sys
import os
# GUI
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtWidgets, QtCore, QtGui


################################################################################
##
##  Relative Modules
##
import gui.project_launcher_window as plw
from homepage import HomePageProject
from login import LoginPage
import setup_helper


def start_dialog():
    """Startup dialog for notice user that the process is being

    Returns:
        QWidget: widget
    """
    widget = QtWidgets.QWidget()
    widget.setWindowFlags(QtCore.Qt.FramelessWindowHint)
    widget.setLayout(QtWidgets.QVBoxLayout())
    widget.layout().setAlignment(QtCore.Qt.AlignCenter)
    widget.layout().addWidget(QtWidgets.QLabel('Connecting..'))
    widget.resize(250,100)
    widget.show()
    return widget

################################################################################
##
##  Class Definitions
##	
class ProjectLauncher(plw.LauncherWindow):
    """_summary_

    """
    def __init__(self, parent=None):
        super(ProjectLauncher, self).__init__(parent)
        self.userlogin = setup_helper.get_user_login()
        self.init_launcher()
    
    def init_launcher(self):
        """Intialize first page
        """
        if not self.userlogin:
            login_widget = LoginPage()
            login_widget.access.connect(self.show_homepage)
            self.setCentralWidget(login_widget)
        else:
            self.show_homepage()
            
    def show_homepage(self, username=''):
        """Show HomePageProject

        Args:
            username (str, optional): user login. Defaults to ''.
        """
        if username:
            self.userlogin = username
        self.mainpage = HomePageProject()
        self.setCentralWidget(self.mainpage)
        ## -- Set username header --
        self.mainpage.homepage.username_lb.setText(self.userlogin)
        self.mainpage.homepage.logout_pb.clicked.connect(self.logout)

    def logout(self):
        """Clear user setup and go to login page
        """
        self.userlogin = ''
        self.init_launcher()
        os.remove(setup_helper.USER_INIT)

    
################################################################################
##
##  Main Function
##	
def main():
    print('Start..')
    app = QtWidgets.QApplication( sys.argv )
    start_win = start_dialog()
    QtWidgets.QApplication.processEvents()
    window = ProjectLauncher()
    start_win.close()
    window.show()
    sys.exit( app.exec_() )


if __name__ == '__main__':
    main()
        