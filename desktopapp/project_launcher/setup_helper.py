#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, April 2021
##
################################################################################
##
##  Standard Library
##
import os
import json
import yaml
import re
import tempfile

from snow.common.lib import config as snow_config

################################################################################
##
##  Global Variables
##
USER_INIT = '{}/.snow/project_launcher.json'.format(tempfile.gettempdir().replace('\\', '/'))
SNOW = 'snow'
if os.getenv('SNOW'):
    SNOW = os.getenv('SNOW')


## Helper fuction for read user's data to startup
def read_user_init(name=''):
    ''' Helper function for loading user init

    return dict - configuration data
    '''
    if not os.path.exists(USER_INIT):
        save_user_init(dict())

    data = dict()
    try:
        with open(USER_INIT) as config:
            data = json.load(config)
        if name:
            return data.get(name, "")
    except IOError:
        print('IOError: Cannot read {}'.format(USER_INIT))
        pass
    except json.decoder.JSONDecodeError:
        os.remove(USER_INIT)
        print('json.decoder.JSONDecodeError : Reset : {}'.format(USER_INIT))

    return data

## Helper fuction for writing user's data to startup
def save_user_init(data):
    ''' Helper function for writing data to user init 
    
    Parameters -
        data : data to write, example format { 'MAYA' : {'ConfigName': 'Name' }
    return data
    '''
    try:
        with open(USER_INIT, 'w+') as configfile:
            json.dump(data, configfile, indent=4)
    except IOError:
        print('IOError: Cannot save {}'.format(USER_INIT))
        pass

    return data

## Helper fuction for read utilites config
def get_utilities():
    data = ''
    utility_config = snow_config.get_tool_config(__file__, filename='utilities.yaml')
    with open(utility_config) as config_file:
        data = yaml.safe_load(config_file)
    return data

### Helper function for loading user init
def load_user_init(app_name):
    ''' Helper function for loading user init

    Parameters
        app_name : name of application (type=str)
    return dict - configuration data
    '''
    app_name = app_name.upper()
    user_init = read_user_init(app_name)
    if user_init:
        return user_init.get('configname')
    return ""

### Helper function for writing utility data to user init 
def write_user_init_utility( utilities ):
    ''' Helper function for writing utility data to user init 
        Parameters
            utilities : a list of utilities (type=list)
    '''
    ## Write user init
    user_data = read_user_init()
    user_data.update({ 'utilities': utilities })
    save_user_init(user_data)

### Helper function for loading user init utility
def load_user_init_utility():
    ''' Helper function for loading user init
    return list - configuration data
    '''
    user_init = read_user_init('utilities')
    if user_init:
        return user_init
    return []


def get_user_login():
    """Get user login

    Returns:
        str: username
    """
    user_init = read_user_init().get('USERNAME')
    return user_init


def save_user_login(username):
    """Save user login

    Args:
        username (str): username
    """
    user_data = read_user_init()
    user_data['USERNAME'] = username
    save_user_init(user_data)


def get_project_integation_tools(project):
    """All tools will show in each project read from here

    Args:
        project (str): project name
    Return (dict) config data
    """
    tool_config = snow_config.get_tool_config(__file__, project=project,
                                            filename='tools.yaml')
    return snow_config.yaml.read(tool_config)
