#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, April 2021
##
################################################################################
##
##  Standard Library
##
import os
from glob import glob
import subprocess
import re

# GUI
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtWidgets, QtCore, QtGui
################################################################################
##
##  Snow Library
##
import snow.common.lib.path_elem as path_elem

################################################################################
##
##  Relative Modules
##
import gui.homepage_widget as hpw
from gui.icon_path import Icon_Default_Project
from project_page import ProjectPage
import setup_helper

################################################################################
##
##  Global Variables
##
Project_Root = 'U:'


################################################################################
##
##  Class Definitions
##
class HomePageProject(QtWidgets.QWidget):
    """Use QStackedLayout contain widget include:
        - HomePage
        - ProjectPage
    """
    def __init__(self, parent=None):
        """Intialize HomePageProject

        Args:
            parent (QWidget, optional): parent widget. Defaults to None.
        """
        super().__init__(parent)
        ## List utilities from config file
        self.utility_data = dict()
        self.utility_data = setup_helper.get_utilities()
        ## Initial object's properties
        self.current_project = ''
        self.user_init_data = dict()

        ## Create Widget
        self.stackedlayout = QtWidgets.QStackedLayout(self)
        self.homepage = hpw.HomePage(self)

        ## -- Insert page --
        self.stackedlayout.insertWidget(0, self.homepage)
        
        self.hpcw = self.homepage.content_widget

        ## Set start page
        self.stackedlayout.setCurrentIndex(0)

        ## Initial widget
        self.initial_project_items()
        self.initial_utility_items()

        ## Connect signal
        self.hpcw.project_listwidget.itemClicked.connect(self.goto_select_project_page)
        self.hpcw.recent_wd.listwidget.itemClicked.connect(self.goto_select_project_page)
        self.hpcw.utilities_listwidget.itemDoubleClicked.connect(self.execute_utility)
        self.hpcw.shelf_utilities_listwidget.itemDoubleClicked.connect(self.execute_utility)
        self.hpcw.shelf_utilities_listwidget.itemChanged.connect(self.add_favorite_utility)

        self.init_user_setup()

    def init_user_setup(self):
        """Read user init file and set data to widget
        """
        self.hpcw.recent_wd.setHidden(True)
        self.user_init_data = setup_helper.read_user_init('HomePageProject') or dict()

        if not self.user_init_data:
            self.user_init_data = dict()
            return

        # -- Set recent project --
        recent_project = self.user_init_data.get('recent_project', [])
        if recent_project:
            self.hpcw.recent_wd.setHidden(False)
            # -- Maximum display is 5 project --
            for proj in recent_project[:6]:
                found = self.hpcw.project_listwidget.findItems(
                            proj, QtCore.Qt.MatchExactly)
                if found:
                    self.hpcw.recent_wd.listwidget.addItem(found[0].clone())

        # -- Set Favorite Utitities --
        favorite_utility = self.user_init_data.get('utilities', [])
        if favorite_utility:
            for ut in favorite_utility:
                item = QtWidgets.QListWidgetItem(self.hpcw.shelf_utilities_listwidget)
                item.setText(ut)

    def hideEvent(self, event):
        """Write user setup

        Args:
            event (QCloseEvent ): close event
        """
        data = setup_helper.read_user_init()

        # -- Keep recent project --
        self.user_init_data['recent_project'] = []
        for row in range(self.hpcw.recent_wd.listwidget.count()):
            item = self.hpcw.recent_wd.listwidget.item(row)
            if not item.text() in self.user_init_data['recent_project']:
                self.user_init_data['recent_project'].append(item.text())

        # -- Keep Favorite Utitities --
        self.user_init_data['utilities'] = []
        for row in range(self.hpcw.shelf_utilities_listwidget.count()):
            item = self.hpcw.shelf_utilities_listwidget.item(row)
            self.user_init_data['utilities'].append(item.text())

        data['HomePageProject'] = self.user_init_data or []
        setup_helper.save_user_init(data)
        event.accept()

    def initial_project_items(self):
        ''' Adding projects to show '''
        project_list = path_elem.get_all_project_code()
        for name in project_list:
            if not name:
                continue
            
            project_dir = '{}/{}'.format(Project_Root, name)
            icon_path = Icon_Default_Project
            icon_found = glob('{}/thumbnail.*'.format(project_dir))
            if icon_found:
                icon_path = icon_found[0].replace('\\','/')

            pix = QtGui.QPixmap(icon_path)
            pix.scaled(150, 150, QtCore.Qt.KeepAspectRatioByExpanding)
            icon = QtGui.QIcon(pix)

            item = hpw.ProjectWidgetItem(self.hpcw.project_listwidget)
            item.setText(name)
            item.setIcon(icon)
            item.setData(QtCore.Qt.UserRole, project_dir)
   
    def initial_utility_items(self):
        ''' Adding utilities to show '''
        for ut in sorted(self.utility_data):
            item = QtWidgets.QListWidgetItem(self.hpcw.utilities_listwidget)
            item.setText(ut)

    def goto_select_project_page(self, item, page_index=1):
        ''' Go to a project page.
            Parameters
                item : project iteme (type=QListWidgetItem)
                page_index : page index will be loaded (type=int)
        return None
        '''
        projectpage = ProjectPage(self)
        # -- Insert widget to StackLayout
        self.stackedlayout.insertWidget(page_index, projectpage)
        self.stackedlayout.setCurrentIndex(page_index)

        self.current_project = item.text()
        project_page_widget = self.stackedlayout.currentWidget()
        widget = project_page_widget
        widget.setProject(self.current_project)

        # -- add recent project to user init --
        if self.hpcw.recent_wd.isHidden():
            self.hpcw.recent_wd.setHidden(False)

        found_recent = self.hpcw.recent_wd.listwidget.findItems(
                            self.current_project, QtCore.Qt.MatchExactly)
        if found_recent:
            self.hpcw.recent_wd.listwidget.takeItem(
                        self.hpcw.recent_wd.listwidget.row(found_recent[0]))
        self.hpcw.recent_wd.listwidget.insertItem(0, item.clone())
        # -- limit display to 5 item
        if self.hpcw.recent_wd.listwidget.count() > 5:
            self.hpcw.recent_wd.listwidget.takeItem(4)

    def execute_utility(self, item):
        """Execute utility

        Args:
            item (QListWidgetItem): utility item
        """
        utility_name = item.text()
        utility_cmd = self.utility_data[utility_name].get('cmd')
        print(utility_cmd)
        subprocess.Popen(utility_cmd, shell=True, 
                            creationflags=subprocess.CREATE_NEW_CONSOLE)
           
    def add_favorite_utility(self, text):
        """Add tool in my favorite list

        Args:
            text (str): word to search
        """
        all_item = self.hpcw.shelf_utilities_listwidget.findItems(
                                                                r'.*.', QtCore.Qt.MatchRegExp)
        utilities = []
        for item in all_item:
            utilities.append(item.text())

