#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, April 2021
##
################################################################################
##
##  Standard Library
##
import sys

# GUI
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtWidgets, QtCore, QtGui

################################################################################
##
##  Relative Modules
##
import gui.header_widget as hw
from gui.icon_path import Studio_Icon

################################################################################
##
##  Class Definitions
##
class ProjectWidgetItem( QtWidgets.QListWidgetItem ):
    ''' Item for Project QListWidget'''
    def __init__( self, parent=None ):
        super( ProjectWidgetItem, self ).__init__(parent)

        self.setTextAlignment(QtCore.Qt.AlignHCenter| QtCore.Qt.AlignBottom)
        self.setSizeHint(QtCore.QSize(
                self.listWidget().iconSize().width(), 
                self.listWidget().iconSize().height()))

class ListWidget( QtWidgets.QListWidget ):
    item_dropped = QtCore.Signal(str)
    def __init__( self, icon_mode=True, space=10, 
                        drag=False, drop=False, wrapping=False ):
        super( ListWidget, self ).__init__()
        self.setWordWrap(True)
        self.setContextMenuPolicy( QtCore.Qt.CustomContextMenu )

        if icon_mode:
            self.setFlow(QtWidgets.QListView.LeftToRight)
            self.setResizeMode(QtWidgets.QListView.Adjust)
            self.setViewMode(QtWidgets.QListView.IconMode)
            self.setIconSize(QtCore.QSize(100,120))
            self.setSpacing(space)
        
        if drag:
            self.setDragEnabled( drag )
        
        if drop:
            self.setAcceptDrops(drop)
            self.setDefaultDropAction(QtCore.Qt.CopyAction)

        if wrapping:
            self.setWrapping(wrapping)

    def dropEvent(self, event):
        if len(event.source().selectedItems()) > 0:
            for item in event.source().selectedItems():
                found = self.findItems(item.text(), QtCore.Qt.MatchExactly)
                if not found:
                # Default action
                    QtWidgets.QListWidget.dropEvent(self, event)
                    self.item_dropped.emit(item.text())

class FavoriteUtilityListWidget( ListWidget ):
    def __init__(self, **kwarg):
        super( FavoriteUtilityListWidget, self).__init__(**kwarg)

    ## Event press del to delete item
    def keyPressEvent( self, event ):
        if event.key() == QtCore.Qt.Key_Delete:
            for item in self.selectedItems():
                print('remove', item)
                row = self.row(item)
                self.takeItem( row )
                self.itemChanged.emit(item)

class RightSideBarWidget( QtWidgets.QWidget ):

    # FIXME: Next phase : User Profile
    
    def __init__(self, parent=None):
        super(RightSideBarWidget, self).__init__(parent)
        # self.setStyleSheet("margin:0px;padding:0px;background:black;")

        ## Initial Widget
        self.__widget = QtWidgets.QWidget()

        ## Initial Layout
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().setContentsMargins(0,0,0,0)
        self.layout().addWidget(self.__widget)
        self.__widget.setLayout(QtWidgets.QVBoxLayout())
        self.__widget.setContentsMargins(0,10,0,0)
        self.__widget.setProperty('rightside_homepage',True)
        self.__widget.layout().setAlignment(QtCore.Qt.AlignTop )

        label = QtWidgets.QLabel()
        pixmap = QtGui.QPixmap(Studio_Icon)
        label.setPixmap(pixmap.scaledToWidth( 50, QtCore.Qt.SmoothTransformation ))
        self.__widget.layout().addWidget(label)
        self.__widget.layout().addStretch()


class ContentWidget( QtWidgets.QWidget ):

    def __init__(self, parent=None):
        super(ContentWidget, self).__init__(parent)
        
        ## Initial Widget
        self.__widget = QtWidgets.QWidget()
        self.username_header = hw.UserLoginHeader()
        self.homepage_header = hw.HomePageHeader()
        self.project_listwidget = ListWidget()
        self.recent_wd = self.recent_widget()
        self.utilities_listwidget = ListWidget(icon_mode=False, drag=True, wrapping=True)
        self.shelf_utilities_listwidget = FavoriteUtilityListWidget(icon_mode=False, drop=True)

        self.search_utilities = QtWidgets.QLineEdit()
        self.search_utilities.setPlaceholderText('search here')

        ## Initial Layout
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().setContentsMargins(0,0,0,0)
        self.layout().setSpacing(0)
        self.layout().addWidget(self.username_header)
        self.layout().addWidget(self.homepage_header)
        self.homepage_header.setContentsMargins(0,10,0,0)

        self.layout().addWidget(self.__widget)
        self.stackedLayout = QtWidgets.QStackedLayout()
        self.__widget.setLayout(self.stackedLayout)
        self.__widget.setContentsMargins(0,0,0,0)
        self.__widget.layout().setAlignment(QtCore.Qt.AlignTop )

        ## - Insert page
        self.stackedLayout.insertWidget(0, self.main_page_widget())
        self.stackedLayout.insertWidget(1, self.utilities_page_widget())


        ## Connect signal
        self.homepage_header.main_menu_projects.clicked.connect( 
                                        lambda : 
                                        self.stackedLayout.setCurrentIndex(0))


        self.homepage_header.main_menu_utilities.clicked.connect( 
                                        lambda : 
                                        self.stackedLayout.setCurrentIndex(1))

        self.homepage_header.main_menu_projects.setChecked(QtCore.Qt.Checked)

    def main_page_widget(self):

        widget = QtWidgets.QWidget()
        widget.setLayout(QtWidgets.QVBoxLayout())

        ## Add more sectin here
        # ..
        #
        widget.layout().addWidget(self.recent_wd)

        ## Project List Section
        frame = QtWidgets.QGroupBox(self)
        frame.setMinimumHeight(300)
        widget.layout().addWidget(frame)
        frame.setStyleSheet("font: 16px;")    
        frame.setTitle("Projects")
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.project_listwidget)
        frame.setLayout(layout)


        ## Add more sectin here
        # ..
        #
        scroll = QtWidgets.QScrollArea()
        scroll.setMinimumSize(400,400)
        scroll.setWidgetResizable(True)
        scroll.setWidget(widget)
        return scroll
    
    def recent_widget(self):

        widget = QtWidgets.QGroupBox(self)
        widget.listwidget = ListWidget()

        widget.setStyleSheet("font: 16px;")    
        widget.setTitle("Recent:")
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(widget.listwidget)
        widget.setLayout(layout)
        return widget
    
    def utilities_page_widget(self):

        widget = QtWidgets.QWidget()
        widget.setLayout(QtWidgets.QVBoxLayout())
        widget.layout().setAlignment(QtCore.Qt.AlignTop)

        ## Add more sectin here
        # ..
        #

        ## My Shelf Section
        frame = QtWidgets.QGroupBox(self)
        frame.setMaximumHeight(250)
        widget.layout().addWidget(frame)
        frame.setStyleSheet("font: 16px;")    
        frame.setTitle("My Favorites")
        QtWidgets.QVBoxLayout(frame)
        frame.layout().addWidget(self.shelf_utilities_listwidget)

        ## Utilities List Section
        frame = QtWidgets.QGroupBox(self)
        widget.layout().addWidget(frame)
        frame.setStyleSheet("font: 16px;")    
        frame.setTitle("Utilities")
        QtWidgets.QVBoxLayout(frame)
        
        layout = QtWidgets.QHBoxLayout()
        frame.layout().addLayout(layout)
        layout.addWidget(self.search_utilities)
        
        frame.layout().addWidget(self.utilities_listwidget)

        ## Add more sectin here
        # ..
        #

        return widget

class HomePage(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        ## Create widget
        self.right_sidebar = RightSideBarWidget()
        self.content_widget = ContentWidget()
        self.username_lb = self.content_widget.username_header.username
        self.logout_pb = self.content_widget.username_header.logout_pb

        ## Initial layout
        self.setContentsMargins(0,0,0,0)
        self.setLayout(QtWidgets.QHBoxLayout())

        self.layout().setContentsMargins(0,0,0,0)
        self.layout().setSpacing(0)
        self.layout().setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)

        self.layout().addWidget(self.right_sidebar)
        self.right_sidebar.setContentsMargins(0,0,0,0)

        self.layout().addWidget(self.content_widget)
        self.content_widget.setContentsMargins(0,0,0,0)


class HomePageProjectWidget(QtWidgets.QWidget):

    def __init__(self, parent=None):
        ''' Homepage '''
        super().__init__(parent)

        self.stackedlayout = QtWidgets.QStackedLayout(self)
        self.homepage = HomePage(self)

