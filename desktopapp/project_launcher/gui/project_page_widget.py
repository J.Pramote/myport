#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, April 2021
##
################################################################################
##
##  Standard Library
##
import os
# GUI
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtWidgets, QtCore, QtGui

# from snow.common.lib import config as snow_config


################################################################################
##
##  Relative Modules
##
import gui.header_widget as hw

################################################################################
##
##  Class Definitions
##	
class AppItem(QtWidgets.QDialog):
    data = ''
    config_root = ''
    config_name = ''
    parent = None
    def __init__( self, app_name, app_version ):
        '''
            app_name : Application name (type=str)
            app_version : Application version (type=str or int)
        '''
        ## Object's properties
        self.__app_name = app_name
        self.__app_version = app_version
        self.__config_name = ''

        super(AppItem, self).__init__()

        self.height = 120
        self.width = 280
        self.setFixedSize(self.width, self.height)
        self.setStyleSheet( self.styleSheet )

        ## Create widget
        self.depart_combobox = QtWidgets.QComboBox()
        self.launch_button = QtWidgets.QPushButton("Launch")

        ## Connnect widget emit
        self.depart_combobox.currentTextChanged.connect(self.set_config_name)
        self.launch_button.clicked.connect(self.launch)

        ## Initailize Layout
        self.setContentsMargins(0,0,0,0)
        self.setLayout( QtWidgets.QVBoxLayout() )
        self.layout().setAlignment(QtCore.Qt.AlignCenter )
        self.layout().setContentsMargins(0, 0, 0, 0)
        inform_widget = self.create_inform_widget()
        inform_widget.setProperty("inform",True)

        self.layout().addWidget(inform_widget)
        self.layout().addWidget(self.launch_button)
    
    @QtCore.Slot()
    def data(self):
        return self.__data

    # @QtCore.Slot()
    # def parent(self):
    #     return self.parent()

    @QtCore.Slot()
    def config_root(self):
        return self.__config_root

    @QtCore.Slot()
    def config_name(self):
        return self.__config_name
    
    @QtCore.Slot()
    def app_name(self):
        return self.__app_name

    @QtCore.Slot()
    def app_version(self):
        return self.__app_version
    
    @QtCore.Slot()
    def launch(self):
        self.parentWidget().itemLaunchClicked.emit(self)
    
    def add_config_menu(self, config_menu):
        self.depart_combobox.clear()
        config_menu = [os.path.splitext(c)[0] for c in config_menu]
        self.depart_combobox.addItems(config_menu)

    def set_config_root(self, path):
        self.__config_root = path

    def set_parent(self, widget):
        self.__parent = widget
    
    def set_config_name(self, name):
        self.__config_name = name
    
    def set_current_config(self, name):
        self.depart_combobox.setCurrentText(name)

    def create_inform_widget(self):

        widget = QtWidgets.QWidget()
        widget.setLayout(QtWidgets.QVBoxLayout())
        widget.layout().setAlignment(QtCore.Qt.AlignCenter)
        label = QtWidgets.QLabel(
                                '{} {}'.format(self.__app_name, 
                                                self.__app_version) )
        label.setProperty("header", True)
        label.setAlignment(QtCore.Qt.AlignCenter )
        label.setStyleSheet("font: bold;")
        widget.layout().addWidget( label )

        layout = QtWidgets.QHBoxLayout()
        label = QtWidgets.QLabel( "Config:" )
        label.setStyleSheet("font: bold;")
        layout.addWidget(label)
        self.depart_combobox.setMinimumWidth(140)
        layout.addWidget(self.depart_combobox)
        widget.layout().addLayout( layout )

        return widget
            
    def setUnavaible( self ):
        self.setStyleSheet("background-color:rgb(150,150,150);")

    @property
    def styleSheet(self):
        return """
                *{
                        background: #5f6974;
                        border:none;
                        border-radius:4px;
                        color: #F5F5F5;
                        }
                *[inform=true]{
                    background: none;
                }

                QPushButton{
                    font: bold;
                    color: #1d2024;
                    text-align: center;
                    /*width: 280;
                    height: 20;*/
                    margin: 0px,0px,150,120px;
                    padding: 5px,5px,0px,0px;
                    background: rgb(50,193,216);
                    /*border: none;*/
                    border-radius:8px;
                    /*border: 1px solid  #1d2024;*/
                }

                QPushButton:hover{
                    border: 1px solid rgb(50,193,216);
                    background: rgb(31, 171, 194);
                    color: #FFFFFF;
                }
                QPushButton:pressed{
                    background: rgb(32, 136, 153);
                }
                QPushButton:disabled {
                    background-color:rgb(136, 136, 136);
                    }
                QLabel{ 
                        background:none;
                        }

                QLabel[header="true"]{ 
                        font: 20px;
                        background:none;
                        }
                
                QDialog{
                    border: 1px solid rgb(50,193,216);
                }

                QDialog:hover{
                        background:rgb(129,139,152);
                        /*background-color:rgb(187,187,187);*/
                        color: #1d2024;
                        }

                QComboBox{
                    border: 1px outset #5f6974;
                    border-radius:0px;
                    background-color: rgb(187,187,187);
                    color:#1d2024;
                }
            """
    
class AppListWidget( QtWidgets.QWidget ):

    itemLaunchClicked = QtCore.Signal(QtWidgets.QDialog)

    def __init__( self ):
        super().__init__()
        self.setLayout(QtWidgets.QGridLayout())
        self.max_column = 3
        self.row=0
        self.column=0
        self.setObjectName('AppListWidget')

        self.layout().setAlignment( QtCore.Qt.AlignTop )
        self.setContentsMargins( 0, 0, 0, 0 )

    def widget_items(self):
        """List application items
        """
        items = []
        for index in range(self.layout().count()):
            widget_item = self.layout().itemAt(index)
            if not widget_item:
                continue
            items.append(widget_item)
        return items

    ## Add an asset item
    def addItem( self, item ):
        """Add an asset item
        Parameters:-
                item : AssetItem
        return None
        """
        if (self.layout().count() % self.max_column)==0:
            self.row += 1
        else:
            self.column += 1

        item.setParent(self)
        self.layout().addWidget(item,self.row,self.column)

    def removeItem( self, item ):
        """ Parameters
                item : AssetItem
        return None
        """
        if item not in self.widget_items():
            return
        if item.widget():
            self.layout().removeWidget(item.widget())
            return
        index = self.layout().indexOf(item)
        self.layout().takeAt(index)
       
    def clear(self):
        """Clear all application items
        """
        for item in self.widget_items():
            if item.widget():
                self.layout().removeWidget(item.widget())
                continue
            index = self.layout().indexOf(item)
            self.layout().takeAt(index)


class ToolPickerList(QtWidgets.QListWidget):
    
    def __init__(self, *argv):
        super().__init__(*argv)

        self.setStyleSheet('border: 1px solid #5f6974;')
        self.icon_size = QtCore.QSize(50, 50)
        self.setObjectName("tool_picker")
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding,
                                            QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        self.setSizePolicy(sizePolicy)
        self.setFixedHeight(80)

        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.h_scrollbar = self.horizontalScrollBar()
        self.setMovement(QtWidgets.QListView.Snap)
        self.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)

        self.setDropIndicatorShown(True)
        self.setProperty("isWrapping", False)
        self.setViewMode(QtWidgets.QListView.IconMode)
        self.setIconSize(self.icon_size)
    
    def dropEvent(self, event):
        item = self.currentItem()

        if (event.source().dropIndicatorPosition() == QtWidgets.QAbstractItemView.DropIndicatorPosition.OnViewport):
            self.takeItem(self.currentRow())
            self.addItem(item)
        
        elif event.source().dropIndicatorPosition() == QtWidgets.QAbstractItemView.DropIndicatorPosition.OnItem:
            target_index = self.row(self.itemAt(event.pos()))
            self.takeItem(self.currentRow())
            self.insertItem(target_index, item)
    

class ProjectPageWidget(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        QtWidgets.QVBoxLayout(self)
        self.layout().setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        self.parent = parent
        self.setObjectName("ProjectPageWidget")

        ## Create widget
        self.projectpage_header = hw.ProjectPageHeader(self)
        self.menubar = self.menu_widget()
        self.content = QtWidgets.QWidget()#self.content_widget()
        self.content.setObjectName('contentwidget')
        self.content.setLayout(QtWidgets.QVBoxLayout())
        self.mode_cbb = QtWidgets.QComboBox()
        self.tool_picker = ToolPickerList(self)
        self.back_pb = QtWidgets.QPushButton('<')
        self.back_pb.setAutoRepeat(True)

        self.next_pb = QtWidgets.QPushButton('>')
        self.next_pb.setAutoRepeat(True)

        self.next_pb.pressed.connect(self.scroll_to_next)
        self.back_pb.pressed.connect(self.scroll_to_back)

        ## Initial layout
        h_layout = QtWidgets.QHBoxLayout()
        h_layout.setAlignment(QtCore.Qt.AlignVCenter)
        self.layout().addLayout(h_layout)
        # h_layout.setSpacing(0)
        h_layout.addWidget(self.projectpage_header)
        h_layout.addSpacing(10)
        h_layout.addWidget(self.mode_cbb)
        self.mode_cbb.setMinimumWidth(200)
        h_layout.addSpacing(50)
        h_layout.addWidget(self.tool_picker_widget())

        self.layout().addWidget(self.content)

    def scroll_to_next(self):
        self.tool_picker.h_scrollbar.setSliderPosition(
            self.tool_picker.h_scrollbar.sliderPosition()+20)

    def scroll_to_back(self):
        self.tool_picker.h_scrollbar.setSliderPosition(
            self.tool_picker.h_scrollbar.sliderPosition()-20)

    def create_tool_icon(self, tool_data):
        self.m_iconSize = self.tool_picker.icon_size
        
        painter = QtGui.QPainter()

        gradient = QtGui.QLinearGradient(0, 0, 0, self.m_iconSize.height())
        gradient.setColorAt(0.0, QtGui.QColor(240, 240, 240))
        gradient.setColorAt(1.0, QtGui.QColor(224, 224, 224))

        brush = QtGui.QBrush(gradient) 

        for data in tool_data:
            pix = QtGui.QPixmap(self.m_iconSize)
            icon_path = data.get('icon_path')
            if icon_path:
                if not os.path.isfile(icon_path):
                    icon_path = 'P:/pipeline/icons/' + icon_path

            if os.path.exists(icon_path):
                pix.load(icon_path)
            else:
                painter.begin(pix)
                rect = QtCore.QRect(QtCore.QPoint(0, 0), self.m_iconSize)
                painter.fillRect(rect, brush)
                font = painter.font()
                font.setPixelSize(data['icon_font_size'])
                painter.setFont(font)
                painter.drawText(rect, QtCore.Qt.AlignCenter, data['icon_shortname'])
                painter.end()

            item = QtWidgets.QListWidgetItem()
            item.setIcon(QtGui.QIcon(pix))
            item.setToolTip(data['name'])
            item.setData(QtCore.Qt.UserRole, data['path'])
            item.setFlags(QtCore.Qt.ItemIsDropEnabled | item.flags())
            self.tool_picker.addItem(item)

    def menu_widget(self):
        widget = QtWidgets.QWidget()
        widget.setProperty("homepage_header_menu",True)
        widget.setLayout(QtWidgets.QHBoxLayout())
        widget.layout().setSpacing(0)
        self.layout().addWidget(widget)

        widget.layout().setContentsMargins(0,0,0,0)
        widget.layout().setAlignment(
            QtCore.Qt.AlignBottom | QtCore.Qt.AlignLeft)
        
        widget.group = QtWidgets.QButtonGroup(widget)
        widget.group.setExclusive( True )
     
        return widget

    def tool_picker_widget(self):
        widget = QtWidgets.QWidget()
        QtWidgets.QHBoxLayout(widget)
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.tool_picker.setViewportMargins(5, 10, 0, 0)
        self.back_pb.setProperty('scroll', True)
        self.back_pb.setProperty('right', True)

        widget.layout().addWidget(self.back_pb)
        widget.layout().addWidget(self.tool_picker)
        self.next_pb.setProperty('scroll', True)
        self.next_pb.setProperty('left', True)

        widget.layout().addWidget(self.next_pb)
        return widget
    
    def content_widget(self):
        widget = QtWidgets.QWidget()
        widget.stackedLayout = QtWidgets.QStackedLayout()
        widget.setLayout(widget.stackedLayout)
        return widget
    
    def insert_page_bar(self, index, button, page_widget):
        button.setCheckable(True)
        button.setProperty("homepage_header_menu",True)
        self.menubar.group.addButton(button)
        self.menubar.layout().addWidget(button)
        self.content.stackedLayout.insertWidget(index, page_widget)

        ## Connect signal
        button.clicked.connect(
            lambda : self.content.stackedLayout.setCurrentIndex(index))
