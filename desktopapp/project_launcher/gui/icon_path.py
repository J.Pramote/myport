from . import icon

Icon_Dir = icon.__path__[0]
Icon_Default_Project = '{}/project.png'.format(Icon_Dir)

Studio_Icon = '{}/studio.png'.format(Icon_Dir)
Home_Icon = '{}/home.png'.format(Icon_Dir)
Back_Icon = '{}/back.png'.format(Icon_Dir)
