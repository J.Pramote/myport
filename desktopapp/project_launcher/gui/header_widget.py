#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, April 2021
##
################################################################################
##
##  Standard Library
##
# GUI
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtWidgets, QtCore, QtGui

from snow.common.lib.widget import website

################################################################################
##
##  Relative Modules
##
from gui.icon_path import Home_Icon, Back_Icon


################################################################################
##
##  Class Definitions
##	
class HeaderWidget(QtWidgets.QWidget):
    def __init__(self,mainwindow=None):
        super(HeaderWidget, self).__init__(mainwindow)
        self.mainwindow = mainwindow
        self.init_width = self.mainwindow.width()
        self.init_height = self.mainwindow.height()

        self.setLayout(QtWidgets.QVBoxLayout())

        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignRight)
        self.mainwindow.centralWidget().layout().addLayout(layout)

        self.minimize_button = QtWidgets.QPushButton('_')
        self.minimize_button.clicked.connect(self.minimize_window)
        layout.addWidget(self.minimize_button)

        self.maximize_button = QtWidgets.QPushButton('[]')
        self.maximize_button.clicked.connect(self.maximize_window)
        layout.addWidget(self.maximize_button)

        self.close_button = QtWidgets.QPushButton('x')
        self.close_button.clicked.connect(self.mainwindow.close)
        layout.addWidget(self.close_button)

        self.resize(self.mainwindow.width(), self.height())
    
    def maximize_window(self):
        if self.mainwindow.windowState() == QtCore.Qt.WindowMaximized:
            self.mainwindow.setWindowState(QtCore.Qt.WindowNoState)
        else:
            self.mainwindow.setWindowState(QtCore.Qt.WindowMaximized)

    def minimize_window(self):
        self.mainwindow.setWindowState( QtCore.Qt.WindowMinimized)	

class HomePageHeader(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(HomePageHeader, self).__init__(parent)

        ## Initial Widget
        self.main_menu_projects = QtWidgets.QPushButton("Projects")
        self.main_menu_utilities = QtWidgets.QPushButton("Utilities")

        self.main_menu_projects.setCheckable(True)
        self.main_menu_utilities.setCheckable(True)

        self.main_menu_projects.setProperty("homepage_header_menu",True)
        self.main_menu_utilities.setProperty("homepage_header_menu", True)
        
        # Initial Layout
        self.setContentsMargins(0,0,0,0)
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().setContentsMargins(0,0,0,0)
        self.layout().setAlignment(QtCore.Qt.AlignBottom )
        widget = QtWidgets.QWidget()
        widget.setProperty("homepage_header_menu",True)

        widget.setLayout(QtWidgets.QHBoxLayout())
        widget.layout().setSpacing(0)
        self.layout().addWidget(widget)
        
        widget.layout().setContentsMargins(0,0,0,0)
        widget.layout().setAlignment(QtCore.Qt.AlignBottom | QtCore.Qt.AlignLeft)
        
        self.stepMenu = QtWidgets.QButtonGroup(widget)
        self.stepMenu.setExclusive( True )
        self.stepMenu.addButton(self.main_menu_projects)
        self.stepMenu.addButton(self.main_menu_utilities)

        widget.layout().addWidget(self.main_menu_projects)
        widget.layout().addWidget(self.main_menu_utilities)

class ProjectPageHeader(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        ## Initial widget
        self.goto_home_button = QtWidgets.QPushButton()
        self.back_button = QtWidgets.QPushButton()
        self.project_label = QtWidgets.QLabel()

        home_pixmap = QtGui.QPixmap(Home_Icon)
        self.goto_home_button.setIcon(QtGui.QIcon(home_pixmap))
        self.goto_home_button.setProperty("projectpage_header_menu",True)
        self.goto_home_button.setIconSize(self.goto_home_button.sizeHint())

        back_pixmap = QtGui.QPixmap(Back_Icon)
        self.back_button.setIcon(QtGui.QIcon(back_pixmap))
        self.back_button.setProperty("projectpage_header_menu",True)
        self.back_button.setIconSize(self.back_button.sizeHint())

        self.project_label.setProperty("projectpage_header_menu",True)

        ## Initial Layout
        self.setLayout(QtWidgets.QHBoxLayout())
        # self.layout().setContentsMargins(0,0,0,0)
        # self.layout().setAlignment(QtCore.Qt.AlignVCenter)

        '''
        widget = QtWidgets.QWidget()
        widget.setLayout(QtWidgets.QHBoxLayout())
        widget.layout().setAlignment(QtCore.Qt.AlignLeft)
        widget.layout().addWidget(self.goto_home_button)
        # widget.layout().addWidget(self.back_button)
        self.layout().addWidget(widget)
        '''

        self.layout().addWidget(self.goto_home_button)
        # self.layout().addStretch(1)
        # self.project_label.setAlignment(QtCore.Qt.AlignCenter)
        self.layout().addWidget(self.project_label)

        h_btn = website.help_button('https://', 20, 20)
        h_btn.setContentsMargins(0, 0, 0, 50)
        # self.layout().addWidget(h_btn)


class UserLoginHeader(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        QtWidgets.QHBoxLayout(self)
        self.layout().setContentsMargins(0,5,10,0)
        self.setStyleSheet('font: 13px; color: #a3a3a3;')
        self.layout().setAlignment(QtCore.Qt.AlignRight)
        label = QtWidgets.QLabel('Username:')
        # label.setStyleSheet('font: italic;')

        self.layout().addWidget(label)
        self.username = QtWidgets.QLabel()
        self.layout().addWidget(self.username)
        self.logout_pb = QtWidgets.QPushButton('Logout')
        self.logout_pb.setStyleSheet(
                        'QPushButton{border: none;}'
                        'QPushButton:hover{color: rgb(50,193,216)}')
        self.logout_pb.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.layout().addWidget(self.logout_pb)

        self.layout().addWidget(website.help_button(url='https://'))
