#!Python3
################################################################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
################################################################################
##
##  Standard Library
##
import sys
import os
import yaml
import re
from glob import glob
# GUI
try:
    from PySide2 import QtWidgets, QtCore, QtGui
except ImportError:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets

################################################################################
##
##  Logging
##
from snow.common.lib import code_logging
Logger = code_logging.create_log(__name__)

################################################################################
##
##  Snow Modules
##
from snow.common.lib import media_convert
from snow.common.lib.xml_rw import  ReadXml
from snow.common.lib.widget import message_box

################################################################################
##
##  Relative Modules
##
import edit_publish_gui as gui
import edit_publish_helper as helper

################################################################################
##
##  Global Variables
##
Config_Task = 'P:/pipeline/config/task.yaml'


################################################################################
##
##      Helper Functions
##
def get_task_list():
    with open(Config_Task, 'r') as yfile:
        config = yaml.safe_load(yfile)
    return config['department_tasks'].get('editorial') or []


def find_movies(dir_path, regx_mov=r'(\.mov$|\.mp4$)'):
    files = []
    for file in os.listdir(dir_path):
        if re.search(regx_mov, file):
            files.append('{}/{}'.format(dir_path, file))
    return sorted(files)


################################################################################
##
##		Main Class
## 
class EditPublish(gui.EditPublishWindow):

    def __init__(self, parent=None):
        super(EditPublish, self).__init__(parent)
        self.shotgrid_loading_dl = gui.ShotGridLoadingDialog(self)

        ## Connect Signal
        self.widget.xml_pb.clicked.connect(self.open_browser)
        self.widget.xml_le.textChanged.connect(self.init_content)
        self.widget.episode_ccb.currentIndexChanged.connect(self.init_shots)
        self.widget.select_all_cb.stateChanged.connect(self.select_all)
        self.widget.publish_pb.clicked.connect(self.submit)

        ## Set content
        self.widget.task_cbb.addItems(sorted(get_task_list()))
        self.widget.sound_cb.setCheckState(QtCore.Qt.Checked)
        self.widget.select_all_cb.setCheckState(QtCore.Qt.Checked)

    def closeEvent(self, e):
        ## Stop thread pool
        if hasattr(self, 'publish_thread_pool'):
            self.publish_thread_pool.thread_pool.clear()
        e.accept()

    def reset(self):
        self.widget.project_lb.clear()
        self.widget.episode_ccb.clear()
        self.widget.shot_tw.clear()
        self.widget.movie_cb.clear()

    @QtCore.Slot()
    def select_all(self, state):
        ''' Select all item
        Parameters:-
            state : Checked / Unchecked
        '''

        state = self.widget.select_all_cb.checkState()
        items = self.widget.shot_tw.findItems('',QtCore.Qt.MatchContains, 2 )
        for item in items:
            item.setCheckState(0, state)

    @QtCore.Slot()
    def open_browser(self):
        current = self.widget.xml_le.text()
        diaLog = QtWidgets.QFileDialog()
        result = diaLog.getOpenFileName(self, "Open File", os.path.dirname(current) or "U:/",
                                        "*.xml")
        if result[0]:
            self.widget.xml_le.setText(result[0])

    @QtCore.Slot()
    def init_content(self, xml_path):
        ''' Initialize all content when xml has updated
        Parameter:
            xml_path : xml file path
        '''
        if not xml_path:
            return

        self.reset()
        self.xml_path = xml_path.replace('\\','/')
        Logger.info('XML Input - {}'.format(self.xml_path))

        ## Read xml
        self.xml_read = ReadXml(xml_path)
        self.xml_framerate = self.xml_read.frame_rate
        self.xml_data = self.xml_read.cuts
        self.xml_shots = [v for v in sorted(self.xml_data.items(), 
                                key=lambda x: x[1]['cut_order'])]
        
        ## Search movie file and add to widget
        self.widget.movie_cb.addItems(find_movies(os.path.dirname(self.xml_path)))
        index = self.widget.movie_cb.findText(''.join(re.findall(r'_(\d+)\.xml$', self.xml_path)), 
                                        QtCore.Qt.MatchContains)
        self.widget.movie_cb.setCurrentIndex(index)

        ## Find project from xml path
        self.project = ''.join( re.findall(r'\w:/(\w+)/', self.xml_path))
        self.widget.project_lb.setText(self.project)

        ## Find episode from xml path
        self.query_episode = helper.QueryEpisodeThread(project=self.project)
        self.query_episode.started.connect( self.init_episodes )
        self.query_episode.finished.connect(self.init_shots)
        self.query_episode.episodes.connect(self.set_episode_item)
        self.query_episode.start()

    def init_episodes(self):
        self.widget.episode_ccb.clear()
        self.widget.episode_ccb.addItem('loading..')
        self.widget.episode_ccb.setEnabled(False)

    def init_shots(self):
        self.widget.shot_tw.clear()
        if not self.widget.episode_ccb.currentIndex() > 0:
            return
        
        if hasattr(self, 'query_shot'):
            if self.query_shot.isRunning():
                return

        ## Query shot data from Shotgrid in thread
        self.query_shot = helper.QueryShotThread(project=self.project, 
                                                episode=self.widget.episode_ccb.currentText(),
                                                shots=self.xml_data)
        self.query_shot.started.connect(self.started_load)
        self.query_shot.finished.connect(self.finished_load)
        self.query_shot.shot_data.connect(self.set_shot_item)

        self.query_shot.start()

    def set_episode_item(self, episodes):
        self.widget.episode_ccb.clear()
        self.widget.shot_tw.clear()
        self.widget.episode_ccb.addItem('-select-')
        self.widget.episode_ccb.addItems(episodes)

        index = self.widget.episode_ccb.findText(
                        ''.join( re.findall(r'/all/episode/(\w+)/', 
                                            self.xml_path)), 
                        QtCore.Qt.MatchContains )
        self.widget.episode_ccb.setCurrentIndex(index)	
        self.widget.episode_ccb.setEnabled(True)

    @QtCore.Slot()
    def set_shot_item(self, shot, sg_shot_data):
        ''' Create shot items
          0		1		2		 3		  4			5			6
        |	| Status | Shot | Cut In | Cut Out | Duration | Cut Order |
        Parameters:-
            shot : <str> shot name
            sg_shot_data : <dict> shot data in ShotGrid 
        '''
        xml_shot_data = self.xml_data[shot]

        sg_status = sg_shot_data.get('sg_status_list')
        item = gui.ShotTreeWidgetItem(self.widget.shot_tw)
        item.setData(0, QtCore.Qt.UserRole, sg_shot_data)
        item.setText(1, sg_status)
        item.setText(2, shot)
        item.setText(3, str(xml_shot_data['cut_in']))
        item.setText(4, str(xml_shot_data['cut_out']))
        item.setText(5, str(xml_shot_data['duration']))
        item.setText(6, str(xml_shot_data['cut_order']))

        if sg_status:
            if sg_status == 'omt':
                item.set_background_gray()

    @QtCore.Slot()
    def started_load(self):
        ''' Widget action when data is loading from ShotGrid
        '''
        self.shotgrid_loading_dl.show()
        self.setEnabled(False)

    @QtCore.Slot()
    def finished_load(self):
        ''' Widget action when data has loaded from ShotGrid
        '''
        self.shotgrid_loading_dl.hide()
        self.setEnabled(True)

    def check_compare_cut(self):
        ''' Compare cutin cutout cutorder '''
        result = True
        diff = dict()
        for item in self.get_selected_shot():
            sg_data = item.data(0,QtCore.Qt.UserRole)
            shot = item.text(2)
            ## Difference cut in / out
            if sg_data.get('sg_cut_in') and  sg_data.get('sg_cut_out'):
                sg_cut_duration = sg_data['sg_cut_out'] - sg_data['sg_cut_in'] + 1
                cut_dur_item = int(item.text(5))
                if sg_cut_duration != cut_dur_item:
                    value = diff.get(shot) or {}
                    value.update({ 'duration':  [sg_cut_duration, cut_dur_item] })
                    diff[shot] = value
            ## Difference cut order
            if 'sg_cut_order' in sg_data.keys():
                cut_order_item = int(item.text(6))
                if sg_data['sg_cut_order'] != cut_order_item:
                    value = diff.get(shot) or {}
                    value.update({ 'cut order':  [sg_data['sg_cut_order'], cut_order_item ] })
                    diff[shot] = value

        if diff:
            dialog = gui.ConfirmShotPublishDialog(self, '<h3>These shots are changing cuts, ' 
                                                        'please check before continue.</h3>', )
            for shot, data in diff.items():
                msg = []
                for k, v in data.items():
                    msg.append( 'Change "{}" from "{}" to "{}"'.format(k, v[0], v[1]))
                dialog.insert_row_data(shot, '\n'.join(msg) )
            result = dialog.exec_()
            if dialog.result:
                for remove_shot in dialog.result:
                    self.set_checkstate_shot(remove_shot, QtCore.Qt.Unchecked)

        return result

    def check_shot_status(self):
        ''' Is omitted status 
        Return: a status anwser, 0 is cancel
        '''
        diff = dict()
        result = True
        for item in self.get_selected_shot():
            shot = item.text(2)
            if item.text(1) == 'omt':
                value = diff.get(shot) or {}
                value.update({ 'status':  [ 'omt', 'rdy' ] })
                diff[shot] = value

        if diff:
            dialog = gui.ConfirmShotPublishDialog(self, '<h3>Editor must confirm with coordinator '
                                        'and Ep Di to change status before update shot.</h3>' )
            for shot, data in diff.items():
                msg = []
                for k, v in data.items():
                    msg.append( 'Change "{}" from "{}" to "{}"'.format(k, v[0], v[1]))
                dialog.insert_row_data(shot, '\n'.join(msg) )
            result = dialog.exec_()
            if dialog.result:
                for remove_shot in dialog.result:
                    self.set_checkstate_shot(remove_shot, QtCore.Qt.Unchecked)

        return result

    def set_checkstate_shot(self, shot, state=QtCore.Qt.Unchecked):
        ''' Set check state on shot item
        Parameters:-
            shot : <str> shot name
            state : <Qt.Unchecked> | <Qt.Checked >
        '''
        item = self.widget.shot_tw.find_item_by_shot( shot )
        item.setCheckState(0, QtCore.Qt.Unchecked)

    def get_selected_shot(self):
        ''' Get shot items are selected
        Return : <list> items
        '''
        result = []
        items = self.widget.shot_tw.findItems('',QtCore.Qt.MatchContains, 2 )
        for item in items:
            if item.checkState(0) == QtCore.Qt.Checked:
                result.append(item)
        return result

    def get_shot_item_data(self, items):
        ''' Get all item data, which ShotGrid's shot and xml
        Return : <dict> - {"SH0010": {"sg_cut_in": 1001, ..}, .. }
        '''
        data = dict()
        for item in items:
            shot_name = item.text(2)
            data[shot_name] = item.data(0, QtCore.Qt.UserRole)
        return data

    @QtCore.Slot()
    def thread_fishised_event(self, shot):
        ''' Action after a job thread has finished
        '''
        item = self.widget.shot_tw.find_item_by_shot( shot )
        item.set_foreground_green()

    @QtCore.Slot()
    def thread_failed_event(self, shot):
        ''' Action after a job thread has failed
        '''
        item = self.widget.shot_tw.find_item_by_shot( shot )
        item.set_foreground_red()

    @QtCore.Slot()
    def thread_accepted_event(self):
        ''' Action after a thread pool has done
        '''
        Logger.info('Pool close')
        self.setEnabled(True)
        message_box.MessageBox(self).success()

    def submit_on_machine(self, movie_path, shots):
        ''' Submit on user's machine and how take item is append on
        number of machine's thread
        Parameters:-
            movie_path : <str> a movie file path
            shots : <dict> shot data
        '''
        self.widget.status_lb.showMessage('Start..')
        self.setEnabled(False)

        self.publish_thread_pool = helper.PublishTheadPool(
                        xml=self.xml_path, 
                        movie=movie_path, 
                        extract_sound=self.widget.sound_cb.isChecked(), 
                        project=self.widget.project_lb.text(), 
                        episode=self.widget.episode_ccb.text(), 
                        task= self.widget.task_cbb.currentText(),
                        framerate=self.xml_framerate, 
                        shots=shots)
        self.publish_thread_pool.finished.connect(self.thread_fishised_event)
        self.publish_thread_pool.failed.connect(self.thread_failed_event)
        self.publish_thread_pool.accepted.connect(self.thread_accepted_event)
        self.publish_thread_pool.progress.connect(self.widget.status_lb.showMessage)
        self.publish_thread_pool.start()

    @QtCore.Slot()
    def submit(self):
        ''' Submit publish
        '''
        if not self.widget.xml_le.text():
            message_box.MessageBox(self).error(
                            text='Please input xml path')
            return			
        elif not os.path.exists(self.widget.xml_le.text()):
            message_box.MessageBox(self).error(
                            text='Xml not exists!\n'
                                'Please check xml file again.')
            return		

        if not self.check_compare_cut():
            return

        if not self.check_shot_status():
            return

        movie_path = self.widget.movie_cb.currentText()
        Logger.info('Movie input - {}'.format(movie_path))
    
        if not os.path.exists(movie_path):
            message_box.MessageBox(self).error(
                            text='Not found movie file!')
            return

        if not self.widget.project_lb.text():
            message_box.MessageBox(self).error(
                            text='Not match a project!\n'
                            'Please keep xml in collect directory.')
            return

        if not self.widget.episode_ccb.currentIndex() > 0:
            message_box.MessageBox(self).error(
                            text='Please choose an episode.')
            return

        selected_shot = self.get_selected_shot()
        shots = self.get_shot_item_data(selected_shot)

        # if not self.widget.deadline_cb.isChecked():
        # 	self.submit_on_machine(movie_path, shots)

        self.widget.status_lb.showMessage('Start..')
        self.setEnabled(False)

        data = dict(
                movie=movie_path, 
                extract_sound=self.widget.sound_cb.isChecked(), 
                project=self.widget.project_lb.text(), 
                episode=self.widget.episode_ccb.currentText(), 
                task= self.widget.task_cbb.currentText(),
                framerate=self.xml_framerate, 
                shots=shots)

        for k, v in data.items():
            Logger.info('{} : {}'.format(k,v))

        self.publish_thread_pool = helper.PublishTheadPool(**data)
        if self.widget.deadline_cb.isChecked():
            self.publish_thread_pool.set_submit_to_deadline(True)

        self.publish_thread_pool.finished.connect(self.thread_fishised_event)
        self.publish_thread_pool.failed.connect(self.thread_failed_event)
        self.publish_thread_pool.accepted.connect(self.thread_accepted_event)
        self.publish_thread_pool.progress.connect(self.widget.status_lb.showMessage)
        Logger.info('Start Pool...')
        self.publish_thread_pool.start()

################################################################################
##
##		Main Function
##
def main():
    app = QtWidgets.QApplication( sys.argv )
    window = EditPublish()
    window.show()

    sys.exit( app.exec_() )

if __name__ == '__main__':
    main()