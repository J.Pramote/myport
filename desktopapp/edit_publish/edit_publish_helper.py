#!Python3
################################################################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
################################################################################
##
##  Standard Library
##
import re
import os
import subprocess
import urllib.parse
try:
    from PySide2 import QtCore
except ImportError:
    from PySide import QtCore

################################################################################
##
##  Snow Modules
##
import snow.shotgrid.lib.wrap as sg_wrap
from snow.common.bot.edit_publish_submit import Shot_Media_Dir
from snow.common.bot.edit_publish_submit import Sound_Media_Dir
from snow.common.bot.edit_publish_submit import Shot_Media_File
from snow.common.bot.edit_publish_submit import Sound_Media_File
################################################################################
##
##  Global Variables
##
SNOW = re.sub( r'snow\\.*.', 'snow', 
    os.path.dirname(__file__)).replace('\\','/')
REPO = os.path.dirname(SNOW)

Deadline_Worker_Test =  '' # TODO: Your deadline worker name for test 
Deadline_Exe = "C:/Program Files/Thinkbox/Deadline10/bin/deadlinecommand.exe"
Deadline_PythonPath = 'P:/pipeline/thirdparty/Windows/python3_packages;{}'.format(REPO)
Deadline_Pool = 'pip_shotgrid'
Python_Exe = "P:/pipeline/thirdparty/Windows/WinPython/python-3.9.2.amd64/python3.exe"
Publish_Submit_Py = SNOW + '/common/bot/edit_publish_submit.py'

################################################################################
##
##		Class Definitions
## 
class QueryEpisodeThread(QtCore.QThread):
    ''' Get shot's data on ShotGrid
    Parameters:-
        project : <str> project name / code
    Signals:-
        episodes : emit <list> episodes
    '''
    episodes = QtCore.Signal(list)

    def __init__(self, project):
        super(QueryEpisodeThread, self).__init__()
        self.project = project

    def run(self):
        self.episodes.emit(sg_wrap.episode.get_episodes(self.project))

class QueryShotThread(QtCore.QThread):
    ''' Get shot's data on ShotGrid
    Parameters:-
        project : <str> project name / code
        episode : <str> episode
        shots : <dict> shot data
    Signals:-
        shot_data : emit <str> shot name and <dict> shot's data
    '''
    shot_data = QtCore.Signal(str, dict)

    def __init__(self, project, episode, shots):
        super(QueryShotThread, self).__init__()
        self.project = project
        self.episode = episode
        self.shots = shots
    
    def run(self):
        sg_result = sg_wrap.shot.get_shots(self.project, self.episode)
        reorder_shots = [v for v in sorted(self.shots.items(), 
                                key=lambda x: x[1]['cut_order'])]
        for shot, data in reorder_shots:
            value = data
            for sg_shot in sg_result:
                if re.search('_'+shot+'$', sg_shot['code']):
                    value.update(sg_shot)
                    self.shot_data.emit(shot, value)
                    break
            else:
                self.shot_data.emit(shot, value)

class JobObject( QtCore.QObject ):
    ''' Singals '''
    finished = QtCore.Signal( str )
    started = QtCore.Signal( QtCore.QRunnable )
    failed = QtCore.Signal( str )
    progress = QtCore.Signal( str )
    accepted = QtCore.Signal()
            
class PublishRunable(QtCore.QRunnable):
    ''' PublishThreadPool's thread 
    Parameters:-
        shot : <str> shot
        cmd : <str> command line
    '''
    count = 0
    def __init__(self, shot, cmd):
        # print(cmd)
        super(PublishRunable, self).__init__()
        type(self).count += 1
        self.cmd = cmd
        self.shot = shot
        self.process = QtCore.QProcess()
        self.signal = JobObject()
        self.currentThread = QtCore.QThread.currentThread()

    def run(self):
        self.signal.started.emit(self)
        print('PublishRunable#{}\n{}'.format(self.count, self.cmd))
        result = subprocess.Popen(self.cmd, shell=True, 
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8' )
        stdout, stderr = result.communicate()
        for line in stdout.splitlines():
            print(line)
        if stderr:
            for line in stderr.splitlines():
                print(line)
        if result.returncode:
            self.signal.failed.emit(self.shot)
        else:
            self.signal.finished.emit(self.shot)

class PublishTheadPool( JobObject) :
    ''' Thread pool for publish shots
    Parameters:-
        xml : <str> xml file
        movie : <str> movie file
        extract_sound : <bool>
        project : <str> project name / code
        episode : <str> episode
        task : <str> task
        framerate : <str> | <int> frame per second
        shots : <dict> shot data
    Signals:-
        finished : emit <str> finished shot
        started : emit <QRunnable> starting job
        failed : emit <str> faild shot
        progress : emit <str> message
        accepted : emit <none> pool done
    '''
    def __init__(self, movie, extract_sound, 
                    project, episode, task, framerate, shots):
        super(PublishTheadPool, self).__init__( )
        self.movie = movie
        self.extract_sound = extract_sound
        self.project =project
        self.episode = episode
        self.task = task
        self.framerate = framerate
        self.shots = shots
        self.shot_count = len(shots.keys())
        self.finished_count = 0
        self.job_threads = []
        self.is_submit_deadline = False

        self.job_output_mov_dir = Shot_Media_Dir.format(project=self.project, 
                                    episode=self.episode, 
                                    task=self.task)
        self.job_output_sound_dir = Sound_Media_Dir.format(project=self.project, 
                                    episode=self.episode)

        self.thread_pool = QtCore.QThreadPool.globalInstance()
        self.thread_pool.setMaxThreadCount( self.thread_pool.maxThreadCount()-1 )
        self.thread_pool.setExpiryTimeout( 3000 )

    @QtCore.Slot()
    def complete_job_event(self, shot):
        self.finished_count += 1
        self.progress.emit('Publish...  {} ({}/{} shot)'.format( shot,
                                    self.finished_count, self.shot_count))
        if self.finished_count == self.shot_count:
            self.progress.emit('')
            self.accepted.emit()
    
    def set_submit_to_deadline(self, state):
        self.is_submit_deadline = state
    
    def shot_arguments(self, shot, data, local=True):
        cmd = ['{}'.format(Publish_Submit_Py)]
        if not local:
            cmd += ['--input_movie "{}"'.format(urllib.parse.quote(self.movie))]
        else:
            cmd += ['--input_movie "{}"'.format(self.movie)]
        # cmd += ['--input_movie {}'.format(self.movie.replace(' ','%20'))]

        if self.extract_sound:
            cmd += ['--extract_sound']
        cmd += ['--project {}'.format(self.project)]
        cmd += ['--episode {}'.format(self.episode)]
        cmd += ['--task {}'.format(self.task)]
        cmd += ['--episode {}'.format(self.episode)]
        cmd += ['--shot {}'.format(shot)]
        cmd += ['--start_frame {}'.format(data.get('frame_in'))]
        cmd += ['--end_frame {}'.format(data.get('frame_out'))]
        cmd += ['--cut_duration {}'.format(data.get('duration'))]
        cmd += ['--cut_order {}'.format(data.get('cut_order'))]
        cmd += ['--framerate {}'.format(self.framerate)]
        return ' '.join(cmd)
    
    def submit_deadline(self):
        for shot, data in self.shots.items():
            job_name = '{}_{}_{}'.format(self.episode, shot, self.task)
            movie_file = Shot_Media_File.format(episode=self.episode, 
                                                    shot=shot, task=self.task)
            movie_file = '{}/{}'.format(self.job_output_mov_dir, movie_file)
            sound_file = Sound_Media_File.format(episode=self.episode, shot=shot)
            sound_file = '{}/{}'.format(self.job_output_sound_dir, sound_file)

            cmd = ['"{}"'.format(Deadline_Exe)]
            cmd += ['-SubmitCommandLineJob']
            cmd += ['-executable "{}"'.format(Python_Exe)]
            cmd += ['-initialstatus Active']
            cmd += ['-arguments "{}"'.format(self.shot_arguments(shot, data, local=False))]
            cmd += ['-prop Name={}'.format(job_name)]
            cmd += ['-prop Priority={}'.format(60)]
            cmd += ['-prop Pool={}'.format(Deadline_Pool)]
            # cmd += ['-prop Group={}'.format(Group)]

            cmd += ['-prop OutputFilename0={}'.format(movie_file)]
            cmd += ['-prop OutputFilename1={}'.format(sound_file)]
            cmd += ['-prop EnvironmentKeyValue0=PYTHONPATH={}'.format(Deadline_PythonPath)]

            if Deadline_Worker_Test:
                cmd += ['-prop Whitelist={}'.format(Deadline_Worker_Test)]

            job = PublishRunable( shot , ' '.join(cmd) )
            job.signal.finished.connect(self.complete_job_event)
            job.signal.finished.connect(self.finished.emit)
            job.signal.failed.connect(self.complete_job_event)
            job.signal.failed.connect(self.failed.emit)

            self.thread_pool.start( job )
            self.job_threads.append(job.currentThread)

    def submit_local(self):
        for shot, data in self.shots.items():
            cmd = [Python_Exe]
            cmd += [self.shot_arguments(shot, data)]
            job = PublishRunable( shot , ' '.join(cmd) )
            job.signal.finished.connect(self.complete_job_event)
            job.signal.finished.connect(self.finished.emit)
            job.signal.failed.connect(self.complete_job_event)
            job.signal.failed.connect(self.failed.emit)

            self.thread_pool.start( job )
            self.job_threads.append(job.currentThread)
    
    def start(self):
        print('Pool Start..')
        if self.is_submit_deadline:
            self.submit_deadline()
        else:
            self.submit_local()