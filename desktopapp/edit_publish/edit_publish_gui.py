#!Python3
################################################################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
################################################################################
##
##  Standard Library
##
# GUI
import os
try:
	from PySide2 import QtWidgets, QtCore, QtGui
except ImportError:
	from PySide import QtGui, QtCore
	import PySide.QtGui as QtWidgets

from snow.common.lib.widget import drop_file_widget

################################################################################
##
##  Global Variables
##
Stylesheet = os.path.dirname(__file__)+"/stylesheet.css"

################################################################################
##
##                             Custom Widget Classes
##
class ShotTreeWidget(QtWidgets.QTreeWidget):
	def __init__(self):
		super(ShotTreeWidget, self).__init__()
		self.setHeaderLabels( ["","Status","Shot", "Cut In", 
							"Cut Out", "Duration", "Cut Order"] )
		self.setColumnWidth( 0, 20 )
		self.setColumnWidth(1 , 30 )
	
	def find_item_by_shot(self, shot):
		match = self.findItems(shot, QtCore.Qt.MatchExactly, 2 )
		if match:
			if len(match) > 1:
				raise ValueError('{} more than one'.format(shot))
			return match[0]

class ShotTreeWidgetItem(QtWidgets.QTreeWidgetItem):
	def __init__(self, parent=None):
		super(ShotTreeWidgetItem, self).__init__(parent)
		self.setCheckState(0, QtCore.Qt.Checked)
		# for column in range(self.treeWidget().columnCount()):
		# 	self.setForeground(column, QtGui.QBrush(QtGui.QColor(255,255,255)))

	def set_background_gray(self):
		for column in range(self.treeWidget().columnCount()):
			self.setBackground(column, QtGui.QBrush(QtGui.QColor(0,0,0)))

	def set_foreground_green(self):
		for column in range(self.treeWidget().columnCount()):
			self.setForeground(column, QtGui.QBrush(QtCore.Qt.green))

	def set_foreground_red(self):
		for column in range(self.treeWidget().columnCount()):
			self.setForeground(column, QtGui.QBrush(QtCore.Qt.red))

################################################################################
##
##                                Dialog Classes
## 
class ShotGridLoadingDialog(QtWidgets.QWidget ):
	def __init__(self, parent=None):
		super( ShotGridLoadingDialog, self ).__init__(parent)
		self.setWindowFlags(QtCore.Qt.Tool | QtCore.Qt.FramelessWindowHint)
		QtWidgets.QVBoxLayout(self)
		self.layout().setAlignment(QtCore.Qt.AlignCenter)
		label = QtWidgets.QLabel('Loading..')
		self.layout().addWidget(label)
		self.resize(300,100)

class ConfirmShotPublishDialog( QtWidgets.QDialog ):
	result = []
	def __init__(self, parent=None, msg=''):
		super( ConfirmShotPublishDialog, self ).__init__(parent)
		self.setContentsMargins(0,10,0,10)
		self.setWindowFlags(QtCore.Qt.Dialog)
		self.setWindowTitle('Changing Confirm')
		QtWidgets.QVBoxLayout(self)

		self.column = ["", "Shot", "Detail"]
		
		## Init Widget
		self.select_all_cb = QtWidgets.QCheckBox('Select All')
		self.shot_tw = QtWidgets.QTreeWidget()
		self.shot_tw.setWordWrap(True)
		self.shot_tw.setHeaderLabels( self.column )
		self.shot_tw.setColumnWidth( 0, 10 )
		self.shot_tw.setColumnWidth(1 , 80 )

		self.continue_pb = QtWidgets.QPushButton('Continue')
		self.cancel_pb = QtWidgets.QPushButton('Cancel')

		## Connect Signals
		self.select_all_cb.stateChanged.connect(self.select_all)
		self.continue_pb.clicked.connect(self.get_result)
		self.cancel_pb.clicked.connect(self.reject)

		## Set Content
		self.select_all_cb.setCheckState(QtCore.Qt.Checked)
		
		## Init layout
		label = QtWidgets.QLabel(msg)
		label.setWordWrap(True)
		label.setStyleSheet('color: red;')
		self.layout().addWidget(label)
		self.layout().addWidget(self.select_all_cb)
		self.layout().addWidget(self.shot_tw)
		layout = QtWidgets.QHBoxLayout()
		layout.setAlignment(QtCore.Qt.AlignRight)
		self.layout().addLayout( layout )
		layout.addWidget(self.continue_pb)
		layout.addWidget(self.cancel_pb)
		
		self.resize(600,400)
		
	# def showEvent(self, e):
	# 	self.parent().setEnabled(False)
	# 	e.accept()
	
	# def closeEvent(self, e):
	# 	self.parent().setEnabled(True)
	# 	e.accept()

	def select_all(self, state):
		state = self.select_all_cb.checkState()
		items = self.shot_tw.findItems('',QtCore.Qt.MatchContains, 2 )
		for item in items:
			item.setCheckState(0, state)
	
	def insert_row_data(self, shot, msg):
		
		item = QtWidgets.QTreeWidgetItem( self.shot_tw )
		item.setCheckState(0, QtCore.Qt.Checked)
		# item.setTextAlignment(0, QtCore.Qt.AlignTop)
		item.setText(1, shot)
		# item.setTextAlignment(1, QtCore.Qt.AlignTop)
		c_item = QtWidgets.QTreeWidgetItem(item)
		c_item.setText(2, msg)
		c_item.setTextAlignment(2, QtCore.Qt.AlignTop)

		item.setExpanded(True)
	
	def get_result(self):
		result = []
		items = self.shot_tw.findItems('',QtCore.Qt.MatchContains, 0 )
		for item in items:
			if item.checkState(0) == QtCore.Qt.Unchecked:
				result.append(item.text(1))		
		self.result = result
		self.accept()

################################################################################
##
##                                Window Classes
## 
class Widget(QtCore.QObject):
	def __init__(self):
		super(Widget, self).__init__()
		self.xml_le = drop_file_widget.DropLineEdit(ph_text='Copy & paste .xml path here', ext=['.xml'])
		self.xml_pb = QtWidgets.QPushButton('Browse')

		self.movie_cb = QtWidgets.QComboBox()

		self.project_lb = QtWidgets.QLabel()
		self.episode_ccb = QtWidgets.QComboBox()
		self.task_cbb = QtWidgets.QComboBox()
		self.select_all_cb = QtWidgets.QCheckBox('Select All')
		self.sound_cb = QtWidgets.QCheckBox('Export Sound')
		self.shot_tw = ShotTreeWidget()
		self.deadline_cb = QtWidgets.QCheckBox('Submit to Deadline')
		self.publish_pb = QtWidgets.QPushButton('Publish')

		self.status_lb = QtWidgets.QStatusBar()

class EditPublishWindow(QtWidgets.QMainWindow):
	def __init__(self, parent=None):
		super(EditPublishWindow, self).__init__(parent)
		self.setWindowTitle('Edit Publish Window')
		self.setCentralWidget(QtWidgets.QWidget(self))
		QtWidgets.QVBoxLayout(self.centralWidget())
		self.setStyleSheet(open(Stylesheet ,'r').read())

		self.widget = Widget()
		self.setStatusBar(self.widget.status_lb)

		self.init_layout()
		self.resize(700, 600)
	
	def init_layout(self):
		self.centralWidget().setContentsMargins(10, 10, 10, 10)
		self.centralWidget().layout().setAlignment(QtCore.Qt.AlignTop)
		layout = QtWidgets.QHBoxLayout()
		self.centralWidget().layout().addLayout(layout)
		label = QtWidgets.QLabel('XML:')
		layout.addWidget(label)
		layout.addWidget(self.widget.xml_le)
		layout.addWidget(self.widget.xml_pb)

		layout = QtWidgets.QHBoxLayout()
		layout.setAlignment(QtCore.Qt.AlignLeft)
		self.centralWidget().layout().addLayout(layout)
		label = QtWidgets.QLabel('Movie:')
		layout.addWidget(label)
		layout.addWidget(self.widget.movie_cb)
		self.widget.movie_cb.setMinimumWidth(600)

		self.centralWidget().layout().addSpacing(10)

		layout = QtWidgets.QHBoxLayout()
		self.centralWidget().layout().addLayout(layout)
		proj_layout = QtWidgets.QHBoxLayout()
		proj_layout.setAlignment(QtCore.Qt.AlignLeft)
		layout.addLayout(proj_layout)
		label = QtWidgets.QLabel('<h4>Project:</h4>')
		proj_layout.addWidget(label)
		proj_layout.addWidget(self.widget.project_lb)

		layout.addSpacing(20)

		ep_layout = QtWidgets.QHBoxLayout()
		ep_layout.setAlignment(QtCore.Qt.AlignLeft)
		layout.addLayout(ep_layout)
		label = QtWidgets.QLabel('<h4>Episode:</h4>')
		ep_layout.addWidget(label)
		ep_layout.addWidget(self.widget.episode_ccb)

		self.centralWidget().layout().addSpacing(10)

		task_layout = QtWidgets.QHBoxLayout()
		task_layout.setAlignment(QtCore.Qt.AlignLeft)
		layout.addLayout(task_layout)
		label = QtWidgets.QLabel('<h4>Task:</h4>')
		task_layout.addWidget(label)
		task_layout.addWidget(self.widget.task_cbb)
		
		layout.addSpacing(10)

		layout.addWidget(self.widget.sound_cb)

		self.centralWidget().layout().addSpacing(10)

		layout = QtWidgets.QHBoxLayout()
		layout.setAlignment(QtCore.Qt.AlignLeft)
		self.centralWidget().layout().addLayout(layout)
		layout.addWidget(self.widget.select_all_cb)
		

		self.centralWidget().layout().addWidget(self.widget.shot_tw)

		layout = QtWidgets.QHBoxLayout()
		layout.setAlignment(QtCore.Qt.AlignRight)
		self.centralWidget().layout().addLayout(layout)
		layout.addWidget(self.widget.deadline_cb)
		layout.addWidget(self.widget.publish_pb)

		layout = QtWidgets.QHBoxLayout()
		layout.setAlignment(QtCore.Qt.AlignLeft)
		self.centralWidget().layout().addLayout(layout)
		layout.addWidget(self.widget.status_lb)