from django.shortcuts import render
from rest_framework import generics, mixins
from rest_framework.response import Response
from rest_framework.exceptions import AuthenticationFailed

# from rest_framework.authentication import TokenAuthentication
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from .models import Seed
from .serializers import SeedSerializer
from django.views import generic
from django.urls import reverse, reverse_lazy
from django.shortcuts import redirect
from django.contrib import messages

# Create your views here.
class SeedCreate(generics.CreateAPIView):
    queryset = Seed.objects.all()
    serializer_class = SeedSerializer
    

class SeedListCreate(generics.ListCreateAPIView):
    queryset = Seed.objects.all()
    serializer_class = SeedSerializer

    def get(self, *args, **kwargs):
        user = token.get_user(self.request)
        if user:
            return super().get(*args, **kwargs)
        return HttpResponseRedirect(redirect("user:login"))

    def post(self, *args, **kwargs):
        super().post(*args, **kwargs)
        return redirect('seed:all')
    

class SeedRetrieve(generics.RetrieveAPIView):
    queryset = Seed.objects.all()
    serializer_class = SeedSerializer

    
class SeedRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Seed.objects.all()
    serializer_class = SeedSerializer
    lookup_field = "pk"


class SeedDetroy(generics.RetrieveDestroyAPIView):
    queryset = Seed.objects.all()
    serializer_class = SeedSerializer
    lookup_field = "pk"
