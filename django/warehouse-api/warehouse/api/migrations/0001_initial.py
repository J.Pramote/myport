# Generated by Django 4.1.13 on 2024-05-05 10:09

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Seed',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rep_date', models.IntegerField(null=True)),
                ('year', models.IntegerField(null=True)),
                ('year_week', models.IntegerField(null=True)),
                ('varity', models.CharField(max_length=100)),
                ('rdcsd', models.CharField(max_length=100)),
                ('stock2sale', models.CharField(max_length=100)),
                ('season', models.IntegerField(null=True)),
                ('crop_year', models.CharField(max_length=100)),
            ],
        ),
    ]
