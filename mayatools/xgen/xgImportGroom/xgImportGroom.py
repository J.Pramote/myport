#! c:\python27\python.exe
#######################################
#
#	@ 2021
#	Written by Jutamas (Nook) Pramote
#
#######################################
# Stand Library
import os
import re
from glob import glob
import json
# GUI
try:
	from PySide import QtGui, QtCore
	import PySide.QtGui as QtWidgets
	import shiboken
except:
	from PySide2 import QtWidgets, QtCore, QtGui
	import shiboken2 as shiboken

#######################################
from info.fileInfo import FileAssetInfo,FileShotInfo

from xgSpecialCaseHelper import MapFurAssetJson

from xgImportGroomGui import XgImportGroomDialog

#######################################
# Maya Library
import maya.cmds as mc


#######################################
#
# Class Definition
#
class MayaScene:
	def __init__( self ):
		self.sceneFileObj = FileShotInfo( self.currentFile )
	
	@property
	def currentFile( self ):
		return mc.file(q=True,loc=True)

	@property
	def episode( self ):
		if not hasattr( self.sceneFileObj, 'ep' ):
			return ''
		return self.sceneFileObj.ep

	@property
	def shot( self ):
		if not hasattr( self.sceneFileObj, 'shot' ):
			raise ValueError( "You have to work in shot file!" )
		return self.sceneFileObj.shot
	
	@property
	def projectPublishDir( self ):
		return self.sceneFileObj.publish_dir
	
	@property
	def shotPublishDir( self ):
		return self.sceneFileObj.shotPublishDir

	@property
	def animPublishDir( self ):
		fileObj = FileShotInfo( self.currentFile ,step='anim')

		if os.path.isdir( fileObj.stepPublishDir ):
			return fileObj.stepPublishDir
		else:
			fileObj = FileShotInfo( self.currentFile ,step='anm')

			return fileObj.stepPublishDir

	@property
	def cachedAnimJsonFile( self ):
		jsonFile = ''
		jsonDir1 = '{}/_data_/json_cache'.format( self.shotPublishDir )

		if self.animPublishDir:

			lastVer = ''

			for fol in os.listdir( self.animPublishDir ):
				if re.search('^v\d+.',fol):
					if fol > lastVer:
						lastVer = fol
			if not lastVer:
				print('Not found .json version folder.')

			jsonDir2 = '{}/{}/data/json_cache'.format( self.animPublishDir, lastVer )
			
		jsonFile = self.getCachedAnimJsonFile( jsonDir1 ) or self.getCachedAnimJsonFile( jsonDir2 ) 

		if not jsonFile:
			raise 'Not found .json file in '+ jsonDir2

		return jsonFile

	def getCachedAnimJsonFile( self, cacheDir ):
		if not os.path.exists( cacheDir ):
			return

		jsonList = sorted(glob('{}/*{}*.json'.format( cacheDir, self.shot )))
		if jsonList:
			return jsonList[-1].replace('\\','/')

class FurAsset( FileAssetInfo ):

	def __init__(self, assetpath='', assetFurName = ''):
		FileAssetInfo.__init__(self,assetpath,step='fur') 
		furFormat = '{assetName}_fur*.ma'
		self.assetFurName = assetFurName
		self.prefixAsset = furFormat.format(assetName=assetFurName)
		print( "init fur", self.assetFurName, self.prefixAsset )
	
	@property
	def furPublishDir( self ):
		return self.stepPublishDir.replace(self.asset ,self.assetFurName)
	
	def __isLoaded__(self,target_file):
		allRefND = mc.ls(references=True)
		for ar in allRefND:
			refFile = mc.referenceQuery(ar,f=True,wcn=True)
			if refFile == target_file:
				print('Fur is loaded',refFile)
				return True
		return False
	
	def __maxVersion__(self):

		listMA1 = sorted(glob('{}/{}'.format(self.furPublishDir,self.prefixAsset)))
		listMA2 = sorted(glob('{}/v*/{}'.format(self.furPublishDir,self.prefixAsset)))

		listMA = listMA1 or listMA2

		if listMA:
			print('__maxVersion__',listMA[-1].replace('\\','/'))
			return listMA[-1].replace('\\','/')
		else:
			print ('# Not found a fur file. #')
			print ('{}/v*/{}'.format(self.furPublishDir,self.prefixAsset))
			return		

	def reference(self,namespace='',force=False):
		maxFile = self.__maxVersion__()
		if not maxFile:
			print('FurAsset.reference()','Not found a fur file.')
			return None

		nsFur = '{}_fur'.format(namespace) 

		if force:
			nodeFur = mc.file(maxFile,r=True,namespace=nsFur,options="v=0;") 
			print('reference namespace',nsFur)
			print('FurAsset.reference()',maxFile)
			return nodeFur

		if not self.__isLoaded__(maxFile):
			nodeFur = mc.file(maxFile,r=True,namespace=nsFur,options="v=0;") 
			print('reference namespace',nsFur)
			print('FurAsset.reference()',maxFile)
			return nodeFur

class CharacterHelper:

	def __init__(self,assetname='',assetpath=''):
		self.charName = assetname
		self.assetPath = assetpath
		self.furAssetName = ''

	def initializeMatchFurData( self, publishDir, epCode ):
		print ("initializeMatchFurData")
		fur_json = "{}/asset/char/{}_matchFur.json".format( publishDir, epCode )
		
		if not os.path.exists(fur_json):
			print( self.charName+ " is not a special case." )
			self.furAssetName = self.charName
					
		else:
			print( self.charName + " is a special case in " + fur_json )
			mapFurAsset = MapFurAssetJson( fur_json )
			self.furAssetName = mapFurAsset.mapAssetKey( self.charName  )

	@property
	def charGeoguideGroup( self ):

		print('get_GRP_','*:{}_geoguide_0001_GRP'.format(self.charName))
		grp = mc.ls('*:{}_geoguide_0001_GRP'.format(self.charName))
		return grp


	def getFurGeoguideGroup( self, nodeFur ):

		allObj =  mc.referenceQuery(nodeFur,n=True)
		grp = [n for n in allObj if re.search('\w+?:\w+_geoguide_0001_GRP',n)]
		if grp:
			return grp[0]

	def getAllFurGeoGuideGroup(self):

		# Get fur reference node
		fur_compile = re.compile('\w+/publish/(asset|assets)/char/{}/fur/\w+'.format(self.furAssetName))
		charFurRefND = []
		for nd in mc.ls(references=True):
			try:
				if fur_compile.search(mc.referenceQuery(nd,f=True,wcn=True)):
					charFurRefND.append(nd)
			except:
				print('# junk node:',nd)
				pass
		#
		furGrp = []
		for cf in charFurRefND:
			furGeoGuideGrp = self.getFurGeoguideGroup( cf )
			if furGeoGuideGrp:
				furGrp.append( furGeoGuideGrp )

		return furGrp

	def findUnconnectFur(self):

		allBNode = mc.ls(type='blendShape') 
		furGrps = self.getAllFurGeoGuideGroup()
		for bs in allBNode:
			mc.select(mc.blendShape(bs,q=True,g=True))
			mc.pickWalk( direction='up' )
			mc.pickWalk( direction='up' )
			base_grp = mc.ls(sl=True)

			if base_grp[0] in furGrps:
				print('remove:',base_grp[0])
				furGrps.remove(base_grp[0])
		print('#',furGrps)

		return furGrps

	def isBlended(self,grp):
		try:
			mc.blendShape(grp,q=True,t=True)
			return True
		except:
			return False


	def blendShape(self,base,target):

		source = mc.blendShape(base,target,automatic=True,origin='world')
		indexCount = mc.blendShape(source[0],q=True,wc=True)
		for i in range(indexCount):
			mc.blendShape(source[0],e=True,w=(i,1))
	
	def connectFur( self ):

		furAsset = FurAsset( self.assetPath, self.furAssetName )

		for charGeoGuideGrp in self.charGeoguideGroup:
			nsGeo = ':'.join(charGeoGuideGrp.split(':')[:-1])
			if not self.isBlended(charGeoGuideGrp):
				_fur_ = self.findUnconnectFur()
				if not _fur_:
					furnode = furAsset.reference(namespace=nsGeo,force=True)
					if not furnode:
						return
					furGeoGuideGrp = self.getFurGeoguideGroup(furnode)

				print('_grp_',charGeoGuideGrp)
				print('_fur_',furGeoGuideGrp)

				self.blendShape(charGeoGuideGrp,furGeoGuideGrp)
				print('Connect Fur:',charGeoGuideGrp+" -> "+furGeoGuideGrp)

class XgImportGroom:
	def __init__( self ):
		self.mayascene = MayaScene()

	@property
	def charAnimData( self ):
		""" return - dict() is { assetName:assetName_rig.ma, }
		"""
		if not self.mayascene.shot:
			return

		charDict = {}
		jsonFile = self.mayascene.cachedAnimJsonFile
		if not os.path.exists( jsonFile):
			raise TypeError("Not found data/json_cache!") 

		with open( jsonFile,'r' ) as f:
			datastore = json.load(f)

		objData = ''

		if type( datastore['object'] ) == dict:
			objData = datastore['object']['obj']

		else:
			objData =  datastore['object']

		for ds in objData:
			if ds.values():
				path = ds.values()[0]
				if os.path.isfile( path ):

					assetObj = FileAssetInfo( path )
					if assetObj.category.startswith( 'char' ):
						name = assetObj.asset
						if name:
							print( '# name:', name)
							charDict.update( { name:path } )

				else:
					print( '# not found asset path:', path )

		return charDict

	def importGroom( self, assetList ):
		print('Import')
		cacheAnimData = self.charAnimData
		for assetName in assetList:
			assetPath = cacheAnimData[ assetName ]
			charHelper = CharacterHelper( assetName, assetPath )
			charHelper.initializeMatchFurData( self.mayascene.projectPublishDir, 
											self.mayascene.episode )
			charHelper.connectFur()

#######################################
#
# Main Function
#
def xgImportGroom():

	app = QtWidgets.QApplication.activeWindow()
	widget = app.findChild( QtWidgets.QDialog, "XgImportGroomDialog" )
	if widget:
		widget.close()
		widget.deleteLater()
	
	xgUtil = XgImportGroom()

	dialog = XgImportGroomDialog(parent=app)
	dialog.setObjectName("XgImportGroomDialog")
	dialog.importGroomClicked.connect( lambda arg:xgUtil.importGroom(arg) )

	for charName in xgUtil.charAnimData.keys():
		dialog.addGroomItem(charName)
	
	dialog.show()
	return dialog