#######################################
#
#	@ 2021
#	Written by Jutamas (Nook) Pramote
#
#######################################
# Stand Library
import re
import os
import json

class MapFurAssetJson:
	""" Some project has fur initialize files to use a other fur asset file
		That file contain main asset with a list of related asset 
		For example:
			{
				'charKellyAvA':['charKellyAvB','charKellyAvC','charKellyAvD'],
				'charKellyBvA':['charKellyBvA','charKellyBvB'],
				'charMaximAvA':['charMaximAvB'],
			}
			In this case, charKellyAvB has to use a charKellyAvA's fur file.
	"""
	
	def __init__( self, jsonfile ):
		
		if not os.path.isfile( jsonfile ):
			raise IOError( jsonfile + " is not exists." )

		if not jsonfile.endswith( ".json" ):
			raise ValueError("Must be JSON file")

		self.__jsonfile = jsonfile

	@property
	def jsonfile( self ):
		print( self.__jsonfile )
		return self.__jsonfile
		
	@property
	def mapData( self ):

		fur_special = {}
		with open(self.__jsonfile ,'r') as jfile:
			fur_special = json.load(jfile)    

		return fur_special
	
	def mapAssetKey( self, assetName ):
		mainFurAsset = ''
		if self.mapData:
			for assetKey in self.mapData.keys():
				if assetName in self.mapData[ assetKey ]:
					print( "Use {}'s fur file".format( assetKey ) )
					mainFurAsset = assetKey
					break
		else:
			""" In case asset name out of a list will automatic find
				main variation asset, A is main
			""" 
			if re.search('Av[B-Z]$',assetName):
				mainFurAsset = re.sub('[B-Z]$','A',assetName)

		return mainFurAsset

