#! c:\python27\python.exe
#######################################
#
# 	@ 2021
# 	Written by Jutamas (Nook) Pramote
#
#######################################
# Stand Library

# GUI
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except:
    from PySide2 import QtWidgets, QtCore, QtGui


class XgImportGroomDialog(QtWidgets.QDialog):

    importGroomClicked = QtCore.Signal(list)

    def __init__(self, parent=None):
        super(XgImportGroomDialog, self).__init__(parent)
        self.setWindowTitle('Import Groom')
        self.setWindowFlags(QtCore.Qt.Tool)

        self.layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.layout)

        self.initializeWidget()
        self.initializeLayout()

        self.resize(300, 300)

    def initializeWidget(self):

        self.importAllCheckBox = QtWidgets.QCheckBox("Select All")
        self.groomListWidget = QtWidgets.QListWidget()
        self.importButton = QtWidgets.QPushButton("Import")
        self.importButton.clicked.connect(self.importGroom)

        self.importAllCheckBox.setCheckState(QtCore.Qt.Checked)
        self.importAllCheckBox.stateChanged.connect(self.setCheckStateGroom)

    def initializeLayout(self):

        layout = QtWidgets.QVBoxLayout()
        self.layout.addLayout(layout)
        layout.addWidget(self.importAllCheckBox)
        layout.addWidget(self.groomListWidget)
        layout.addWidget(self.importButton)

    @QtCore.Slot(str)
    def addGroomItem(self, name):
        item = QtWidgets.QListWidgetItem(name)
        item.setCheckState(QtCore.Qt.Checked)
        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
        self.groomListWidget.addItem(item)

    def importGroom(self):
        count = self.groomListWidget.count()
        groomList = []
        for row in range(count):
            item = self.groomListWidget.item(row)
            if item.checkState():
                groomList.append(item.text())

        self.importGroomClicked.emit(groomList)

    def setCheckStateGroom(self, state):
        count = self.groomListWidget.count()
        for row in range(count):
            item = self.groomListWidget.item(row)
            if state:
                item.setCheckState(QtCore.Qt.Checked)
            else:
                item.setCheckState(QtCore.Qt.Unchecked)
