######################################
#
#	Written by Jutamas (Nook) Pramote
# 
#######################################
# Stand Library
import sys
import os
import shutil
import subprocess
import codecs
import re

#######################################
import info.fileInfo as fi
from sceneutils import cleanJunkNode

import xgCurveGuideExport
import xgGeoGuideExport

#######################################
# Maya Library
import maya.cmds as mc
import maya.mel as mel
import xgenm.xgGlobal as xgg
import xgenm as xg

#######################################
# Helper Function
def repathAllObjectAttrs( palette='', xgDataPath='' ):
	for description in xg.descriptions( palette ):
		descriptionDir = xgDataPath + '/' + description
		des_objs = xg.objects( palette, description )
		
		for dobj in des_objs:
			for dobj_attr in xg.allAttrs( palette, description, dobj ):
				current = xg.getAttr( dobj_attr, palette, description, dobj )

				if '${DESC}' in current:

					value = current.replace( '${DESC}', descriptionDir )
					xg.setAttr( dobj_attr, value, palette, description, dobj )
				
		for fxmobj in xg.fxModules( palette, description ):
			for fxmobj_attr in xg.allAttrs( palette, description, fxmobj ):
				current = xg.getAttr( fxmobj_attr, palette, description, fxmobj )

				if '${DESC}' in current:

					value = current.replace( '${DESC}', descriptionDir )
					xg.setAttr( fxmobj_attr, value, palette, description, fxmobj )			

def initDescription( palette ):
	for description in xg.descriptions( palette ):
		
		__xgSetAttr( palette=palette, description=description, attr='percent', value='10', obj='GLRenderer' )

		if mc.getAttr("defaultRenderGlobals.currentRenderer") == 'redshift':
			__xgSetAttr( palette=palette, description=description, attr='renderer', value='Redshift', obj='RendermanRenderer' )	

		elif mc.getAttr("defaultRenderGlobals.currentRenderer") == 'arnold':
			__xgSetAttr( palette=palette, description=description, attr='renderer', value='Arnold', obj='RendermanRenderer' )	

def __xgSetAttr( palette='', description='', attr='', value='', obj='' ):
	
	current = xg.getAttr( attr, palette, description, obj )
	changeValue = current != value
	if changeValue:
			xg.setAttr( attr, value, palette, description, obj )

	return changeValue

def setAttrCmd( object, attr, value ):
	""" Set attribute value through an undoable command. Returns True if the value was changed"""         
	current = mc.getAttr( object, attr )
	changeValue = current != value
	if changeValue:
		# don't set the attribute if value is different than the current value
		xg.setAttr( object, attr, value )
	return changeValue

def checkUpdatePreviewAuto( state ):
	xgg.Playblast= state
	if ( xgg.Maya and ((xgg.Playblast and not mc.objExists('xgmRefreshPreview')) or (not xgg.Playblast and not mc.objExists('xgmPreviewWarning'))) ):
		# this will avoid logging xgmAddExpressions on the undo stack when it's not necessary
		mel.eval("xgmAddExpressions")
	
def exportPalettes( colXgenWorkDir, colXgenPubDir ):
		
	for palet in xg.palettes():
		# fix pathch name
		dstXgen= '{}/{}'.format( colXgenPubDir, palet ) 
		print( "# Export palette" ) 
		print ( colXgenWorkDir+'/'+palet , dstXgen )
		shutil.copytree( colXgenWorkDir+'/'+palet , dstXgen )
		__xgSetAttr( palette=palet, attr="xgDataPath", value=dstXgen )

		xg.fixPatchNames( palet )

		initDescription( palet )
		repathAllObjectAttrs( palet, dstXgen )

def newPublishVersionDir( stepPublishDir, versionFound ):

	verfurPubDir = '{}/v001'.format( stepPublishDir )
	if versionFound:
		newVer = 'v{:03d}'.format( int(versionFound[-1][1:])+1 )
		verfurPubDir = '{}/{}'.format( stepPublishDir, newVer )
	
	return verfurPubDir

def changeXgenToFullPathInMayaFile( maFile ):

	temp = maFile+'.repath'
	# Prepair output file is empty file
	f = open( temp, 'w+')
	f.write('')
	f.close()

	encodec = 'ascii'

	with codecs.open( maFile, 'r', encoding=encodec, errors='ignore' ) as reader, open( temp, 'w+') as writer:

		line = reader.read()
		for xgenFile in re.findall( '"(\w+\.xgen)"',line ):
			line = line.replace( xgenFile, "{}/{}".format( os.path.dirname( maFile ), xgenFile ) )

		line = line.replace("\r\n",'')

		writer.write( line )
		writer.close()

	if os.path.exists(maFile):
		os.remove(maFile)
		
	os.rename( temp, maFile )

#######################################
# Main
def publish():
	result = mc.confirmDialog( title='Confirm', message='Are you sure to publish?', button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No' )
	if result == "No":
		return

	#######
	# Check if working in work directory
	workScene = mc.file(q=True,loc=True)
	assetInfo = fi.FileAssetInfo()
	if assetInfo.invalid:
		mel.eval('print "Open work file before!"')
		return

	#######
	# Save current file first
	# check if there are unsaved changes
	# fileCheckState = mc.file(q=True, modified=True)
	# if fileCheckState:
	# 	mc.file( save=True, type='mayaAscii' )

	#######
	# Prepare folder in publish directory
	furWorkDir = assetInfo.stepWorkDir
	verfurPubDir = newPublishVersionDir( assetInfo.stepPublishDir, 
												assetInfo.allVersionsPublish( full_path=False ) )

	publishMaya = "{}/{}_{}.ma".format( verfurPubDir, assetInfo.asset, assetInfo.step )

	colXgenWorkDir = furWorkDir+'/maya/xgen/collections'
	colXgenPubDir = verfurPubDir+'/xgen/collections'

	if not os.path.exists( colXgenPubDir ):
		os.makedirs(colXgenPubDir)

	#######
	# Export palettes
	exportPalettes( colXgenWorkDir, colXgenPubDir )

	#######	
	# Clean up
	cleanJunkNode.SceneCleanup().run()
	mc.delete( mc.ls(type="file") )

	#######
	# Show only palettes
	otherTrns = list(filter(lambda trn: trn not in xg.palettes() ,mc.ls(assemblies=True)))
	if otherTrns:
		mc.hide( otherTrns )

	#######
	# Turn off Update Preview Auto
	checkUpdatePreviewAuto(False)

	#######
	# Save _fur.ma in publish directory
	mc.file( rename=publishMaya )
	mc.file( save=True, type='mayaAscii' )

	#######
	# Export curveguide
	print("# Export curveguide")
	maCurveGuide = "{}/{}_curveguide.ma".format( verfurPubDir, assetInfo.asset )
	xgCurveGuideExport.exportGuideToCurves( assetInfo.asset ,maCurveGuide )

	######
	# Export geoguide
	print("# Export geoguide")
	maGeoGuide = "{}/{}_geoguide.abc".format( verfurPubDir, assetInfo.asset )
	xgGeoGuideExport.exportGeoGuide( assetInfo.asset, maGeoGuide )

	#######
	# Reopen work / original file
	print("Open",workScene)
	mc.file( workScene, open=True, force=True )

	#######
	# Open publish version folder
	if os.path.exists( verfurPubDir ):
		subprocess.Popen('explorer "{}"'.format( os.path.realpath( verfurPubDir ) ) ,shell=False )
	else:
		mc.confirmDialog( title='Inform', message='Publish failed!')

