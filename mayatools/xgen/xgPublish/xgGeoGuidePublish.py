######################################
#
#	Written by Jutamas (Nook) Pramote
# 
#######################################
# Standard Library
import os
import subprocess

#######################################
import info.fileInfo as fi

import xgGeoGuideExport

#######################################
# Maya Library
import maya.cmds as mc
import maya.mel as mel


#######################################
# Main Function
def publish():
	print("# Export geoguide")
	verfurPubDir = os.path.dirname( mc.file( q=True ,loc=True ) )
	assetInfo = fi.FileAssetInfo()
	maGeoGuide = "{}/{}_geoguide.abc".format( verfurPubDir, assetInfo.asset )
	xgGeoGuideExport.exportGeoGuide( assetInfo.asset, maGeoGuide )

	#######
	# Open publish version folder
	if os.path.exists( maGeoGuide ):
		subprocess.Popen('explorer "{}"'.format( os.path.realpath( verfurPubDir ) ) ,shell=False )
	else:
		mc.confirmDialog( title='Inform', message='Publish failed!')

	mel.eval('print "Result: {}"'.format( maGeoGuide) )