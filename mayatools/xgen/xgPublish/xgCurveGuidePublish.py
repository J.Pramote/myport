######################################
#
#	Written by Jutamas (Nook) Pramote
# 
#######################################
# Standard Library
import os
import subprocess

#######################################
# YGG Module
import info.fileInfo as fi

import xgCurveGuideExport

#######################################
# Maya Library
import maya.cmds as mc
import maya.mel as mel

#######################################
# Main Function
def publish():
	print("# Export curveguide")
	verfurPubDir = os.path.dirname( mc.file( q=True ,loc=True ) )
	assetInfo = fi.FileAssetInfo()
	maCurveGuide = "{}/{}_curveguide.ma".format( verfurPubDir, assetInfo.asset )
	result = xgCurveGuideExport.exportGuideToCurves( assetInfo.asset ,maCurveGuide )
	mc.delete( result )

	#######
	# Open publish version folder
	if os.path.exists( maCurveGuide ):
		subprocess.Popen('explorer "{}"'.format( os.path.realpath( verfurPubDir ) ) ,shell=False )
	else:
		mc.confirmDialog( title='Inform', message='Publish failed!')

	mel.eval('print "Result: {}"'.format( maCurveGuide) )