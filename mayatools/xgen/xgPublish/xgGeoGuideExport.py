######################################
#
#	Written by Jutamas (Nook) Pramote
# 
#######################################
# Standard Library
import re

#######################################
# Maya Library
import maya.cmds as mc

#######################################
# Helper Function
def exportAbc( start=1, end=1, rootObj='', filePath='' , attrList=[] ):

	jobAbc =  "-frameRange {start} {end} {attribute} "
	jobAbc += "-writeVisibility -dataFormat ogawa "
	jobAbc += "-root {rootObj} -file {filePath} "

	attrList = attrList + ['visibility']
	attrAbc = ' '.join(["-attr "+a for a in attrList])

	data = { 'start' : 1, 
			'end' : 1, 
			'attribute': attrAbc,
			'rootObj': rootObj,
			'filePath': filePath }
			
	jobAbc = jobAbc.format(**data)
	print( jobAbc )
	mc.AbcExport(j=jobAbc)

def renameChildrenNode( oldName ,newName ,rootGrp ):
	print( oldName ,newName ,rootGrp)
	descendentList = mc.listRelatives(rootGrp,ad=True,typ='transform')
	for target in descendentList:
		newName = target.replace( oldName+'_', newName+'_'  )
		mc.rename( target, newName ) 

#######################################
# Main
def exportGeoGuide( assetName, exportPath ):
	postFix = "_geoguide_0001_GRP"
	currentGeoGuideGrp = mc.ls("|*"+postFix )
	
	if currentGeoGuideGrp:
		currentAssetName = re.findall( "^(\w+)"+postFix, currentGeoGuideGrp[0] )[0]
		namingGeoGuideGrp = '|{}{}'.format( assetName, postFix )
		mc.rename( currentGeoGuideGrp[0], namingGeoGuideGrp )
		renameChildrenNode( currentAssetName, assetName, namingGeoGuideGrp )		
		exportAbc(  rootObj=namingGeoGuideGrp, filePath=exportPath )
