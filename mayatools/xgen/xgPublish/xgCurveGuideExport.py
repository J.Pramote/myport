######################################
#
#	Written by Jutamas (Nook) Pramote
# 
#######################################
# Standard Library
import re
#######################################
# Maya Library
import maya.cmds as mc
import maya.mel as mel
import xgenm as xg

#######################################
# Helper Function
def listAllPalettes():
	palettes = xg.palettes()
	return palettes

def listDescription( palette='' ):
	if palette:
		return xg.descriptions( palette )
	else:
		desList = []
		for palette in listAllPalettes():
			des = xg.descriptions( palette )
			if des:
				desList += des
		return desList

def renameDescriptionCurves( curvePrefix, curveList ):
	
	curveName = curvePrefix+"_{:04d}_CUV"
	for num in range( len( curveList )):
		naming = curveName.format(num+1)
		mc.rename( curveList[num], naming )

def renameCollectionCurvesGrp( curvePrefix, curveGrp ):
	namingCurveGrp = curvePrefix+"_GRP"

	return mc.rename( curveGrp, namingCurveGrp )

#######################################
# Main
def exportGuideToCurves( assetName ,exportPath ):
	curvGrpList = []
	for palette in xg.palettes():
		currentAssetName = re.findall("^(\w+)_\w+_CLT?" , palette )[0]
		for description in listDescription( palette ):
			mc.select( description )
			# Create Curves
			newCurvesList = mel.eval( "xgmCreateCurvesFromGuides {guideStat} {lockLength}".format( guideStat=0,lockLength=0 ) )
			newCurvesGrp = mel.eval('listTransforms "{}";'.format( newCurvesList[0] ))

			# Naming
			namingDescription = description.replace( currentAssetName+'_', assetName+'_' )
			curvePrefix = namingDescription.replace("_DCT","")

			renameDescriptionCurves( curvePrefix, newCurvesList )
		
			curvGrpList.append( renameCollectionCurvesGrp( curvePrefix, newCurvesGrp ) )

	mc.delete( "xgGroomLayer" )
	# Rename curveguide root
	curvPaletName = assetName + "_curveguide_0001_GRP"  # It does't make sense to count, for what?
	curvePaletGrp = mel.eval('listTransforms "{}";'.format( curvGrpList[0] ))	

	curvPaletName =  mc.rename( curvePaletGrp[0], curvPaletName )

	mc.select( curvPaletName )
	#
	mc.file( exportPath,f=True, options="v=0;", typ="mayaAscii", pr=True, es=True )
	return curvPaletName
