#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, Mar 2022
##
################################################################################
##
##  Standard Library
##
import os
import re


################################################################################
##
##  Maya Lib
##
import maya.cmds as cmds


################################################################################
##
##  Snow Module
##
from snow.maya.lib.build_scene import lookdev
from snow.common.lib import config
import snow.maya.lib.wrap as m_wrap
from snow.maya.lib import attributes as sn_attr


def list_loop_task_folder(hero_loop_dir, task='', seperate_frame=False):
    """List task publish folder in pattern '{task}_{frame}'

    Args:
        hero_loop_dir (str): root path to find
        task (str, optional): task name. Defaults to ''.
        seperate_frame (bool, optional): split task and frame from folder name. 
            Defaults to False.

    Returns:
        list: folder name list
    """
    result = []
    all_folder = sorted(os.listdir(hero_loop_dir))
    for fol in all_folder:
        found_task = re.findall(config.folder_regx('asset_loop_task'), fol)
        found_frame = re.findall(config.folder_regx('asset_loop_frame'), fol)

        if found_task and seperate_frame:
            fol = (found_task[0], found_frame[0])
    
        if found_task and fol not in result:
            if not task:
                result.append(fol)
            else:
                if task == found_task[0]:
                    result.append(fol)

    return result


def get_exists_locator(root, prefix):
    """Get exists locators

    Args:
        root (str): root of locators
        prefix (str): locator prefix

    Returns:
        list: locators include long path
    """
    loc_regx = '{root}|{prefix}_*_Loc'.format(root=root, prefix=prefix)
    return cmds.ls(loc_regx, l=True)


def locator_name(root, prefix):
    """Create locator name

    Args:
        root (str): root of locators
        prefix (str): locator prefix

    Returns:
        str: locator name and increase number
    """
    pattern = '{prefix}_{number:03d}_Loc'
    found = get_exists_locator(root, prefix)
    if not found:
        return pattern.format(prefix=prefix, number=1)
    
    num_exists = []
    number_regx = re.compile(r'_(\d{3})_Loc')
    for loc in found:
        num_found = number_regx.findall(loc)
        num_exists.append(int(num_found[0]))

    increase_num = sorted(num_exists)[-1] + 1
    return pattern.format(prefix=prefix, number=increase_num)


def create_group(name, parent=''):
    """Create new group except exists group

    Args:
        name (str): group name
        parent (str, optional): group parent. Defaults to ''.

    Returns:
        str: new group name
    """
    cmds.select(cl=True)
    sep_path = name.split('|')

    for index in list(range(len(sep_path))):
        current = '|'.join(sep_path[:index+1])
        if not sep_path[index] or cmds.objExists(current):
            if current:
                cmds.select(current)
            continue

        sel = cmds.ls(sl=True)
        name = sep_path[index]
        new_grp = cmds.group(n=name, em=True)
        if sel:
            cmds.parent(new_grp, sel[0])

    group_sel = cmds.ls(sl=True)
    if group_sel:
        if parent:
            cmds.parent(group_sel, parent)
        return group_sel[0]


def import_abc(file, parent='', namespace=''):
    """Import alembic file

    Args:
        file (str): file path
        parent (str, optional): locator name. Defaults to ''.
        namespace (str, optional): namespace. Defaults to ''.
    """
    m_wrap.abc.refernce(file, namespace=namespace)
    abc_node = m_wrap.abc.list_abc_node(file)
    # -- anim loop time --
    cmds.setAttr('{}.cycleType'.format(abc_node[0]), 1)
    # -- assign shader --
    no_assigned_objects, invalid_shade_paths = lookdev.ref_and_assign_shade(ref_file=file, look_type='preview', name_space=namespace)
    if parent:
        abc_obj = m_wrap.abc.Alembic(str(file))
        root_grp = '{}:{}'.format(namespace, abc_obj.get_root())
        cmds.parent(root_grp, parent)


def import_gpu(file, parent='', namespace=''):
    """Import GPU file

    Args:
        file (str): file path
        parent (str, optional): locator name. Defaults to ''.
        namespace (str, optional): namespace. Defaults to ''.
    """
    node = m_wrap.gpu._import(file, name=namespace)
    if parent:
        cmds.parent(node, parent)


def import_rsproxy(file, parent='', namespace=''):
    """Import RsProxy file

    Args:
        file (str): file path
        parent (str, optional): locator name. Defaults to ''.
        namespace (str, optional): namespace. Defaults to ''.
    """
    node = m_wrap.redshift._import_rs_prx(
                                        path=file, name=namespace,
                                        use_sequence=True, display_mode=1)
    if parent:
        cmds.parent(node, parent)


def create_locator(name, attr_data):
    """Create locator

    Args:
        name (str): locator name
        attr_data (dict): data to set attribute

    Returns:
        list: result of create locator command
    """
    loc = cmds.spaceLocator(name=name)
    sn_attr.add('|' + loc[0], 'snow__pub_location', attr_data['publish'])
    sn_attr.add('|' + loc[0], 'snow__pub_alembic', attr_data['Alembic'])
    sn_attr.add('|' + loc[0], 'snow__pub_gpu', attr_data['GPU'])
    sn_attr.add('|' + loc[0], 'snow__pub_rsproxy', attr_data['RsProxy'])
    return loc


def set_position(obj_list, origin=[0, 0, 0]):
    """Align objects to avoid a conflict position

    Args:
        obj_list (list): Object list
        origin (list, optional): origin to align position [x, y, z]. Defaults to [0, 0, 0].
    """
    sum_width = 0
    sum_dept = 0
    offset_space = 1
    # -- get object summery width and dept
    for obj in obj_list:
        bb = cmds.xform(obj , q=True, ws=True, bb=True)
        # -- get center transform only head and end object
        # and plus offset unless last object --
        width = (bb[3] - bb[0])
        if 0 < obj_list.index(obj) < (len(obj_list)-1):
            width += offset_space
        elif obj_list.index(obj) == 0:
            width = (width/2) + offset_space

        dept = bb[5] - bb[2]
        sum_width += width
        sum_dept += dept
        
    middle_width = float(sum_width/2)
    area_length_x = [-middle_width, middle_width]
    index = 0
    max_dept = 0
    z = origin[2]
    y = origin[1]
    x = area_length_x[0] + origin[0]
    for obj in obj_list:
        bb = cmds.xform(obj , q=True, ws=True, bb=True )
        width = bb[3] - bb[0]
        dept = bb[5] - bb[2]
        cmds.setAttr('{}.tx'.format(obj), x)
        cmds.setAttr('{}.tz'.format(obj), z)
        cmds.setAttr('{}.ty'.format(obj), y)
        x += width + offset_space

        if dept > max_dept:
            max_dept = dept
            
        index += 1
