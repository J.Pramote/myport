#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, Mar 2022
##
################################################################################
##
##  Standard Library
##
try:
    from PySide2 import QtWidgets, QtCore, QtGui
except ImportError:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets


################################################################################
##
##                             Custom Widget Classes
##
class Widget(QtCore.QObject):
    def __init__(self):
        super(Widget, self).__init__()
        regx_digit = QtGui.QRegExpValidator(r'\d+')
        self.deadline_lb = QtWidgets.QLabel('No')
        self.start_le = QtWidgets.QLineEdit()
        self.start_le.setValidator(regx_digit)
        self.end_le = QtWidgets.QLineEdit()
        self.end_le.setValidator(regx_digit)
        self.start_loop_le = QtWidgets.QLineEdit()
        self.start_loop_le.setPlaceholderText('1,5,8,..')
        regx_validator = QtGui.QRegExpValidator(r'^(\d+,)*')
        self.start_loop_le.setValidator(regx_validator)
        self.publish_pb = QtWidgets.QPushButton('Publish')


class AssetLoopPublishWindow(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super(AssetLoopPublishWindow, self).__init__(parent)
        self.setWindowTitle('Asset Loop Publish')

        self.widget = Widget()

        QtWidgets.QVBoxLayout(self)
        self.layout().setAlignment(QtCore.Qt.AlignTop)
        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignLeft)
        self.layout().addLayout(layout)
        label = QtWidgets.QLabel('Deadline installed:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.deadline_lb)

        line = QtWidgets.QFrame(self)
        line.setFrameShape(line.HLine)
        line.setFrameShadow(line.Sunken)
        line.setLineWidth(1)
        self.layout().addWidget(line)

        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignLeft)
        self.layout().addLayout(layout)
        label = QtWidgets.QLabel('Framerange:')
        # label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.start_le)
        layout.addWidget(self.widget.end_le)

        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignLeft)
        self.layout().addLayout(layout)
        label = QtWidgets.QLabel('Start Loop:')
        # label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.start_loop_le)

        self.layout().addWidget(self.widget.publish_pb)

