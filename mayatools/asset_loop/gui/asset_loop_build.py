#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, Mar 2022
##
################################################################################
##
##  Standard Library
##
import sys
import os

try:
    from PySide2 import QtWidgets, QtCore, QtGui
except ImportError:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets


from snow.common.lib import path_elem


class TransformWidget(QtWidgets.QWidget):
    def __init__(self, label='', x=0, y=0, z=0):
        super(TransformWidget, self).__init__()
        # self.setContentsMargins(0, 0, 0, 0)
        layout = QtWidgets.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        label = QtWidgets.QLabel(label)
        layout.addWidget(label)
        self.x = QtWidgets.QLineEdit()
        self.x.setText(str(x))
        self.x.setFixedWidth(80)
        self.y = QtWidgets.QLineEdit()
        self.y.setText(str(y))
        self.y.setFixedWidth(80)
        self.z = QtWidgets.QLineEdit()
        self.z.setText(str(z))
        self.z.setFixedWidth(80)

        layout.addWidget(self.x)
        layout.addWidget(self.y)
        layout.addWidget(self.z)


class AssetLoopLibWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(AssetLoopLibWindow, self).__init__(parent)
        self.setWindowTitle('Anim Loop Asset')
        self.setLayout(QtWidgets.QVBoxLayout())

        self.project_cbb = QtWidgets.QComboBox()
        self.project_cbb.addItem('--select--')
        self.project_cbb.addItems(sorted(path_elem.get_all_project_code()))
        self.type_cbb = QtWidgets.QComboBox()
        self.subtype_cbb = QtWidgets.QComboBox()

        self.image_listwidget = QtWidgets.QListWidget()
        self.image_listwidget.setMovement(QtWidgets.QListView.Static)
        self.image_listwidget.setViewMode(QtWidgets.QListView.IconMode)
        self.image_listwidget.thumbnail_size = QtCore.QSize(150, 150)
        self.file_type_cbb = QtWidgets.QComboBox()
        self.file_type_cbb.addItems(['Alembic', 'GPU', 'RsProxy'])
        self.loop_lw = QtWidgets.QListWidget()
        self.rot_widget = self.transform_widget(label='Rotation:')
        self.scale_widget = self.transform_widget(label='Scale:', min=[1,1,1], max=[1,1,1])
        self.trans_widget = self.transform_widget(label='Rotation:')
        self.at_point_cbb = QtWidgets.QComboBox()
        self.at_point_cbb.addItems(['World Origin', 'Selected Object'])
        self.number_of_snb = QtWidgets.QSpinBox()
        self.number_of_snb.setMinimum(1)
        self.create_pb = QtWidgets.QPushButton('Create')

        self.initial_layout()
        self.resize(800, 600)

    def initial_layout(self):
        self.layout().setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)

        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignLeft)
        self.layout().addLayout(layout)
        label = QtWidgets.QLabel('Project:')
        layout.addWidget(label)
        layout.addWidget(self.project_cbb)
        label = QtWidgets.QLabel('Type:')
        layout.addWidget(label)
        layout.addWidget(self.type_cbb)
        label = QtWidgets.QLabel('Subtype:')
        layout.addWidget(label)
        layout.addWidget(self.subtype_cbb)

        # layout = QtWidgets.QHBoxLayout()
        splitter = QtWidgets.QSplitter()
        splitter.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        # splitter.setSizes([200, 600])
        self.layout().addWidget(splitter)
        # self.layout().addLayout(layout)
        splitter.addWidget(self.image_listwidget)
        self.image_listwidget.setMinimumWidth(500)
        splitter.addWidget(self.option_widget())

    def option_widget(self):
        widget = QtWidgets.QWidget()
        QtWidgets.QVBoxLayout(widget)

        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignCenter)
        widget.layout().addLayout(layout)
        label = QtWidgets.QLabel('File Type:')
        layout.addWidget(label)
        layout.addWidget(self.file_type_cbb)

        widget.layout().addWidget(self.loop_lw)
        
        # group = QtWidgets.QGroupBox('Transform Random')
        # widget.layout().addWidget(group)
        # group_layout = QtWidgets.QVBoxLayout(group)
        # scroll_area = QtWidgets.QScrollArea()
        # scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        # scroll_area.setWidget(self.transform_random_widget())
        # group_layout.addWidget(scroll_area)

        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignCenter)
        widget.layout().addLayout(layout)
        label = QtWidgets.QLabel('Create at point:')
        layout.addWidget(label)
        layout.addWidget(self.at_point_cbb)

        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignCenter)
        widget.layout().addLayout(layout)
        label = QtWidgets.QLabel('Number:')
        layout.addWidget(label)
        layout.addWidget(self.number_of_snb)

        widget.layout().addWidget(self.create_pb)
        return widget

    def transform_random_widget(self):
        widget = QtWidgets.QWidget()
        # widget = QtWidgets.QGroupBox('Transform Random')
        # widget.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding )
        # widget.setFixedWidth(200)
        QtWidgets.QVBoxLayout(widget)
        widget.layout().setAlignment(QtCore.Qt.AlignTop)
        widget.layout().addWidget(self.rot_widget)
        widget.layout().addWidget(self.scale_widget)
        widget.layout().addWidget(self.trans_widget)
        # widget.adjustSize()

        return widget
    
    def transform_widget(self, label='', min=[0, 0, 0], max=[0, 0, 0]):
        widget = QtWidgets.QWidget()
        widget.setContentsMargins(0, 0, 0 ,0)
        QtWidgets.QVBoxLayout(widget)
        widget.layout().setAlignment(QtCore.Qt.AlignTop)

        label = QtWidgets.QLabel(label)
        widget.layout().addWidget(label)
        widget.min = TransformWidget(label='Min:', x=min[0], y=min[1], z=min[2])
        # widget.min.layout().setAlignment(QtCore.Qt.AlignLeft)
        widget.layout().addWidget(widget.min)
        widget.max = TransformWidget(label='Max:', x=max[0], y=max[1], z=max[2])
        # widget.max.layout().setAlignment(QtCore.Qt.AlignLeft)
        widget.layout().addWidget(widget.max)

        return widget

    def create_image_widget(self, item, image_path, label=''):
        widget = QtWidgets.QWidget()
        widget.setLayout(QtWidgets.QVBoxLayout())
        widget.layout().setAlignment(QtCore.Qt.AlignCenter)
        widget.setStyleSheet('padding: 0px;'
                            'background: none;'
                            'border: none;')

        image_label = QtWidgets.QLabel()
        movie = QtGui.QMovie(image_path)
        movie.setScaledSize(self.image_listwidget.thumbnail_size)
        image_label.setMovie(movie)
        movie.start()
        widget.layout().addWidget(image_label)
        if not label:
            label = os.path.basename(image_path)
        name_label = QtWidgets.QLabel(label)
        name_label.setAlignment(QtCore.Qt.AlignCenter)
        widget.layout().addWidget(name_label)
        widget.setToolTip(name_label.text())
        return widget

    def add_preview_item(self, image_path, label='', data=''):
        item = QtWidgets.QListWidgetItem()
        item.setSizeHint(self.image_listwidget.thumbnail_size)
        self.image_listwidget.addItem(item)
        widget = self.create_image_widget(item, image_path, label=label)
        self.image_listwidget.setItemWidget(item, widget)
        item.setData(QtCore.Qt.UserRole, data)
        item.setToolTip(label)

