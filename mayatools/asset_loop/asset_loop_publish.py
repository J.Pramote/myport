#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, Mar 2022
##
################################################################################
##
##  Standard Library
##
try:
    from PySide2 import QtWidgets, QtCore, QtGui
except ImportError:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets


################################################################################
##
##  Snow Modules
##
from snow.common.lib.widget import message_box
import snow.maya.lib.wrap as m_wrap
import snow.deadline as deadline
from snow.common.lib import asset_loader
import snow.shotgrid.lib.wrap as sg_wrap
from snow.common.lib import code_logging

################################################################################
##
##  Relative Modules
##
import gui.asset_loop_publish as gui
import publish_helper
import asset_loop_helper


################################################################################
##
##  Global Variables
##
from snow.maya.lib.widgets import Maya_Main_Window
__Object_ID__ = 'AssetLoopPublish'
LOG = code_logging.create_log(__name__)


class AssetLoopPublish(gui.AssetLoopPublishWindow):
    def __init__(self, parent=None):
        super(AssetLoopPublish, self).__init__(parent)

        current_scene = m_wrap.file_control.scene_name()
        self.asset_ld = asset_loader.AssetLoader(current_scene)

        self.widget.publish_pb.clicked.connect(self.publish)

        if deadline.DEADLINE_STATE:
            self.widget.deadline_lb.setText('Yes')

        start, end = m_wrap.playback.get_playback_range()
        self.widget.start_le.setText(str(start))
        self.widget.end_le.setText(str(end))
        self.set_start_loop_exported_frame()
    
    def set_start_loop_exported_frame(self):

        hero_loop_dir = self.asset_ld.get_loop_publish_dir()
        task_list = asset_loop_helper.list_loop_task_folder(hero_loop_dir=hero_loop_dir,
                                            task=self.asset_ld.task,
                                            seperate_frame=True)
        text = ','.join(str(int(t[1])) for t in task_list)
        self.widget.start_loop_le.setText(text)

    def publish(self):
        # -- framerange start/end validation --
        if not self.widget.start_le.text() or not self.widget.end_le.text():
            message_box.MessageBox(self).warning('Must input framerage')
            return
        if int(self.widget.start_le.text()) > int(self.widget.end_le.text()):
            message_box.MessageBox(self).warning('End frame must more than start frame')
            return

        start = int(self.widget.start_le.text())
        end = int(self.widget.end_le.text())

        # -- start loop validation --
        if not self.widget.start_loop_le.text():
            message_box.MessageBox(self).warning('Must input Start Loop')
            return
        else:
            start_loop = self.widget.start_loop_le.text().strip(',')
            start_loop = sorted(list(set(start_loop.split(','))))
            out_of_frames = [x for x in start_loop if not start <= int(x) <= end]
            if out_of_frames:
                message_box.MessageBox(self).warning(
                    'These frame out of range: {}'.format(','.join(out_of_frames)))
                return

        # -- prepare task --
        sg_task = sg_wrap.task.Task(self.asset_ld.project,
                                    entity_code=self.asset_ld.asset,
                                    code=self.asset_ld.task,
                                    step=self.asset_ld.dept)

        if not sg_task.entity:
            LOG.info('Create task: %s', self.asset_ld.task)
            entities_found = sg_task.downstream_project_entities(
                                            entity_type='Asset',
                                            filters=[['code','is',self.asset_ld.asset]])
            sg_task.create(code=self.asset_ld.task,
                                linked_entity=entities_found[0])

        # -- start publish --
        LOG.info('publish_helper.export_loop -- start_loop: %s', start_loop)
        publish_helper.export_loop(first_frame=start, end_frame=end, start_loop=start_loop)

        message_box.MessageBox(self).success()

################################################################################
##
##		Main Function
##
def main():
    
    if Maya_Main_Window:
        widget = Maya_Main_Window.findChild(QtWidgets.QWidget, __Object_ID__)
        if widget:
            widget.close()
            widget.deleteLater()

    widget = AssetLoopPublish(parent=Maya_Main_Window)
    widget.setObjectName(__Object_ID__)
    widget.setWindowFlag(QtCore.Qt.Window)
    widget.show()
