#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, Mar 2022
##
################################################################################
##
##  Standard Library
##
import sys
import os

try:
    from PySide2 import QtWidgets, QtCore, QtGui
except ImportError:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets


################################################################################
##
##  Maya Lib
##
import maya.cmds as cmds


################################################################################
##
##  Snow Modules
##
from snow.common.lib import asset_loader
from snow.common.lib import shot_loader
import snow.maya.lib.wrap as m_wrap
from snow.common.lib.widget import message_box

################################################################################
##
##  Relative Modules
##
import gui.asset_loop_build as gui
import asset_loop_helper


################################################################################
##
##  Global Variables
##
__Object_ID__ = 'AssetLoopBuild'
from snow.maya.lib.widgets import Maya_Main_Window


class AssetLoopBuild(gui.AssetLoopLibWindow):
    
    def __init__(self, parent=None):
        super(AssetLoopBuild, self).__init__(parent)
        self.project_cbb.currentIndexChanged.connect(self.get_asset_type)
        self.type_cbb.currentIndexChanged.connect(self.get_subtype_asset)
        self.subtype_cbb.currentIndexChanged.connect(self.get_hero_asset)
        self.image_listwidget.itemClicked.connect(self.get_sub_loop)
        self.create_pb.clicked.connect(self.create_asset)
        self.file_type_cbb.currentTextChanged.connect(self.filter_publish_file)

        scenename = m_wrap.file_control.scene_name()
        if scenename:
            shot_ld = shot_loader.ShotLoader(scenename)
            if shot_ld.project:
                self.project_cbb.setCurrentIndex(
                    self.project_cbb.findText(shot_ld.project, QtCore.Qt.MatchExactly))
                # self.type_cbb.setCurrentIndex(
                #     self.type_cbb.findText('Daemons', QtCore.Qt.MatchExactly))
                # self.subtype_cbb.setCurrentIndex(
                #     self.subtype_cbb.findText('extra', QtCore.Qt.MatchExactly))
        
        # TODO: random transform is disable widget now

    def get_asset_type(self, index):
        self.type_cbb.clear()
        if index <= 0:
            return
        asset_ld = asset_loader.AssetLoader()
        asset_ld.set_drive('work_repo')
        asset_ld.set_project(self.project_cbb.currentText())
        self.type_cbb.addItem('--select--')
        for name in sorted(os.listdir(asset_ld.get_root())):
            path = '{}/{}'.format(asset_ld.get_root(), name)
            if os.path.isdir(path):
                self.type_cbb.addItem(name, path)

    def get_subtype_asset(self, index):
        self.subtype_cbb.clear()
        if index <= 0:
            return
        asset_type_dir = self.type_cbb.currentData(QtCore.Qt.UserRole)
        self.subtype_cbb.addItem('--select--')
        for name in sorted(os.listdir(asset_type_dir)):
            path = '{}/{}'.format(asset_type_dir, name)
            if os.path.isdir(path):
                self.subtype_cbb.addItem(name, path)

    def get_hero_asset(self, index):
        self.image_listwidget.clear()
        if index <= 0:
            return
        asset_dir = self.subtype_cbb.currentData(QtCore.Qt.UserRole)
        asset_ld = asset_loader.AssetLoader()
        for name in sorted(os.listdir(asset_dir)):
            path = '{}/{}/anim'.format(asset_dir, name)
            asset_ld.set_path(path)
            hero_loop_dir = asset_ld.get_loop_publish_dir()
            if not os.path.exists(hero_loop_dir):
                continue
            
            # -- filter all task name --
            loop_dir_list = asset_loop_helper.list_loop_task_folder(
                                            hero_loop_dir=hero_loop_dir,
                                            seperate_frame=True)
            all_tasks = []
            first_loop_list = []
            for task, frame in loop_dir_list:
                if task not in all_tasks:
                    loop_asset = asset_loader.AssetLoader(path)
                    loop_asset.set_task(task)
                    all_tasks.append(task)
                    path = loop_asset.get_loop_publish_dir(frame=frame)
                    first_loop_list.append(path)

                    # -- get .gif from first frame task --
                    gif = loop_asset.get_loop_publish_file(
                                            pub_type='gif', frame=frame)
                    if os.path.exists(gif):
                        label = '{}_{}'.format(loop_asset.asset, task)
                        self.add_preview_item(gif,
                                            label=label,
                                            data=loop_asset)
 
    def get_sub_loop(self, item):
        self.loop_lw.clear()
        loop_asset = item.data(QtCore.Qt.UserRole)

        loop_dir_list = asset_loop_helper.list_loop_task_folder(
                                            hero_loop_dir=loop_asset.get_loop_publish_dir(),
                                            task=loop_asset.task)
        file_type = self.file_type_cbb.currentText()
        for loop in loop_dir_list:
            loop_hero = '{}/{}'.format(loop_asset.get_loop_publish_dir(), loop)
            rsproxy = os.path.basename(loop_asset.get_loop_publish_file(
                                                pub_type='rsproxy'))
            rsproxy = '{0[0]}.1001{0[1]}'.format(os.path.splitext(rsproxy))
            data = {
                'asset': loop_asset.asset,
                'task': loop_asset.task,
                'publish': loop_hero,
                'GPU': '{}/{}'.format(loop_hero, os.path.basename(
                                            loop_asset.get_loop_publish_file(
                                                pub_type='gpu'))),
                'Alembic': '{}/{}'.format(loop_hero, os.path.basename(
                                        loop_asset.get_loop_publish_file(
                                                pub_type='geo'))),
                'RsProxy': '{}/rs/{}'.format(loop_hero, rsproxy)
            }
            
            item = QtWidgets.QListWidgetItem(self.loop_lw)
            # item.setCheckState(QtCore.Qt.Unchecked)
            item.setText(loop)
            item.setData(QtCore.Qt.UserRole, data)

        self.filter_publish_file(file_type)
    
    def filter_publish_file(self, file_type):
        for item in self.loop_lw.findItems('', QtCore.Qt.MatchContains):
            data = item.data(QtCore.Qt.UserRole)
            if os.path.exists(data[file_type]):
                item.setHidden(False)
            else:
                item.setHidden(True)
    
    def group_arrangement(self, asset, task):
        main_grp = 'Loop_Grp'
        asset_grp = '{asset}_Grp'.format(asset=asset)
        task_grp = '{asset}_{task}'.format(asset=asset, task=task)
        
        dest_grp = '|{}|{}|{}'.format(main_grp, asset_grp, task_grp)
        if not cmds.objExists(dest_grp):
            asset_loop_helper.create_group(dest_grp)
        return dest_grp

    def create_asset(self):
        file_type = self.file_type_cbb.currentText()
        created_objects = []
        origin = [0, 0, 0]
        if self.at_point_cbb.currentText() == 'Selected Object':
            target_obj = cmds.ls(sl=True)
            if target_obj:
                origin = cmds.xform(target_obj[0], q=True, ws=True, t=True)
        
        sel_item = self.loop_lw.selectedItems()
        if not sel_item:
            message_box.MessageBox(self).error('Please select an item')
            return

        for item in sel_item:
            # item = self.loop_lw.item(row)
            # if item.checkState() != QtCore.Qt.Checked:
            #     continue
            number = self.number_of_snb.value()
            data = item.data(QtCore.Qt.UserRole)

            loc_grp = self.group_arrangement(asset=data['asset'], task=data['task'])
            prefix = '{asset}_{task_frame}'.format(asset=data['asset'], task_frame=item.text())
            exists_loc = asset_loop_helper.get_exists_locator(root=loc_grp, prefix=prefix)
            if not exists_loc:
                loc_name = asset_loop_helper.locator_name(root=loc_grp, prefix=prefix)
                locator = asset_loop_helper.create_locator(loc_name, data)
                cmds.parent(locator, loc_grp)
                created_objects.append(locator[0])
            
                # -- import first item --
                if file_type == 'Alembic':
                    asset_loop_helper.import_abc(data[file_type],
                                                parent=locator[0], namespace=prefix)
                elif file_type == 'GPU':
                    asset_loop_helper.import_gpu(data[file_type], parent=locator[0], namespace=prefix)
                elif file_type == 'RsProxy':
                    asset_loop_helper.import_rsproxy(data[file_type], parent=locator[0], namespace=prefix)
                number -= 1
            else:
                locator = exists_loc[0]
            
            instance_number = int(number)
            while instance_number:
                loc_name = asset_loop_helper.locator_name(root=loc_grp, prefix=prefix)
                new_locator = cmds.instance(locator, n=loc_name)
                created_objects.append(new_locator[0])
                instance_number -= 1
        
            # -- Align locator position --
            if len(created_objects) > 1:
                asset_loop_helper.set_position(created_objects, origin)

        message_box.MessageBox(self).success()


################################################################################
##
##		Main Function
##
def main():
    widget = Maya_Main_Window.findChild(QtWidgets.QWidget, __Object_ID__)
    if widget:
        widget.close()
        widget.deleteLater()

    widget = AssetLoopBuild(parent=Maya_Main_Window)
    widget.setObjectName(__Object_ID__)
    widget.setWindowFlag(QtCore.Qt.Window)
    widget.show()
