import os
import re
import getpass

import maya.cmds as cmds


################################################################################
##
##  Snow Module
##
import snow.deadline as deadline
from snow.common.lib import asset_loader
from snow.common.lib import config
import snow.maya.lib.wrap as m_wrap
from snow.maya.lib import viewport


def export_loop(first_frame=1, end_frame=1, start_loop=[], init_first_frame=1001, publish=True):
    """Export and publish asset anim loop

    Args:
        first_frame (int): First frame
        end_frame (int): End frame
        start_loop (list, optional): Origin frame to start loop list. Defaults to [].
        init_first_frame (int, optional): start frame to cache. Defaults to 1001.
        publish (bool, optional): Publish after export. Defaults to True.
    """
    publish_files =[]
    current_scene = m_wrap.file_control.scene_name()
    asset_ld = asset_loader.AssetLoader(current_scene)
    camera = 'persp'
    # -- setup viewport --
    obj=asset_ld.geo_grp()
    found_geogrp = cmds.ls('*:{}'.format(obj))
    cmds.xform(camera, ws=True, t=[28.0, 21.0, 28.0], ro=[-27.93835272960238, 45, 0.0])
    cmds.select(found_geogrp[0])
    cmds.viewFit(f=1)
    cmds.select(cl=True)
    vp_helper = viewport.ViewPortHelper()
    vp_helper.set_camera(camera)
    vp_helper.setup()

    # -- setup anim curve --
    m_wrap.animation.set_infinity(m_wrap.animation.list_anim_curve(), post="Cycle", pre="Cycle")
    if not start_loop:
        start_loop.append(first_frame)

    # -- data validation --
    start_loop = [int(s) for s in start_loop]
    if first_frame != init_first_frame and \
        first_frame not in start_loop:
        start_loop.insert(0, first_frame)

    # -- export --
    last_start_frame = init_first_frame
    for start in sorted(start_loop):
        offset = last_start_frame - start
        if offset:
            m_wrap.animation.offset_frame(offset)
        last_start_frame = start
        output_ma = asset_ld.get_loop_publish_file(pub_type='loop', frame=start)
        cmds.fileInfo('loop_origin', start)
        cmds.fileInfo('cache_loop_start', 1001)
        cmds.fileInfo('cache_loop_end', 1001+end_frame-1)
        m_wrap.file_control.save_as(output_ma)
        publish_files.append(output_ma)
    
    # -- re-open --
    m_wrap.file_control.open(current_scene)
    
    if publish:
        sg_version_name = os.path.splitext(os.path.basename(current_scene))[0]
        submit_deadline(project=asset_ld.project, asset_type=asset_ld.asset_type,
                sub_type=asset_ld.sub_type, asset_name=asset_ld.asset,
                task=asset_ld.task,
                sg_version_name=sg_version_name,
                publish_files=publish_files,
                camera=camera)


def submit_deadline(project, asset_type, sub_type, asset_name, task='',
                    sg_version_name='', publish_files=[], camera=''):
    """Automatic to cache, render, and create SG version in Deadline

    Args:
        project (str): project name
        asset_type (str): asset type
        sub_type (str): sub-type
        asset_name (str): asset name
        sg_version_name (str, optional) : SG version code
        publish_files (list, optinal) : maya file list that have been exported
    """
    for ma_file in publish_files:
        fol = os.path.dirname(ma_file)
        frame = re.findall(config.folder_regx('asset_loop_frame'), fol)[0]
        # -- get master file to cache --
        if os.path.exists(ma_file):
            jobname = os.path.splitext(os.path.basename(ma_file))[0]
            jobname = '{}_{}'.format(jobname, frame)
            batchname = sg_version_name or "{} - loop - publish".format(asset_name)

            bot_arg = '--project "{project}" --asset_type "{asset_type}" '\
                    '--sub_type "{sub_type}" --asset_name "{asset_name}" '\
                    '--file "{file}" --task {task} --origin_frame "{origin_frame}" '\
                    '--sg_version_name "{sg_version_name}" '\
                    '--camera {camera} --username "{username}"'
            bot_arg = bot_arg.format(project=project, asset_type=asset_type,
                                    sub_type=sub_type, asset_name=asset_name,
                                    file=ma_file, task=task, origin_frame=frame,
                                    sg_version_name=sg_version_name,
                                    camera=camera, username=getpass.getuser())
            # -- last job will create SG version --
            if publish_files.index(ma_file) == 0:
                bot_arg += ' --sg_publish'

            argument = '"{bot}" {argument}'.format(
                                                bot='snow/maya/bot/publish_asset_loop.py',
                                                argument=bot_arg)
            pluginfo = deadline.plugininfo.MayaPyCommandLine(Arguments=argument, Version=cmds.about(v=True))

            submission = deadline.job.JobSubmission(
                                                Plugin=pluginfo['Name'],
                                                Name="{} - publish".format(jobname),
                                                BatchName=batchname,
                                                Pool='pip_maya')
            submission.set_environment(pluginfo.get('Environment'))
            submission.set_plugin_value(**pluginfo)
            submission.set_failure_detection(OverrideJobFailureDetection=True, FailureDetectionJobErrors=5,
                                            OverrideTaskFailureDetection=True, FailureDetectionTaskErrors=5)
            submission.submit_job()

