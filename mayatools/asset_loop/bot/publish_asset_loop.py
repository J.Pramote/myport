#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, Mar 2022
##

################################################################################
##
##  Description
##
"""
Exectute with C:/Program Files/Autodesk/Maya{version}/bin/mayapy.exe
"""

################################################################################
##
##  Standard Library
##
import os
import sys
import argparse

import maya.standalone
maya.standalone.initialize(name='python')
import maya.cmds as cmds
print("Start Programe")

################################################################################
##
##  Initialize Environment
##
snow_dir = __file__.replace('\\', '/').split('snow')[0]
if snow_dir not in sys.path:
    sys.path.append(snow_dir)
    
if sys.version_info.major == 2:
    py_package = "P:/pipeline/thirdparty/Windows/python_packages"
elif sys.version_info.major == 3:
    py_package = "P:/pipeline/thirdparty/Windows/python3_packages"

if py_package not in sys.path:
    sys.path.append(py_package)

################################################################################
##
##  Snow Module
##
import snow.maya.lib.wrap as m_wrap
from snow.common.lib import asset_loader
from snow.common.lib import config
import snow.deadline as deadline


################################################################################
##
##  Logging
##
from snow.common.lib import code_logging
LOG = code_logging.create_log('maya.bot.publish_asset_loop')

################################################################################
##
##  Global Variables
##
CONVERT = "P:/pipeline/thirdparty/Windows/ImageMagick/convert.exe"
POOL = 'pip_maya'
MAYA_POOL = 'pip_maya'
SG_POOL = 'pip_shotgrid'


class PublishAssetLoop(object):
    def __init__(self, **kwargs):

        self.__dict__ = kwargs
        self.asset_ld = asset_loader.AssetLoader()
        self.asset_ld.drive = config.work_repo()[:-1]
        self.asset_ld.set_project(self.project)
        self.asset_ld.set_asset_path(asset_type=self.asset_type,
                                sub_type=self.sub_type,
                                asset_name=self.asset_name)
        self.asset_ld.task = self.task
        m_wrap.file_control.open(self.file)

    def publish(self):
        if not self.cache_loop_start and cmds.fileInfo('cache_loop_start', q=True):
            self.cache_loop_start = int(cmds.fileInfo('cache_loop_start', q=True)[0])

        if not self.cache_loop_end and cmds.fileInfo('cache_loop_end', q=True):
            self.cache_loop_end = int(cmds.fileInfo('cache_loop_end', q=True)[0])

        post_loop_end = self.cache_loop_start + 500 -1
        all_job_ids = []
        # batchname = os.path.splitext(os.path.basename(self.file))[0]
        batchname = self.sg_version_name or "{} - loop - publish".format(self.asset_ld.asset)
        # jobname = os.path.basename(os.path.dirname(self.file))
        jobname = os.path.splitext(os.path.basename(self.file))[0]
        jobname = '{}_{} - publish'.format(jobname, self.origin_frame)
        geo_grp = '*:{}'.format(self.asset_ld.geo_grp())
        found_geogrp = cmds.ls(geo_grp)
        output_dir = os.path.dirname(self.file)

        def cache():
            """Cache file

            Returns:
                list: output file list
            """
            mayapy_execute = "C:/Program Files/Autodesk/Maya{version}/bin/mayapy.exe".format(version=cmds.about(v=True))
            output_files = []

            def create_auxiliary_file():
                """Write auxiliary files are contained command lines, to cache
                a single file and seperate files by a frame
                Returns:
                    str, int: an auxiliary file and number of command line in
                                that file
                """
                cmd_form = '"{execute}" "{bot}" {argument}'
                command = []

                # -- Alembic Cache --
                filename = self.asset_ld.get_asset_publish_filename(pub_type='geo')
                alembic_file = '{}/{}'.format(output_dir, filename)
                output_files.append(alembic_file)
                alembic_arg = '--file "{file}" --root {root} --start {start} ' \
                            '--end {end} --output "{output}"'.format(
                                                                    file=os.path.realpath(self.file),
                                                                    root=found_geogrp[0],
                                                                    start=self.cache_loop_start,
                                                                    end=self.cache_loop_end, 
                                                                    output=os.path.realpath(alembic_file))

                arg = cmd_form.format(execute=os.path.realpath(mayapy_execute),
                                bot='snow/maya/bot/cache_alembic.py',
                                argument=alembic_arg)
                command.append(arg)

                # -- Gpu Cache --
                filename = self.asset_ld.get_asset_publish_filename(pub_type='gpu')
                alembic_file = '{}/{}'.format(output_dir, filename)
                output_files.append(alembic_file)
                alembic_arg = '--file "{file}" --root {root} --start {start} ' \
                            '--end {end} --output "{output}"'.format(
                                                                    file=os.path.realpath(self.file),
                                                                    root=found_geogrp[0],
                                                                    start=self.cache_loop_start, end=post_loop_end, 
                                                                    output=os.path.realpath(alembic_file))

                arg = cmd_form.format(execute=os.path.realpath(mayapy_execute),
                                bot='snow/maya/bot/cache_gpu.py',
                                argument=alembic_arg)
                command.append(arg)

                # -- RSProxy Cache --
                filename = self.asset_ld.get_asset_publish_filename(pub_type='rsproxy')
                rs_file = '{}/{}'.format(output_dir, filename)
                # rs_file = self.asset_ld.get_loop_publish_file(pub_type='rsproxy', frame=self.origin_frame)
                rs_file = list(os.path.split(rs_file))
                rs_file.insert(1, 'rs')
                rs_file = '/'.join(rs_file)
                output_files.append(rs_file)
                rs_arg = '--file "{file}" --root {root} --start {start} ' \
                            '--end {end} --output ' \
                            '"{output}"'.format(
                                                file=os.path.realpath(self.file),
                                                root=found_geogrp[0],
                                                start=self.cache_loop_start, end=post_loop_end,
                                                output=os.path.realpath(rs_file))

                arg = cmd_form.format(execute=os.path.realpath(mayapy_execute),
                                bot='snow/maya/bot/cache_rsproxy.py',
                                argument=rs_arg)
                command.append(arg)

                auxiliary_file = '{}/commandsfile.txt'.format(deadline.DEADLINE_USER_TEMPDIR)
                f = open(auxiliary_file, "w")
                f.write('\n'.join(command))
                f.close()

                return auxiliary_file, len(command)

            commands_filename, task_numbers = create_auxiliary_file()
            pluginfo = deadline.plugininfo.MayaPyCommandScript()
            submission = deadline.job.JobSubmission(
                                                Plugin=pluginfo['Name'],
                                                Name="{} - cache".format(jobname),
                                                BatchName=batchname,
                                                Pool=MAYA_POOL,
                                                Frames='{}-{}'.format(0, task_numbers-1),
                                                UserName=self.username
                                                )
            submission.set_output(OutputDirectory=output_dir)
            submission.set_failure_detection(OverrideJobFailureDetection=True, FailureDetectionJobErrors=5,
                                    OverrideTaskFailureDetection=True, FailureDetectionTaskErrors=5)
            submission.set_plugin_value(**pluginfo)
            submission.set_environment(pluginfo.get('Environment'))
            submission.add_auxiliary_file(commands_filename)
            job_id = submission.submit_job()
            all_job_ids.append(job_id)

            return output_files

        def render():
            """Setup render setting and submit to render in Deadline

            Returns:
                str, str: job id and output image sequence
            """

            
            # Apply output color transform for out of viewport renders 
            if not cmds.colorManagementPrefs(q=True, ote=True):
                cmds.colorManagementPrefs(e=True, ote=True)
            
            # -- Set Render Setting --
            image_outdir = '{}/preview'.format(output_dir)
            cmds.workspace(fr=['images', image_outdir])
            cmds.setAttr('{}.renderable'.format(self.camera), 1)
            cmds.setAttr("defaultRenderGlobals.currentRenderer", "mayaHardware2", type="string")
            cmds.setAttr('defaultRenderGlobals.imageFilePrefix', self.asset_ld.asset, type='string')
            cmds.setAttr("defaultRenderGlobals.imageFormat", 8)
            cmds.setAttr("defaultRenderGlobals.extensionPadding", 4)
            cmds.setAttr('defaultRenderGlobals.startFrame', self.cache_loop_start)
            cmds.setAttr('defaultRenderGlobals.endFrame', self.cache_loop_end)
            cmds.setAttr('defaultRenderGlobals.outFormatControl', 0)
            cmds.setAttr('defaultRenderGlobals.animation', 1)
            cmds.setAttr('defaultRenderGlobals.putFrameBeforeExt', 1)
            cmds.setAttr('defaultRenderGlobals.periodInExt', 1)
            cmds.setAttr('defaultResolution.width', 320)
            cmds.setAttr('defaultResolution.height', 320)
            cmds.setAttr('hardwareRenderingGlobals.lightingMode', 1)

            # -- Save Current File --
            m_wrap.file_control.save()

            # -- Submit to Deadline
            submission = deadline.job.JobSubmission(
                                        Plugin='MayaBatch',
                                        Frames='{}-{}'.format(self.cache_loop_start, self.cache_loop_end),
                                        Name="{} - render".format(jobname),
                                        Pool=MAYA_POOL,
                                        BatchName=batchname,
                                        UserName=self.username
                                        )
            submission.set_output(OutputFiles=[cmds.workspace(fileRuleEntry='images')])
            submission.set_failure_detection(OverrideJobFailureDetection=True, FailureDetectionJobErrors=5,
                                        OverrideTaskFailureDetection=True, FailureDetectionTaskErrors=5)

            default_renderlayer = cmds.listConnections("renderLayerManager.renderLayerId")

            mayabatch_info = deadline.plugininfo.MayaBatch(
                                        SceneFile=self.file,
                                        OutputFilePath=cmds.workspace(fileRuleEntry='images'),
                                        Version=cmds.about(version=True),
                                        RenderSetupIncludeLights=False,
                                        Renderer=cmds.getAttr("defaultRenderGlobals.currentRenderer"),
                                        RenderLayer=default_renderlayer[0],
                                        ImageHeight=cmds.getAttr("defaultResolution.height"),
                                        ImageWidth=cmds.getAttr("defaultResolution.width"),
                                        Animation=cmds.getAttr(
                                                'defaultRenderGlobals.animation'),
                                        Camera=self.camera)
            submission.set_plugin_value(**mayabatch_info)
            submission.set_environment(mayabatch_info.get('Environment'))

            job_id = submission.submit_job()
            all_job_ids.append(job_id)

            image_seq = '{}/{}.%04d.jpg'.format(cmds.workspace(fileRuleEntry='images'), self.asset_ld.asset)
            
            return job_id, image_seq

        def convert(jobid_depends=[], image_seq=''):
            """Convert image sequence to GIF file to preview

            Args:
                jobid_depends (list, optional): Dependency job ids. Defaults to [].
                image_seq (str, optional): Input image sequence. Defaults to ''.
            """

            def create_auxiliary_file():
                """Write auxiliary files are contained command lines, to cache
                a single file and seperate files by a frame
                Returns:
                    str, int: an auxiliary file and number of command line in
                                that file
                """
                cmd_form = '"{execute}" {argument}'
                command = []

                for pub_type in ['gif', 'mov']:
                    filename = self.asset_ld.get_asset_publish_filename(pub_type=pub_type)
                    output_file = '{}/{}'.format(output_dir, filename)
                    argument = []
                    argument.append('"{}[{}-{}]"'.format(image_seq, self.cache_loop_start, self.cache_loop_end))
                    argument.append('"{}"'.format(output_file))
                    argument = ' '.join(argument)
                    command.append(cmd_form.format(execute=CONVERT, argument=argument))

                auxiliary_file = '{}/commandsfile_convert.txt'.format(deadline.DEADLINE_USER_TEMPDIR)
                f = open(auxiliary_file, "w")
                f.write('\n'.join(command))
                f.close()

                return auxiliary_file, len(command)

            commands_filename, task_numbers = create_auxiliary_file()
            submission = deadline.job.JobSubmission(
                                Plugin='CommandScript',
                                Name="{} - preview".format(jobname),
                                BatchName=batchname,
                                Pool=MAYA_POOL,
                                Frames='{}-{}'.format(0, task_numbers-1),
                                UserName=self.username
                                )
            submission.set_output(OutputDirectory=output_dir)
            submission.set_failure_detection(OverrideJobFailureDetection=True, FailureDetectionJobErrors=5,
                                        OverrideTaskFailureDetection=True, FailureDetectionTaskErrors=5)
            try:
                submission.set_dependencies(','.join(jobid_depends))
            except TypeError:
                LOG.exception('{} {}'.format(type(jobid_depends), jobid_depends))
                return

            pluginfo = deadline.plugininfo.CommandScript()
            submission.set_environment(pluginfo.get('Environment'))

            submission.set_plugin_value(**pluginfo)
            submission.add_auxiliary_file(commands_filename)        

            job_id = submission.submit_job()
            all_job_ids.append(job_id)
        
        def update_sg(jobid_depends=[]):
            """Publish/Create version on ShotGrid

            Args:
                jobid_depends (list, optional): Dependency job ids. Defaults to [].
            """

            image_path = self.asset_ld.get_images_padding_path()
            if not os.path.exists(os.path.dirname(image_path)):
                os.makedirs(os.path.dirname(image_path))

            version_name = self.sg_version_name
            if not version_name:
                version_name = os.path.basename(self.file).split('.')[0]
                version_name += '_recache'

            submission = deadline.job.JobSubmission(
                                        Plugin='CommandLine',
                                        Name="{} - SG".format(jobname),
                                        Pool=SG_POOL,
                                        BatchName=batchname,
                                        UserName=self.username
                                        )
            submission.set_dependencies(','.join(jobid_depends))

            filename = self.asset_ld.get_asset_publish_filename(pub_type='mov')
            movie = '{}/{}'.format(output_dir, filename)

            arguments = 'import snow.shotgrid.lib.wrap as sg_wrap ; ' \
                        'sg_wrap.version.publish(project="""{project}""",' \
                        'entity_code="""{entity_code}""",' \
                        'step="""anim""", task="""{task}""", path_to_frames="""{path_to_frames}""",'\
                        'path_to_movie="""{path_to_movie}""", preview_folder="""{preview_folder}""",' \
                        'code="""{code}""", script_name="""maya/tools/maya_publish""")'\
                        ''.format(project=self.asset_ld.project, entity_code=self.asset_ld.asset,
                                task=self.asset_ld.task, path_to_frames=image_path,
                                path_to_movie=movie,
                                preview_folder=os.path.dirname(image_path),
                                code=version_name)
            arguments = '-c "{}"'.format(arguments)

            if sys.version_info.major == 3:
                pluginfo = deadline.plugininfo.Python3Cmd(Arguments=arguments)
            elif sys.version_info.major == 2:
                pluginfo = deadline.plugininfo.PythonCmd(Arguments=arguments)

            submission.set_plugin_value(**pluginfo)
            submission.set_environment(pluginfo.get('Environment'))

            job_id = submission.submit_job()
            all_job_ids.append(job_id)

        if found_geogrp:
            cache()
            job_id, image_seq = render()
            convert(jobid_depends=[job_id], image_seq=image_seq)
            if self.sg_publish:
                self.asset_ld.dept = 'anim'
                update_sg(all_job_ids)
        else:
            raise IndexError("Not found {}".format(geo_grp))
            # sys.exit(1)


################################################################################
##
##  Main Function
##
def main():
    """ Shell command line run here
    """
    parser = argparse.ArgumentParser(description='Publish Asset Loop')
    parser.add_argument('--project', type=str, required=True,
                        help='Project name')
    parser.add_argument('--asset_type', type=str, required=True,
                        help='Asset type')
    parser.add_argument('--sub_type', type=str, required=True,
                        help='Sub type')
    parser.add_argument('--asset_name', type=str, required=True,
                        help='Asset name')
    parser.add_argument('--file', type=str, required=True,
                        help='Maya file')
    parser.add_argument('--task', type=str, required=True,
                        help='Task name')
    parser.add_argument('--origin_frame', type=str, required=True,
                        help='Origin frame')
    parser.add_argument('--cache_loop_start', type=str, default='',
                        help='Start frame. default=%(default)')
    parser.add_argument('--cache_loop_end', type=str, default='',
                        help='End frame. default=%(default)')
    parser.add_argument('--sg_publish', action='store_true',
                    help='Create task version on ShotGrid. Default is False')
    parser.add_argument('--sg_version_name', type=str, default='',
                        help='SG version code. default=%(default)')
    parser.add_argument('--username', type=str, default='',
                    help='Job owner. default=%(default)')
    parser.add_argument('--camera', type=str, default='',
                    help='Render Camera. ex: "an10004" default=%(default)')

    args = parser.parse_args()
    print('Start Publish..')
    print(args)
    publish_asset = PublishAssetLoop(**vars(args))
    publish_asset.publish()


if __name__ == '__main__':
    main()
