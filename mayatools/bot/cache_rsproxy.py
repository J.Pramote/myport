#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, Feb 2022
##

################################################################################
##
##  Description
##
"""
Exectute with C:/Program Files/Autodesk/Maya{version}/bin/mayapy.exe
"""

################################################################################
##
##  Standard Library
##
import argparse
import os
import sys

import maya.api.OpenMaya as OpenMaya
import maya.standalone
import maya.cmds as cmds

################################################################################
##
##  Snow Module
##
from snow.common.lib import asset_loader, app_env

def cache(mayafile, obj, start, end, output):
    import snow.maya.lib.wrap as m_wrap
    cmds.file(mayafile, f=True, o=True)
    
    outdir = os.path.dirname(output)
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    rs_file = m_wrap.redshift._export_rs_prx(path=output, obj=obj, frame_range=(start, end))
    
    if not rs_file:
        sys.exit(1)


def build(rs_file, ma_output='', loop_frame=[]):
    import snow.maya.lib.wrap as m_wrap
    loader = asset_loader.AssetLoader()
    loader.set_path(m_wrap.file_control.scene_name())
    cmds.file(new=True)
    if not ma_output:
        ma_output = loader.get_loop_publish_file(pub_type='rsgeo')
    print('Rename:', ma_output)
    cmds.file(rename=ma_output)

    loader.set_asset_name()
    trans = m_wrap.redshift._import_rs_prx(name=loader.asset, path=rs_file, use_sequence=True)

    if loop_frame:
        found_node = cmds.listConnections(cmds.listRelatives(trans, s=True), type='RedshiftProxyMesh')
        if found_node:
            rsprx_node = found_node[0]
            cmds.addAttr(rsprx_node, ln="startFrame", min=0, dv=0)
            cmds.addAttr(rsprx_node, ln="endFrame", min=0, dv=0)
            cmds.addAttr(rsprx_node, ln="frame", min=0, dv=0)
            cmds.setAttr(rsprx_node+'.startFrame', loop_frame[0])
            cmds.setAttr(rsprx_node+'.endFrame', loop_frame[1])

            exp='''
            $start = {rsprx_node}.startFrame-{rsprx_node}.frameOffset;
            $end = {rsprx_node}.endFrame-{rsprx_node}.frameOffset;
            if(frame<=0 || frame <= {rsprx_node}.startFrame)
                {
                    {rsprx_node}.frame = {rsprx_node}.startFrame;
                }
                else{
                    if({rsprx_node}.frame<$end-1){
                        {rsprx_node}.frame += 1;
                        }
                    else{
                        {rsprx_node}.frame = $start;
                        }
                }'''.replace('{rsprx_node}', rsprx_node)
            cmds.expression(o=rsprx_node, s=exp)

            cmds.expression(o=rsprx_node, s="frameExtension={}.frame;".format(rsprx_node))


    m_wrap.file_control.save()


################################################################################
##
##  Main Function
##
def main():
    """ Shell command line run here
    """
    parser = argparse.ArgumentParser(description='Cache Alembic')

    parser.add_argument('--file', type=str, required=True,
                        help='Maya file')

    parser.add_argument('--root', type=str, required=True,
                        help='Root object')

    parser.add_argument('--start', type=int, required=True,
                        help='Start frame')

    parser.add_argument('--end', type=int, required=True,
                        help='End frame')

    parser.add_argument('--output', type=str, required=True,
                        help='Output alembic file')

    parser.add_argument('--build', action='store_true',
                        help='Build maya file. Default is False')

    parser.add_argument('--cycle_type',
                        default='hold',
                        nargs='?',
                        choices=['hold', 'loop'],
                        help='Build animation type (default: %(default)s)')

    args = parser.parse_args()

    app_env.update_project_env(app='maya{}'.format(OpenMaya.MGlobal.mayaVersion()), filepath=args.file.replace('\\','/'))
    maya.standalone.initialize(name='python')
    cache(mayafile=args.file.replace('\\', '/'), obj=args.root,
            start=args.start, end=args.end,
            output=args.output.replace('\\', '/'))

    if args.build:
        loop_frame = []
        if args.cycle_type == 'loop':
            loop_frame = [args.start, args.end]
        build(args.output.replace('\\','/'), loop_frame=loop_frame)


if __name__ == '__main__':
    main()

