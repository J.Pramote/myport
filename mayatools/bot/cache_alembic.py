#!Python3
################################################################################
##
##  * Written by Jutamas Pramote <j.pramote153@gmail.com>, Feb 2022
##

################################################################################
##
##  Description
##
"""
Exectute with C:/Program Files/Autodesk/Maya{version}/bin/mayapy.exe
"""

################################################################################
##
##  Standard Library
##
import argparse
import os
import sys

import maya.api.OpenMaya as OpenMaya
import maya.cmds as cmds
import maya.standalone

from snow.common.lib import app_env


def cache(mayafile, obj, start, end, output):
    import snow.maya.lib.wrap as m_wrap
    cmds.file(mayafile, f=True, o=True)

    outdir = os.path.dirname(output)
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    abc_file = m_wrap.abc._export(obj=obj, path=output, frame_range=(start, end))
    if not abc_file:
        sys.exit(1)
        
    return abc_file


def cache_sequence(mayafile, obj, start, end, output):
    import snow.maya.lib.wrap as m_wrap
    cmds.file(mayafile, f=True, o=True)
    m_wrap.abc.export_sequence(obj=obj, path=output, frame_range=(start, end))


################################################################################
##
##  Main Function
##
def main():
    """ Shell command line run here
    """
    parser = argparse.ArgumentParser(description='Cache Alembic')

    parser.add_argument('--file', type=str, required=True,
                        help='Maya file')

    parser.add_argument('--root', type=str, required=True,
                        help='Root object')

    parser.add_argument('--start', type=int, required=True,
                        help='Start frame')

    parser.add_argument('--end', type=int, required=True,
                        help='End frame')

    parser.add_argument('--output', type=str, required=True,
                        help='Output alembic file')

    parser.add_argument('--export_sequence', action='store_true',
                        help='Export sequnece file. Default is False')

    args = parser.parse_args()
    app_env.update_project_env(app='maya{}'.format(OpenMaya.MGlobal.mayaVersion()),
                                filepath=args.file.replace('\\','/'))
    maya.standalone.initialize(name='python')
    if not args.export_sequence:
        cache(mayafile=args.file.replace('\\','/'), obj=args.root, start=args.start, end=args.end,
            output=args.output.replace('\\','/'))
    else:
        cache_sequence(mayafile=args.file.replace('\\','/'), obj=args.root, start=args.start, end=args.end,
            output=args.output.replace('\\','/'))


if __name__ == '__main__':
    main()

