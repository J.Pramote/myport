#!Python3
################################################################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
################################################################################
##
##  Standard Library
##
import sys
import os
try:
    from PySide2 import QtWidgets, QtCore, QtGui
except ImportError:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets	

################################################################################
##
##  Global Variables
##
Stylesheet = os.path.dirname(__file__)+"/stylesheet.css"

################################################################################
##
##                             Custom Widget Classes
##
class Widget(QtCore.QObject):
    def __init__(self):
        super(Widget, self).__init__()
        self.project_lb = QtWidgets.QLabel()
        self.episode_lb = QtWidgets.QLabel()
        self.shot_lb = QtWidgets.QLabel()
        self.renderer_cb = QtWidgets.QComboBox()
        self.tasks_tw = TaskTreeWidget()
        self.publish_pb = QtWidgets.QPushButton('Publish')
        self.status_lb = QtWidgets.QLabel()

class GetSelectedWidgetDelegate(QtWidgets.QWidget):
    clicked = QtCore.Signal()
    changed = QtCore.Signal(str)
    def __init__(self, parent=None):
        super(GetSelectedWidgetDelegate, self).__init__(parent)
        QtWidgets.QHBoxLayout(self)
        self.layout().setAlignment(QtCore.Qt.AlignLeft)
        self.layout().setContentsMargins(0,0,0,0)
        self.button = QtWidgets.QPushButton('>')
        self.button.setContentsMargins(0,0,0,0)
        self.button.clicked.connect(self.clicked.emit)
        self.layout().addWidget(self.button)
        self.lineedit = QtWidgets.QLineEdit()
        self.lineedit.setContentsMargins(0,0,0,0)
        self.lineedit.textChanged.connect(self.changed.emit)
        self.lineedit.setReadOnly(True)
        self.lineedit.setStyleSheet('border: none; background: none;')

    def text(self):
        return self.lineedit().text()


class AbstractItemDelegate( QtWidgets.QItemDelegate ):
    ''' Delegate QTreeWidgetItem'''

    def __init__(self, parent=None):
        super(AbstractItemDelegate, self).__init__(parent)

    def createEditor(self, parent, option, index):
        validator = QtGui.QIntValidator(0,9999999)

        if index.column() == 1:
            widget = GetSelectedWidgetDelegate(parent)
            widget.clicked.connect(lambda :self.commitData.emit(widget))
            widget.changed.connect(lambda :self.setModelData(widget, index.model(), index))
            return widget

        elif index.column() == 2:
            type_cb = QtWidgets.QComboBox(parent)
            type_cb.currentIndexChanged.connect(lambda :self.commitData.emit(type_cb))
            type_cb.currentIndexChanged.connect(lambda :self.setModelData(type_cb, index.model(), index))
            return type_cb

        elif index.column() == 4:
            start_le = QtWidgets.QLineEdit(parent)
            start_le.setValidator(validator)
            start_le.textChanged.connect(lambda :self.commitData.emit(start_le))
            start_le.textChanged.connect(lambda :self.setModelData(start_le, index.model(), index))
            return start_le

        elif index.column() == 5:
            end_le = QtWidgets.QLineEdit(parent)
            end_le.setValidator(validator)
            end_le.textChanged.connect(lambda :self.commitData.emit(end_le))
            end_le.textChanged.connect(lambda :self.setModelData(end_le, index.model(), index))
            return end_le
    
    def updateEditorGeometry(self, editor, option, index):

        option.rect.setSize(editor.minimumSizeHint().expandedTo(option.rect.size()))
        if isinstance(editor, QtWidgets.QComboBox):
            editor.setGeometry(option.rect)
        elif isinstance(editor, QtWidgets.QTextEdit):
            editor.setMinimumWidth(480)
            editor.setMinimumHeight(160)
        elif isinstance(editor, GetSelectedWidgetDelegate):
            editor.setGeometry(option.rect)

        else:
            return QtWidgets.QItemDelegate.updateEditorGeometry(
                self, editor, option, index)
    
    def setModelData(self, editor, model, index):
        if isinstance(editor, QtWidgets.QComboBox):
            model.setData(index, editor.currentText())

        elif isinstance(editor, QtWidgets.QLineEdit):
            model.setData(index, editor.text())
        
        elif isinstance(editor, GetSelectedWidgetDelegate):
            model.setData(index, editor.lineedit.text())


class TaskTreeWidget(QtWidgets.QTreeWidget):
    type_changed = QtCore.Signal(QtWidgets.QWidgetItem, int)
    group_changed = QtCore.Signal(QtWidgets.QWidgetItem, int)
    get_clicked = QtCore.Signal(QtWidgets.QWidgetItem, int)
    # right_clicked = QtCore.Signal()
    def __init__(self, parent=None):
        super(TaskTreeWidget,self).__init__(parent)
        self.setHeaderLabels( ["Tasks","Export Group", "Export Type",
                            "Export Shade", "Start Frame", "End Frame"] )

        # self.context_menu = QtWidgets.QMenu()
        # self.context_menu.aboutToShow.connect(self.right_clicked.emit)
        # self.context_menu.addAction('Clear')
                
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        self.setColumnWidth(1 , 250 )
        delegate = AbstractItemDelegate(self)
        self.setItemDelegate( delegate)
        delegate.commitData.connect(self.__changed)

    @QtCore.Slot()
    def __changed(self, editor):
        item = self.itemAt(editor.pos())
        if editor is self.itemWidget(item, 1):
            self.get_clicked.emit(item, 1)
        elif editor is self.itemWidget(item, 2):
            self.type_changed.emit(item, 2)


class TaskTreeWidgetItem(QtWidgets.QTreeWidgetItem):
    def __init__(self,*argv):
        super(TaskTreeWidgetItem, self).__init__(*argv)
        self.treeWidget().openPersistentEditor(self, 1)
        self.treeWidget().openPersistentEditor(self, 2)
        self.setCheckState(3, QtCore.Qt.Unchecked)
        self.treeWidget().openPersistentEditor(self, 4)
        self.treeWidget().openPersistentEditor(self, 5)

        self.setTextAlignment(1, QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
        self.default_flags =self.flags()
    
    def set_data(self, column, data):
        widget = self.treeWidget().itemWidget(self, column)
        current_data = self.data(column, QtCore.Qt.DisplayRole)
        if isinstance(widget, QtWidgets.QComboBox):
            widget.clear()
            if isinstance(data, list):
                widget.addItems(data)
            elif isinstance(data, str):
                widget.addItem(data)		
            
            index = widget.findText(current_data)
            if index > 0:
                widget.setCurrentIndex(index)
            else:
                widget.setCurrentIndex(0)

        elif isinstance(widget, QtWidgets.QLineEdit):
            widget.setText(data)
        
        elif isinstance(widget, QtWidgets.QCheckBox):
            widget.setCheckState(data)

        elif isinstance(widget, GetSelectedWidgetDelegate):
            widget.lineedit.setText(data)
    
    def set_enabled(self, column, state):
        widget = self.treeWidget().itemWidget(self, column)
        if widget:
            widget.setEnabled(state)
    
    def clear_data(self, column):
        widget = self.treeWidget().itemWidget(self, column)
        if widget:
            widget.clear()
        if self.checkState(column):
            self.setCheckState(QtCore.Qt.Unchecked)


class FxPublishWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(FxPublishWindow, self).__init__(parent)
        self.setWindowTitle('FX Publish')
        self.setStyleSheet(open(Stylesheet ,'r').read())
        QtWidgets.QVBoxLayout(self)
        self.widget = Widget()
        
        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignLeft)
        self.layout().addLayout(layout)
        label = QtWidgets.QLabel('Project:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.project_lb)
        self.widget.project_lb.setStyleSheet('font:italic;')
        layout.addSpacing(10)
        label = QtWidgets.QLabel('Episode:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.episode_lb)
        self.widget.episode_lb.setStyleSheet('font:italic;')
        layout.addSpacing(10)
        label = QtWidgets.QLabel('Shot:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.shot_lb)
        self.widget.shot_lb.setStyleSheet('font:italic;')
        self.layout().addSpacing(10)

        layout.addStretch()

        label = QtWidgets.QLabel('Renderer:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.renderer_cb)
        self.widget.renderer_cb.setMinimumWidth(150)

        layout = QtWidgets.QVBoxLayout()
        self.layout().addLayout(layout)
        layout.addWidget(self.widget.tasks_tw)

        layout = QtWidgets.QHBoxLayout()
        self.layout().addLayout(layout)
        layout.addWidget(self.widget.status_lb)
        layout.addStretch()
        layout.addWidget(self.widget.publish_pb)
        self.widget.publish_pb.setProperty('main_button', True)

        self.resize(800, 280)

