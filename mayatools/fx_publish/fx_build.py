#!Python3
################################################################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
################################################################################
##
##  Standard Library
##
import os
import re
from glob import glob
from pprint import pprint

try:
    from PySide2 import QtWidgets, QtCore, QtGui
except ImportError:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets

################################################################################
##
##  Maya Library
##
import maya.cmds as cmds
import maya.mel as mel
import maya.OpenMayaUI as omui


################################################################################
##
##  Snow Modules
##
from snow.common.lib.widget import message_box
from snow.common.lib import ig_path
import snow.maya.lib.wrap as m_wrap
from snow.maya.lib.widgets import Maya_Main_Window

################################################################################
##
##  Relative Modules
##
import fx_build_gui as gui
import fx_publish_helper as helper
from . import Export_Type, RendererList, Ext_Name, Shade_Name
from . import Logger
Logger.debug("Working on %s", __name__)

################################################################################
##
##  Global Variables
##
__Object_ID__ = 'FxBuild'


################################################################################
##
##		Window Class
##
class FxBuild(gui.FxBuildWindow):

    def __init__(self, parent=None, scenefile=''):
        super(FxBuild, self).__init__(parent)

        self.setObjectName(__Object_ID__)
        self.cur_scene = scenefile or cmds.file(q=True, loc=True)
        Logger.debug('cur_scene=%s', self.cur_scene)

        try:
            self.ig_path = ig_path.Path(path=self.cur_scene)
        except:
            msg = "It isn't  format path"
            message_box.MessageBox(self).error(msg)
            Logger.exception(msg)

        self.file_project = self.ig_path.project_root_data
        Logger.debug('[file_project=%s]', self.file_project)
        self.file_episode = self.ig_path.episode_data
        Logger.debug('[file_episode=%s]', self.file_episode)
        self.file_seqeunce = self.ig_path.sequence_data
        Logger.debug('[file_seqeunce=%s]', self.file_seqeunce)
        self.file_shot = self.ig_path.shot_data
        Logger.debug('[file_shot=%s]', self.file_shot)
        self.step = 'FX'
        Logger.debug('[step=%s]', self.step)

        self.dep_publish_dir = self.ig_path.getHeroDir(
                                        department=self.step).replace(
                                                        'hero', 'publish')
        self.shot_entity = dict()
        self.query_shotgrid_thread = helper.QueryTasksThread(
            project=self.file_project,
            episode=self.file_episode,
            shot=self.file_shot,
            step=self.step)

        ## Connect Signal
        self.widget.tasks_tw.version_changed.connect(self.version_change)
        # self.widget.tasks_tw.itemChanged.connect(self.task_item_changed)
        self.widget.tasks_tw.update_clicked.connect(self.update_element)
        self.widget.update_all_pb.clicked.connect(self.update_all)
        self.widget.renderer_cb.currentTextChanged.connect(
            helper.load_renderer_plugin)
        self.query_shotgrid_thread.started.connect(self.tasks_loading_event)
        self.query_shotgrid_thread.finished.connect(self.tasks_finished_event)
        self.query_shotgrid_thread.tasks_listed.connect(self.add_task_items)
        self.query_shotgrid_thread.shot_entity.connect(
            lambda x: setattr(self, 'shot_entity', x))

        ## Set value in widgets
        self.widget.project_lb.setText(self.file_project)
        self.widget.episode_lb.setText(self.file_episode)
        self.widget.shot_lb.setText(self.file_shot)
        self.widget.renderer_cb.addItems(RendererList)
        renderer = cmds.getAttr("defaultRenderGlobals.currentRenderer")
        self.widget.renderer_cb.setCurrentIndex(
            self.widget.renderer_cb.findText(renderer, QtCore.Qt.MatchExactly))
        self.query_shotgrid_thread.start()

    def closeEvent(self, event):
        Logger.info('Close')
        if self.query_shotgrid_thread.isRunning():
            self.query_shotgrid_thread.terminate()

    def find_task_version(self, taskname):
        """
        Return a list of version data like
                [{ version: {type:<str>, shade:<str>, path:<str>}, prefix:<str>}]
        """
        version_data = dict()
        filename = '{}_{}_{}__VFX_{}'.format(
                                            self.ig_path.episode_data,
                                            self.ig_path.sequence_data,
                                            self.ig_path.shot_data,
                                            taskname.replace(' ','_'))
        Logger.debug('find_task_version : %s',
                    '[dep_publish_dir={}]'.format(self.dep_publish_dir))
        Logger.debug('find_task_version : %s',
                    '[taskname={}]'.format(taskname))
        Logger.debug('find_task_version : %s',
                    '[filename={}]'.format(filename))

        def find_shader(basepath):
            for renderer in Shade_Name.keys():
                shade_file = '{}_{}.ma'.format(basepath, Shade_Name[renderer])
                if os.path.exists(shade_file):
                    return shade_file

        def find_type(basename):
            if re.search('_rs$', basename):
                return Ext_Name['.rs']

        def search_file():
            files = []
            for ext in Ext_Name.keys():
                fullpath = '{}/v*/{}{}'.format(
                                    self.dep_publish_dir, filename, ext)
                files += glob(fullpath)

            fullpath = '{}/v*/{}_rs'.format(
                                self.dep_publish_dir, filename)
            files += glob(fullpath)

            return files

        for path in search_file():
            path = path.replace('\\', '/')
            version = os.path.basename(os.path.dirname(path))
            filepath = path
            if os.path.isdir(path):
                for f in os.listdir(path):
                    f = '{}/{}'.format(path, f)
                    if os.path.isfile(f):
                        filepath = f
                        break
            basename = os.path.basename(filepath)
            basepath, ext = os.path.splitext(filepath)
            version_data[version] = {
                            'type': Ext_Name.get(ext) or find_type(basename),
                            'shade': find_shader(basepath),
                            'path': filepath,
                            'prefix': basename.split('.')[0].replace(' ','_')}

        return version_data

    @QtCore.Slot()
    def tasks_loading_event(self):
        self.widget.tasks_tw.setEnabled(False)
        self.widget.status_lb.setText('Tasks loading..')

    @QtCore.Slot()
    def tasks_finished_event(self):
        self.widget.tasks_tw.setEnabled(True)
        self.widget.status_lb.clear()
        if (self.shot_entity.get('sg_cut_in') and
            self.shot_entity.get('sg_cut_out')):
            Logger.debug('set_playback_range : %s-%s',
                                        self.shot_entity['sg_cut_in'],
                                        self.shot_entity['sg_cut_out'])
            m_wrap.playback.set_playback_range(
                                    self.shot_entity['sg_cut_in'],
                                    self.shot_entity['sg_cut_out'])

    @QtCore.Slot()
    def add_task_items(self, tasks):
        ''' Add tasks to widget '''
        Logger.info('Add task items')
        for sg_task in tasks:
            Logger.info(sg_task)
            taskname = sg_task['content']
            
            item = gui.TaskTreeWidgetItem(self.widget.tasks_tw)
            item.setText(0, taskname)
            task_version_data = self.find_task_version(taskname)
            if not task_version_data:
                continue
            current_version = ''
            for version in sorted(task_version_data):
                version_data = task_version_data.get(version)
                item.set_data(1, version, **version_data)
                if version_data['path'] in cmds.file(q=True, l=True, wcn=True):
                    current_version = version

            if current_version:
                item.setText(1, current_version)

            self.set_status_item(item)

        self.widget.tasks_tw.sortItems(0, QtCore.Qt.AscendingOrder)

    def set_status_item(self, item):
        taskname = item.text(0).replace(' ', '_')
        current_version = item.text(1)
        last_version = item.version_items()[-1]
        version_data = item.data(1, QtCore.Qt.UserRole)

        if not [f for f in cmds.file(q=True, l=True, wcn=True) if taskname in f]:
            item.set_status_item('build')

        elif version_data['path'] in cmds.file(q=True, l=True, wcn=True):
            if current_version == last_version:
                item.set_status_item('exist')
            else:
                item.set_status_item('update')

        else:
            item.set_status_item('build')

    @QtCore.Slot()
    def version_change(self, item, version_data):
        item.setText(2, version_data['type'])
        shade = version_data['shade']
        if shade:
            shade_type = ''.join(
                        re.findall(r'\w+_(\w+)\.\w+$', shade))
            item.setText(3, shade_type)
        else:
            item.setText(3, '')

        self.set_status_item(item)

    @QtCore.Slot()
    def update_element(self, item):
        group_exists = cmds.ls(assemblies=True)
        version_data = item.data(1, QtCore.Qt.UserRole)

        if version_data['type'] == "Alembic":
            Logger.info('Build Alembic: {}'.format(version_data['path']))
            Logger.info('Connect shade: {}'.format(version_data['shade']))
            helper.build_alembic(version_data['path'], version_data['shade'])

        elif version_data['type'] == 'Redshift Proxy':
            Logger.info('Build Redshift Proxy: {}'.format(version_data['path']))
            helper.build_rsproxy(version_data['path'])
            
        elif version_data['type'] == 'mayaAscii':
            Logger.info('Build mayaAscii: {}'.format(version_data['path']))
            helper.build_maya(version_data['path'])

        self.set_status_item(item)
 
        helper.group_all(except_name=group_exists)
        cmds.currentTime(int(self.shot_entity['sg_cut_in']))

    @QtCore.Slot()
    def update_all(self):
        """Update all"""
        items = self.widget.tasks_tw.findItems(
                    '',QtCore.Qt.MatchContains, 2 )
        for item in items:
            version_items = item.version_items()
            if version_items:
                item.setText(1, version_items[-1])
                self.update_element(item)




################################################################################
##
##		Main Function
##
def main(scenefile=''):
    widget = Maya_Main_Window.findChild(QtWidgets.QWidget, __Object_ID__)
    if widget:
        widget.close()
        widget.deleteLater()

    widget = FxBuild(parent=Maya_Main_Window, scenefile=scenefile)
    widget.setWindowFlag(QtCore.Qt.Window)
    widget.show()

