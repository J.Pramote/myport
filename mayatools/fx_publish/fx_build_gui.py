#!Python3
################################################################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
################################################################################
##
##  Standard Library
##
import os
try:
    from PySide2 import QtWidgets, QtCore, QtGui
except ImportError:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets	

################################################################################
##
##  Global Variables
##
Stylesheet = os.path.dirname(__file__)+"/stylesheet.css"


################################################################################
##
##                             Custom Widget Classes
##
class Widget(QtCore.QObject):
    def __init__(self):
        super(Widget, self).__init__()
        self.project_lb = QtWidgets.QLabel()
        self.episode_lb = QtWidgets.QLabel()
        self.shot_lb = QtWidgets.QLabel()
        self.renderer_cb = QtWidgets.QComboBox()
        self.tasks_tw = TaskTreeWidget()
        self.update_all_pb = QtWidgets.QPushButton('Build All')
        self.status_lb = QtWidgets.QLabel()


class AbstractItemDelegate(QtWidgets.QItemDelegate):
    ''' Delegate QTreeWidgetItem'''
    commitData_1 = QtCore.Signal(QtWidgets.QWidgetItem, QtCore.QModelIndex, str)
    commitData_4 = QtCore.Signal(QtWidgets.QWidgetItem, QtCore.QModelIndex)

    def __init__(self, parent=None):
        super(AbstractItemDelegate, self).__init__(parent)

    def createEditor(self, parent, option, index):

        if index.column() == 1:
            version_cb = QtWidgets.QComboBox(parent)
            version_cb.currentTextChanged.connect(
                    lambda x: self.commitData_1.emit(index.model(), index, x))

            version_cb.currentIndexChanged.connect(
                                lambda: self.setModelData(
                                            version_cb, index.model(), index))
            return version_cb

        elif index.column() == 4:
            update_pb = QtWidgets.QPushButton('Build', parent)
            update_pb.setFixedWidth(80)
            update_pb.setProperty('item', True)
            update_pb.clicked.connect(
                    lambda: self.commitData_4.emit(index.model(), index))
            return update_pb

    def updateEditorGeometry(self, editor, option, index):

        option.rect.setSize(editor.minimumSizeHint().expandedTo(
                                                        option.rect.size()))
        if isinstance(editor, QtWidgets.QComboBox):
            editor.setGeometry(option.rect)
        else:
            return QtWidgets.QItemDelegate.updateEditorGeometry(
                self, editor, option, index)

    def setModelData(self, editor, model, index):
        if isinstance(editor, QtWidgets.QComboBox):
            model.setData(index,
                        editor.itemData(
                            editor.currentIndex(), QtCore.Qt.UserRole),
                        QtCore.Qt.UserRole)


class TaskTreeWidgetItem(QtWidgets.QTreeWidgetItem):
    def __init__(self, *argv):
        super(TaskTreeWidgetItem, self).__init__(*argv)
        self.treeWidget().openPersistentEditor(self, 1)
        self.treeWidget().openPersistentEditor(self, 4)

        self.setTextAlignment(1, QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
        self.default_flags = self.flags()
    
    def text(self, column):
        if column == 1:
            widget = self.treeWidget().itemWidget(self, column)
            return widget.currentText()
        else:
            return super(TaskTreeWidgetItem, self).text(column)
    
    def setText(self, column, text):
        if column == 1:
            widget = self.treeWidget().itemWidget(self, column)
            widget.setCurrentIndex(
                    widget.findText(text, QtCore.Qt.MatchExactly))
        else:
            super(TaskTreeWidgetItem, self).setText(column, text)

    def set_data(self, column, data, **kwargs):
        widget = self.treeWidget().itemWidget(self, column)
        if isinstance(widget, QtWidgets.QComboBox):
            if isinstance(data, list):
                widget.addItems(data)
            elif isinstance(data, str):
                widget.addItem(data, kwargs)

            if widget.count():
                widget.setCurrentIndex(widget.count()-1)

        elif isinstance(widget, QtWidgets.QLineEdit):
            widget.setText(data)

        elif isinstance(widget, QtWidgets.QCheckBox):
            widget.setCheckState(data)

    def set_enabled(self, column, state):
        widget = self.treeWidget().itemWidget(self, column)
        if widget:
            widget.setEnabled(state)

    def clear_data(self, column):
        widget = self.treeWidget().itemWidget(self, column)
        if widget:
            widget.clear()
        if self.checkState(column):
            self.setCheckState(QtCore.Qt.Unchecked)
    
    def version_items(self):
        version_cb = self.treeWidget().itemWidget(self, 1)
        if version_cb.count():
            return sorted([version_cb.itemText(v) 
                        for v in range(version_cb.count())])
        return []

    def set_status_item(self, status):
        widget = self.treeWidget().itemWidget(self, 1)
        if status == 'update':
            widget.setStyleSheet('background: rgb(216, 50, 50);')
        elif status == 'build':
            widget.setStyleSheet('background: none;')
        elif status == 'exist':
            widget.setStyleSheet('background: rgb(50, 216, 64);')


class TaskTreeWidget(QtWidgets.QTreeWidget):
    version_changed = QtCore.Signal(QtWidgets.QTreeWidgetItem, dict)
    update_clicked = QtCore.Signal(QtWidgets.QTreeWidgetItem)

    def __init__(self, parent=None):
        super(TaskTreeWidget, self).__init__(parent)
        self.setHeaderLabels(
                    ["Tasks", "Version", "Type", "Shade", ""])

        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        self.setColumnWidth(0, 150)
        delegate = AbstractItemDelegate(self)
        self.setItemDelegate(delegate)
        delegate.commitData_1.connect(self.column_1_changed)
        delegate.commitData_4.connect(self.column_4_changed)

    def column_1_changed(self, model, index):
        data = model.data(index, QtCore.Qt.UserRole)
        item = self.itemFromIndex(index)
        self.version_changed.emit(item, data)

    def column_4_changed(self, model, index):
        item = self.itemFromIndex(index)
        self.update_clicked.emit(item)


class FxBuildWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(FxBuildWindow, self).__init__(parent)
        self.setWindowTitle('FX Build')
        self.setStyleSheet(open(Stylesheet, 'r').read())
        QtWidgets.QVBoxLayout(self)
        self.widget = Widget()
        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignLeft)
        self.layout().addLayout(layout)
        label = QtWidgets.QLabel('Project:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.project_lb)
        self.widget.project_lb.setStyleSheet('font:italic;')
        layout.addSpacing(10)
        label = QtWidgets.QLabel('Episode:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.episode_lb)
        self.widget.episode_lb.setStyleSheet('font:italic;')
        layout.addSpacing(10)
        label = QtWidgets.QLabel('Shot:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.shot_lb)
        self.widget.shot_lb.setStyleSheet('font:italic;')
        self.layout().addSpacing(10)

        layout.addStretch()

        label = QtWidgets.QLabel('Renderer:')
        label.setStyleSheet('font:bold;')
        layout.addWidget(label)
        layout.addWidget(self.widget.renderer_cb)
        self.widget.renderer_cb.setMinimumWidth(150)

        layout = QtWidgets.QVBoxLayout()
        self.layout().addLayout(layout)
        layout.addWidget(self.widget.tasks_tw)

        layout = QtWidgets.QHBoxLayout()
        self.layout().addLayout(layout)
        layout.addWidget(self.widget.status_lb)
        layout.addStretch()
        layout.addWidget(self.widget.update_all_pb)

        self.resize(800, 280)