#!Python3
################################################################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
################################################################################
##
##  Standard Library
##
import os
try:
    from PySide2 import QtCore
except ImportError:
    from PySide import  QtCore

################################################################################
##
##  Maya Library
##
import maya.cmds as cmds
import maya.mel as mel
import maya.OpenMayaUI as omui
import maya.utils as mu
################################################################################
##
##  Logging
##
from . import Logger

################################################################################
##
##  Snow Modules
##
import snow.shotgrid.lib.wrap as sg_wrap
from snow.maya.lib import reference
from snow.maya.lib import shade
import snow.maya.lib.wrap as m_wrap


def load_renderer_plugin(renderer):
    if renderer == 'redshift':
        m_wrap.redshift.load()
    elif renderer == 'arnold':
        m_wrap.arnold.load()
    cmds.setAttr("defaultRenderGlobals.currentRenderer",
                    renderer, type='string')


def get_exists_task_version(file_path):
    namespace = os.path.splitext(
                            os.path.basename(file_path))[0]
    namespace = namespace.split('.')[0].replace(' ', '_')


def group_all(except_name=[]):

    main_grp = "Vfx_Grp"
    if not cmds.objExists(main_grp):
        cmds.group(n=main_grp, em=True)

    except_set = set(cmds.listRelatives(cmds.ls(cameras=True), p=True))
    except_set.add(main_grp)
    trn_set = set(cmds.ls(assemblies=True))
    if except_name:
        trn_set = trn_set.difference(set(except_name))
    element = list(trn_set.difference(except_set))
    if element:
        for e in element:
            obj_exists = '|{}|{}'.format(main_grp, e)
            if cmds.objExists(obj_exists):
                cmds.delete(obj_exists)
        cmds.parent(element, main_grp)


def build_alembic(abc_file, shade_file=''):
    namespace = os.path.splitext(
                            os.path.basename(abc_file))[0]

    Logger.debug('build_alembic : [namespace=%s]', namespace)
    
    node_name = '{}_AlembicNode'.format(namespace)
    exists_nodes = cmds.ls('{}*'.format(node_name))

    Logger.debug('build_alembic : [node_name=%s]', node_name)

    ## Remove old alembic
    if exists_nodes:
        Logger.debug('build_alembic : delete %s', exists_nodes)
        cmds.delete(exists_nodes)

    ## Remove object name as group name
    if cmds.objExists(namespace):
        cmds.delete(namespace)
    
    group_exists = cmds.ls(assemblies=True)

    m_wrap.abc._import(abc_file,
                importTimeRange='override')

    if shade_file:
        cmds.file(shade_file, i=True, rdn=True)
        shade.assign_shade()

    new_abc_node = [grp for grp in cmds.ls(assemblies=True) if grp not in group_exists]
    cmds.group(new_abc_node, n=namespace)


def build_rsproxy(filepath):
    m_wrap.redshift.load()
    namespace = os.path.splitext(
                            os.path.basename(filepath))[0]
    namespace = namespace.split('.')[0]
    prx_mesh = namespace+'_proxymesh'
    prx_holder = namespace+'_placeholder'

    ## For build
    if not cmds.objExists(prx_mesh):

        ## Remove object name as group name
        if cmds.objExists(namespace):
            cmds.delete(namespace)

        cmds.select(cl=True)
        rsProxyMesh, rsProxyPlaceShape, reProxyPlaceHolder = mel.eval( "redshiftCreateProxy" ) 
        reProxyPlaceHolder = cmds.rename( reProxyPlaceHolder, prx_holder )
        rsProxyMesh = cmds.rename(rsProxyMesh, prx_mesh)

    ## For update
    cmds.setAttr(prx_mesh+'.fileName', filepath, type='string')
    cmds.setAttr(prx_mesh+".useFrameExtension", 1)
    cmds.setAttr(prx_mesh+".displayMode", 1)
    cmds.select(prx_mesh)

    if not cmds.objExists(namespace):
        cmds.group(prx_holder, n=namespace)


def build_maya(filepath):
    namespace = os.path.basename(filepath).split('.')[0]
    exists_rfn = ''

    for rfn, data in reference.get_path_data(
                                get_all=True, no_unload=True).items():

        if data['namespace'] == (':'+namespace):
            exists_rfn = rfn
            break

    if exists_rfn:
        Logger.debug('build_maya : has been build : [exists_rfn=%s]', exists_rfn)
        reference.replace_ref(node_name=exists_rfn, path=filepath)
    else:
        Logger.debug(
                'build_maya : build : [filepath=%s, namespace=%s]',
                filepath, namespace)

        if cmds.objExists(namespace):
            cmds.delete(namespace)

        exists_rfn = cmds.file(filepath, reference=True,
                            namespace=namespace, sharedNodes="shadingNetworks")
    return exists_rfn


class QueryTasksThread(QtCore.QThread):
    ''' Get step's task each shot on ShotGrid
    Parameters:-
        project : <str> project name / code
        shot : <str> shot code
        step : <str> step name
    Signals:-
        tasks_listed : emit a list of steps
    '''
    tasks_listed = QtCore.Signal(list)
    shot_entity = QtCore.Signal(dict)

    def __init__(self, project, episode, shot, step):
        super(QueryTasksThread, self).__init__()
        self.project = project
        self.episode = episode
        self.shot = shot
        self.step = step
        self.data = dict(
                        project=self.project,
                        episode=self.episode,
                        entity_code=self.shot,
                        step=self.step)
        Logger.info('ShotGrid query tasks: {}'.format(self.data))

    def run(self):
        Logger.debug('QueryTasksThread : [project=%s]', self.project)
        Logger.debug('QueryTasksThread : [episode=%s]', self.episode)
        Logger.debug('QueryTasksThread : [shot=%s]', self.shot)

        shots = sg_wrap.shot.get_shots(
                                    project=self.project,
                                    episode=self.episode,
                                    shot=self.shot)
        Logger.debug('QueryTasksThread : [shots=%s]', shots)
        self.shot_entity.emit(shots[0])
        entity_code = shots[0]['code']
        tasks = sg_wrap.task.get_step_tasks(entity_code=entity_code,
                                            step=self.step,
                                            project=self.project,
                                            episode=self.episode)
        self.tasks_listed.emit(sorted(tasks))
