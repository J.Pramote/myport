from snow.common.lib import code_logging

Logger = code_logging.create_log(__name__)
Export_Type = ['Alembic', 'Redshift Proxy', 'mayaAscii']
RendererList = ['redshift', 'arnold']
Ext_Name = {'.abc': 'Alembic', '.rs': 'Redshift Proxy', '.ma': 'mayaAscii'}
Shade_Name = {'redshift': 'rsshade',  'arnold': 'anshade'}