#!Python3
################################################################################
##
##	@ 2021
##	Written by Jutamas Pramote <j.pramote153@gmail.com>
##
################################################################################
##
##  Standard Library
##
import os
import shutil
try:
    from PySide2 import QtWidgets, QtCore, QtGui
except ImportError:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets


################################################################################
##
##  Maya Library
##
import maya.cmds as cmds
import maya.mel as mel
import maya.OpenMayaUI as omui

################################################################################
##
##  Logging
##
from . import Logger

################################################################################
##
##  Snow Modules
##
import snow.shotgrid.lib.wrap as sg_wrap
import snow.maya.lib.wrap as m_wrap
from snow.maya.lib import shade
from snow.common.lib import os_file
from snow.common.lib.widget import message_box
from snow.common.lib import ig_path
from snow.maya.lib.widgets import Maya_Main_Window

################################################################################
##
##  Relative Modules
##
import fx_publish_gui as gui
import fx_publish_helper as helper
from . import Export_Type, RendererList, Shade_Name

################################################################################
##
##  Global Variables
##
__Object_ID__ = 'FxPublish'

################################################################################
##
##		Window Class
##
class FxPublish(gui.FxPublishWindow):

    def __init__(self, parent=None):
        super(FxPublish, self).__init__(parent)
        self.setObjectName(__Object_ID__)
        self.cur_scene = cmds.file(q=True, loc=True)
        Logger.info('Current scene:' + self.cur_scene)

        try:
            self.ig_path = ig_path.Path(path=self.cur_scene)
        except:
            msg = "It isn't  format path"
            message_box.MessageBox(self).error(msg)
            Logger.exception(msg)

        self.file_project = self.ig_path.project_root_data
        self.file_episode = self.ig_path.episode_data
        self.file_seqeunce = self.ig_path.sequence_data
        self.file_shot = self.ig_path.shot_data
        self.step = 'FX'
        self.shot_entity = ''
        self.query_shotgrid_thread = helper.QueryTasksThread(
            project=self.file_project,
            episode=self.file_episode,
            shot=self.file_shot,
            step=self.step)

        ## Connect Signal
        self.widget.tasks_tw.get_clicked.connect(self.group_selected)
        self.widget.tasks_tw.itemChanged.connect(self.task_item_changed)
        self.widget.tasks_tw.customContextMenuRequested.connect(
            self.show_context_menu)
        self.widget.tasks_tw.type_changed.connect(self.type_selected)
        self.widget.publish_pb.clicked.connect(self.publish)
        self.widget.renderer_cb.currentTextChanged.connect(
            helper.load_renderer_plugin)
        self.query_shotgrid_thread.started.connect(self.tasks_loading_event)
        self.query_shotgrid_thread.finished.connect(self.tasks_finished_event)
        self.query_shotgrid_thread.tasks_listed.connect(self.add_task_items)
        self.query_shotgrid_thread.shot_entity.connect(
            lambda x: setattr(self, 'shot_entity', x))

        ## Set value in widgets
        self.widget.project_lb.setText(self.file_project)
        self.widget.episode_lb.setText(self.file_episode)
        self.widget.shot_lb.setText(self.file_shot)
        self.widget.renderer_cb.addItems(RendererList)
        renderer = cmds.getAttr("defaultRenderGlobals.currentRenderer")
        self.widget.renderer_cb.setCurrentIndex(
            self.widget.renderer_cb.findText(renderer, QtCore.Qt.MatchExactly))
        self.query_shotgrid_thread.start()

    def closeEvent(self, event):
        if self.query_shotgrid_thread.isRunning():
            self.query_shotgrid_thread.terminate()

    def fill_framerage_item(self, item):
        ''' Set value in start and end frame widget
        '''
        item.set_enabled(4, True)
        item.set_enabled(5, True)
        min_frame, max_frame = m_wrap.playback.get_playback_range()
        item.set_data(4, str(min_frame))
        item.set_data(5, str(max_frame))

    def clear_task_item(self):
        item = self.widget.tasks_tw.currentItem()
        item.setText(1, '')
        item.clear_data(2)
        item.clear_data(3)
        item.clear_data(4)
        item.clear_data(5)

    @QtCore.Slot()
    def show_context_menu(self, pos):
        item = self.widget.tasks_tw.itemAt(pos)
        if not item:
            return
        menu = QtWidgets.QMenu()
        menu.addAction('Clear', self.clear_task_item )
        menu.exec_(self.widget.tasks_tw.mapToGlobal(pos))

    @QtCore.Slot()
    def task_item_changed(self, item, column):
        export_type = item.data(2, QtCore.Qt.DisplayRole)

        ## Export shade has changed
        if column == 3:
            if not export_type:
                item.setCheckState(3, QtCore.Qt.Unchecked)
                return
            ## Check current type
            self.type_selected(item, 2)

    @QtCore.Slot()
    def tasks_loading_event(self):
        self.widget.tasks_tw.setEnabled(False)
        self.widget.publish_pb.setEnabled(False)
        self.widget.status_lb.setText('Tasks loading..')

    @QtCore.Slot()
    def tasks_finished_event(self):
        self.widget.tasks_tw.setEnabled(True)
        self.widget.publish_pb.setEnabled(True)
        self.widget.status_lb.clear()

    @QtCore.Slot()
    def add_task_items(self, tasks):
        ''' Add tasks to widget '''
        for sg_task in tasks:
            item = gui.TaskTreeWidgetItem(self.widget.tasks_tw)
            item.setText(0, sg_task['content'])
        self.widget.tasks_tw.sortItems(0, QtCore.Qt.AscendingOrder)

    @QtCore.Slot()
    def type_selected(self, item, column):
        ''' Export Type Role :-
             -----------------------------------------------------------------------
            | 		Type		|	Enable Export shade	|	Edit start/end frame	|
             -----------------------------------------------------------------------
            | Redshift Proxy	|			NO			|		 	YES				|
            | mayaAscii			|			YES			|			NO				|
            | Alembic			|			YES			|			YES				|

        '''
        type_name = item.data(column, QtCore.Qt.DisplayRole)
        if type_name == 'Redshift Proxy':
            item.setCheckState(3,  QtCore.Qt.Unchecked)
            self.fill_framerage_item(item)

        elif type_name == 'mayaAscii':
            item.clear_data(4)
            item.set_enabled(4, False)
            item.clear_data(5)
            item.set_enabled(5, False)

        else:
            self.fill_framerage_item(item)

    @QtCore.Slot()
    def group_selected(self, item, column):
        ''' Receive for export group signal only'''
        ## Column 1 : Export Group
        if column != 1:
            return

        item.clear_data(2)
        sel = cmds.ls(sl=True)
        if not sel or not m_wrap.selection.is_group_type(sel[0]):
            message_box.MessageBox(self).warning('Must to select only group')
            return

        item.set_data(1, sel[0])
        ## When group is <none> then clear option in export type column
        if item.data(column, QtCore.Qt.DisplayRole):
            item.set_data(2, Export_Type)
            self.type_selected(item, 2)

        else:
            ## Clear nested data if <none> group
            item.setCheckState(3, QtCore.Qt.Unchecked)
            item.clear_data(4)
            item.clear_data(5)

    def export_shade(self, export_path, top_geo_grp):
        renderer = cmds.getAttr('defaultRenderGlobals.currentRenderer')
        if renderer == 'redshift':
            shade_file = '{}_{}.ma'.format(
                        export_path, Shade_Name[renderer])
        elif renderer == 'arnold':
            shade_file = '{}_{}.ma'.format(
                        export_path, Shade_Name[renderer])
        else:
            Logger.warning('Current renderer is {}'.format(renderer))
            shade_file = '{}__shade.ma'.format(export_path)

        shade.export_selected_shade(path=shade_file, 
                                    top_geo_grp=top_geo_grp,
                                    full_path=True)

    def export_vfx(self, data):
        ''' Parameters:
                data : <dict> keys are export type, 
                    values is a list of same type export tasks that include:
                        index 0 : <str> export object name
                        index 1 : <str> export path
                        index 2 : <str> or <None> start frame
                        index 3 : <str> or <None> end frame 
                        index 4 : <str> or <None> taskname 
                        index 5 : <bool> export shade state
        Return - <list> tasks have been exported already
        '''
        exported_tasks = []
        export_list = data.get('Alembic')
        if export_list:
            export_abc = m_wrap.abc.ExportAbc()
            job_task = []
            for e in export_list:
                path = e[1] + '.abc'
                export_abc.add_job(obj=e[0], path=path,
                                    start=int(e[2]), end=int(e[3]))
                ## Export shade
                if e[5]:
                    self.export_shade(e[1], e[0])
                job_task.append(e[4])
            export_abc.run()
            exported_tasks += job_task

        export_list = data.get('Redshift Proxy')
        if export_list:
            for e in export_list:
                folder, prefix = os.path.split(e[1])
                try:
                    result = m_wrap.redshift.export_rs_prx(folder=folder, prefix=prefix,
                                    obj=e[0], start=int(e[2]), end=int(e[3]))
                    print(result)
                except RuntimeError as err:
                    print('RuntimeError: {}'.format(err))
                    ## Clean up if nothing exported
                    rs_seq_dir = '{}/{}_rs'.format( folder,prefix)
                    if os.path.exists(rs_seq_dir):
                        print('will remove >> ', rs_seq_dir)
                        if not os.listdir(rs_seq_dir):
                            print('remove >> ', rs_seq_dir)
                            shutil.rmtree(rs_seq_dir)
                    pass

                ## Export shade
                if e[5]:
                    self.export_shade(e[1], e[0])
                exported_tasks.append(e[4])

        export_list = data.get('mayaAscii')
        if export_list:
            for e in export_list:
                path = e[1] + '.ma'
                if not os.path.exists(os.path.dirname(path)):
                    os.makedirs(os.path.dirname(path))
                cmds.select(e[0])
                # TODO: export maya without namespace
                cmds.file(path, force=True, options="v=0", pr=False,
                        type="mayaAscii", exportSelected=True)
                ## Export shade
                if e[5]:
                    self.export_shade(e[1], e[0])
                exported_tasks.append(e[4])
            cmds.select(cl=True)

        return exported_tasks

    def publish_vfx(self):
        dep_publish_dir = self.ig_path.getHeroDir().replace('hero', 'publish')
        if not os.path.exists(dep_publish_dir):
            os.makedirs(dep_publish_dir)
        new_version = os_file.increase_version(dep_publish_dir)
        Logger.info('Publish Version: {}'.format(new_version))

        ## Init export_data keys
        export_data = dict()
        for e in Export_Type:
            export_data[e] = []

        ## Keep data each task items
        all_task_items = self.widget.tasks_tw.findItems('',
                                            QtCore.Qt.MatchContains, 0)
        for item in all_task_items:
            top_geo_grp = item.data(1, QtCore.Qt.DisplayRole)
            if not top_geo_grp or top_geo_grp == '<none>':
                continue
            taskname = item.data(0, QtCore.Qt.DisplayRole)
            export_type = item.data(2, QtCore.Qt.DisplayRole)
            is_export_shade = item.checkState(3)
            start_frame = item.data(4, QtCore.Qt.DisplayRole)
            end_frame = item.data(5, QtCore.Qt.DisplayRole)

            filename = '{}_{}_{}__VFX_{}'.format(
                                                self.file_episode,
                                                self.file_seqeunce,
                                                self.file_shot,
                                                taskname.replace(' ','_'))
            export_path = '{}/{}'.format(new_version, filename)
            export_list = export_data.get(export_type) or []
            new_list = [top_geo_grp, 
                        export_path, 
                        start_frame or None, 
                        end_frame or None,
                        taskname, 
                        is_export_shade == QtCore.Qt.Checked]

            export_list.append(new_list)
            export_data[export_type] = export_list

            Logger.info('Export VFX : {}'.format(export_data))

        ## Export vfx
        Logger.info('Export VFX : {}'.format(export_data))
        tasks = self.export_vfx(export_data)

        if os.listdir(new_version):
            ## Update status in ShotGrid
            sg_task = sg_wrap.task.Task(
                                project=self.file_project,
                                entity_code=self.shot_entity['code'],
                                step=self.step)
            for taskname in tasks:
                sg_task.set_status( content=taskname,
                                    status='sub')

            message_box.MessageBox(self).success()
            ## Open output directory after finished
            os.system('explorer "{}"'.format(os.path.realpath(new_version)))
        else:
            shutil.rmtree(new_version)
            message_box.MessageBox(self).error('Nothing exported!')

    @QtCore.Slot()
    def publish(self):
        is_build = False
        ## Ask for building after publish
        ans = message_box.MessageBox(self).question(
            'Would you like to build all vfx elements after publish?')
        if ans == QtWidgets.QMessageBox.Yes:
            is_build = True
            cmds.file(f=True, type='mayaAscii', save=True)

        self.publish_vfx()

        if is_build:
            cmds.file(f=True, new=True)
            from snow.maya.tools.fx_publish import fx_build
            fx_build.main(self.cur_scene)
            self.close()
        
            
################################################################################
##
##		Main Function
##
def main():
    if Maya_Main_Window:
        widget = Maya_Main_Window.findChild( QtWidgets.QWidget, __Object_ID__ )
        if widget:
            widget.close()
            widget.deleteLater()

    widget = FxPublish(parent=Maya_Main_Window)
    widget.setWindowFlag(QtCore.Qt.Window)
    widget.show()
