#!/usr/bin/env python3
""" THIS MODULE SUPPORT PYTHON 2 and 3
Texture Publisher Commandline
"""
# Standard Library
import sys
import argparse
import os


def publish_texture_cmd(args):
    """Publish Texture

    Args:
        args.job (str) : project name
        args.category (str) : asset category
        args.asset (str) : asset name
        args.element (str) : department
        args.model_variation (str) : model variation
        args.shader_variation (str) : shader variation
        args.ddr (int) : ddr
        args.branch_id (int) : branch id
        args.status_id (int): ticket status
        args.texture_list (list) : textures
        args.user_name (str) : user name
        args.workfile (list) : work files
        args.comment_txt (str) : comment
        args.ticket_name (str) : ticket name

    """
    # TODO: For nodeName, After Exporter has been doen, it will get from texture set name
    # in meta data file, which generate by Exporter tool.
    shader_asset = mnk_asset.MnkShaderAsset(
        jobName=args.job,
        categoryName=args.category,
        shareName=args.asset,
        variationName=args.shader_variation,
        elementName=args.element,
        # nodeName=args.texture_set,
        modelVariationName=args.model_variation,
    )

    if not shader_asset.isRegisterToDatabase():
        shader_asset.registerToDatabase()
    if not shader_asset.getIsActive():
        shader_asset.activate()

    # Publish version
    shader_asset.increasePublishVersion()
    publish_version = shader_asset.getPublishVersion()
    base_folder = shader_asset.getPublishBaseFolder()

    publish_tex_folder = os.path.join(base_folder, "tex", "v%03d" % (publish_version))
    mnk_sys.MnkCreateFolder(publish_tex_folder)

    # -----------------------------
    # publish scene
    publish_scene_folder = publish_tex_folder.replace("/tex/", "/srcfile/")
    if not os.path.exists(publish_scene_folder):
        mnk_sys.MnkCreateFolder(publish_scene_folder)
    for w_file in args.work_file:
        publish_scene_file = "{}/{}".format(
            publish_scene_folder, os.path.basename(w_file)
        )

        # lock current file #
        if os.access(w_file, os.W_OK):
            mnk_sys.MnkMakeReadOnly(w_file)

        backup_path = ""
        if os.path.exists(publish_scene_file):
            backup_path = "{0}.bak".format(publish_scene_file)
            mnk_sys.MnkMove(publish_scene_file, backup_path)
        print('echo "## save work file to {}"'.format(publish_scene_file))
        mnk_sys.MnkCopy(w_file, publish_scene_file)
        if os.path.exists(backup_path):
            mnk_sys.MnkRemove(backup_path)

    # ------------------------------
    # publish texture
    texture_asset_list = []
    thd = ThreadManager.ThreadManager(env=os.environ.copy())

    texture_list = file_op.read_json(args.texture_list_json).get("texture_list", [])

    for tex_file in texture_list:
        tex_basename = os.path.basename(tex_file)
        tex_name = os.path.splitext(tex_basename)[0]
        texture_asset = mnk_asset.MnkTextureAsset.fromMnkShaderAsset(
            tex_name, shader_asset
        )
        if not texture_asset.isRegisterToDatabase():
            texture_asset.registerToDatabase()

        # Copy to publish process #
        texture_asset.setPublishVersion(publish_version)
        publish_tex_folder = texture_asset.getPublishFolder(createIfNotExist=True)
        publish_file_path = os.path.join(publish_tex_folder, tex_basename)

        tmp_publish_path = "{}.new".format(publish_file_path)

        publish_file_dict = {publish_file_path: tmp_publish_path}
        cmd = ["import mnk_sys", "from mnk_common_utils import proj"]
        cmd += [
            "mnk_sys.MnkCopy('{tex_file}', '{tmp_publish_path}', force=True)".format(
                tex_file=os.path.realpath(tex_file),
                tmp_publish_path=os.path.realpath(tmp_publish_path),
            )
        ]

        if args.need_tx:
            # covert texture in tmp
            tx_publish_path = os.path.join(publish_tex_folder, tex_name + ".tx")
            tmp_tx_path = os.path.join(publish_tex_folder, tex_name + ".new.tx")
            publish_file_dict[tx_publish_path] = tmp_tx_path
            cmd += [
                "from mnk_common_utils import image_utils",
                "image_utils.oiiotool_convert_tx('{tex_file}', '{tmp_tx_path}', resize='{size}')".format(
                    tex_file=os.path.realpath(tex_file),
                    tmp_tx_path=os.path.realpath(tmp_tx_path),
                    size="" if args.resize is None else args.resize,
                ),
            ]

        for p_file in list(publish_file_dict.keys()):
            cmd += [
                "mnk_sys.MnkRemove('{publish_file_path}')".format(
                    publish_file_path=p_file
                )
            ]

        for p_file, t_file in publish_file_dict.items():
            cmd += [
                "mnk_sys.MnkMove('{tmp_publish_path}', '{publish_file_path}')".format(
                    tmp_publish_path=os.path.realpath(t_file),
                    publish_file_path=os.path.realpath(p_file),
                ),
                "proj.runFunction('{job}', 'publish_texture', proj.dummyFunction)('{texture_file}')".format(
                    job=args.job, texture_file=p_file
                ),
            ]

        thd.addToQueue('python3 -c "{}"'.format("\n".join(cmd)))

        #
        thd.waitUntilFinish()
        texture_asset_list.append(texture_asset)

    # -------------------------------
    # Record DB #
    branchid = args.branch_id  # is Bangkok branch
    staffid = database_utils.getStaffId(args.user_name)
    comment_text = urllib.parse.unquote(args.comment_txt)

    shader_asset.recordPublication(
        None,
        None,
        texture_asset_list,
        staffid,
        branchid,
        args.ddr,
        args.work_file[0] if args.work_file else "",
        comment_str=comment_text,
        statusdetailid=15,  # is "publish success" status
        ticket_name=args.ticket_name,
        extra_info=(
            "ddrTake={ddr} comment={comment} workFiles={workFiles}".format(
                ddr=args.ddr, comment=comment_text, workFiles=args.work_file
            ),
        ),
    )

    print("Result='{}'".format(shader_asset.publishId))

    # ------------------------------
    # Limit publish version
    asset_publish_ctrl = version_ctrl.PublishVersionCtrl(shader_asset)
    publish_folder = shader_asset.getPublishBaseFolder()
    if os.path.exists(publish_folder):
        asset_publish_ctrl.remove_over_limit(subfolders=os.listdir(publish_folder))

    # -------------------------------
    # send message to related staff
    receiver_list = im.getReceiverList(
        args.job,
        "share",
        args.category,
        args.asset,
        args.element,
        "publish",
        staffName=args.user_name,
    )

    # For debug mode
    if os.getenv("MNK_DEBUG"):
        receiver_list = [args.user_name]

    im.sendMessage(
        receiver_list,
        args.user_name,
        "publish",
        args.job,
        "share",
        args.category,
        args.asset,
        args.element,
        args.ddr,
        comment_text,
        args.ticket_name,
    )

    os.system('echo "## `date`: sent message to {}"'.format(receiver_list))

    os.system('echo "### `date`: [%s] Publish Done."' % (shader_asset))


if __name__ == "__main__":

    ARG_PARSER = argparse.ArgumentParser(
        prog=os.path.basename(__file__),
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="publish texture",
    )
    ARG_PARSER.add_argument(
        "-j", "-job", dest="job", required=True, type=str, help="job name"
    )
    ARG_PARSER.add_argument(
        "-c",
        "-category",
        dest="category",
        required=True,
        type=str,
        help="category name",
    )
    ARG_PARSER.add_argument(
        "-a", "-asset", dest="asset", required=True, type=str, help="asset name"
    )
    ARG_PARSER.add_argument(
        "-e", "-element", dest="element", required=True, type=str, help="element name"
    )
    ARG_PARSER.add_argument(
        "-mv",
        "-model_variation",
        dest="model_variation",
        type=str,
        required=True,
        help="model variation name",
    )
    ARG_PARSER.add_argument(
        "-sv",
        "-shader_variation",
        dest="shader_variation",
        type=str,
        required=True,
        help="shader variation name",
    )
    ARG_PARSER.add_argument(
        "-d", "-ddr", dest="ddr", default=0, type=int, help="ddr number"
    )
    ARG_PARSER.add_argument(
        "-b", "-branch", dest="branch_id", default=1, type=int, help="branch id"
    )
    ARG_PARSER.add_argument(
        "-st",
        "-status",
        dest="status_id",
        default=10,
        type=int,
        help="stutus id (Default is 'pending' status",
    )

    ARG_PARSER.add_argument(
        "-t",
        "-texture_list_json",
        dest="texture_list_json",
        required=True,
        type=str,
        help="texture list json",
    )

    ARG_PARSER.add_argument(
        "-u", "-user", dest="user_name", required=True, help="user name"
    )

    ARG_PARSER.add_argument(
        "-w",
        "-workfile",
        dest="work_file",
        default=[],
        nargs="*",
        type=str,
        help="working file (could have more than one)",
    )

    ARG_PARSER.add_argument(
        "-co",
        "-comment",
        dest="comment_txt",
        default="",
        type=str,
        help="publish comment",
    )
    ARG_PARSER.add_argument(
        "-tk",
        "-ticket",
        dest="ticket_name",
        required=True,
        type=str,
        help="ticket name",
    )

    CONVERT_ARG_PARSER = ARG_PARSER.add_argument_group("converter arguments")

    CONVERT_ARG_PARSER.add_argument(
        "-txc",
        "-tx_convert",
        dest="need_tx",
        action="store_true",
        default=False,
        help="convert texture to .tx",
    )

    CONVERT_ARG_PARSER.add_argument(
        "-r",
        "-resize",
        dest="resize",
        type=str,
        help="resize texture when convert process (Example: 64x64)",
    )

    ARG_PARSER.add_argument(
        "-tp",
        "-testProj",
        dest="isTestProj",
        action="store_true",
        default=False,
        help="use tmp workspace projlib",
    )

    ARG_PARSER.add_argument(
        "-tc",
        "-testCommon",
        dest="isTestCommon",
        action="store_true",
        default=False,
        help="use tmp workspace commonlib",
    )

    try:
        ARGS = ARG_PARSER.parse_args()
    except IOError as msg:
        ARG_PARSER.error(str(msg))

    MNK_COMMON_SCRIPT_PATH = (
        os.getenv("MNK_COMMON_DEV", "/tmp/workspace/commonlib/scripts")
        if ARGS.isTestCommon or os.getenv("MNK_DEBUG")
        else "/mnt/studio/commonlib/scripts"
    )
    PER_PROJECT_SCRIPT_PATH = (
        os.getenv("PROJLIB_DEV_PATH", "/tmp/workspace/projlib")
        if ARGS.isTestProj or os.getenv("MNK_DEBUG")
        else "/mnt/studio/projlib"
    )
    MNK_PROJLIB_PATH = os.path.join(
        PER_PROJECT_SCRIPT_PATH, "common", "scripts", "python"
    )

    TEXTURE_LIB_PATH = (
        os.getenv("TEXTURE_DEV_PATH", "/tmp/workspace/texture")
        if ARGS.isTestProj or os.getenv("MNK_DEBUG")
        else os.getenv("TEXTURE_LIB_PATH", "/mnt/studio/texture")
    )

    if MNK_COMMON_SCRIPT_PATH not in sys.path:
        sys.path.insert(0, MNK_COMMON_SCRIPT_PATH)
    if MNK_PROJLIB_PATH not in sys.path:
        sys.path.insert(0, MNK_PROJLIB_PATH)
    if TEXTURE_LIB_PATH not in sys.path:
        sys.path.insert(0, TEXTURE_LIB_PATH)
    os.environ["PYTHONPATH"] = os.pathsep.join([p for p in sys.path if p])

    print("## MNK_COMMON_SCRIPT_PATH = {0}".format(MNK_COMMON_SCRIPT_PATH))
    print("## MNK_PROJLIB_PATH = {0}".format(MNK_PROJLIB_PATH))
    print("## TEXTURE_LIB_PATH = {0}".format(TEXTURE_LIB_PATH))
    print("## PER_PROJECT_SCRIPT_PATH = {0}".format(PER_PROJECT_SCRIPT_PATH))

    os.environ["PER_PROJECT_SCRIPT_PATH"] = PER_PROJECT_SCRIPT_PATH

    if ARGS.isTestCommon or ARGS.isTestProj:
        os.environ["MNK_DEBUG"] = "1"
        os.environ["PROJLIB_DEV_PATH"] = PER_PROJECT_SCRIPT_PATH
        os.environ["MNK_COMMON_SCRIPT_PATH"] = MNK_COMMON_SCRIPT_PATH
        os.environ["TEXTURE_LIB_PATH"] = TEXTURE_LIB_PATH

    # Mnk Commonlib
    import mnk_sys
    import mnk_asset

    from mnk_common_utils import database_utils, version_ctrl
    from mnk_common_utils import message as im
    from mnk_core import file_op
    import ThreadManager

    # future lib
    from future import standard_library

    standard_library.install_aliases()
    from builtins import str
    import urllib.request, urllib.parse, urllib.error

    publish_texture_cmd(ARGS)

