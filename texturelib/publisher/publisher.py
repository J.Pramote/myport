#!/usr/bin/env python3
"""THIS MODULE SUPPORT PYTHON 3"""

# Standard Library
import os
import sys
import time
import subprocess
import getpass
import urllib.parse as urllib
import logging
import re

# Mnk Commonlib
import mnk_sys
import mnk_resource
import mnk_asset
from mnk_qt.Qt import QtWidgets, QtCore
from mnk_qt import qt_utils

import mnk_core.file_op as file_op
from mnk_core import path as mnk_path, ddr
from mnk_common_utils import file_utils, message as im, pref

# Relative Modules
from core.ui.base_widget import FileBrowserWidget
from core.base_ui import (
    InformationUI,
    AssetBrowserUI,
    ShaderPublishInformationUI,
    SUBMIT_STATE,
    STYLESHEET,
)

LOGGER = logging.getLogger(__name__)


class SubelementComboBox(QtWidgets.QComboBox):
    """SubelementComboBox
    """

    def __init__(self):
        """Initialize Function
        """
        super(SubelementComboBox, self).__init__()
        self.setEditable(True)
        self.setMouseTracking(True)
        self.addItem("")


class NewWorkFileDialog(QtWidgets.QDialog, InformationUI):
    """NewWorkFileDialog
    """

    output_result = []

    def __init__(self, parent=None, directory="", input_files=[]):
        """Initialize Function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
            directory (str, optional): root directory. Defaults to "".
            input_files (list, optional): a list of source files. Defaults to [].
        """
        super(NewWorkFileDialog, self).__init__(parent=parent)
        # settings
        self.setWindowFlags(QtCore.Qt.Dialog)
        self.setWindowModality(QtCore.Qt.WindowModal)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("Add Work File")

        # declar properties
        self.file_items = []
        self.subelements = []

        # declar widgets and layouts
        self.work_dir_lb = QtWidgets.QLabel(directory)
        self.apply_pb = QtWidgets.QPushButton("Apply")
        self.cancel_pb = QtWidgets.QPushButton("Cancel")
        self.file_layout = QtWidgets.QGridLayout()

        # connect signals
        self.apply_pb.clicked.connect(self.apply)
        self.cancel_pb.clicked.connect(self.reject)

        # get all subelements
        for scene in os.listdir(directory):
            fullpath = "{}/{}".format(directory, scene)
            if os.path.isfile(fullpath):
                found = re.findall(
                    r".{element}(\w+)_{class_name}".format(
                        element=self.asset_info.get("element"),
                        class_name=self.asset_info.get("class_name"),
                    ),
                    scene,
                )
                if found and found[0] not in self.subelements:
                    self.subelements.append(found[0])

        # initial layout
        self.init_layout()

        # intial content
        for count, i_file in enumerate(input_files):
            new_file_wd = self.add_file_item(count, i_file)
            new_file_wd.combobox.currentIndexChanged.connect(self.set_output_file)
            self.file_layout.addWidget(new_file_wd, count, 0)
            self.file_items.append(new_file_wd)

    def init_layout(self):
        """Main initialize layout
        """
        self.setLayout(QtWidgets.QVBoxLayout())
        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignLeft)
        layout.addWidget(QtWidgets.QLabel("<b>Directory</b>"))
        layout.addWidget(self.work_dir_lb)
        self.layout().addLayout(layout)

        self.file_layout = QtWidgets.QGridLayout()
        self.layout().addLayout(self.file_layout)
        self.layout().addSpacing(30)
        layout = QtWidgets.QHBoxLayout()
        layout.addWidget(self.apply_pb)
        layout.addWidget(self.cancel_pb)
        self.layout().addLayout(layout)

    def set_output_file(self):
        """Set output path on a file item and set all comboboxs of a list of subelements
        to be same values
        """
        file_item = self.sender().parent()
        ext = os.path.splitext(file_item.output_file.text())[-1]
        path = file_utils.getAutoNamingFilePath(
            jobName=self.asset_info.get("job"),
            className=self.asset_info.get("class_name"),
            sceneName=self.asset_info.get("category"),
            shotName=self.asset_info.get("asset"),
            elementName=self.asset_info.get("element"),
            subElementName=self.sender().currentText(),
            extension=ext,
        )
        file_item.output_file.setText(os.path.basename(path))
        file_item.data[1] = path

        new_subelements = [
            self.sender().itemText(index)
            for index in range(1, self.sender().count())
            if not self.sender().itemText(index) in self.subelements
        ]
        if new_subelements:
            self.subelements += new_subelements
            for combobox in [
                c
                for c in self.findChildren(SubelementComboBox)
                if not c is self.sender()
            ]:
                combobox.addItems(new_subelements)

    def add_file_item(self, count, input_path):
        """Create file item widget

        Args:
            count (int): index of the input file
            input_path (str): input file

        Returns:
            QWidget: file item widget
        """
        output_path = file_utils.getAutoNamingFilePath(
            jobName=self.asset_info.get("job"),
            className=self.asset_info.get("class_name"),
            sceneName=self.asset_info.get("category"),
            shotName=self.asset_info.get("asset"),
            elementName=self.asset_info.get("element"),
            subElementName=self.asset_info.get("subelement"),
            extension=os.path.splitext(input_path)[-1],
        )

        widget = QtWidgets.QWidget()
        # declar children widget
        widget.combobox = SubelementComboBox()
        widget.combobox.setMinimumWidth(100)
        widget.combobox.addItems(self.subelements)
        widget.output_file = QtWidgets.QLabel(os.path.basename(output_path))

        # initial layout
        QtWidgets.QVBoxLayout(widget)
        layout = QtWidgets.QHBoxLayout()
        widget.layout().addLayout(layout)
        layout.addWidget(
            QtWidgets.QLabel(
                "Input file#{}={}".format(count + 1, os.path.basename(input_path))
            )
        )
        layout.addStretch()
        layout.addWidget(QtWidgets.QLabel("Subelement"))
        layout.addWidget(widget.combobox)

        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignLeft)
        widget.layout().addLayout(layout)
        layout.addWidget(QtWidgets.QLabel("Output file#{}=".format(count + 1)))
        layout.addWidget(widget.output_file)

        widget.data = [input_path, output_path]
        return widget

    def apply(self):
        """Do copy and rename process.
        """
        for item in self.file_items:
            mnk_sys.MnkCopy(*item.data)
            self.output_result.append(item.data[1])
        self.accept()

    def keyPressEvent(self, event):
        """Override this class method to ignore Enter key

        Args:
            event (QKeyEvent): a pressed key
        """
        if event.key() == QtCore.Qt.Key_Enter:
            event.ignore()


class PublishTexture(QtWidgets.QWidget, InformationUI):
    """PublishTexture
    """

    def __init__(self, parent=None):
        """Initialize Function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
        """
        super(PublishTexture, self).__init__(parent)
        self.settings = QtCore.QSettings("Studio", "texture_publisher")
        self.asset_info.update({k: self.settings.value(k) for k in self.settings.allKeys()})
        self.setWindowTitle("Publish Texture")
        qt_utils.setStyleSheet(
            self, styleSheetPath=STYLESHEET,
        )
        # Intial variables
        self.element = "Tex"
        self.current_shader_var = ""
        self.current_model_var = ""
        self.current_ticket_item = ""
        self.current_ddr_take = ""
        self.work_dir = ""
        self.hero_dir = ""

        # Declare widgets and layouts
        self.texture_layout = QtWidgets.QHBoxLayout()
        self.browser_wd = AssetBrowserUI(element=self.element)
        self.publish_info_wd = ShaderPublishInformationUI(
            model_widget=self.browser_wd.modelVariationListWidget,
            shader_widget=self.browser_wd.shaderVariationListWidget,
        )

        self.work_file_wd = FileBrowserWidget(
            label="Work file <i>(able to drop files)</i>", headers=["Filename (*.spp)"],
        )
        self.work_file_wd.treewidget.setAcceptDrops(True)
        self.texture_set_wd = FileBrowserWidget(
            label="Texture file", headers=["Resolution", "Texture Set"]
        )
        self.convert_option_gb = QtWidgets.QGroupBox("Convert Options")
        self.convert_option_gb.setCheckable(True)
        self.convert_option_gb.setChecked(False)
        self.convert_tx_cb = QtWidgets.QCheckBox(".tx")
        self.resize_cbb = QtWidgets.QComboBox()
        self.resize_cbb.addItems(
            [
                "None",
                "128x128",
                "256x256",
                "512x512",
                "1024x1024",
                "2048x2048",
                "4096x4096",
            ]
        )

        # Connect signals
        self.browser_wd.root_changed.connect(self.set_directory)
        self.publish_info_wd.publish_pb.clicked.connect(self.on_publish)
        self.convert_option_gb.clicked.connect(self.set_convert_format)
        self.work_file_wd.treewidget.multiple_dropped.connect(self.new_work_file)

        # Initial layouts
        self.init_layout()
        self.browser_wd.set_root_dir()

    def init_layout(self):
        """Main initialize layouts
        """
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().setAlignment(QtCore.Qt.AlignTop)

        h_layout = QtWidgets.QHBoxLayout()
        h_layout.setAlignment(QtCore.Qt.AlignLeft)
        h_layout.addWidget(self.browser_wd)
        self.browser_wd.setMinimumHeight(200)
        h_layout.addLayout(self.texture_layout)
        self.texture_layout.setAlignment(QtCore.Qt.AlignTop)
        self.layout().addLayout(h_layout)

        texture_spliter_wd = QtWidgets.QSplitter(self)
        texture_spliter_wd.setHandleWidth(2)
        texture_spliter_wd.setStyleSheet(
            "QSplitter:handle{background: rgba(0,0,0,50);margin-left: 5px; margin-right: 5px;}"
        )
        self.layout().addWidget(texture_spliter_wd)
        texture_spliter_wd.addWidget(self.work_file_wd)
        texture_spliter_wd.addWidget(self.texture_set_wd)

        self.convert_option_gb.setLayout(QtWidgets.QVBoxLayout())
        self.convert_option_gb.layout().setAlignment(QtCore.Qt.AlignTop)
        self.convert_option_gb.layout().addSpacing(20)
        _layout = QtWidgets.QHBoxLayout()
        _layout.setAlignment(QtCore.Qt.AlignLeft)
        self.convert_option_gb.layout().addLayout(_layout)
        _layout.addWidget(QtWidgets.QLabel("output format"))
        _layout.addWidget(self.convert_tx_cb)

        _layout = QtWidgets.QHBoxLayout()
        _layout.setAlignment(QtCore.Qt.AlignLeft)
        self.convert_option_gb.layout().addLayout(_layout)
        _layout.addWidget(QtWidgets.QLabel("resize"))
        _layout.addWidget(self.resize_cbb)

        texture_spliter_wd.addWidget(self.convert_option_gb)

        self.layout().addWidget(self.publish_info_wd)

        self.publish_info_wd.setMinimumSize(800, 400)

        self.resize(1200, 800)

    def closeEvent(self, event):
        """Override methed to do before it close

        Args:
            event (QCloseEvent): QCloseEvent
        """
        self.settings.setValue("job", self.asset_info.get("job"))
        self.settings.setValue("category", self.asset_info.get("category"))
        self.settings.setValue("asset", self.asset_info.get("asset"))

    def new_work_file(self, path_list=[]):
        """Add new work files from wherever to the work folder.

        Args:
            path_list (list, optional): a list of input files. Defaults to [].
        """
        if os.path.isdir(self.work_dir) and os.path.exists(self.work_dir):

            dialog = NewWorkFileDialog(
                parent=self, directory=self.work_dir, input_files=path_list
            )
            dialog.exec()
            if dialog.output_result:
                self.reload_work_files()

    def get_convert_format_checked(self):
        """Get all output file format is checked

        Returns:
            list: a list of file format that is a label on the combobox
        """
        return (
            [
                c.text()
                for c in self.convert_option_gb.findChildren(QtWidgets.QCheckBox) or []
                if c.isChecked()
            ]
            if self.convert_option_gb.isChecked()
            else []
        )

    def set_convert_format(self):
        """Set default value when Convert Options is checked
        """
        if self.convert_option_gb.isChecked():
            output_format_cb_list = self.convert_option_gb.findChildren(
                QtWidgets.QCheckBox
            )
            # Check first option by default if there is no an option has been checked
            if not [c for c in output_format_cb_list if c.isChecked()]:
                output_format_cb_list[0].setCheckState(QtCore.Qt.Checked)

    def pre_submit_publish(self, activity):
        """Processes needed to be run before submission/publishing

        Args:

        - *activity* (str): "submit" or "publish"

        Returns:
            dict - if we should proceed to submission/publishing otherwise False
        """
        pre_data = {}
        staff = mnk_resource.staff.getStaffFromName(getpass.getuser())
        if staff is None:
            QtWidgets.QMessageBox().warning(
                self,
                "WARNING",
                "Sorry!! You do not have permission to {activity}!!".format(
                    activity=activity
                ),
            )
            return False
        pre_data["staff"] = staff

        error_messages, warning_messages = self.validate_scene_info(activity)
        if error_messages:
            QtWidgets.QMessageBox().warning(self, "WARNING", "\n".join(error_messages))
            return False
        elif warning_messages:
            warning_messages.append("Press Ok to continue publish.")
            answer = QtWidgets.QMessageBox().question(
                self, "Caution", "\n".join(warning_messages)
            )
            if not answer == QtWidgets.QMessageBox.Ok:
                return False

        comment = self.publish_info_wd.comment_te.toPlainText()
        if not comment:
            QtWidgets.QMessageBox().warning(
                self, "WARNING", "Please write comment!!",
            )
            return False
        pre_data["comment"] = comment
        return pre_data

    def validate_scene_info(self, activity):
        """Check if current path and UI inputs are valid.

        Args:

        - *activity* (str): "submit" or "publish"

		Returns:

		- (list): error message list
		- (list): warning message list
        """
        error_messages = []
        warning_messages = []

        if not self.asset_info.get("job"):
            error_messages.append("Job name is missing")
        if not self.asset_info.get("category"):
            error_messages.append("CategoryName name is missing")
        if not self.asset_info.get("asset"):
            error_messages.append("Asset name is missing")

        self.current_model_var = self.publish_info_wd.model_var
        if not self.current_model_var:
            error_messages.append("Please select model variation")
            return error_messages, warning_messages

        self.current_shader_var = self.publish_info_wd.shader_var
        if not self.current_shader_var:
            error_messages.append("Please select shader variation")
            return error_messages, warning_messages

        self.current_ticket_item = self.publish_info_wd.get_current_ticket_item()
        if not self.current_ticket_item:
            error_messages.append("Ticket is missing. Please select one")

        self.current_ddr_take = self.publish_info_wd.get_ddr_take()
        if not self.current_ddr_take or self.current_ddr_take == "0":
            error_messages.append("DDR is missing. Please shoot ddr")
        else:
            ddr_info = ddr.DDRInfo(self.current_ddr_take)
            # check if DDR is int/digit and if exists
            if not ddr_info.is_valid():
                error_messages.append(
                    "Invalid DDR Take {0}".format(self.current_ddr_take)
                )
            elif ddr_info.job_name != self.asset_info.get("job"):
                error_messages.append(
                    "DDR Take: {ddr} does not come from job: {job}".format(
                        ddr=self.current_ddr_take, job=self.asset_info.get("job")
                    )
                )
            elif not ddr_info.is_up_to_date():
                warning_messages.append(
                    "The input ddr is older than image file\n"
                    "The image files will not be copied to hero folder.\n"
                    "\n"
                )

        # OCIO : not found command to query file path in scene
        # Skip validate OCIO

        if activity == "submit":
            ticket_list = self.publish_info_wd.get_selected_ticket_info()
            for ticket_info in ticket_list:
                if ticket_info["submittable"] == SUBMIT_STATE["unchecked"]:
                    error_messages.append(
                        'Cannot submit with "%s" ticket' % ticket_info["name"]
                    )

        return error_messages, warning_messages

    def reload_work_files(self):
        """Reload work file in the treewidget
        """
        self.work_file_wd.treewidget.clear()
        if os.path.exists(self.work_dir):
            all_works = [f for f in os.listdir(self.work_dir) if f.endswith(".spp")]

            for filename in all_works:
                item = QtWidgets.QTreeWidgetItem([filename])
                item.setData(
                    0, QtCore.Qt.UserRole, "{}/{}".format(self.work_dir, filename)
                )
                item.setCheckState(0, QtCore.Qt.Unchecked)
                self.work_file_wd.treewidget.addTopLevelItem(item)

            self.work_file_wd.treewidget.sortItems(0, QtCore.Qt.AscendingOrder)

    def set_directory(self, asset_dir):
        """Add file items into each browser widget

        Args:
            asset_dir (str): root asset directory
        """
        element_dir = "{}/{}".format(asset_dir, self.element)
        self.work_dir = "{}/version".format(element_dir)
        self.hero_dir = "{}/hero".format(element_dir)

        self.info_signal.reloaded.emit()

        # add work file items
        self.reload_work_files()

        # add texture file item
        self.texture_set_wd.treewidget.clear()
        if os.path.exists(self.hero_dir):
            for res in os.listdir(self.hero_dir):
                if os.path.isdir("{}/{}".format(self.hero_dir, res)):
                    res_item = QtWidgets.QTreeWidgetItem(
                        self.texture_set_wd.treewidget, [res]
                    )
                    res_item.setCheckState(0, QtCore.Qt.Unchecked)
                    for texture in os.listdir("{}/{}".format(self.hero_dir, res)):
                        texture_path = "{}/{}/{}".format(self.hero_dir, res, texture)
                        if os.path.isfile(texture_path) and not texture.startswith("."):
                            texture_item = QtWidgets.QTreeWidgetItem(
                                res_item, ["", texture]
                            )
                            texture_item.setData(
                                0, QtCore.Qt.UserRole, texture_path,
                            )

                    if not res_item.childCount():
                        self.texture_set_wd.treewidget.takeTopLevelItem(
                            self.texture_set_wd.treewidget.indexOfTopLevelItem(res_item)
                        )
                    else:
                        res_item.sortChildren(1, QtCore.Qt.AscendingOrder)

    def get_mnk_asset(self):
        """
        Get asset object from current scene info.

        Returns:
            MnkAsset
        """
        ass = mnk_asset.MnkAsset(
            jobName=self.asset_info.get("job"),
            categoryName=self.asset_info.get("category"),
            shareName=self.asset_info.get("asset"),
            elementName=self.asset_info.get("element"),
        )

        if not ass.isRegisterToDatabase():
            ass.registerToDatabase()
        if not ass.getIsActive():
            ass.activate()

        return ass

    def write_shell_file(
        self,
        publish_time,
        publish_status,
        publish_workfile_list,
        texture_list_json,
        convert_format_list,
    ):
        """Write shell publish script

        Args:
            publish_time (int): time
            publish_status (dict): publish status
            publish_workfile_list (list): a list of workfiles
            texture_list_json (str): json file
            convert_format_list (list): a list of converting file extensions

        Returns:
            str : script path
        """
        user = getpass.getuser()
        sh_folder = os.path.join(
            mnk_sys.MNK_BASE_DIR,
            self.asset_info.get("job"),
            self.asset_info.get("class_name"),
            self.asset_info.get("category"),
            self.asset_info.get("asset"),
            self.asset_info.get("element"),
            "shell",
        )

        mnk_sys.MnkCreateFolder(sh_folder)

        sh_file_name = "%s_%s.%sPublish_%s.sh" % (
            self.asset_info.get("category"),
            self.asset_info.get("asset"),
            self.asset_info.get("element"),
            publish_time,
        )

        sh_file = os.path.join(sh_folder, sh_file_name)

        publish_args = [
            "python3",
            os.path.join(
                os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
                "bin",
                "publisher_cmd.py",
            ),
            "-job {job}".format(job=self.asset_info.get("job")),
            "-category {category}".format(category=self.asset_info.get("category")),
            "-asset {asset}".format(asset=self.asset_info.get("asset")),
            "-element {element}".format(element=self.asset_info.get("element")),
            "-model_variation {model}".format(model=self.current_model_var),
            "-shader_variation {}".format(self.current_shader_var),
            "-texture_list_json {}".format(texture_list_json),
            "-user '{user}'".format(user=getpass.getuser()),
            "-ticket {ticket}".format(ticket=self.current_ticket_item.text(0)),
            "-ddr {ddr}".format(ddr=self.current_ddr_take),
            '-comment "{comment}"'.format(
                comment=urllib.quote(publish_status["comment"])
            ),
        ]
        if publish_workfile_list:
            publish_args.append(
                "-workfile {}".format(
                    " ".join(['"{}"'.format(t) for t in publish_workfile_list])
                )
            )

        convert_args = []
        if convert_format_list:
            if ".tx" in convert_format_list:
                convert_args.append("-txc")

            # Add other format here
            # ..

            # All output format use same resize flag
            resize = (
                self.resize_cbb.currentText() if self.resize_cbb.currentIndex() else ""
            )
            if convert_args and resize:
                convert_args.append('-r "{}"'.format(resize))

        publish_args += convert_args

        m_asset = self.get_mnk_asset()
        content = [
            "#!/bin/tcsh",
            "\n# Environment from project's config.",
            pref.getEnvironmentVariableSetCmd(
                self.asset_info.get("job"),
                "",
                self.asset_info.get("category"),
                self.asset_info.get("asset"),
                self.asset_info.get("element"),
            ),
            "setenv MNK_COMMON_SCRIPT_PATH '{}'".format(
                os.environ["MNK_COMMON_SCRIPT_PATH"]
            ),
            "setenv PER_PROJECT_SCRIPT_PATH '{}'".format(
                os.environ["PER_PROJECT_SCRIPT_PATH"]
            ),
            "setenv MNK_DEBUG '1'" if os.environ.get("MNK_DEBUG") == "1" else "",
            "# Ad hoc environment.",
            "",
            "# set queue related variables",
            "set queueid = `printenv MSBKJOBID`",
            "set machinename = `printenv MSBKNODE`",
            "set renderframe = `printenv MSBKFRAME`",
            "",
            "# publish command",
            " ".join(publish_args),
            "# send message if error",
            "if ($? != 0) then",
            "  set now = `date +'%Y/%d/%m %H:%M:%S'`",
            "  {cmd}".format(
                cmd=im.get_failed_message_cmd(
                    user,
                    m_asset,
                    msg="Submit Failed!",
                    sh_path=sh_file,
                    need_queue_info=True,
                )
            ),
            "  {cmd}".format(
                cmd=im.get_failed_message_cmd(
                    user,
                    m_asset,
                    msg="Submit Failed!",
                    sh_path=sh_file,
                    need_queue_info=True,
                    to_pipeTD=True,
                )
            ),
            "  exit 0",
            "endif",
            "# send message to submitter",
            im.get_success_message_cmd(
                user, m_asset, msg="Submit successfully finished!",
            ),
        ]

        # write shell file
        content_str = "\n".join(content)
        file_op.write_text_file(sh_file, content_str.encode("utf-8"))
        mnk_sys.MnkChmod(sh_file, 0o777)

        return sh_file

    def on_publish(self):
        """Publish files
        """
        selected_workfile = [
            item.data(0, QtCore.Qt.UserRole)
            for item in self.work_file_wd.treewidget.findItems(
                "", QtCore.Qt.MatchContains
            )
            if item.checkState(0) == QtCore.Qt.Checked
        ]

        if not selected_workfile:
            answer = QtWidgets.QMessageBox().warning(
                self,
                "Warning",
                "Are you sure to publish without a work file?",
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
            )
            if answer == QtWidgets.QMessageBox.No:
                return

        selected_texture_items = [
            item
            for item in self.texture_set_wd.treewidget.findItems(
                "", QtCore.Qt.MatchContains
            )
            if item.checkState(0) == QtCore.Qt.Checked
        ]

        if not selected_texture_items:
            QtWidgets.QMessageBox().warning(self, "Warning", "Must select texture.")
            return

        ddr_number = self.publish_info_wd.ddr_le.text()
        if not ddr_number:
            QtWidgets.QMessageBox().warning(self, "Warning", "Please input ddr.")
            return

        convert_format_list = self.get_convert_format_checked()
        if self.convert_option_gb.isChecked() and not self.get_convert_format_checked():
            QtWidgets.QMessageBox().warning(
                self,
                "Warning",
                "Please select some file format to convert or uncheck Covert Options if you don't want to convert.",
            )
            return

        publish_status = self.pre_submit_publish("publish")
        if not publish_status:
            return 1

        publish_workfile_list = selected_workfile
        publish_texture = []
        for res_item in selected_texture_items:
            publish_texture = [
                res_item.child(i).data(0, QtCore.Qt.UserRole)
                for i in range(res_item.childCount())
            ]

        # # write shell script
        mnk_env = os.environ.copy()

        # generate texture list file.
        auto_naming_file = file_utils.getAutoNamingFilePath(
            jobName=self.asset_info["job"],
            className=self.asset_info["class_name"],
            sceneName=self.asset_info["category"],
            shotName=self.asset_info["asset"],
            elementName=self.asset_info["element"],
        )

        element_folder = mnk_path.dirname(mnk_path.dirname(auto_naming_file))
        publish_time = int(time.time())
        texture_list_json = mnk_path.join(
            element_folder, "shell", "texture_list_{}.json".format(publish_time)
        )
        file_op.write_json(texture_list_json, {"texture_list": publish_texture})

        sh_file = self.write_shell_file(
            publish_time,
            publish_status,
            publish_workfile_list,
            texture_list_json,
            convert_format_list,
        )

        print("sh_file: {}".format(sh_file))

        log_file_path = os.path.splitext(sh_file)[0] + ".publishLog.txt"
        QtCore.QCoreApplication.processEvents()

        result = subprocess.Popen(
            "{} 2>&1 | tee {}".format(sh_file, log_file_path), env=mnk_env, shell=True,
        )
        output, err = result.communicate()
        if err:
            LOGGER.error(err.decode("utf-8"))

        if not err:
            QtWidgets.QMessageBox().information(
                self, "Finished Publishing!!", "Succesfully published!!"
            )

        else:
            QtWidgets.QMessageBox().critical(
                self, "Error!!", "Unsuccesfully published!!"
            )


def main():
    """Main function to call programe.
    """
    app = QtWidgets.QApplication(sys.argv)
    widget = PublishTexture()
    widget.show()
    sys.exit(app.exec_())
