#!/usr/bin/env python3
"""THIS MODULE SUPPORT PYTHON 3"""

import os
import webbrowser
from functools import partial
import json
import datetime

# Substance Painter Library
import substance_painter

# Mnk Commonlib
from mnk_qt.Qt import QtWidgets, QtCore, QtGui
import mnk_sys
import mnk_core

# Relative Modules
from core.base_ui import ComboBoxWidget, InformationUI
from core.ui import base_widget

__PLUGIN__ = "EXPORTER"
__OBJECT_ID__ = "EXPORTER_WINDOW"


def convert_uv_to_udim(u, v):
    """Convert UV tils to UDIM number

    Args:
        u (int): U coordinate
        v (int): V coordinate

    Returns:
        int: UDIM
    """
    return 1000 + (u + 1) + (v * 10)


class TextureSizeWidget(ComboBoxWidget):
    """ComboBox for texture size include label and combobox widgets
    """

    def __init__(self, parent=None, layout=None):
        """Initialize function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
            layout (QtWidget.QBoxLayout, optional): layout. Defaults to None.
        """
        super().__init__(parent, layout=None, title="Texture Size")
        log2_values = [
            ["128p", {"log2": 7, "name": "128p"}],
            ["256p", {"log2": 8, "name": "256p"}],
            ["512p", {"log2": 9, "name": "512p"}],
            ["1k", {"log2": 10, "name": "1k"}],
            ["2k", {"log2": 11, "name": "2k"}],
            ["4k", {"log2": 12, "name": "4k"}],
            ["8k", {"log2": 13, "name": "8k"}],
        ]

        self.combobox.setToolTip(
            "This is for identifing output directory. It doesn't change texture resolution."
        )

        for value in log2_values:
            self.add_item(*value)


class TextureSetTreeWidget(QtWidgets.QTreeWidget):
    """For show the texture set details
    """

    def __init__(self, parent=None):
        """Initialize function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
        """
        super().__init__(parent)
        ticket_widget_item = QtWidgets.QTreeWidgetItem(["Texture Set", "", ""], 0)
        self.setHeaderItem(ticket_widget_item)
        self.header().setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.load_texture_set()

    def load_texture_set(self):
        """Add all texture set and data items
        """
        texture_sets = substance_painter.textureset.all_texture_sets()
        for tex in texture_sets:
            tex_item = QtWidgets.QTreeWidgetItem([tex.name()])
            tex_item.setCheckState(0, QtCore.Qt.Checked)
            map_header_item = QtWidgets.QTreeWidgetItem(tex_item)
            map_header_item.setText(0, "Maps")
            uv_header_item = QtWidgets.QTreeWidgetItem(tex_item)
            uv_header_item.setText(0, "UV Tiles")
            self.addTopLevelItem(tex_item)

    def get_texture_item(self, name):
        """Get a texture set item

        Args:
            name (str): texture set name

        Returns:
            QTreeWidgetItem: a texture set item
        """
        founds = self.findItems(name, QtCore.Qt.MatchExactly)
        return founds[0] if founds else None

    def get_map_header_item(self, texture_item):
        """Get a parent of texture maps item

        Args:
            texture_item (QTreeWidgetItem): a texture set item

        Returns:
            QTreeWidgetItem: a header item of texture maps
        """
        return texture_item.child(0)

    def get_uv_header_item(self, texture_item):
        """Get a parent of uv tiles item

        Args:
            texture_item (QTreeWidgetItem): a texture set item

        Returns:
            QTreeWidgetItem: a header item of uv tiles
        """
        return texture_item.child(1)

    def get_map_item(self, texture_item, map_name=""):
        """Get a texture map item or all

        Args:
            texture_item (QTreeWidgetItem): a texture set item
            map_name (str, optional): the map name. Defaults to "".

        Returns:
            list: all items or an item if map_name is specified
        """
        all_items = [
            self.get_map_header_item(texture_item).child(index)
            for index in range(self.get_map_header_item(texture_item).childCount())
        ]
        if not map_name:
            return all_items

        return [item for item in all_items if map_name == item.text(1)]

    def get_uv_item(self, texture_item, uv_tile=""):
        """Get a uv tile item or all

        Args:
            texture_item (QTreeWidgetItem): a texture set item
            uv_tile (str, optional): the map name. Defaults to None.

        Returns:
            list: all items or an item if uv_tile is specified
        """
        all_items = [
            self.get_uv_header_item(texture_item).child(index)
            for index in range(self.get_uv_header_item(texture_item).childCount())
        ]
        if not uv_tile:
            return all_items

        return [item for item in all_items if uv_tile == item.data(1)]

    def add_tile_items(self, uv_header_item, texture_set):
        """Add uv tile items of a texture set 

        Args:
            uv_header_item (QtWidgets.QTreeWidgetItem): parent item
            texture_set (substance_painter.textureset.TextureSet): texture set object
        """
        check_state_list = dict(
            [
                [item.text(1), item.checkState(1)]
                for item in self.get_uv_item(uv_header_item.parent())
            ]
        )
        uv_header_item.takeChildren()
        for tile in texture_set.all_uv_tiles():
            tile_res = tile.get_resolution()
            item_label = "{} ({})".format(
                convert_uv_to_udim(tile.u, tile.v), tile_res.width
            )
            uv_item = QtWidgets.QTreeWidgetItem(uv_header_item, ["", item_label,],)
            uv_item.setData(1, QtCore.Qt.UserRole, [tile.u, tile.v])
            uv_item.setCheckState(
                1, check_state_list.get(item_label, QtCore.Qt.Checked)
            )


class ListTextureTreeWidget(QtWidgets.QTreeWidget):
    """For show texture file that will export
    """

    def __init__(self, parent=None):
        """Initialize function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
        """
        super().__init__(parent)
        ticket_widget_item = QtWidgets.QTreeWidgetItem(["Texture Set", "File"], 0)

        self.setHeaderItem(ticket_widget_item)


class ExportTextureWindow(QtWidgets.QWidget, InformationUI):
    """Main UI
    """

    def __init__(self, parent=None, current_scene=""):
        """Initialize function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
            current_scene (str, optional): scene file. Defaults to "".
        """
        super().__init__(parent)
        # Intial variables
        self.hero_dir = ""
        self.output_dir = ""
        self.output_type = ""
        self.export_document_maps = {}
        self.current_scene = current_scene
        self.set_current_scene(current_scene)
        self.pt_proj_metadata = substance_painter.project.Metadata(__PLUGIN__)

        # Declare widgets
        self.export_preset_wd = ComboBoxWidget(title="Export Preset")
        self.resolution_wd = TextureSizeWidget()
        self.export_dir_wd = base_widget.FolderLineEditWidget()
        self.export_pb = QtWidgets.QPushButton("Export")
        self.texture_set_tw = TextureSetTreeWidget()
        self.list_texture_tw = ListTextureTreeWidget()
        self.refresh_pb = QtWidgets.QPushButton("Refresh")

        # Connect signals
        self.export_dir_wd.lineedit.textChanged.connect(
            partial(setattr, self, "output_dir")
        )
        self.export_dir_wd.lineedit.textChanged.connect(self.update_texture_list)
        self.resolution_wd.combobox.currentTextChanged.connect(self.set_export_dir)
        self.texture_set_tw.itemChanged.connect(self.update_texture_list)
        self.export_pb.clicked.connect(self.export_to_hero)
        self.export_preset_wd.combobox.activated.connect(self.preset_changed)
        self.refresh_pb.clicked.connect(self.refresh_content)

        # Initial contents
        self.resolution_wd.set_current_text("4k")
        self.init_content()
        self.get_presets()

        # get medata in scene file
        self.setup_tool()

        # Initial layouts
        self.init_layout()

    def init_layout(self):
        """Main initialize layouts
        """
        self.setWindowTitle("Export Texture")
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().setAlignment(QtCore.Qt.AlignTop)
        h1_layout = QtWidgets.QHBoxLayout()
        self.layout().addLayout(h1_layout)

        h1_layout.addWidget(self.export_preset_wd)
        h1_layout.addWidget(self.refresh_pb)

        self.export_preset_wd.combobox.setMinimumWidth(250)
        self.export_preset_wd.layout().setAlignment(QtCore.Qt.AlignLeft)
        self.export_preset_wd.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().addWidget(self.resolution_wd)
        self.resolution_wd.layout().setAlignment(QtCore.Qt.AlignLeft)
        self.resolution_wd.layout().setContentsMargins(0, 0, 0, 0)

        h2_layout = QtWidgets.QHBoxLayout()
        self.layout().addLayout(h2_layout)

        h3_layout = QtWidgets.QHBoxLayout()
        h3_layout.addWidget(QtWidgets.QLabel("Export Directory"))
        h3_layout.addWidget(self.export_dir_wd)
        self.export_dir_wd.setMinimumWidth(300)

        h2_layout.addLayout(h3_layout)
        h2_layout.addWidget(self.export_pb)

        splitter = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        self.layout().addWidget(splitter)
        splitter.addWidget(self.texture_set_tw)

        list_export_wd = QtWidgets.QWidget()
        splitter.addWidget(list_export_wd)
        list_export_wd.setLayout(QtWidgets.QVBoxLayout())
        list_export_wd.layout().addWidget(QtWidgets.QLabel("List Of Exports"))
        list_export_wd.layout().addWidget(self.list_texture_tw)
        self.resize(400, 800)

    def init_content(self):
        """Intialize content in widgets
        """
        # Update scene info
        current_scene = substance_painter.project.file_path()
        if self.current_scene == current_scene:
            self.info_signal.reloaded.emit()
        else:
            self.set_current_scene(current_scene)

        # Set paths
        element_dir = mnk_core.path.join(
            mnk_sys.MNK_BASE_DIR,
            self.asset_info.get("job"),
            self.asset_info.get("class_name"),
            self.asset_info.get("category"),
            self.asset_info.get("asset"),
            self.asset_info.get("element"),
        )
        self.hero_dir = "{}/hero".format(element_dir)
        self.set_export_dir()

        self.texture_set_tw.expandAll()

    def setup_tool(self):
        """Setup by metadata in the scene
        """
        if self.pt_proj_metadata.get("export_list"):
            for index in range(self.texture_set_tw.topLevelItemCount()):
                item = self.texture_set_tw.topLevelItem(index)
                if not [
                    tex
                    for tex in self.pt_proj_metadata.get("export_list")
                    if tex.get("rootPath") == item.text(0)
                ]:
                    item.setCheckState(0, QtCore.Qt.Unchecked)
                else:
                    filters = [
                        tex.get("filter")
                        for tex in self.pt_proj_metadata.get("export_list")
                        if tex.get("rootPath") == item.text(0)
                    ]
                    if filters:

                        for map_item in self.texture_set_tw.get_map_item(item):
                            if not map_item.text(1) in filters[0].get("outputMaps", []):
                                map_item.setCheckState(1, QtCore.Qt.Unchecked)

                        for uv_item in self.texture_set_tw.get_uv_item(item):
                            if not uv_item.data(1, QtCore.Qt.UserRole) in filters[
                                0
                            ].get("uvTiles", []):
                                uv_item.setCheckState(1, QtCore.Qt.Unchecked)

    def preset_changed(self):
        """When the preset widget has changed, it will use the preset to get export data 
        and update texture set widget
        """
        if not self.export_preset_wd.current_data():
            return

        # Set project export preset
        try:
            substance_painter.js.evaluate(
                'alg.mapexport.setProjectExportPreset("resource://{context}/{name}")'.format(
                    **self.export_preset_wd.current_data()
                )
            )
        except RuntimeError:
            QtWidgets.QMessageBox().warning(
                self, "WARNING", "Can't use this preset!",
            )
            self.update_current_export_preset()
            return

        current_preset = substance_painter.js.evaluate(
            "alg.mapexport.getProjectExportPreset()"
        )

        export_config = self.get_export_config()
        if not export_config:
            return
        export_list = substance_painter.export.list_project_textures(export_config)
        # Get output format from the preset by this way because there is no a method to get it
        self.output_type = os.path.splitext(list(export_list.values())[0][0])[1][1:]

        if not current_preset or not self.output_dir or not self.output_type:
            return

        self.export_document_maps = substance_painter.js.evaluate(
            'alg.mapexport.getPathsExportDocumentMaps("{preset}", "{output_path}", "{format}")'.format(
                preset=current_preset,
                output_path=self.output_dir,
                format=self.output_type,
            )
        )
        self.update_texture_set()

    def refresh_content(self):
        """Refresh content in widgets
        """
        self.init_content()
        self.update_texture_set()

    def set_export_dir(self):
        """Set export directory
        """
        res = self.resolution_wd.current_text()
        self.export_dir_wd.set_directory("{}/{}".format(self.hero_dir, res))

    def get_presets(self):
        """Get presets in project assets location
        """
        self.export_preset_wd.clear()
        location_name = "{proj}_assets".format(proj=self.asset_info.get("job"))

        substance_painter.resource.Shelves.all()
        project_shelf = [
            s
            for s in substance_painter.resource.Shelves.all()
            if s.name() == location_name
        ]
        if project_shelf:
            location = substance_painter.resource.Shelf(location_name).path()
            presets_dir = "{}/export-presets".format(location)

        else:
            QtWidgets.QMessageBox().warning(
                self,
                "WARNING",
                "Not found location name: {}\nPlease add the location before open a project.".format(
                    location_name
                ),
            )
            self.close()

        project_presets = [
            os.path.splitext(p)[0]
            for p in os.listdir(presets_dir)
            if p.endswith(".spexp")
        ]
        for name in project_presets:
            resource = substance_painter.resource.ResourceID(location_name, name)
            self.export_preset_wd.add_item(name, resource.__dict__)

        self.update_current_export_preset()

    def update_current_export_preset(self):
        """Set export preset widget as same as a current preset in the project"""
        current_preset = substance_painter.js.evaluate(
            "alg.resources.getResourceInfo(alg.mapexport.getProjectExportPreset()).name"
        )
        if current_preset:
            self.export_preset_wd.combobox.setCurrentIndex(
                self.export_preset_wd.combobox.findText(current_preset)
            )

    def get_texture_set_export_list(self):
        """Get texture set items that is checked

        Returns:
            list: texture set list [{"rootPath": texture_set},,]
        """
        export_list = []
        for tex_item in self.texture_set_tw.findItems("", QtCore.Qt.MatchContains):
            if tex_item.checkState(0) != QtCore.Qt.Checked:
                continue
            texture_output = {"rootPath": tex_item.text(0)}

            filter_texture = {}
            map_header = self.texture_set_tw.get_map_header_item(tex_item)
            if map_header.childCount():
                output_maps = [
                    map_header.child(index).text(1)
                    for index in range(map_header.childCount())
                    if map_header.child(index).checkState(1) == QtCore.Qt.Checked
                ]
                filter_texture.update({"outputMaps": output_maps})

            uv_header = self.texture_set_tw.get_uv_header_item(tex_item)
            if uv_header.childCount():
                output_uv = [
                    uv_header.child(index).data(1, QtCore.Qt.UserRole)
                    for index in range(uv_header.childCount())
                    if uv_header.child(index).checkState(1) == QtCore.Qt.Checked
                ]
                filter_texture.update({"uvTiles": output_uv})

            if filter_texture:
                texture_output.update({"filter": filter_texture})

            export_list.append(texture_output)

        return export_list

    def get_export_config(self, export_list=None):
        """Create config data to export
        Args:
            export_list (list) : a list of a pair of the texture data that will export.

        Returns:
            dict: config data
        """
        preset_resource_url = substance_painter.js.evaluate(
            "alg.mapexport.getProjectExportPreset()"
        )

        if not export_list:
            export_list = []
            for tex_item in self.texture_set_tw.findItems("", QtCore.Qt.MatchContains):
                if tex_item.checkState(0) != QtCore.Qt.Checked:
                    continue
                export_list.append({"rootPath": tex_item.text(0)})

        export_config = {
            "exportShaderParams": False,
            "exportPath": self.output_dir,
            "defaultExportPreset": preset_resource_url,
            "exportParameters": [
                {
                    "parameters": {
                        "sizeLog2": self.resolution_wd.current_data().get("log2"),
                        "paddingAlgorithm": "infinite",
                    }
                }
            ],
            "exportList": export_list or [],
        }

        return export_config

    def update_texture_set(self):
        """Update texture set items
        """
        # Add texture map and uv tiles items
        for tex_name, map_data in self.export_document_maps.items():
            tex_item = self.texture_set_tw.get_texture_item(tex_name)
            map_item = self.texture_set_tw.get_map_header_item(tex_item)
            check_state_list = dict(
                [
                    [item.text(1), item.checkState(1)]
                    for item in self.texture_set_tw.get_map_item(tex_item)
                ]
            )
            map_item.takeChildren()
            for map_name in list(map_data.keys()):
                item = QtWidgets.QTreeWidgetItem(map_item, ["", map_name])
                item.setCheckState(1, check_state_list.get(map_name, QtCore.Qt.Checked))

            uv_item = self.texture_set_tw.get_uv_header_item(tex_item)
            self.texture_set_tw.add_tile_items(
                uv_item,
                substance_painter.textureset.TextureSet.from_name(tex_item.text(0)),
            )

        if self.export_preset_wd.current_data() == self.pt_proj_metadata.get(
            "export_preset"
        ):
            self.setup_tool()

    def update_texture_list(self):
        """Update export texture list 
        """
        self.list_texture_tw.clear()

        export_config = self.get_export_config(self.get_texture_set_export_list())
        if not export_config:
            return
        export_list = substance_painter.export.list_project_textures(export_config)
        if not export_list:
            return

        # Preview texture files
        for k, v in export_list.items():
            if not v:
                continue
            texture_set_item = QtWidgets.QTreeWidgetItem(self.list_texture_tw, [k[0]])
            for filename in sorted(v):
                file_item = QtWidgets.QTreeWidgetItem(
                    texture_set_item, ["", os.path.basename(filename)]
                )
                file_item.setData(1, QtCore.Qt.UserRole, filename)
        self.list_texture_tw.expandAll()

    def result_export_texture_list(self, export_result):
        """Set export result to the item list of texture files

        Args:
            export_result (dict): a pair of export result
        
        Return:
            dict : a pair of status of files
        """
        report_result = {"success": [], "fail": [], "total_files": 0}
        for key, value in export_result.items():
            item_found = self.list_texture_tw.findItems(key[0], QtCore.Qt.MatchExactly)
            if item_found:
                for index in range(item_found[0].childCount()):
                    file_item = item_found[0].child(index)
                    file_path = file_item.data(1, QtCore.Qt.UserRole)
                    icon = QtGui.QIcon()

                    if not file_path in value:
                        report_result["fail"].append(file_path)
                        icon.addFile(
                            mnk_core.path.join(
                                mnk_sys.COMMON_SCRIPT_PATH,
                                "icons",
                                "triangle_warning.png",
                            )
                        )
                    else:
                        report_result["success"].append(file_path)
                        icon.addFile(
                            mnk_core.path.join(
                                mnk_sys.COMMON_SCRIPT_PATH, "icons", "blue_check.png"
                            )
                        )
                    file_item.setIcon(1, icon)
                    report_result["total_files"] += 1

        return report_result

    def export_to_hero(self):
        """Export texture to Hero directory
        """
        texture_set_list = self.get_texture_set_export_list()
        if not texture_set_list:
            QtWidgets.QMessageBox().warning(
                self, "WARNING", "Please select a texture set to export!!",
            )
            return

        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        export_config = self.get_export_config(texture_set_list)

        export_result = substance_painter.export.export_project_textures(export_config)
        if export_result.status != substance_painter.export.ExportStatus.Success:
            QtWidgets.QMessageBox().warning(self, "Error", export_result.message)
            return

        expoert_status_data = self.result_export_texture_list(export_result.textures)

        # write meta json
        meta_file = "{hero_dir}/{category}_{asset}.{element}_export.json".format(
            hero_dir=self.hero_dir,
            category=self.asset_info.get("category"),
            asset=self.asset_info.get("asset"),
            element=self.asset_info.get("element"),
        )
        meta_data = {}
        if os.path.exists(meta_file) and os.path.getsize(meta_file):
            meta_data = json.load(open(meta_file))
        for set_name, texture_list in export_result.textures.items():
            for texture in texture_list:
                meta_data[texture] = {
                    "texture_set": set_name[0],
                    "workfile": self.current_scene,
                    "date_time:": current_time,
                }

        with open(meta_file, "w") as out_file:
            out_file.write(json.dumps(meta_data, indent=4))

        # Write metadata in current scene
        if os.access(self.current_scene, os.W_OK):
            self.pt_proj_metadata.set(
                "export_list", texture_set_list,
            )
            self.pt_proj_metadata.set(
                "export_preset", self.export_preset_wd.current_data()
            )
            try:
                substance_painter.project.save()
            except substance_painter.exception.ProjectError:
                QtWidgets.QMessageBox().information(
                    self,
                    "Error",
                    "The project is only available in read only because it's locked by another process."
                    "\nSuggest to close the another process then open this scene again.",
                )
                return

        QtWidgets.QMessageBox().information(
            self,
            "Success",
            "{}\nExported {} of {} files".format(
                export_result.message,
                len(expoert_status_data["success"]),
                expoert_status_data["total_files"],
            ),
        )

        webbrowser.open(self.output_dir)


def main():
    """Main function to call programe.
    """
    main_window = substance_painter.ui.get_main_window()

    widget = main_window.findChild(QtWidgets.QWidget, __OBJECT_ID__)
    if widget:
        widget.close()
        widget.deleteLater()

    if substance_painter.project.is_open():

        scene = substance_painter.project.file_path()
        if not os.access(scene, os.W_OK):
            ans = QtWidgets.QMessageBox().question(
                main_window,
                "WARNING",
                "This file is read-only. All modification cannot save.\nDo you want to continue?",
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
            )
            if ans == QtWidgets.QMessageBox.StandardButton.No:
                return

        widget = ExportTextureWindow(parent=main_window, current_scene=scene)
        widget.setObjectName(__OBJECT_ID__)
        widget.setWindowFlag(QtCore.Qt.Tool)
        widget.show()
    else:
        QtWidgets.QMessageBox().warning(
            main_window, "WARNING", "Please open project file!!",
        )

