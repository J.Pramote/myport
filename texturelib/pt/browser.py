# Standard Library
import glob
import os
import importlib

# Substance Painter Library
import substance_painter

# Mnk Commonlib
import mnk_sys
from mnk_qt.Qt import QtWidgets, QtCore
from mnk_qt.ui_widget import MnkBaseOpen

# Global Variables
__PLUGIN__ = "FOLDER_BROWSER"
__OBJECT_ID__ = "FOLDER_BROWSER_WINDOW"


class FolderBrowser(MnkBaseOpen.MnkBaseOpenDialog):
    """FolderBrowser
    """

    def __init__(self, parent=None):
        """Initialize function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
        """
        super(FolderBrowser, self).__init__(
            typ="substancePainter", fileExtFilterList=[".spp"], parent=parent
        )

        # add auto naming button
        auto_naming_btn = QtWidgets.QPushButton("Launch [Auto Naming]")
        self.buttonRightLayout.addWidget(auto_naming_btn)

        # connect signals
        auto_naming_btn.clicked.connect(self.launch_auto_naming)
        self.lwgJobOpen.itemClicked.connect(lambda: self.updatePath(status="job"))
        self.lwgClassOpen.itemClicked.connect(lambda: self.updatePath(status="class"))
        self.lwgSceneOpen.itemClicked.connect(lambda: self.updatePath(status="scene"))
        self.lwgShotOpen.itemClicked.connect(lambda: self.updatePath(status="shot"))
        self.lwgElementOpen.itemClicked.connect(lambda: self.updatePath(status="elem"))

        # auto select job scene shot by current scene
        if substance_painter.project.is_open():
            current_scene = substance_painter.project.file_path()
            if current_scene:
                path_info = self.getInfoFromPath(current_scene)
                self.lnePathOpen.setText(current_scene)
                self.updateListWidget(path_info)

    def launch_auto_naming(self):
        """Launch auto naming tool
        """
        auto_naming = importlib.import_module("pt.auto_naming")
        auto_naming.main()

    def updatePath(self, status):
        """Override method to update directory in the LineEdit to show current path

        Args:
            status (str): a type name that is updating
        """
        self.lineEditPathUpdate(st=status)

    def openFile(self, file_path):
        """Override methhod to open a file

        Args:
            file_path (str): a file path to open
        """
        if (
            substance_painter.project.is_open()
            and substance_painter.project.needs_saving()
        ):
            message = (
                "The current file has been modified!\nWould you like to save it first?"
            )
            answer = QtWidgets.QMessageBox().question(
                self,
                "save file",
                message,
                QtWidgets.QMessageBox.Yes
                | QtWidgets.QMessageBox.No
                | QtWidgets.QMessageBox.Cancel,
            )

            if answer == QtWidgets.QMessageBox.StandardButton.Yes:
                substance_painter.project.save()
                substance_painter.project.close()

            elif answer == QtWidgets.QMessageBox.StandardButton.Cancel:
                return

        try:
            substance_painter.project.open(file_path)
            self.close()
        except substance_painter.exception.ProjectError as err:
            QtWidgets.QMessageBox().critical(
                self, "ERROR", "{}\nFailed to open {}".format(err, file_path),
            )


def main():
    """To start the programe
    """
    main_window = substance_painter.ui.get_main_window()

    widget = main_window.findChild(QtWidgets.QWidget, __OBJECT_ID__)
    if widget:
        widget.close()
        widget.deleteLater()

    widget = FolderBrowser(parent=main_window)
    widget.setObjectName(__OBJECT_ID__)
    widget.setWindowFlag(QtCore.Qt.Tool)
    widget.show()

