import substance_painter.ui

from PySide2 import QtWidgets

def start_plugin():
    main_window = substance_painter.ui.get_main_window()
    menu_bar = main_window.menuBar()
    pipeline_menu = PipelineToolMenu()
    menu_bar.addMenu(pipeline_menu)
    pipeline_menu.triggered.connect(call_script)


def call_script(action):
    cmd = action.data()
    exec(cmd)


class PipelineToolMenu(QtWidgets.QMenu):
    def __init__(self):
        super(PipelineToolMenu, self).__init__()
        self.setTitle("Pipeline Tool")

        publish_texture_tool = self.addAction("Export Texture")
        publish_texture_tool.setData(
            "from pt.exporter import exporter\nexporter.main()"
        )

        auto_naming_tool = self.addAction("Auto Naming")
        auto_naming_tool.setData("from pt import auto_naming\nauto_naming.main()")

        browser_tool = self.addAction("Browser")
        browser_tool.setData("from pt import browser\nbrowser.main()")
