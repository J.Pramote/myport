# Standard Library
import glob
import os

# Substance Painter Library
import substance_painter

# Mnk Commonlib
import mnk_sys
import mnk_core
from mnk_qt.Qt import QtWidgets, QtCore
from mnk_qt.ui_widget import MnkBaseAutoNaming
from mnk_common_utils import file_utils
import mnk_qt.ui_widget.MnkDialog as dialog

# Global Variables
__PLUGIN__ = "AUTONAMING"
__OBJECT_ID__ = "AUTONAMING_WINDOW"


class AutoNaming(MnkBaseAutoNaming.MnkBaseAutoNaming):
    """AutoNaming
    """

    def __init__(self, parent=None):
        """Initialize function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
        """
        super(AutoNaming, self).__init__(
            typ="substancePainter", fileExtFilterList=[".spp"], parent=parent
        )

        self.setWindowTitle("Auto Naming")
        # set initial info if there's an opened scene
        current_path = substance_painter.project.file_path()
        if current_path:
            info_dict = file_utils.getInfoDictFromCurrentScene(current_path)
            self.configUI(
                info_dict["job"],
                info_dict["class"],
                info_dict["scene"],
                info_dict["shot"],
                info_dict["element"],
            )

    def saveFile(self, file_path):
        """Override method to save a substance painter file

        Args:
            file_path (_type_): file path .spp
        """
        folder = os.path.dirname(file_path)
        mnk_sys.MnkCreateFolder(folder)

        substance_painter.project.save_as(file_path)

        if os.path.exists(file_path):
            dialog.showInfoDialog("Finish", "     File saved successfully..     ")
        else:
            dialog.showWarningDialog("     Save failed..     ")

    def createElementFolderStructure(self, element_folder):
        """Override method to create folder

        Args:
            element_folder (str): a root path of the destination
        """
        # create folder structure
        folder_to_copy_list = glob.glob(
            mnk_core.path.join(
                mnk_sys.MNK_BASE_DIR,
                "asset",
                "pipePref",
                "template",
                "rsrc",
                "templateScn",
                "templateShot",
                "templateElem",
                "*",
            )
        )
        for folder_to_copy in folder_to_copy_list:
            mnk_sys.MnkCopy(folder_to_copy, element_folder, recursive=True)


def main():

    main_window = substance_painter.ui.get_main_window()

    widget = main_window.findChild(QtWidgets.QWidget, __OBJECT_ID__)
    if widget:
        widget.close()
        widget.deleteLater()

    if substance_painter.project.is_open():
        widget = AutoNaming(parent=main_window)
        widget.setObjectName(__OBJECT_ID__)
        widget.setWindowFlag(QtCore.Qt.Tool)
        widget.show()
    else:
        QtWidgets.QMessageBox().warning(
            main_window, "WARNING", "Must open/new a project file before name it!",
        )
