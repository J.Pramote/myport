#!/usr/bin/env python3
""" DESCRIPTION
This module is only ui without anything connect with.
It contains widget elements.
"""
# Mnk Commonlib
from mnk_qt.Qt import QtWidgets, QtCore
from mnk_qt.gui.widget import mnk_ticket_tree_widget


class ComboBoxWidget(QtWidgets.QWidget):
    """ComboBox include label and combobox widgets
    """

    def __init__(self, parent=None, layout=None, title=""):
        """Initialize function

        Args:
            parent (QtWidgets.QWidget, optional): parent widget. Defaults to None.
            layout (QtWidgets.QBoxLayout, optional): layout. Defaults to None.
            title (str, optional): title. Defaults to "".
        """
        super(ComboBoxWidget, self).__init__(parent)
        self.title = QtWidgets.QLabel(title)
        self.combobox = QtWidgets.QComboBox()

        layout = QtWidgets.QHBoxLayout() if not layout else layout

        self.setLayout(layout)
        self.layout().addWidget(self.title)
        self.layout().addWidget(self.combobox)

    def current_text(self):
        """Current text on QComboBox 

        Returns:
            str: item name
        """
        return self.combobox.currentText()

    def current_data(self):
        """Current data on QComboBox

        Returns:
            muti-type: data that contain in each item.
        """
        return self.combobox.currentData()

    def set_current_text(self, text):
        """To Set current item on QComboBox

        Args:
            text (str): item name
        """
        index = self.combobox.findText(text)
        self.combobox.setCurrentIndex(index)

    def set_current_data(self, text):
        """To set current item on QComboBox by item data

        Args:
            text (multi-type): item data
        """
        index = self.combobox.findData(text)
        self.combobox.setCurrentIndex(index)

    def add_item(self, name, data=None):
        """Add an item into QComboBox

        Args:
            name (str): item label
            data (multi-type, optional): embedded data on an item. Defaults to None.
        """
        self.combobox.addItem(name, data)

    def clear(self):
        """Clear all items in the combobox
        """
        self.combobox.clear()


class TreeWidget(QtWidgets.QTreeWidget):
    """TreeWidget
    
    Signals:
        dropped (str): emit every single file is dropped
        multiple_dropped (list): a list of files are dropped
    """

    dropped = QtCore.Signal(str)
    multiple_dropped = QtCore.Signal(list)

    def __init__(self, *args):
        """Initialize Function
        """
        super(TreeWidget, self).__init__(*args)
        self.setSortingEnabled(True)

    def dragEnterEvent(self, event):
        """Override this class method.
        """
        if event.mimeData().hasUrls:
            event.accept()

        else:
            event.ignore()

    def dragMoveEvent(self, event):
        """Override this class method.
        """
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()

        else:
            event.ignore()

    def dropEvent(self, event):
        """Override this class method.
        """
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(url.path())
                self.dropped.emit(url.path())
            self.multiple_dropped.emit(links)
        else:
            event.ignore()


class ListWidget(QtWidgets.QWidget):
    """ Contains:
        * label - name for category widget
        * search - to search an item in the listwidget
        * listwidget - show item list
    """

    def __init__(self, parent=None, title=""):
        """Initialize Function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
            title (str, optional): title name. Defaults to "".
        """
        super(ListWidget, self).__init__(parent=parent)
        # Declare widgets
        self.label = QtWidgets.QLabel(title)
        self.search = QtWidgets.QLineEdit()
        self.listwidget = QtWidgets.QListWidget()

        # Connect signals
        self.search.textChanged.connect(self.filter_item)
        self.search.editingFinished.connect(self.search.clearFocus)

        # Initial content in widgets
        self.search.setPlaceholderText("Search here")

        # Intial layout
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(self.label)
        self.layout().addWidget(self.search)
        self.layout().addWidget(self.listwidget)
        self.listwidget.setMinimumHeight(200)

    def add_items(self, labels=[]):
        """Add item into treewidget

        Args:
            labels (list, optional): a list of string. Defaults to [].
        """
        self.listwidget.addItems(labels)
        self.listwidget.sortItems()

    def current_text(self):
        """Get name of a selected item.

        Returns:
            str: item name
        """
        item = self.listwidget.selectedItems()
        return item[0].text() if item else ""

    def set_current_text(self, text):
        """Select an item by name

        Args:
            text (str): item name
        """
        found_item = self.listwidget.findItems(text, QtCore.Qt.MatchExactly)
        if found_item:
            self.listwidget.setCurrentItem(found_item[0])

    def filter_item(self, word):
        """Filter item

        Args:
            word (str): string to find items which include the word
        """
        found_items = []
        all_items = self.listwidget.findItems("", QtCore.Qt.MatchContains)

        if word:
            found_items = self.listwidget.findItems(word, QtCore.Qt.MatchContains)
            for item in all_items:
                item.setHidden(True)
            for item in found_items:
                item.setHidden(False)
        else:
            for item in all_items:
                item.setHidden(False)


class TicketWidget(QtWidgets.QWidget):
    """This widget is about tickets include
        * search_le - to find ticket by name
        * ticket_lw - show ticket items
    """

    def __init__(self, parent=None):
        """Initialize Function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
        """
        super(TicketWidget, self).__init__(parent)
        # Declare widgets
        self.search_le = QtWidgets.QLineEdit()
        self.ticket_tw = (
            mnk_ticket_tree_widget.MnkTicketTreeWidget()
        )

        # Connect signals
        self.search_le.textChanged.connect(self.filter_ticket)

        # Initial content in widgets
        ticket_widget_item = QtWidgets.QTreeWidgetItem(
            ["ticket name", "due date", "status", "submittable"], 0
        )
        self.ticket_tw.setHeaderItem(ticket_widget_item)

        # Intial layouts
        self.init_layout()

    def init_layout(self):
        """Main initialize layout
        """
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().setAlignment(QtCore.Qt.AlignTop)
        self.layout().setContentsMargins(0, 0, 0, 0)
        h_layout = QtWidgets.QHBoxLayout()
        self.layout().addLayout(h_layout)
        h_layout.addStretch()
        h_layout.addWidget(QtWidgets.QLabel("Search Ticket"))
        h_layout.addWidget(self.search_le)
        self.layout().addWidget(self.ticket_tw)

    def filter_ticket(self, word):
        """Filter item

        Args:
            word (str): string to find items which include the word
        """
        found_items = []
        all_items = self.ticket_tw.findItems("", QtCore.Qt.MatchContains)

        if word:
            found_items = self.ticket_tw.findItems(word, QtCore.Qt.MatchContains)
            for item in all_items:
                item.setHidden(True)
            for item in found_items:
                item.setHidden(False)
        else:
            for item in all_items:
                item.setHidden(False)

    def clear_widget(self):
        """Clear content in this widget
        """
        self.ticket_tw.clear()

    def add_item(self, item):
        """Add an item into treewidget

        Args:
            item (QtWidget.QTreeWidgetItem): ticket item
        """
        self.ticket_tw.addTopLevelItem(item)

    def current_item(self):
        """Get a selected item

        Returns:
            QtWidget.QTreeWidgetItem: a selected item
        """
        return self.ticket_tw.currentItem()


class PublishWidget(QtWidgets.QWidget):
    """Widgets that always use when publish, include ddr, publish button, and etc.
    """

    def __init__(self, parent=None, **kwargs):
        """Initialize Function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
        """
        super(PublishWidget, self).__init__(parent)

        # Declare widgets
        self.ddr_le = QtWidgets.QLineEdit()
        self.last_ddr_pb = QtWidgets.QPushButton("get lastest ddr")
        self.play_ddr_pb = QtWidgets.QPushButton("play ddr")
        self.ticket_wd = TicketWidget()
        self.reload_ticket_pb = QtWidgets.QPushButton("Reload")
        self.comment_te = QtWidgets.QTextEdit()
        self.publish_pb = QtWidgets.QPushButton("Publish")

        # Initialize layout
        self.ticket_ctrl_layout = QtWidgets.QHBoxLayout()
        self.init_layout()

    def init_layout(self):
        """Main initialize layout
        """
        self.setMinimumWidth(400)
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addLayout(self.ticket_ctrl_layout)
        self.ticket_ctrl_layout.setAlignment(QtCore.Qt.AlignTop)
        self.ticket_ctrl_layout.addWidget(self.ticket_wd)
        self.layout().addSpacing(20)

    def ddr_widget(self, margins=None):
        """Widgets that operate with DDR

        Args:
            margins (list, optional): to set content margin. Defaults to [0, 0, 0, 0].

        Returns:
            QtWidget.QWidget: widget
        """
        margins = margins or [0, 0, 0, 0]
        widget = QtWidgets.QWidget()
        widget.setLayout(QtWidgets.QHBoxLayout())
        widget.layout().setContentsMargins(*margins)
        widget.layout().setAlignment(QtCore.Qt.AlignLeft)
        widget.layout().addWidget(QtWidgets.QLabel("DDR:"))
        widget.layout().addWidget(self.ddr_le)
        self.ddr_le.setFixedWidth(100)
        widget.layout().addWidget(self.last_ddr_pb)
        widget.layout().addWidget(self.play_ddr_pb)

        return widget

    def publish_widget(self, margins=[0, 0, 0, 0], comment=True):
        """The main widget is Publish Button. It can show/hide comment widget as well.

        Args:
            margins (list, optional): to set content margin. Defaults to [0, 0, 0, 0].
            comment (bool, optional): show comment widget. Defaults to True.

        Returns:
            QtWidget.QWidget: widget
        """
        widget = QtWidgets.QWidget()
        widget.setLayout(QtWidgets.QHBoxLayout())
        widget.layout().setContentsMargins(*margins)
        widget.layout().setAlignment(QtCore.Qt.AlignLeft)
        if comment:
            widget.layout().addWidget(QtWidgets.QLabel("Comment:"))
            widget.layout().addWidget(self.comment_te)
        widget.layout().addWidget(self.publish_pb)
        return widget


class FileBrowserWidget(QtWidgets.QWidget):
    """Widget to show files.
        * label - title name
        * info - show number of selected files
        * treewidget - show file items
    """

    def __init__(self, parent=None, label="", headers=[]):
        """Initialize Function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
            label (str, optional): title name. Defaults to "".
            headers (list, optional): name of columns. Defaults to [].
        """
        super(FileBrowserWidget, self).__init__(parent)
        # Declare properties
        self.info_format = "{count} file(s)"

        # Declare widgets
        self.label = QtWidgets.QLabel(label)
        self.info = QtWidgets.QLabel(self.info_format.format(count=0))
        self.treewidget = TreeWidget()

        # Connect signals
        self.treewidget.itemClicked.connect(self.set_info)

        # Intial content in widget
        header_item = QtWidgets.QTreeWidgetItem(headers, 0)
        self.treewidget.setHeaderItem(header_item)

        # Intial layout
        self.init_layout()

    def init_layout(self):
        """Main initialize layout
        """
        self.setLayout(QtWidgets.QVBoxLayout())
        h_layout = QtWidgets.QHBoxLayout()
        h_layout.addWidget(self.label)
        h_layout.addWidget(self.info)
        self.layout().addLayout(h_layout)
        self.layout().addWidget(self.treewidget)
        self.treewidget.setMinimumHeight(200)

    def set_info(self):
        """Show total of activated files
        """
        checked_items = [
            item
            for item in self.treewidget.findItems("", QtCore.Qt.MatchContains)
            if item and item.checkState(0) == QtCore.Qt.Checked
        ]
        count = 0
        for item in checked_items:
            children = item.childCount()
            count += children if children else 1

        self.info.setText(self.info_format.format(count=count))


class FolderLineEditWidget(QtWidgets.QWidget):
    def __init__(self, parent=None, default_path=""):
        super(FolderLineEditWidget, self).__init__(parent)
        self.default_path = default_path
        self.checkbox = QtWidgets.QCheckBox("Use default")
        self.lineedit = QtWidgets.QLineEdit(self.default_path)

        self.checkbox.stateChanged.connect(self.browse)

        self.checkbox.setCheckState(QtCore.Qt.Checked)

        QtWidgets.QHBoxLayout(self)
        self.layout().addWidget(self.lineedit)
        self.layout().addWidget(self.checkbox)

    def browse(self):
        if not self.checkbox.isChecked():
            self.lineedit.setEnabled(True)
            dir_path = str(
                QtWidgets.QFileDialog.getExistingDirectory(self, "Select Directory")
            )
            self.lineedit.setText(dir_path)
        else:
            self.lineedit.setEnabled(False)
            self.lineedit.setText(self.default_path)

    def set_directory(self, path):
        self.default_path = path
        self.lineedit.setText(self.default_path)

