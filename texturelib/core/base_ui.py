#!/usr/bin/env python3
""" THIS MODULE SUPPORT PYTHON 3
It contains UI that combine with basic data.
"""
# Standard Library
import os
import subprocess
import logging

# Mnk Commonlib
import mnk_sys
import mnk_core
from mnk_qt.Qt import QtWidgets, QtCore
from mnk_qt.ui_widget.browser.MnkShareBrowserWidget import MnkShareBrowserWidget
from mnk_common_utils import file_utils, database_utils


# Relative Modules
from .ui.base_widget import PublishWidget

# Global Variables
LOGGER = logging.getLogger(__name__)
SUBMIT_STATE = {"checked": u"\u2714", "unchecked": u"\u2718"}
STYLESHEET = os.getenv("MNK_COMMON_SCRIPT_PATH") + "/mnk_qt/ui_widget/style.qss"


class InfoSignal(QtCore.QObject):
    """It contains signals to trigger when the data has changed.
        * scene_changed - emit when current scene has changed.
        * reloaded - emit for refresh the data.
    """

    scene_changed = QtCore.Signal(str)
    reloaded = QtCore.Signal()


class InformationUI:
    """It use for apply to widgets to get the same asset data.
    """

    current_scene = ""
    asset_info = {}
    info_signal = InfoSignal()

    def set_current_scene(self, scene=""):
        """Assign asset info

        Args:
            scene (str, optional): scene path. Defaults to "".
        """
        self.current_scene = scene
        info_dict = file_utils.getInfoDictFromCurrentScene(scene)
        self.asset_info["job"] = info_dict.get("job", "")
        self.asset_info["scene"] = self.current_scene
        self.asset_info["class_name"] = info_dict.get("class", "")
        self.asset_info["asset"] = info_dict.get("shot", "")
        self.asset_info["category"] = info_dict.get("scene", "")
        self.asset_info["element"] = info_dict.get("element", "")
        self.asset_info["subelement"] = info_dict.get("subElement", "")
        self.asset_info["version"] = info_dict.get("version", "")
        self.info_signal.scene_changed.emit(self.current_scene)


class PublishInformationUI(PublishWidget, InformationUI):
    """PublishInformationUI
    """

    def __init__(self, parent=None, **kwargs):
        """Initialize Function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
        """
        super(PublishInformationUI, self).__init__(parent, **self.asset_info)
        # Connect signals
        self.last_ddr_pb.clicked.connect(self.reload_lastest_ddr)
        self.play_ddr_pb.clicked.connect(self.play_ddr)

        # Initial layouts
        v_layout = QtWidgets.QVBoxLayout()
        widget = self.ddr_widget()
        v_layout.addWidget(widget)
        widget.layout().insertStretch(0, 1)
        widget.layout().insertStretch(-1, 1)
        v_layout.addWidget(self.publish_widget())
        self.publish_pb.setFixedSize(150, 50)
        self.layout().addLayout(v_layout)

    def play_ddr(self):
        """Play ddr
        """
        ddr_take = self.get_ddr_take()
        subprocess.Popen(["ddrplay", ddr_take])

    def reload_lastest_ddr(self):
        """
        Get latest ddr from database and set value to widget
        """
        self.ddr_le.setText(
            str(
                database_utils.getLatestDDR(
                    self.asset_info.get("job"),
                    self.asset_info.get("class_name"),
                    self.asset_info.get("category"),
                    self.asset_info.get("asset"),
                    "",
                    "",
                )
            )
        )

    def get_current_ticket_item(self):
        """Get current item

        Returns:
            QtWidget.QTreeWidgetItem: a selected item
        """
        return self.ticket_wd.current_item()

    def get_selected_ticket_info(self):
        """Get informatation of selected tickets

        Returns:
            a list of ticket info dict::

                {
                    "name": ticket name,
                    "status": current ticket status,
                    "submittable": bool if this ticket can be submitted
                }
        """
        return [
            {
                "name": ticket.text(0),
                "status": ticket.text(2),
                "submittable": ticket.text(3),
            }
            for ticket in self.ticket_tw.selectedItems()
        ]

    def get_ddr_take(self):
        """Get ddr in the line edit

        Returns:
            str: ddr
        """
        return self.ddr_le.text()


class ShaderPublishInformationUI(PublishInformationUI):
    """ShaderPublishInformationUI
    """

    def __init__(self, parent=None, model_widget=None, shader_widget=None, **kwargs):
        """Initialize Function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
        """
        super(ShaderPublishInformationUI, self).__init__(parent, **self.asset_info)

        # Declare widgets
        self.model_var_tw = model_widget
        self.shader_var_wd = shader_widget

        # Initialize layouts
        self.ticket_ctrl_layout.insertWidget(0, self.model_var_tw)
        self.model_var_tw.setFixedWidth(150)
        self.ticket_ctrl_layout.insertWidget(1, self.shader_var_wd)
        self.shader_var_wd.setFixedWidth(200)

        # Connect signals
        self.shader_var_wd.currentItemChanged.connect(self.get_ticket)

    @property
    def model_var(self):
        """Current model variation

        Returns:
            str: variation name
        """
        item = self.model_var_tw.listWidget.currentItem()
        return item.text() if item else None

    @property
    def shader_var(self):
        """Current shader variation

        Returns:
            str: variation name
        """
        item = self.shader_var_wd.listWidget.currentItem()
        return item.text() if item else None

    def get_ticket(self):
        """Add ticket items
        """
        self.ticket_wd.clear_widget()

        ticket_items = database_utils.get_publishable_shade_ticket_list(
            self.asset_info.get("job"),
            self.asset_info.get("category"),
            self.asset_info.get("asset"),
            self.asset_info.get("element"),
            self.model_var,
            self.shader_var,
        )
        if ticket_items:
            for ticket in ticket_items:
                self.ticket_wd.add_item(
                    QtWidgets.QTreeWidgetItem(
                        self.ticket_wd.ticket_tw._generate_ticket_tree_item_info(ticket)
                    )
                )


class AssetBrowserUI(MnkShareBrowserWidget, InformationUI):
    """AssetBrowserUI

    Signals:
        root_changed (str) - asset directory, emit when path has changed.
    """

    root_changed = QtCore.Signal(str)

    def __init__(self, parent=None, class_name="share", element=""):
        """Initialize Function

        Args:
            parent (QtWidget.QWidget, optional): parent widget. Defaults to None.
            class_name (str, optional): class name. Defaults to "share".
            element (str, optional): element name. Defaults to "".
        """
        MnkShareBrowserWidget.__init__(
            self,
            needResolution=False,
            needShaderVariation=True,
            needModelVariation=True,
            addibleList=[],
            parent=parent,
        )
        InformationUI.__init__(self)

        # Intial variables
        self.asset_info["class_name"] = class_name
        self.asset_info["element"] = element

        # Connect signals
        self.jobListWidget.currentItemChanged.connect(
            lambda name: self.asset_info.update({"job": name})
        )
        self.categoryListWidget.currentItemChanged.connect(
            lambda name: self.asset_info.update({"category": name})
        )
        self.assetListWidget.selectedItemChange.connect(self.set_root_dir)

        self.jobListWidget.setItem(self.asset_info.get("job"))
        self.categoryListWidget.setItem(self.asset_info.get("category"))
        self.assetListWidget.setItem(self.asset_info.get("asset"))

    def set_root_dir(self):
        """Assemble root directory
        """
        if not self.assetName:
            return

        self.asset_info["asset"] = self.assetName

        path = mnk_core.path.join(
            mnk_sys.MNK_BASE_DIR,
            self.asset_info.get("job"),
            self.asset_info.get("class_name"),
            self.asset_info.get("category"),
            self.asset_info.get("asset"),
        )
        self.root_changed.emit(path if os.path.exists(path) else "")
